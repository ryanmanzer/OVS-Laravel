var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.sass('app.scss');
});

elixir(function(mix){
    mix.scripts([
        'app.js',
        'source/source.js',
        'modal/modal.js',
        'vendor/vendor.js',
        'survey/survey.js',
        'survey/tracehr.js',
        'pagination/pagination.js',
        'certs/certs.js',
        'staff/staff.js',
        'libs/jquery.min.js',
    ]);
});
