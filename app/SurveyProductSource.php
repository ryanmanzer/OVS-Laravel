<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyProductSource extends Model
{
    /**
     * Table to which this model relates
     *
     * @var string
     */
    protected $table="surveyProductSources";

    public function survey()
    {
        return $this->belongsTo('App\survey','fk_survey_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product','fk_product_id');
    }

    public function source()
    {
        return $this->belongsTo('App\Source','fk_source_id');
    }
    public function sp()
    {
      return $this->belongsTo('App\SurveyProduct','surveyProducts_id');
    }
}
