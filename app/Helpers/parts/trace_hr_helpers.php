<?php
use App\TraceHrQuestion;
use App\TraceHrSurveyQuestion;
use App\TraceHrAnswer;

//Function returns count of questions exlcuding follow ups for trace/hr surveys
function thr_qcount($trace_hr_survey)
{
  $qCount=DB::table('trace_hr_survey_questions')->join('trace_hr_questions','trace_hr_question_id','=','trace_hr_questions.id')->where('trace_hr_questions.is_followUp',0)->where('trace_hr_survey_questions.trace_hr_survey_id',$trace_hr_survey->id)->count();
  return $qCount;
}
//Returns the answer map value for a trace_hr_question record as a PHP array that can be iterated over
function ans_map($trace_hr_question)
{
  return json_decode($trace_hr_question->answer_map);
}
//Returns any follow up questions assigned to this survey question if and only if that follow up question has bee assigned in this survey.
function has_follow_up($question_id,$survey_id)
{
  return DB::table('trace_hr_survey_questions')->where('trace_hr_question_id',$question_id)->where('trace_hr_survey_id',$survey_id)->select('*')->get();
}

//Returns a collection of TraceHrQuestion objects representing the recods in the trace_hr_questions table that are follow up questions to the questino object passed in
function get_follow_up($quest,$sid)
{
  if(!is_null($quest->followUp_trigger))
  {
    $fu = TraceHrQuestion::join('trace_hr_survey_questions','trace_hr_question_id','=','trace_hr_questions.id')->where('trace_hr_survey_questions.trace_hr_survey_id',$sid)->where('trace_hr_questions.parent_question_id',$quest->id)->get();
    if(count($fu)>0)
    {
      return $fu;
    }
    else {
      return false;
    }
  }
  else {
    return false;
  }
}
//This returns the survey question object identified by the unique combination of question and survey ids.  It uses the first() method to ensure we return a single object rather than a collection
function get_survey_question($qid,$sid)
{
  return TraceHrSurveyQuestion::where('trace_hr_survey_id',$sid)->where('trace_hr_question_id',$qid)->first();
}

function has_answer($sqid,$response)
{
  $ans = TraceHrAnswer::where('survey_questions_id',"=", $sqid)->where('response', "=", $response)->first();
  if(!is_null($ans)) 
  {
    return True;
  } else {
    return false;
  }
}

 ?>
