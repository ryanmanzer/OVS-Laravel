<?php


Use App\Vendor;
Use App\Source;
use App\Country;
use Symfony\Component\HttpKernel\DataCollector\AjaxDataCollector;
use PhpParser\Builder;
Use App\SeafoodSpecies;
Use App\SeafoodCategories;
Use App\harvestMethods;
Use App\methodCategories;

//A FUNCTION USED TO FLASH MESSAGES TO THE HOME SCREENS OF USERS
//FLASH MESSAGES ARE ONLY DISPLAYED ONCE, WHEN SCREEN REFRESHES THEY DO NOT REAPPEAR
function flashMessage($message,$level='info')
{
    session()->flash('flash_message',$message);
    session()->flash('flash_message_level',$level);
}
function errorMessage($message,$level='danger')
{
  session()->flash('error_message', $message);
  session()->flash('error_message_level',$level);
}
//A FUNCTION USED TO PUT PERSISTENT MESSAGES ON THE HOMESCREENS OF USERS
//PERSISTENT MESSAGES REMAIN IN PLACE UNTIL THE USER DISMISSES THEM. --NOT BUILT OUT YET
function persist($message,$level='info')
{

}
?>
