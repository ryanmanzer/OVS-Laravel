<?php
//CODE TO ASSIST WITH VENDOR RELATED MATTERS


use App\Vendor;
use App\Source;
use App\Product;
/*
function orderedSources(Vendor $vendor)
{
  return Source::join('seafoodCategories','sources.fk_seafoodCategory_id','=','seafoodCategories.id')
  ->where('sources.fk_vendor_id','=',$vendor->id)
  ->orderBy('seafoodCategories.category')
  ->select('sources.*')
  ->get();
}
function orderProducts(Vendor $vendor)
{
  return Product::join('seafoodCategories','products.fk_seafoodCategory_id','=','seafoodCategories.id')
  ->where('products.fk_vendor_id','=',$vendor->id)
  ->orderBy('seafoodCategories.category')
  ->select('products.*')
  ->get();

} */
//THIS IS A FUNCTION TO GENERATE SUBHEADERS FOR TABLES OF SOURCES OR PRODUCTS
function catTable($record,$nCols)
{
  if(session()->has('seafood_category')) //IF WE'VE ALREADY SET IT, WE NEED TO CHECK IT
  {
    if (session()->get('seafood_category') == $record->seafoodCategory->category)  //IF THE CATEGORIES ARE THE SAME
    {
      return ''; //NOTHING TO DO HERE
    }
  }
  $cat=$record->seafoodCategory->category;
  $html="<tr>
          <td colspan='$nCols' class='subheader alert alert-info'>
            <strong>$cat</strong>
          </td>
        </tr>";
  session()->set('seafood_category',$cat);
  return $html;
}
 ?>
