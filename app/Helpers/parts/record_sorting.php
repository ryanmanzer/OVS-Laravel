<?php
use App\Vendor;
use App\Source;
use App\Product;
use App\survey;
use App\SurveyProduct;

function orderedSources(Vendor $vendor)
{
  return Source::join('seafoodCategories','sources.fk_seafoodCategory_id','=','seafoodCategories.id')
  ->join('seafoodSpecies','sources.fk_seafoodSpecies_id','=','seafoodSpecies.id')
  ->join('countries','sources.fk_harvestCountry_id','=','countries.id')
  ->join('harvestMethods','sources.fk_harvestMethod_id','=','harvestMethods.id')
  ->where('sources.fk_vendor_id','=',$vendor->id)
  ->where('show','=',1)
  ->orderBy('seafoodCategories.category','asc')
  ->orderBy('seafoodSpecies.seafoodSpecies','asc')
  ->orderBy('supplierCompany','asc')
  ->orderBy('sources.wild_farmed','asc')
  ->orderBy('countries.countryName','asc')
  ->orderBy('harvestMethods.harvestMethod','asc')
  ->select('sources.*')
  ->paginate(200);
}
function orderProducts(Vendor $vendor)
{
  return Product::join('seafoodCategories','products.fk_seafoodCategory_id','=','seafoodCategories.id')
  ->where('products.fk_vendor_id','=',$vendor->id)
  ->orderBy('seafoodCategories.category','asc')
  ->orderBy('products.retailerProductCode','asc')
  ->select('products.*')
  ->get();

}
function ordSurveyProducts(survey $survey)
{
  return SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
  ->join('seafoodCategories','products.fk_seafoodCategory_id','=','seafoodCategories.id')
  ->where('surveyProducts.fk_survey_id','=',$survey->id)
  ->orderBy('seafoodCategories.category','asc')
  ->orderBy('products.retailerProductCode','asc')
  ->select('surveyProducts.*')
  ->paginate(200);
}
 ?>
