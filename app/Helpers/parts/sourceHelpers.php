<?php
/**
 * THIS HELPERS FILE CONTAINS THOSE FUNCTIONS WE FIND USEFUL FOR DEALING WITH SOURCE PROCESSING
 */

 Use App\Vendor;
 Use App\Source;
 use App\Country;
 use App\SeafoodSpecies;
 use Symfony\Component\HttpKernel\DataCollector\AjaxDataCollector;
 use PhpParser\Builder;
 use Hamcrest\Core\DescribedAs;
 Use App\SeafoodCategories;
 Use App\harvestMethods;
 Use App\methodCategories;
 Use App\SurveyProduct;
 Use App\SurveyProductSource;
 Use App\Certification;
 Use App\SurveyProductCert;

//A FUNCTION USED TO HANDLE ALL THE ID FETCHING AND SUCH THAT MUST BE DONE TO GO FROM
//THE DATA ENTERED IN THE NEW SOURCE AND EDIT SOURCE FORMS TO AN ACTUAL SOURCE RECORD
function buildSource($args)
{
    $makeSource=true; // we start by assuming we will make a source
    $errs=array();
    if(array_key_exists('sourceArray',$args))
    {
        $sourceArray=$args['sourceArray'];
    }
    if(array_key_exists('vendor',$args))
    {
        $vendor=$args['vendor'];
        $vendorID=$vendor->id;
    }
    elseif(array_key_exists('source',$args))
    {
        $source=$args['source'];
        $vendorID=$source->fk_vendor_id;
    }

    if(array_key_exists('_token',$sourceArray)){
        unset($sourceArray['_token']);
    }
    if(array_key_exists('seafood',$sourceArray))
    {/*
        $fish_id=SeafoodSpecies::select('id')->where('seafoodSpecies',$sourceArray['fish'])->first();
        $fishCatID=SeafoodCategories::select('id')->where('category',$sourceArray['seafoodCategory'])->first();
        */
        $fish_id=$sourceArray['seafood'];
        $fishCatID=$sourceArray['seafoodCategory'];

        unset($sourceArray['fish'],$sourceArray['seafoodCategory']);
        $sourceArray['fk_seafoodSpecies_id']=$fish_id;
        $sourceArray['fk_seafoodCategory_id']=$fishCatID;
    }
    $gearID=$sourceArray['gear_type'];
    $gearCatID=$sourceArray['method_cat'];
    $sourceArray['method_category_id']=$gearCatID;
    /*
    $gearID=harvestMethods::select('id')->where('harvestMethod',$sourceArray['gear_type'])->first();
    $gearCatID=methodCategories::select('id')->where('methodCategory',$sourceArray['method_cat']); */
    $sourceArray['fk_harvestMethod_id']=$gearID;
    //Getting our field names to match our tables
    $sourceArray['fk_vendor_id']=$vendorID;
    $wf=$sourceArray['wf']; unset($sourceArray['wf']); $sourceArray['wild_farmed'] = $wf;
    unset($sourceArray['gear_type'],$sourceArray['method_cat']);
    $sourceArray['createdBy']= Auth::id();
    //build in a loop to replace all country fields with ID values
    $countryFields=array('catch_country'=>'fk_harvestCountry_id','cool'=>'fk_coolCountry_id',
                         'vessel_flag'=>'fk_flagOfVesselCountry_id');
    foreach($countryFields as $key=>$value)
    {
        $countryID=$sourceArray[$key];
        $sourceArray[$value]=$countryID;
        unset($sourceArray[$key]);
    }
    //return var_dump($sourceArray);
    if ($makeSource) {
      $mysource = new Source($sourceArray);
      $mysource->show=1;
      return $mysource;
    } else {
      foreach ($errs as $key => $value) {

      }
      return $makeSource;
    }
}
// FUNCTION USED TO COMPARE VALUES IN ONE SOURCE AGAINST ANOTHER
// RETURNS TRUE IS SOURCES ARE DIFFERENT
function sourceCompare($source1,$source2)
{
    $bool=true;
    // We get the sizes of both arrays as a means to check which source has
    // the fewer fields.  Records not commited to the database will automatically
    // have less fields and we want to only compare the values passed in.  Therefore
    // we want to fnd the smallest array and compare those values.
    if(!empty($source1) and !empty($source2))
    {
      $fieldArray1=$source1->getAttributes();
      $fieldArray2=$source2->getAttributes();
      if (sizeof($fieldArray1) > sizeof($fieldArray2))
      {
        $vars=$fieldArray2;
        $rec=$fieldArray1;
      }
      else
      {
        $rec=$fieldArray2;
        $vars=$fieldArray1;
      }
      foreach($vars as $key=>$value)
      {
        if(strpos($key,'Comments')==false)
        {
          if($value != $rec[$key])
          {
              $bool=false;
          }
        }
      }
      return $bool;
    }
    else
    {
      return false;
    }

}
/*
function compareSources(Source $source1, Source $source2 ){
  if{ $source1-> == 





  }
}
*/
function unAssignedCerts(SurveyProduct $sp)
    {
        $allCerts = Certification::where('fk_vendor_id','=',$sp->fk_vendor_id)->get();
        ;
        $unAssignedCerts = array();
            
        foreach ($allCerts as $allCert) {
            $sps = SurveyProductCert::where('certification_id', '=', $allCert->id)
            ->where('survey_product_id', '=', $sp->id)->first();
            if(is_null($sps)){
                array_push($unAssignedCerts, $allCert);       
            }
        }
        return $unAssignedCerts;

    }

function unAssignedSources(SurveyProduct $sp)
    {
        $allSources = Source::where('fk_vendor_id','=',$sp->fk_vendor_id)
        ->where('fk_seafoodCategory_id' , '=', $sp->product->fk_seafoodCategory_id)
        ->where('show' , '=', 1)
        ->get();
        ;
        $unAssignedSources = array();
            
        foreach ($allSources as $allSource) {
            $sps = SurveyProductSource::where('fk_source_id', '=', $allSource->id)
            ->where('surveyProducts_id', '=', $sp->id)->first();
            if(is_null($sps)){
                array_push($unAssignedSources, $allSource);       
            }
        }
        return $unAssignedSources;

    }

// FUNCTION THAT TAKES IN A PRODUCT AND RETURNS THE TOTAL PERCENT OF PRODUCT THAT
// HAS BEEN ASSIGNED TO SOURCES, AS DEFINED BY THE RECORDS IN THE surveyProductSources
// TABLE
function sourcePercent($product)
    {
        $percent=0;
        $sps=$product->sps;
        if(count($sps) > 0)
        {
            foreach($sps as $rec)
            {
                $percent+= $rec->proportionOfProduct;
            }
        }

        return (1-$percent> 0.02) ? $percent : 1;
    }
// THIS WILL DETERMINE HOW MANY PRODUCTS HAVE BEEN COMPLETELY ASSIGNED SOUCRES
// AND RETURN THE PERCENT COMPLETE FROM THE SURVEY
function perComplete($survey)
 {
    $products=$survey->products;
    $count=count($products);
    if($count>0)
    {
      $comp=0;
      foreach($products as $prod)
      {
          if(sourcePercent($prod) == 1)
          {
              $comp+=1;
          }
      }
      return ($comp/$count);
    }
    else {
      return 0;
    }

 }

 //SOME DEFENSIVE CODING TO PROPERLY ASSOCIATE SEAFOOD CATEGORIES WITH SOURCE
 //RECORDS WHERE THERE IS A 0 FOR SEAFOOD CATEGORY BUT A VALUE FOR SEAFOOD SeafoodSpecies
 function catAssign()
 {
   $sources=Source::where([
     ['fk_seafoodSpecies_id','>',0],
     ['fk_seafoodCategory_id','=',0]
   ])->get();
   foreach ($sources as $source) {
     $ssp_id=$source->fk_seafoodSpecies_id;
     $sp=seafoodSpecies::find($ssp_id);
     $source->fk_seafoodCategory_id=$sp->fk_seafoodCategory_id;
     $source->save();
   }
 }


//FUNCTION FOR GENERATING TOOLTIP TEXT FOR SOURCES <- NOW OUTPUTS TO PANEL
// It would be nice to just loop through all the fields and write them out but we've got way too many IDs and other stuff so we need to build a custom string of fields and stuff...This started out being intended for a tooltip but I think it works better in a panel (which is how it is currently implemented)
function tooltipText(Source $source)
{
  $noVal="None given";
  $vFlag= ($source->vesselFlag) ? $source->vesselFlag->countryName : $noVal;
  $seaP= ($source->SeafoodSpecies) ? $source->SeafoodSpecies->seafoodSpecies : $noVal;
  $methCat=($source->harvestMethod) ? $source->harvestMethod->category->methodCategory : $noVal;
  $spMeth=($source->harvestMethod) ? $source->harvestMethod->harvestMethod : $noVal;
  $catchC=($source->catchCountry) ? $source->catchCountry->countryName : $noVal;
  $cool=($source->COOL) ? $source->COOL->countryName : $noVal;
  $certT=($source->certification) ? $source->certification->certType : $noVal;
  $ttt= "<p>" .
  "<strong>Source Company</strong>: " . $source->supplierCompany . "</p><p>" .
  "<strong>Seafood Species:</strong> " . $seaP . "</p><p>" .
  "<strong>Harvest Method Category:</strong> " . $methCat . "</p><p>" .
  "<strong>Specific Method:</strong> " . $spMeth  . "</p><p>" .
  "<strong>Catch Country:</strong> "  . $catchC . "</p><p>" .
  "<strong>Catch Region:</strong> " . $source->harvestRegion . "</p><p>" .
  "<strong>COOL:</strong> " . $cool . "</p><p>" .
  "<strong>FAO Major Fishing Area:</strong> " . $source->faoMajorFishingArea . "</p><p>" .
  "<strong>RFMO High Seas Name:</strong> " . $source->highSeasName . "</p><p>" .
  "<strong>Flag of Vessel:</strong> " . $vFlag . "</p><p>" .
  "<strong>Certification Type:</strong> " . $certT . "</p><p>" .
  "<strong>Certification Number:</strong> " . $source->certificationNumber . "</p><p>" .
  "<strong>Comments:</strong> " . $source->vendorComments . "</p>";

  return $ttt;
}
