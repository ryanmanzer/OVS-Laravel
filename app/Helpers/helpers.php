<?php
/**
 * A COLLECTION OF HELPER FUNCTIONS THAT PERFORM VARIOUS ODDS AND ENDS.
 * IF THIS COLLECTION GETS TOO NUMEROUS I MAY BREAK IT UP INTO DIFFERENT
 * COMPONENTS AND COMBINE THEM USING THE INCLUDE TAG. -- INCLUDE TAG NOT WORKING HERE, MORE RESEARCH IS NEEDED
 */

Use App\Vendor;
Use App\Source;
use App\Country;
use Symfony\Component\HttpKernel\DataCollector\AjaxDataCollector;
use PhpParser\Builder;
Use App\SeafoodSpecies;
Use App\SeafoodCategories;
Use App\harvestMethods;
Use App\methodCategories;

include 'parts/messages.php'; // GETTING THE CODE TO HELP WITH MESSAGING OUR USERS

include 'parts/sourceHelpers.php'; // GETTING THE CODE TO HELP WITH SOURCE DATA

include 'parts/vendor_functions.php'; // GETTING USEFUL VENDOR CODE

include 'parts/record_sorting.php'; //GETTING USEFUL RECORD SORTING FUNCTIONS

include 'parts/trace_hr_helpers.php';//GETTING HELPER FUNCTIONS FOR TRACE HR SURVEYS

// FUNCTION TO RETURN THE APPROPRIATE STATUS BADGE DEPENDING ON THE PERCENT
// OF PRODUCT THAT HAS BEEN ACCOUNTED FOR BY ASSIGNED SOURCES
function getBadge($percent)
{
    if($percent==1)
    {
        $badge=  '<i class="fa fa-thumbs-o-up" data-toggle="tooltip"
            title="100% of product accounted for by assigned sources." style="color:green"
          aria-hidden="true"></i>';
    }
    elseif($percent>1)
    {
         $badge="<i class='fa fa-exclamation' aria-hidden='true' data-toggle='tooltip'
           title='Assigned sources total to more than 100%.' style='color:red'></i>";
    }
    else
    {
         $badge="<i class='fa fa-question-circle' data-toggle='tooltip'
         title='Less than 100% reported.' style='color:orange' aria-hidden='true'></i>";
    }
    return $badge;
}

//THIS FUNCTION CONSTRUCTS THE DETAILS FOR THE HOME PAGE FOR EACH USER.
//THE TABS DETERMINE WHAT PAGES GET LOADED INTO EACH TAB FOR THE USERS
//HOME SCREEN AND THE LOADCOMP VARIABLE BUILDS THE AJAX CALLS THAT LOAD
//THE RESOURCES.
function getTabsScripts($user)
{

  $tabs=array();
  $loadComp=array();

  switch ($user->user_type) {
    case 'ngo':

      //defining the tabs shown in the general info <div>
        $tabs['retailers']="Retailers";
        $tabs['vendors']="Vendors";
        $tabs['surveys']="Surveys";
        $tabs['users']="Users";
    // providing the tags and urls for all the loadComponent function calls
        $loadComp['retailers']='/ngo/retailers';
        $loadComp['vendors']='/ngo/vendors';
        $loadComp['contact_info']='ngo/' . $user->org_id . '/info';
        $loadComp['surveys']='/ngo/surveys';
        $loadComp['users']='/admin/'. $user->id .'/users';
    // defining the javascript functions we will run as part of initializing
    // an NGO user's home page
        $customInit='staffInit';
      break;
    case 'vendor':
      //echo "this is a vendor person";
      $tabs['surveys']='Surveys';
      $tabs['sources']='Seafood Sources';

      $loadComp['contact_info']='/vendor/'. $user->org_id  . '/info';
      $loadComp['surveys']='/vendor/' . $user->org_id . '/surveys';
      $loadComp['sources']='/vendor/' . $user->org_id . '/sources';
    //defining the js functions run as part of initializing vendor home page
      $customInit='vendorInit';
      break;
    case 'retailer':
      //echo "this is a retailer person";
      $tabs['vendors']='Seafood Vendors';
      $tabs['surveys']='Vendor Surveys';
      $tabs['products']='Seafood Products';

      $loadComp['contact_info'] = 'retailer/' . $user->org_id . '/info';
      $loadComp['vendors'] = '/retailer/' . $user->org_id . '/vendors';
      $loadComp['products'] = '/retailer/' . $user->org_id . '/products';
      $loadComp['surveys']='/retailer/' . $user->org_id . '/surveys';
      $customInit='';
      break;
    default:
      echo "There is nothing to see here, move along.";
    }
    $results=array('tabs'=>$tabs, 'loadComp'=>$loadComp,'init'=>$customInit);
    return $results;
}
//This function merely determines if a password reset token has been sent
 function passwordReset($user)
 {
   $check = DB::table('password_resets')
   ->select('created_at')
   ->where('email',$user->email)
   ->get();
   if(count($check)>0)
    {
      return "True";
    }
    else {
      return "False";
    }
 }
?>
