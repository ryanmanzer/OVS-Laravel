<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class survey extends Model
{
      /*
     * FUNCTIONS THAT DEFINE RELATIONSHIP TO OTHER TABLES
     */
    public function vendor()
    {
        return $this->belongsTo('App\Vendor','fk_vendor_id');
    }

    public function retailer()
    {
        return $this->belongsTo('App\Retailer','fk_retailer_id');
    }

    public function sps()
    {
        return $this->hasMany('App\SurveyProductSource','fk_survey_id','id');
    }

    public function sp()
    {
        return $this->hasMany('App\SurveyProduct','fk_survey_id','id');
    }
    public function products()
    {
        return $this->belongsToMany('App\Product','surveyProducts','fk_survey_id','fk_product_id');
    }
    public function sources()
    {
        return $this->belongsToMany('App\Source','surveyProductSources','fk_survey_id','fk_source_id');
    }
    
}
