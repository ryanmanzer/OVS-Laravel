<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraceHrQuestion extends Model
{

    public function answers()
    {
      return $this->belongsToMany('Ap\TraceHrAnswer','App\TraceHrSurveyQuestion','survey_questions_id','trace_hr_survey_id');
    }
    // For follow up questions, defining the parent
    public function parentQuestion()
    {
      return $this->belongsTo('App\TraceHrQuestion','parent_question_id');
    }
    //For parent questions, definig the follow up
    public function followUp()
    {
      return $this->hasMany('App\TraceHrQuestion','parent_question_id');
    }
    public function surveyQuestions()
    {
      return $this->hasMany('App\TraceHrSurveyQuestion','trace_hr_question_id');
    }
    public function sqid($surveyid)
    {
      return $this->surveyQuestion->where('trace_hr_survey_id',$surveyid)->first()->id;
    }
    public function sq($surveyid)
    {
      return $this->surveyQuestion->where('trace_hr_survey_id',$surveyid)->first();
    }
}
