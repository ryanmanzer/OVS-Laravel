<?php
use App\SurveyProductSource;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::get('/', 'HomeController@home');
Route::get('/home','HomeController@home');
Route::auth();
Route::get('/heatlh','HomeController@health');
 /*
  *----------------------------------------------------------------------------
  * SURVEY ROUTING
  * ---------------------------------------------------------------------------
*/
// GET REQUESTS
Route::get('/survey/{survey}/products','SurveyController@products');
Route::get('/survey/{survey}/products/{sp}/assign','SurveyController@products');
Route::get('/surveyProduct/{sp}/assign', 'SurveyController@assign');
Route::get('/surveyProduct/{sp}/view','SurveyController@viewSources');
Route::get('/surveyProduct/{sp}/percent','SurveyController@updatePercent');
Route::get('/surveyProduct/{sp}/newModalTitle','SurveyController@newModalTitle');
Route::get('/surveyProduct/{sp}/sfCatGroup','SurveyController@sfCatGroup');
Route::get('/surveyProduct/{sp}/sfGroup','SurveyController@sfGroup');


// POST REQUESTS
Route::post('/survey/{survey}/status','SurveyController@updateStatus');
Route::post('/sps/{sps}','SurveyController@destroySPS');
Route::post('/surveyProduct/{sp}/assign','SurveyController@assignSPS');
Route::post('/surveyProduct/{sp}/assignCert','SurveyController@assignCert');
Route::post('/surveyProduct/{sp}/assignRepack','SurveyController@assignRepack');
Route::post('/spcert/{id}/rm','SurveyController@removeCert');

/*
 * --------------------------------------------------------------------------
 *  VENDOR ROUTING - everything here calls functions in VendorController
 * --------------------------------------------------------------------------
 */
 // GET REQUESTS
Route::get('/vendor','VendorController@show'); //getting general info for vendor
Route::get('/vendor/{vendor}/edit','VendorController@edit'); //getting the edit menue for updates to contact info
// AJAX CALLED GET REQUESTS FOR VENDOR HOME PAGE
Route::get('/vendor/{vendor}/info','VendorController@info'); //providing account info tab
Route::get('/vendor/{vendor}/contactInfo','VendorController@contactInfo'); //providing contact info
Route::get('/vendor/{vendor}/surveys','VendorController@surveys'); //returning a table of surveys
Route::get('/vendor/{vendor}/sources','VendorController@sources'); //returning a table of sources
Route::get('/vendor/{vendor}/sources/{source}/details','VendorController@sources'); //returning a table of sources with one source detail open

Route::get('/vendor/{vendor}/products','VendorController@products'); //returning a table of associated products
Route::get('/vps/{product}/show','VendorController@productSources');
// GET REQUESTS THAT BUILD OUR NEW SOURCE FORM
Route::get("/vendor/{id}/meth_list",'VendorController@meth_list');
Route::get("/vendor/{vendor}/newSource",'VendorController@newSource'); //return a refresehd contents of the add new modal window
Route::get("/vendor/{vendor}/newSource/{sp}/sp",'VendorController@newSource'); //return a refreshed contents of the add new modal when triggered from a survey product
Route::get("/vendor/refreshSfCat",'VendorController@refreshSfCat'); //return a refresehd contents of 
Route::get("/vendor/refreshSf",'VendorController@refreshSf');

//Route::get("/vendor/{id}/sf_list","VendorController@sf_list"); //returning a filtered list of seafood species
Route::get('/vendor/{wf}/cat_list','VendorController@cat_list'); //returning a filtered list of method categories
Route::get('/vendor/{vendor}/certs','VendorController@certs');
Route::get('/vendor/{vendor}/newCertModal','VendorController@newCertModal');


//REQUESTS FOR THE TRACEABILITY/HUMAN RIGHTS SURVEYS
Route::get('/vendor/{vendor}/thr',"VendorController@thrSurveys");
Route::get('/confirmRemoveCert/{cert}','VendorController@confirmRemoveCert');

// PATCH/PUT/UPDATE REQUESTS
Route::post('/vendor/{vendor}','VendorController@update');
Route::post('/vendor/{vendor}','VendorController@update');
Route::post('/vendor/{vendor}/certs','VendorController@addCert');
Route::post('/removeCert/{cert}','VendorController@rmCert');
/*
 * ----------------------------------------------------------------------------
 *  SOURCE ROUTING
 * ----------------------------------------------------------------------------
 */
// GET REQUESTS
Route::get('/source/{source}/edit', 'SourceController@edit');
Route::get('/source/{source}/details','SourceController@details');
Route::get('/certTypes','SourceController@certTypes');
Route::get('/source/{wf}/cert_list','SourceController@certList');
Route::get('/source/{vendor}/newSourceRefresh','SourceController@newSourceRefresh');
Route::get('/source/{source}/sourceDeleteConfirm','SourceController@sourceDeleteConfirm');
Route::get('/source/{source}/sourceEditConfirm','SourceController@sourceEditConfirm');
Route::get('/source/{source}/countSps','SourceController@countSps');
Route::get('/source/{source}/updateCountSps','SourceController@updateCountSps');
Route::get('/source/{source}/sourceEditsListed','SourceController@sourceEditsListed');
Route::get('/source/{source}/sourceDeletesListed','SourceController@sourceDeletesListed');




// POST REQUESTS
Route::post('/vendor/{vendor}/addsource', 'SourceController@Add');
Route::post('/source/{source}/delete','SourceController@unshow');
Route::post('/source/{source}/duplicate','SourceController@duplicate');
Route::post('/source/{source}/deleteAllSps','SourceController@deleteAllSps');
Route::post('/source/{oldSource}/{newSource}/updateSps','SourceController@updateSps');


// PATCH/UPDATE REQUESTS
Route::post('source/{source}/update','SourceController@update');

/*
//  * ----------------------------------------------------------------------------
//  *  NGO STAFF ROUTING
//  * ----------------------------------------------------------------------------
//  */
//GET REQUESTS
Route::get('ngo','StaffController@home');
Route::get('/ngo/variable','StaffController@message');
// AJAX CALLED GET REQUESTS TO BUILD NGO STAFF HOMEPAGE <- CURRENTLY ONLY FISHWISE, UNSURE IF OTHER NGO WILL GET ACCESS
Route::get('ngo/{ngo}/info','StaffController@info');
Route::get('ngo/retailers','StaffController@retailerList');
Route::get('ngo/vendors','StaffController@vendorList');
Route::get('ngo/surveys','StaffController@surveyList');


/*
 * ----------------------------------------------------------------------------
 * RETAILER ROUTING
 * ---------------------------------------------------------------------------
//  */
// //GET REQUESTS
// Route::get('retailer/{retialer}/products','RetailerController@products');
   Route::get('/retailer','RetailerController@show');
   Route::get('/retailer/{retailer}/info','RetailerController@info');
   Route::get('/retailer/{retailer}/products','RetailerController@products');
   Route::get('/retailer/{retailer}/surveys','RetailerController@surveys');
   Route::get('/retailer/{retailer}/vendors','RetailerController@vendors');
/*
 * ----------------------------------------------------------------------------
 *  MODAL WINDOW GENERATOR ROUTES
 * ----------------------------------------------------------------------------
 */
 //
 Route::get('modal/get','ModalController@basic');
 Route::get('modal/delete','ModalController@delete');

/**
  * ----------------------------------------------------------------------------
  * TEST ROUTING
  * ----------------------------------------------------------------------------
*/
  Route::get('/test','TestController@testjax');
  Route::post('/test','TestController@testjax');
  Route::get('/pages','TestController@pages');
  Route::get('/is_logged_in','HomeController@logcheck');
  //Route::get('/rmSeafoodCat','HomeController@rmSeaCat');

/**
*  ----------------------------------------------------------------------------
*   ADMIN ROUTING
*  ----------------------------------------------------------------------------
*/
Route::get('/admin/{user}/users','StaffController@users');
/**
* ------------------------------------------------------------------------------
* TRACE/HR SURVEY ROUTING
* ------------------------------------------------------------------------------
*/
Route::get('/thrsurveys/{id}','TraceHrController@show');
Route::get('/thrsurveys/{id}/clear','TraceHrController@clear');
Route::post('thrsurveys/{id}','TraceHrController@submit');

/**
* ------------------------------------------------------------------------------
* Step Through Survey Routing - For now we'll place this all in its own controller
* ------------------------------------------------------------------------------
*/
Route::get('/{vendor}/splash','StepByStepController@splash');
Route::get('/vendor/{vendor}/surveysHome','StepByStepController@surveysHome');
Route::get('/survey/{survey}/productSurveyHome','StepByStepController@productSurveyHome');
Route::get('/survey/{survey}/surveyCatList','StepByStepController@surveyCatList');
Route::get('/survey/{survey}/sfCat/{sfCat}','StepByStepController@surveyCatHome');
Route::get('/surveyProduct/{sp}/productEntry','StepByStepController@productEntry');
Route::get('/surveyProduct/{sp}/productEntry/section/{section}','StepByStepController@productEntry');
Route::get('/surveyProduct/{sp}/productDataEntry','StepByStepController@productDataEntry');
Route::get('/surveyProduct/{sp}/newSourceEntryForm','StepByStepController@newSourceEntryForm');
Route::get('/surveyProduct/{sp}/enterSource','StepByStepController@enterSource');
Route::get('/survey/{survey}/catSideBar/{sfCat}/openSp/{openSp}','StepByStepController@catSideBar');
Route::post('/surveyProduct/{sp}/addsource', 'StepByStepController@EnterSource');
Route::post('/sps/{sps}/assignPercent/{percent}', 'StepByStepController@assignPercent');
Route::get('/sps/{spSource}/removeSps', 'StepByStepController@removeSps');
Route::get('/sp/{sp}/source/{source}/fillSource', 'StepByStepController@sourceEntryForm');
Route::get('/source/{sp}/clearSourceForm', 'StepByStepController@sourceEntryForm');
Route::get('/sp/{sp}/sps/{sps}/addSourceAmount', 'StepByStepController@addSourceAmount');
Route::get('/surveyProduct/{sp}/productDataEntry/section/{section}','StepByStepController@productDataEntry');
Route::post('/surveyProduct/{sp}/addsource/{oldSource}/oldSource', 'StepByStepController@EnterSource');
Route::get('/sp/{sp}/sourcesList','StepByStepController@sourcesList');
Route::get('/surveyProduct/{sp}/completeSources','StepByStepController@completeSources');
Route::get('/surveyProduct/{sp}/progressBar','StepByStepController@progressBar');
Route::get('/surveyProduct/{sp}/progressBar/{section}','StepByStepController@progressBar');
Route::get('/surveyProduct/{sp}/certsList','StepByStepController@certsList');
Route::get('/surveyProduct/{sp}/certSelect','StepByStepController@certSelect');
Route::get('/surveyProduct/{sp}/certsEntry','StepByStepController@certsEntry');
Route::get('/surveyProduct/{sp}/newCertEntry','StepByStepController@newCertEntry');
Route::post('/surveyProduct/{sp}/AddNewCert','StepByStepController@AddNewCert');
Route::post('/surveyProduct/{sp}/AddNewCert/oldCert/{oldCert}','StepByStepController@AddNewCert');
Route::get('/spCert/{spCert}/removeCert', 'StepByStepController@removeCert');
Route::get('/surveyProduct/{sp}/doneWithCerts','StepByStepController@doneWithCerts');
Route::get('/surveyProduct/{sp}/reOpenCerts','StepByStepController@reOpenCerts');
Route::get('/surveyProduct/{sp}/newCertForm','StepByStepController@newCertForm');
Route::get('/surveyProduct/{sp}/oldCert/{cert}/fillForm','StepByStepController@newCertForm');
Route::get('/surveyProduct/{sp}/addCertsQuestion','StepByStepController@addCertsQuestion');
Route::get('/surveyProduct/{sp}/updateProgressBar','StepByStepController@updateProgressBar');
Route::get('/surveyProduct/{sp}/updateProgressBar/{section}','StepByStepController@updateProgressBar');
Route::get('/surveyProduct/{sp}/repacking/{repacking}','StepByStepController@repackingAnswer');
Route::get('/surveyProduct/{sp}/repackingStatus','StepByStepController@repackingStatus');
Route::get('/surveyProduct/{sp}/reOpenRepacking','StepByStepController@reOpenRepacking');
Route::get('/surveyProduct/{sp}/active/{active}','StepByStepController@activeAnswer');
Route::get('/surveyProduct/{sp}/activeStatus','StepByStepController@activeStatus');
Route::get('/surveyProduct/{sp}/reOpenActive','StepByStepController@reOpenActive');
Route::get('/surveyProduct/{sp}/submit','StepByStepController@submitSurveyProduct');
Route::get('/surveyProduct/{sp}/submitStatus','StepByStepController@submitStatus');
Route::get('/surveyProduct/{sp}/reOpenSources','StepByStepController@reOpenSources');
Route::get('/surveyProduct/{sp}/onToCertifications','StepByStepController@onToCertifications');
Route::get('/surveyProduct/{sp}/onToActive','StepByStepController@onToActive');
Route::get('/surveyProduct/{sp}/activeEntry','StepByStepController@activeEntry');
Route::get('/surveyProduct/{sp}/onToSubmit','StepByStepController@onToSubmit'); 
Route::get('/surveyProduct/{sp}/reOpenSubmit','StepByStepController@reOpenSubmit');
Route::get('/surveyProduct/{sp}/completedEntry','StepByStepController@completedEntry');

// Load a Trace HR Survey 
Route::get('/thrSurvey/{thrSurvey}/thrSurveyHome','StepByStepController@thrSurveyHome');
//Load up an empty form of the current thrSurvey
Route::get('/thrSurvey/{thrSurvey}/thrClear','StepByStepController@thrClear');
//Save the results of a tracehr survey
Route::post('thrSurvey/{thrSurvey}/save','StepByStepController@thrSave');
Route::get('thrSurvey/{thrSurvey}/save','StepByStepController@thrSurveySummaryAfterSave');

//Submit the results of a tracehr survey
Route::get('thrSurvey/{thrSurvey}/thrSubmit','StepByStepController@thrSubmit');


//Route::get('/source/getWF','StepByStepController@getWF');
Route::get('/source/clearSpecMeth','StepByStepController@setSpecMethByWF');
//Udpate dropdowns after changing WildFarmed
Route::get('/source/{wf}/wfSpecMeth','StepByStepController@setSpecMethByWF');
Route::get('/source/{wf}/setCertList','StepByStepController@setCertList');
Route::get('/source/{wf}/setMethCat','StepByStepController@setMethCat');

//Udpate dropdowns after changing WildFarmed to a blank value
Route::get('/source//setMethCat','StepByStepController@setMethCat');

Route::get("/source/{sfCat}/sf_list","StepByStepController@setSpeciesList");
Route::get("/source//sf_list","StepByStepController@setSpeciesList");

Route::get('/source/{specMeth}/setMethCatByGear','StepByStepController@setMethCatByGear');
Route::get('/source//setSpecMeth','StepByStepController@setSpecMeth');
Route::get('/source//setCertList','StepByStepController@setCertList');

//Updating the specific method options when a harvest method category is selected 
Route::get('/source/{methCat}/setSpecMeth','StepByStepController@setSpecMeth');

//What to display when opening a seafood category that is 100% complete 
Route::get('/survey/{survey}/sfCat/{sfCat}/completed','StepByStepController@catCompleted');

Route::get('/thrSurvey/thrSurveySubmittedAlert','StepByStepController@thrSurveySubmittedAlert');
Route::get('/thrSurvey/thrSurveyCompletedAlert','StepByStepController@thrSurveyCompletedAlert');

//Submit the survey as completed
Route::get('/survey/{survey}/complete','StepByStepController@completeSurvey');

//Set the last opened field for the product survey
Route::post('survey/{survey}/setLastOpened','StepByStepController@setLastOpened');
 
//display the list surveys for that retailer
Route::get("/vendor/{vendor}/retailer/{retailer}/surveysHome",'StepByStepController@surveysHome');

// Tutorial
Route::group(['prefix' => 'api'], function() {
    Route::get('tutorial/{tutorial}','HomeController@loadTutorial');
    Route::post('tutorial/{tutorial}','HomeController@completeTutorial');

});
