<?php

namespace App\Http\Controllers;
Use DB;
Use Gate;
Use Auth;
use \Redirect;
use Illuminate\Http\Request;
Use App\Vendor;
Use App\Source;
use App\Country;
Use App\SeafoodSpecies;
Use App\SeafoodCategories;
Use App\harvestMethods;
Use App\methodCategories;
Use App\SurveyProductSource;
use App\Http\Requests;
use App\CertificationType;

class SourceController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    // FIRST STAB AT ADDING NEW SOURCE WITH RELATION TO VENDOR -- SEEMS TO WORK OKAY BUT IS A LOT OF CODE
    // I WILL CONSIDER REVISING FOR OVS 2.0
    public function Add(Request $request, Vendor $vendor)
    {
      $this->validate($request,[
        'supplierCompany'=>'required',
        'seafoodCategory'=>'required',
        'seafood'=>'required',
        'wf'=>'required',
        'method_cat'=>'required',
        'gear_type'=>'required',
        'catch_country'=>'required',
        'harvestRegion'=>'required',
        'scientificName'=>'required',
        'cool'=>'required'
      ]);
      if(Gate::allows('org-check',$vendor))
      {

         $sourceArray = $request->all();
         $args=array('sourceArray'=>$sourceArray,'vendor'=>$vendor);
         $mysource=buildSource($args); // THIS FUNCTION CAN BE FOUND IN App/Helpers/Helpers.php

         $mysource->createdBy=$request->user()->id;
         $mysource->ovs_downloadFlag=1;
         $vendor->addSource($mysource);
         catAssign(); //<-THIS IS A SAFETY CHECK WHILE I HUNT DOWN WHY SEAFOOD CATEGORY ID IS NOT BEING SET
                      // DONT DEPEND ON THIS!
         flashMessage('Source Record Created','success');
         echo "1";
      }
      else {
        echo "0";
      }
    }
    //SENDING USER TO EDIT PAGE FOR SOURCE
    public function edit(Source $source)
    {
      if(Gate::allows('source-check',$source))
      {
        session()->flash('view_only',false);
        // Getting the categories we will need to start building our Edit Source form
        $vendor=Vendor::where("id", $source->fk_vendor_id)->first(); //gathering all seafood categories
        $sfCat=SeafoodCategories::all(); //gathering all seafood categories
        $gearCat=methodCategories::all(); //gathering all gear categories
        $sfs=SeafoodSpecies::all();//loading all seafood species
        $hMeths=harvestMethods::all();//loading all harvest methods
        $countries=Country::orderBy('sortOrder')->get();//loading all countries
        //If the harvest method is set so we have wild or farmed, filtering the certifications available to the drop down to match.
        $certTypes=($source->wild_farmed) ? CertificationType::where('wild_farmed',$source->harvestMethod->wild_farmed)->orderBy('certType')->get() : CertificationType::orderBy('certType')->get();
        $sps = SurveyProductSource::where('fk_source_id', $source->id)->get();
        return view('source.edit',compact('source','sfCat','gearCat','sfs','hMeths','countries','certTypes', 'sps', 'vendor'));
      }
      else {
        echo "0";
      }
    }

    //UPDATING DETAILS FOR SOURCE
    public function update(Request $request, Source $source)
    {
      $this->validate($request,[
        'supplierCompany'=>'required',
        'seafoodCategory'=>'required',
        'seafood'=>'required',
        'wf'=>'required',
        'method_cat'=>'required',
        'gear_type'=>'required',
        'catch_country'=>'required',
        'harvestRegion'=>'required',
        'scientificName'=>'required',
        'cool'=>'required'
      ]);
      if(Gate::allows('source-check',$source))
      {
        $sourceArray=$request->all();
        $args=array('sourceArray'=>$sourceArray,'source'=>$source);
        $updateSource=buildSource($args);
        if(!sourceCompare($source,$updateSource))
        {
            $source->show=0;$source->save();
            $sps=SurveyProductSource::where('fk_source_id', "=" , $source->id)->get();
            $updateSource->modifiedBy=Auth::id();
            $updateSource->ovs_downloadFlag=1;
            $updateSource->save();
            $source=$updateSource;
            catAssign();
            flashMessage('Source updated');
            foreach ($sps as $sp) {
               $sp->fk_source_id = $updateSource->id;
               $sp->save();
            }
        }
        else
        {

          $source->vendorComments=$updateSource->vendorComments;
          $source->save();
        }
        echo $source->id;
      }
      else {
        echo "0";
      }

    }
    public function details(Request $request,Source $source)
    {
      if(Gate::allows('source-check',$source))
        {
          session()->flash('view_only',true);
          $vendor = Vendor::where('id', $source->fk_vendor_id)->first();
          $sps = SurveyProductSource::where('fk_source_id', $source->id)->get();
          return view('source.edit',compact('source','sps', 'vendor'));
        }
      else
      {
        return Redirect::to('/');
      }
    }
    //SETTING SHOWSOURCE TO 0 AND RELOADING VENDOR PAGE
    public function unshow(Request $request, Source $source)
    {
      if(Gate::allows('source-check',$source))
        {
          $source->show=0; $source->ovs_downloadFlag=1;
          $source->save();
          echo "1";
        }
      else
      {
        flashMessage('Action not allowed','danger');
        echo "0";
      }
    }
    //FUNCTION FOR CREATING DUPLICATE RECORDS
    public function duplicate(Request $request,Source $source)
    {
      if(Gate::allows('source-check',$source))
        {
          $s2=$source->replicate();
          $s2->fk_etl_id=null;
          $s2->ovs_downloadFlag=1;
          $s2->save();
          echo "1";
        }
      else
      {
        echo "0";
      }
    }
    public function certTypes()
    {
      $certs=CertificationType::orderBy('certType')->get();
      return view('source.parts.certTypes',compact('certs'));
    }
    
    public function certList(Request $request, $wf)
    {
      $certs=CertificationType::where('wild_farmed',$wf)->orderBy('certType')->get();
      return view('source.parts.certTypes',compact('certs'));
    }

    public function deleteAllSps(Request $request, source $source)
    {
      //$sps=SurveyProductSource::all();
      $id = $source->id;
      $sps=SurveyProductSource::where('fk_source_id',$id)->get();
      foreach ($sps as $sp) {
         SurveyProductSource::destroy($sp->id);
      }
      echo $sps;
    }

    public function updateSps(Request $request, $oldsource, $newsource)
    {
      //$sps=SurveyProductSource::all();
      //$id = $source->id;
      $sps=SurveyProductSource::where('fk_source_id',$oldsource)->get();
      foreach ($sps as $sp) {
         $sp->fk_source_id = $newsource;
         $sp->save();
      }
      echo $sps;
    }

//THis one might become useless.  my fault for not realizing the A in ajax mean asynchronous
    public function countSps(Request $request, $source)
    {
      $sps=SurveyProductSource::where('fk_source_id',$source)->get();
          $spsCount = count($sps);
      /*echo '<input type="hidden" id="sps_count" value="'.$spsCount.'">';*/      
      echo $spsCount;
      
    }

    public function updateCountSps(Request $request, $source)
    {
      $sps=SurveyProductSource::where('fk_source_id',$source)->get();
          $spsCount = count($sps);
      echo '<input type="hidden" id="'.$source.'_sps_count" value="'.$spsCount.'">';   
      
    }

    public function sourceDeleteConfirm(Request $request, $source)
    {
      $sps=SurveyProductSource::where('fk_source_id',$source)->get();
      if (count($sps) == 0) {
        $msg = 'Are you sure you want to remove this source from your table?';
      } else {
        $msg='This source is listed as supplying the following products:<br><br>';
        foreach ($sps as $sp) {
          $msg .= $sp->product->retailer->name.' '.$sp->product->retailerProductCode.'<br>';
          $sp->save();
        }
        $msg .= '<br>Removing this source will affect the reported products.<br><br>Are you sure you want to remove this source from your table?';
      }
      echo $msg;
    }

    public function newSourceRefresh(Request $request, vendor $vendor)
    { 
      $sfCat=SeafoodCategories::all(); //gathering all seafood categories
      $countries=Country::orderBy('sortOrder')->get();//loading all countries
      return view('vendor.parts.new_source_refresh',compact('vendor','sfCat','countries'));
      echo "1";
    }



    public function sourceEditConfirm(Request $request, $source)
    {
      $sps=SurveyProductSource::where('fk_source_id',$source)->get();
      if (count($sps) == 0) {
        $msg = 'Are you sure you want to edit this source?';
      } else {
        $msg='This source is listed as supplying the following products:<br><br>';
        foreach ($sps as $sp) {
          $msg .= $sp->product->retailer->name.' '.$sp->product->retailerProductCode.'<br>';
        }
        $msg .= '<br>Editing this source will affect the reported products.<br><br>Are you sure you want to edit this source?';
      }
      echo $msg;
    }

    public function sourceEditsListed(Request $request, $source)
    {
      $sps=SurveyProductSource::where('fk_source_id',$source)->get();
      if (count($sps) == 0) {
        $msg = 'Source has been updated.';
      } else {
        $msg='The following products have been updated to include this source information:<br><br>';
        foreach ($sps as $sp) {
          $msg .= $sp->product->retailer->name.' '.$sp->product->retailerProductCode.'<br>';
        }     
      }
      echo $msg;
    }



    public function sourceDeletesListed(Request $request, $source)
    {
      $sps=SurveyProductSource::where('fk_source_id',$source)->get();
      if (count($sps) == 0) {
        $msg = 'Source has been deleted.';
      } else {
        $msg='The following products have been updated to reflect this source being deleted:<br><br>';
        foreach ($sps as $sp) {
          $msg .= $sp->product->retailer->name.' '.$sp->product->retailerProductCode.'<br>';
        }     
        $msg .='<br>These products are all incomplete now and need to be assigned sources.';
      }
      echo $msg;
    }

}