<?php

namespace App\Http\Controllers;
use Auth;
use \Redirect;
use App\Http\Requests;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth',['except'=>['welcome','coming']]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return "Hello World!";
        return view('welcome');
    }
    public function home(Request $request)
    {
        $user=$request->user();
        if(!$user){
            return view('welcome');
        } else {
            switch ($user->user_type){
                case "ngo":
                    return Redirect::action('StaffController@home');
                    break;
                case "vendor":
                    return Redirect::action('VendorController@show');
                    break;
                case "retailer":
                    return Redirect::action('RetailerController@show');
                    break;
            }
        }
    }
    public function logCheck()
    {
      if(Auth::user())
      {
        echo "true";
      }
      else {
        echo "false";
      }

    }
    public function health()
    {
      return "<html><h1>I'm doing just fine</h1></html>";
    }
    //FUNCTION TO UNSET THE SEAFOOD CATEGORY VALUE IN SESSION
    public function rmSeaCat()
    {
      if(session()->has('seafood_category'))
      {
        session()->set('seafood_category','Figs');
      }
    }

    public function completeTutorial($tutorial)
    {
        auth()->user()->completeTutorial($tutorial);
    }

    public function loadTutorial($tutorial)
    {
        $hidden = auth()->user()->hasCompletedTutorial($tutorial);
        return view('tutorials.' . $tutorial, compact(['tutorial', 'hidden']));
    }
}
