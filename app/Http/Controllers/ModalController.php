<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class ModalController extends Controller
{
    //A CONTROLLER TO HANDLE MODAL DIALOG WINDOWS
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function basic(Request $request)
    {
        return view('dialogs.modal');
    }

    public function delete(Request $request)
    {
      return view('dialogs.delete');
    }
}
