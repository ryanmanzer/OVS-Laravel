<?php

namespace App\Http\Controllers;

use Gate;
use Illuminate\Http\Request;
use Illuminate\Http\Resonse;
use Illuminate\Support\Facades\Redirect;
Use App\Vendor;
Use App\Source;
Use App\SeafoodSpecies;
Use App\SeafoodCategories;
Use App\methodCategories;
Use App\harvestMethods;
Use App\Country;
use App\Http\Requests;
use App\Product;
use App\CertificationType;
use App\Certification;
use App\SurveyProductCert;
use App\SurveyProductSource;
use App\SurveyProduct;
use App\User;

class VendorController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>'welcome']);

    }
    //setting basic middleware that any call to this controller with use when constructing itself
    //The construct method is called any time a object of this class is instantiated.

    public function show(Request $request)
    { // We will assume that if we got this far the user (whose data is in the request object) will
      // be associated with a a vendor.  Therefore we only need to provide the request to get all our
      // data
        // Getting the categories we will need to start building our New Source form
        $sfCat=SeafoodCategories::all(); //gathering all seafood categories
        $gearCat=methodCategories::all(); //gathering all gear categories
        $sfs=SeafoodSpecies::all();//loading all seafood species
        $hMeths=harvestMethods::all();//loading all harvest methods
        $countries=Country::orderBy('sortOrder')->get();//loading all countries
        //Getting the vendor information from the user
        $user=$request->user();
        $vendor=$user->org; //getting the vendor record from the user
        $surveys_count=$vendor->surveys->count(); //getting a count of surveys for this vendor
        $vendID=$vendor->id; //getting vendor ID
        $result=getTabsScripts($user);
        $tabs=$result['tabs']; $loadComp=$result['loadComp'];
        $certTypes=CertificationType::all();
        return view('vendor.home',
                    ['vendor'=>$vendor,'surveys_count'=>$surveys_count,
                     'sfCat'=>$sfCat,'gearCat'=>$gearCat,'seafood'=>$sfs,'methods'=>$hMeths,
                     'countries'=>$countries,'certTypes'=>$certTypes]);

    }
    // ROUTING VENDOR TO PAGE FOR EDITING VENDOR DETAILS
    public function edit(Request $request, Vendor $vendor)
    {
      if(Gate::allows('org-check',$vendor))
      {
        $org=$vendor;
        $user=$request->user();
        return view('vendor.parts.accountInfo.edit_contact_info',compact('org','user'));
      }
      else
      {
        return Redirect::back();
      }

    }


    // PROCESSING THE UPDATE TO THE VENDOR RECORD
    public function update(Request $request, Vendor $vendor)
    {
      $this->validate($request,
      [
          'contact_email' => 'required',
          'contact_name' => 'required',

      ]);
      if(Gate::allows('org-check',$vendor))
      {
        $vendor->modified_by=$request->user()->id;
        $vendor->ovs_downloadFlag=1;
        $vendor->update($request->except('_token'));
        return "1";
      }
      else
      {

        return "0";
      }

    }
    // THIS RETURNS THE CONTACT INFO FOR VENDOR HOME VIEW - AJAX
    public function info(Request $request, Vendor $vendor)
    {
      if(Gate::allows('org-check',$vendor))
      {
        $org=$vendor;
        $type='vendor';
        $user_accounts = User::where('org_id', $vendor->id)
          ->where('user_type', 'vendor')
          ->get();
        return view('vendor.parts.account_info',compact('org','user', 'type' , 'user_accounts'));
/*        return response()->view('general.info',compact('org','type')); */
      }
      else {
        return Redirect::back();
      }
    }

    public function contactInfo(Request $request, Vendor $vendor)
    {
      if(Gate::allows('org-check',$vendor))
      {
        $org=$vendor;
        $type='vendor';
        return view('vendor.parts.accountInfo.show_contact_info',compact('org', 'type'));
/*        return response()->view('general.info',compact('org','type')); */
      }
      else {
        return Redirect::back();
      }
    }

    // RETURNS SURVEYS FOR A PARTICULAR VENDOR AND IS PART OF VENDOR HOME VIEW - AJAX
    public function surveys(Request $request, Vendor $vendor)
    {
      $user=$request->user();
      if(Gate::allows('org-check',$vendor))
      {
        $surveys=$vendor->surveys;
        return response()->view('vendor.parts.survey_list',compact('surveys','user'));
      }
      elseif (Gate::allows('retailer-vender',$vendor))
      {
        $surveys=$vendor->surveys()->where('fk_retailer_id',$request->user()->org_id);
        return response()->view('vendor.parts.survey_list',compact('surveys','user'));
      }
      else {
        echo "0";
      }

    }
    // RETURNS VENDOR SOURCES AND IS PART OF VENDOR HOME VIEW - AJAX
    //Added ooptional paramter to pass a source id whihc will be open in edit 
    public function sources(Request $request, Vendor $vendor, Source $source = NULL)
    {
      if(Gate::allows('org-check',$vendor))
      {
        $sources_count=$vendor->sources->where('showSource',1)->count(); //getting paginated sources
        $sources=orderedSources($vendor);
        If(!is_null($source)) 
        {
         $edit_source = $source;
         session()->flash('view_only',true);
         $sps = SurveyProductSource::where('fk_source_id', $source->id)->get();
        } 

        return response()->view('vendor.parts.sources_list',compact('sources','sources_count','vendor', 'edit_source' , 'sps'));
      }
      else {
        return Redirect::back();
      }
    }
    public function productSources(Request $request, Product $product)
    {
      if(Gate::allows('product-check',$product))
        {
          $sps=$product->sps;
          $sp=$product->sp;
          $user=$request->user();
          return response()->view('vendor.parts.productSources',compact('sps','sp','user','product'));
        }
      else
      {
        return Redirect::back();
      }
    }
    /**
     * New Source form building functions
     */
    // GET LIST OF SEAFOOD CATEGORIES
    public function sf_list(Request $request, $id)
    {
        if ($id=='undefined')
        {
          $seafood=SeafoodSpecies::all();
        }
        else if ($id==0)
        {
          $seafood=SeafoodSpecies::all();
        }
        else
        {
          $sfCat = SeafoodCategories::find(intval($id));
          $seafood=$sfCat->seafood;
        }
          return response()->view('vendor.parts.newSource.sf_list',compact('seafood'));

    }
    //GET LIST OF METHOD CATEGORIES
    public function cat_list(Request $request,$wf)
    {
        $cats=methodCategories::where('wild_farmed',$wf)->get();
        return response()->view('vendor.parts.newSource.cat_list',compact('cats'));
    }
    //GET FULL LIST OF HARVEST METHODS
    public function meth_list(Request $request,$catid)
    {
      if($catid =="undefined" or $catid==0)
      {
        $methods = harvestMethods::all();
      }
      else {
          $methods=harvestMethods::where('fk_methodCategory_id',intval($catid))->get();
      }
        return response()->view('vendor.parts.newSource.meth_list',compact('methods'));
    }
    public function products(Request $request, Vendor $vendor)
    {
      if(Gate::allows('org-check',$vendor))
        {
          $products = orderProducts($vendor);
          $mypath='https://' . $request->fullUrl();
          flashMessage($mypath,'info');
          return response()->view('vendor.parts.products',compact('products','mypath'));
        }
      else
        {
          return Redirect::back();
        }
    }
    public function productShow(Product $product)
    {
      $vendor = Vendor::find($product->fk_vendor_id);
      if(Gate::allows('org-check',$vendor))
        {
          $sp=$product->sp;
          $sps=$product->sps;
          session()->flash('view_only',true);
          return response()->view('survey.parts.productSources',compact('sp','sps'));
        }
      else
      {
        echo "0";
      }

    }
    public function thrSurveys(Request $request, Vendor $vendor)
    {
      if(Gate::allows('org-check',$vendor))
        {
          $thrSurveys = $vendor->THR_surveys;
          return view('vendor.parts.trace_hr.survey_list',compact('thrSurveys'));
        }
      else
      {
        echo "0";
      }
    }
    public function certs(Request $request, Vendor $vendor)
    {
      if(Gate::allows('org-check',$vendor))
      {
        $certs=$vendor->certifications;
        $cert_types=CertificationType::orderBy('certType')->get();
        return view('vendor.parts.cert_list',compact('certs','cert_types','vendor'));
      }
    }

    public function confirmRemoveCert(Request $request, Certification $cert)
    {
      $productcerts=SurveyProductCert::where('certification_id',$cert->id)->get();
      if (count($productcerts) == 0) {
        $msg = 'Are you sure you want to remove this certification?';
      } else {
        $msg='This certification is listed for the following products:<br><br>';
        foreach ($productcerts as $productcert) {
          $msg .= $productcert->survey_product->product->retailer->name.' '.$productcert->survey_product->product->retailerProductCode.'<br>';
        }
        $msg .= '<br>Removing this certification will affect the reported products.<br><br>Are you sure you want to remove this certification?';
      }
      echo $msg;
    }

    public function addCert(Request $request, Vendor $vendor)
    {
      $this->validate($request,[
        'certification_type_id'=>'required',
        'certification_number'=>'required',
        'description'=>'required',
        
      ]);
      if(Gate::allows('org-check',$vendor))
      {
        $data=$request->all();
        $cert=new Certification($data);
        $vendor->addCert($cert);
        echo "1";
      }
      else {
        echo "0";
      }

    }
    
    public function rmCert(Request $request, Certification $cert)
    {
      if(Gate::allows('cert-check',$cert))
      {
        $productcerts=SurveyProductCert::where('certification_id',$cert->id)->get();
        foreach($productcerts as $productcert ){
          SurveyProductCert::destroy($productcert->id);
        }
        $cert->delete();
        echo "1";
      }
      else {
        echo "0";
      }

    }
    
    public function newSource(Request $request, Vendor $vendor, $sp = NULL)
    {
      $sfCat=SeafoodCategories::all(); //gathering all seafood categories
      $countries=Country::orderBy('sortOrder')->get();//loading all countries
      if(is_null($sp)) 
      {
      return view('vendor.parts.new_source_refresh', compact ('vendor', 'sfCat','countries'));
      } else {
      $sp = SurveyProduct::where('id', $sp)->first();
      $sfs=SeafoodSpecies::where('fk_seafoodCategory_id', "=", $sp->product->fk_seafoodCategory_id)->get();
      return view('vendor.parts.new_source_refresh', compact ('vendor', 'sfCat','countries', 'sp', 'sfs'));        
      }
    }

    public function refreshSfCat(Request $request, Vendor $vendor)
    {
      $sfCat=SeafoodCategories::all(); //gathering all seafood categories
      return view('source.parts.sfCatForm', compact ('sfCat' ) );
    }
    
    public function refreshSf(Request $request, Vendor $vendor)
    {
      $sfCat=SeafoodCategories::all(); //gathering all seafood categories
      return view('source.parts.sfGroup');
    }

    public function newCertModal(Request $request, Vendor $vendor)
    {
      $cert_types=CertificationType::orderBy('certType')->get(); 
      return view('vendor.parts.add_cert_modal', compact ('vendor', 'cert_types'));
    }



}
