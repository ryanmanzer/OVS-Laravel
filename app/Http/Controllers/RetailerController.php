<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Http\Requests;
use \Redirect;
use App\Retailer;

class RetailerController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    //THIS
    public function products(Request $request,Retailer $retailer)
    {
      if(Gate::allows('org-check',$retailer))
      {
        $products=$retailer->products;
        return view('retailer.parts.products',compact('products'));
      }
      else {
        return Redirect::back();
      }
    }

    public function show(Request $request)
    {
      $user=$request->user();
      $org = $user->org; //getting the organization this user belogngs to
      if(Gate::allows('org-check',$org))
        {
          $result=getTabsScripts($user);
          $tabs=$result['tabs']; $loadComp=$result['loadComp'];
          return view('home',compact("org",'tabs','loadComp'));
        }
      else
      {
        return Redirect::back();
      }
    }
    public function info(Request $request, Retailer $retailer)
    {
      $org=$retailer;
      if(Gate::allows('org-check',$retailer))
      {
        $type="retailer";
        return view('general.info',compact('org','type'));
      }
      else
      {
          return Redirect::back();
      }

    }
    public function surveys(Request $request,Retailer $retailer)
    {
      if(Gate::allows('org-check',$retailer))
        {
          $surveys=$retailer->surveys;
          return view('retailer.parts.surveys',compact('surveys'));
        }
      else
      {
        return Redirect::back();
      }
    }
    public function vendors(Request $request,Retailer $retailer)
    {
      if(Gate::allows('org-check',$retailer))
        {
          $vendors=$retailer->vendors->where('show',1);
          return view('retailer.parts.vendors',compact('vendors'));
        }
      else
      {
        return Redirect::back();
      }
    }
}
