<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\TraceHrSurvey;
use App\TraceHrSurveyQuestion;
use App\TraceHrAnswer;
use Gate;
class TraceHrController extends Controller
{
    //CONTROLLER FOR HANLDING REQUESTS RELATED TO TRACE/HR SURVEYS
    public function __construct()
    {
        $this->middleware('auth',['except'=>'welcome']);

    }
    public function show(Request $request, $id)
    {
      $survey=TraceHrSurvey::find($id);

      if(Gate::allows('thr-check',$survey))
      {
        return view('vendor.parts.trace_hr.surveyForm',compact('survey'));
      }
      else {
        return "Perhaps there is something wrong with thr-check?";
      }
    }

    public function clear(Request $request, $id)
    {
      $survey=TraceHrSurvey::find($id);
      $survclear = 1;
      if(Gate::allows('thr-check',$survey))
      {
        return view('vendor.parts.trace_hr.surveyForm',compact('survey', 'survclear'));
      }
      else {
        return "Perhaps there is something wrong with thr-check?";
      }
    }

    public function submit(Request $request, $id)
    {
      $thr=TraceHrSurvey::find($id);
      if(Gate::allows('thr-check',$thr))
      {
        $thsqs = TraceHrSurveyQuestion::where('trace_hr_survey_questions.trace_hr_survey_id', '=' , $id)->get();
        foreach ($thsqs as $thsq) 
        {

          $ex_ans = TraceHrAnswer::where('trace_hr_answers.survey_questions_id', "=", $thsq->id)->get();
          foreach ($ex_ans as $ex_an)
          {
            TraceHrAnswer::destroy($ex_an->id);
          }
        }


        $jdata=$request->json('survey_response');
        foreach ($jdata as $arr)
        {
          $ans = new TraceHrAnswer;
          $ans->survey_questions_id = intval($arr['sq_id']);
          if(array_key_exists('additional_info', $arr))
          {
            $ans->additional_info = $arr['additional_info'];
          }
          else {
            $ans->response = $arr['response'];
          }
          $ans->ovs_downloadFlag=1;
          $ans->save();
        }
        /*$thr->complete=1;*/
        $thr->save();
        echo "1";
      }
      else {
        return "No submitting for you!";
      }
    }
}
