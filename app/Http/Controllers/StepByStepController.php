<?php


namespace App\Http\Controllers;
Use DB;
Use Gate;
Use Auth;
use \Redirect;
use Illuminate\Http\Request;
use App\Retailer;
use App\Vendor;
use App\survey;
use App\Source;
use App\Country;
use App\SeafoodSpecies;
use App\SeafoodCategories;
use App\harvestMethods;
use App\methodCategories;
use App\SurveyProductSource;
use App\Http\Requests;
use App\Certification;
use App\CertificationType;
use App\TraceHrSurvey;
use App\TraceHrSurveyQuestion;
use App\TraceHrAnswer;
use App\SurveyProduct;
use App\SurveyProductCert;
use App\FaoMajorFishingArea;


class StepByStepController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth',['except'=>'welcome']);

    }

  public function splash(Request $request, Vendor $vendor)
    {
        //route  =  "/{vendor}/splash"
        // to the vendor's splash page for the stepthrough survey
        $surveys = Survey::where('fk_vendor_id', '=', $vendor->id)->get();
        $thr_surveys = TraceHrSurvey::where('fk_vendor_id', '=', $vendor->id)->get();
        $retailer_ids = array();
        foreach ($surveys as $survey) {
            array_push($retailer_ids, $survey->fk_retailer_id);
        }
        foreach ($thr_surveys as $thr_survey) {
            array_push($retailer_ids, $thr_survey->fk_retailer_id);
        }
        $retailer_ids = array_unique($retailer_ids);
        $retailers = array();

        foreach ($retailer_ids as $retailer_id) {
            if(!is_null($retailer_id)){
                $retailer = Retailer::where('id' , '=', $retailer_id)->first();
                array_push($retailers, $retailer);
            }
        }
       // $request->user()->login_count = $request->user()->login_count + 1; 
        //$request->user()->save();
        return view('step_through.splash', compact ('vendor', 'retailers') );


    }
    
  public function surveysHome(Request $request, Vendor $vendor , Retailer $retailer)
    {
        $surveys = Survey::where('fk_vendor_id', $vendor->id)->get();
        foreach ($surveys as $survey) {
                $completedProducts = Null;
                $prodCount = count($survey->sp);
                $completedProducts = SurveyProduct::where('surveyProducts.fk_survey_id', "=" , $survey->id)
                    ->where('surveyProducts.submitted', "=" , 1)
                    ->get();
                $percentComplete = round(count($completedProducts) / $prodCount , 2);
                $survey->progress = $percentComplete;
                $survey->save();
            }

        $thr_surveys = TraceHrSurvey::where('fk_vendor_id', $vendor->id)->get();

        return view('step_through.surveysHome', compact ('vendor', 'surveys', 'thr_surveys') );
    }


    

  public function productSurveyHome(Request $request, Survey $survey)
    {
 
        return view('step_through.productSurveys.surveySplash', compact ('survey') );

    }



    public function surveyCatList(Request $request, Survey $survey)
    {
        $sps = ordSurveyProducts($survey);
        $inclCats = array();
        foreach ($sps as $sp) {
            array_push($inclCats, $sp->product->seafoodCategory);
        }
        $inclCats = array_unique($inclCats);
        $surveyCats = array();
        foreach($inclCats as $inclCat ){
            $catProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
            ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
            ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
            ->get();
            $finishedCatProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
            ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
            ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
            ->where('surveyProducts.submitted', "=" , 1)
            ->get();
            $percentComplete = 100 * round(count($finishedCatProducts) / count($catProducts), 2);
            //build an array with a row for each seafoodcat (columns are seafoodCategory.id, seafoodCategory.category, count of products and precent of products oi that ctegory marked as complete)
            array_push($surveyCats, array($inclCat->id ,$inclCat->category , count($catProducts), $percentComplete));
        }
        //someday there will be logic here where the last active state of the survey is stored somewhere and reterieved
        // 
        return view('step_through.productSurveys.surveyCatList', compact ('vendor', 'survey', 'surveyCats') );
    }
    
    public function surveyCatHome(Request $request, Survey $survey, SeafoodCategories $sfCat)
    {
      $sps = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
            ->where("products.fk_seafoodCategory_id",'=', $sfCat->id )
            ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
            ->orderBy('products.retailerProductCode','asc')
            ->select('surveyProducts.*')
            ->get();
      $openSp = Null;
      foreach ($sps as $sp) {
          if(is_null($openSp)){
            if($sp->submitted != 1){
                $openSp = $sp;
            }
          }
      }
      return view('step_through.productSurveys.surveyCatHome' , compact('survey', 'sps', 'openSp', 'sfCat'));
    }

    public function catSideBar(Request $request, Survey $survey, SeafoodCategories $sfCat, SurveyProduct $openSp)
    {
      $sps = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
            ->where("products.fk_seafoodCategory_id",'=', $sfCat->id )
            ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
            ->orderBy('products.retailerProductCode','asc')
            ->select('surveyProducts.*')
            ->get();
      /*$openSp = Null;
      foreach ($sps as $sp){
        if(!is_null($openSp))
            if($sp->markedComplete != 1){
                $openSp = $sp;
            }
        }
    */
      return view('step_through.productSurveys.productEntry.catSidebar' , compact('survey', 'sps', 'openSp', 'sfCat'));
    }


    public function productEntry(Request $request, SurveyProduct $sp = Null, $section = Null){
        return view('step_through.productSurveys.productEntry.productEntry' , compact('sp', 'section'));
    }

    public function productDataEntry(Request $request, SurveyProduct $sp = Null, $section = Null){
        If(is_null($sp)){
            echo "All products in this seafood category have been completed.  Click on a product to make changes to submitted information.";
        } else {
            if(count($sp->sps) > 0){
                $sps = $sp->sps;
                $spCerts = SurveyProductCert::where('survey_product_id','=', $sp->id)->get();
            }    
            if(!is_null($section)){
                if($section == "sources"){
                    return view('step_through.productSurveys.productEntry.sourceEntry' , compact('sp', 'sps'));
                } elseif($section == "certs") {
                    return view('step_through.productSurveys.productEntry.certsEntry' , compact('sp', 'sps'));
                } elseif($section == "repacking") {
                    return view('step_through.productSurveys.productEntry.repackingEntry' , compact('sp', 'sps'));
                } elseif($section == "active") {
                    return view('step_through.productSurveys.productEntry.activeEntry' , compact('sp', 'sps', 'spCerts'));
                } elseif($section == "submit") {
                    return view('step_through.productSurveys.productEntry.submitEntry' , compact('sp', 'sps', 'spCerts'));
                }
            } else {
                if(1==2){
                 } elseif ($sp->sourceProgress == 2){
                    return view('step_through.productSurveys.productEntry.sourceEntry' , compact('sp', 'sps'));
                } elseif ($sp->certProgress == 2){
                    return view('step_through.productSurveys.productEntry.certsEntry' , compact('sp', 'sps'));
                } elseif ($sp->repackingProgress == 2){
                    return view('step_through.productSurveys.productEntry.repackingEntry' , compact('sp', 'sps'));
                } elseif ($sp->activeProgress == 2){
                    return view('step_through.productSurveys.productEntry.activeEntry' , compact('sp', 'sps'));
                } elseif ($sp->submitted == 2){
                    return view('step_through.productSurveys.productEntry.submitEntry' , compact('sp', 'sps'));
                } elseif ($sp->submitted == 1){
                    return view('step_through.productSurveys.productEntry.completedEntry' , compact('sp', 'sps'));
                } else {
                    return view('step_through.productSurveys.productEntry.sourceEntry' , compact('sp', 'sps'));
                }
            }
        }
    }

    public function sourceEntry(Request $request, SurveyProduct $sp){
        $sps = $sp->$sps;
        return view('step_through.productSurveys.productEntry.sourceEntry', compact('sp', 'sps'));
 
    }

    public function activeEntry(Request $request, SurveyProduct $sp){
        return view('step_through.productSurveys.productEntry.activeEntry', compact('sp'));
    }



    public function newSourceEntryForm(Request $request, SurveyProduct $sp){
        $countries= Country::orderby('typeSort')->orderBy('sortOrder')->get();//loading all countries
        $faoAreas = FaoMajorFishingArea::all();
        if($sp->product->fk_seafoodCategory_id != "9999") {
           $sfs = SeafoodSpecies::where('seafoodSpecies.fk_seafoodCategory_id','=', $sp->product->fk_seafoodCategory_id)
           ->orderBy('seafoodSpecies.seafoodSpecies','asc')
           ->get();
            $sources = Source::where('fk_vendor_id' , '=' , $sp->fk_vendor_id)
                ->where('sources.fk_seafoodCategory_id', '=', $sp->product->fk_seafoodCategory_id )
                ->where('sources.show', '=', 1 )
                ->join('seafoodSpecies', 'sources.fk_seafoodSpecies_id', '=', 'seafoodSpecies.id' )
                ->orderBy('seafoodSpecies.seafoodSpecies', 'asc')
                ->select('sources.*')
                ->get();
            $sfSpecies = Null;
            $firstSourceList = array();
            
            //$firstSources = array();
            foreach ($sources as $source) {
                if($source->fk_seafoodSpecies_id != $sfSpecies){
                    $sfSpecies = $source->fk_seafoodSpecies_id;
                    array_push($firstSourceList, $source->id);
                    }
                }
        } else {
            $sfCats = SeafoodCategories::orderBy('seafoodCategories.category','asc')->get(); //gathering all seafood categories
            $sources = Source::where('fk_vendor_id' , '=' , $sp->fk_vendor_id)
                ->where('show', '=', 1 )
                ->join('seafoodSpecies', 'sources.fk_seafoodSpecies_id', '=', 'seafoodSpecies.id' )
                ->orderBy('seafoodSpecies.seafoodSpecies')
                ->select('sources.*')
                ->get();
            $sfSpecies = Null;
            $firstSourceList = array();
            
            //$firstSources = array();
            foreach ($sources as $source) {
                if($source->fk_seafoodSpecies_id != $sfSpecies){
                    $sfSpecies = $source->fk_seafoodSpecies_id;
                    array_push($firstSourceList, $source->id);
                    }
                }
        }
        //$unAssignedSources = unAssignedSources($sp);

        return view('step_through.productSurveys.productEntry.source.newSourceEntry', compact('sp', 'countries', 'sfs', 'sfCats', 'sources', 'faoAreas', 'firstSourceList'));
       
    }



    public function AddNewCert(Request $request, SurveyProduct $sp, Certification $oldCert = Null){
        $vendor = Vendor::where('id','=',$sp->fk_vendor_id)->first();
        $this->validate($request,[
        'certification_type_id'=>'required',
        'certification_number'=>'required',
        'description'=>'required',
        ]);
        $certArray = $request->all();
        $certification_type_id = $_POST["certification_type_id"];
        $certification_number = trim($_POST["certification_number"]);
        $description = trim($_POST["description"]);

        $myCert = Certification::where('fk_vendor_id', '=', $sp->fk_vendor_id)->
            where('certification_type_id', '=', $certification_type_id)->
            where('certification_number', '=', $certification_number)->
            where('description', '=', $description)
            ->first();
        if(is_null($myCert)){
            $myCert = new Certification;
            $myCert->certification_type_id = $certification_type_id;
            $myCert->certification_number = $certification_number;
            $myCert->fk_vendor_id = $sp->fk_vendor_id;
            $myCert->description = $description;            
            $myCert->save();
             
        } 
        $cert_id = $myCert->id;

        $spCert = New SurveyProductCert;
        $spCert->survey_product_id = $sp->id;
        $spCert->vendor_id = $sp->fk_vendor_id;
        $spCert->certification_id = $cert_id;
        $spCert->save();
        echo "1";

    }

    public function enterSource(Request $request, SurveyProduct $sp, Source $oldSource = Null){
        $vendor = Vendor::where('id','=',$sp->fk_vendor_id)->first();
        $this->validate($request,[
        'supplierCompany'=>'required',
        'fk_seafoodCategory_id'=>'required',
        'fk_seafoodSpecies_id'=>'required',
        'wild_farmed'=>'required',
        'method_category_id'=>'required',
        'fk_harvestMethod_id'=>'required',
        'fk_harvestCountry_id'=>'required',
        'harvestRegion'=>'required',
        'scientificName'=>'required',
        'fk_coolCountry_id'=>'required'
        ]);
        $sourceArray = $request->all();
        if(Gate::allows('org-check',$vendor))
        {

            $sourceArray = $request->all();
            unset($sourceArray['_token']);
            $args=array('sourceArray'=>$sourceArray,'vendor'=>$vendor);

            $mysource = new Source($sourceArray);
            $mysource->fk_vendor_id= $vendor->id;
            $mysource->createdBy=$request->user()->id;
            $mysource->show=1; 
            $mysource->ovs_downloadFlag=1;         
            $mysource->save();         
            $source_id = $mysource->id;        
            if(!is_null($oldSource->id)) {
                if(
                    $mysource->supplierCompany == $oldSource->supplierCompany &&
                    $mysource->fk_seafoodSpecies_id == $oldSource->fk_seafoodSpecies_id &&
                    $mysource->fk_harvestMethod_id == $oldSource->fk_harvestMethod_id &&
                    $mysource->fk_coolCountry_id == $oldSource->fk_coolCountry_id &&
                    $mysource->fk_harvestCountry_id == $oldSource->fk_harvestCountry_id &&
                    $mysource->harvestRegion == $oldSource->harvestRegion &&
                    $mysource->fk_seafoodCategory_id == $oldSource->fk_seafoodCategory_id &&
                    $mysource->wild_farmed == $oldSource->wild_farmed &&
                    $mysource->fk_seafoodCategory_id == $oldSource->fk_seafoodCategory_id &&
                    $mysource->method_category_id == $oldSource->method_category_id &&
                    $mysource->certification_types_id == $oldSource->certification_types_id &&
                    $mysource->scientificName == $oldSource->scientificName &&
                    $mysource->fip_name == $oldSource->fip_name
                 ){
                    Source::destroy($mysource->id);
                    $source_id = $oldSource->id;
                /*if(sourceCompare($oldSource,$mysource)){
                    Source::destroy($mysource->id);
                    $source_id = $oldSource->id;
                }*/
                }
            }
         //$vendor->addSource($mysource);
         //catAssign(); //<-THIS IS A SAFETY CHECK WHILE I HUNT DOWN WHY SEAFOOD CATEGORY ID IS NOT BEING SET
                      // DONT DEPEND ON THIS!
         //flashMessage('Source Record Created','success');
         

        $sps = new SurveyProductSource();
        $sps->fk_source_id = $source_id;
        $sps->fk_product_id = $sp->fk_product_id;
        $sps->fk_survey_id = $sp->fk_survey_id;
        $sps->surveyProducts_id = $sp->id;
        $sps->created_by = $request->user()->id;
        $sps->ovs_downloadFlag=1;
        $sps->fk_vendor_id = $sp->fk_vendor_id;
        $sps->save();
        echo $source_id;
        } else {
        echo "0";
        }
    }



    public function assignPercent(Request $request, SurveyProductSource $sps, $percent){
        $sp = $sps->sp;
        $perc = $percent / 100 ;
        $percentRemaining = 1 - sourcePercent($sp->product); 
        $sps->proportionOfProduct = $perc;
        $sps->save();
        if($perc == $percentRemaining){
            $sp->sourceProgress = 1;
            $sp->certProgress = 2;
            $sp->surveyProgress = 25;
            $sp->save();           
        }
        
    }

    public function removeSps(Request $request, $spSource){
        $spSource = SurveyProductSource::where('id', '=', $spSource)->first();
        
            $sp = $spSource->sp;
            $sp->submitted = 0;
            $sp->repackingProgress = 0;
            $sp->activeProgress = 0;
            $sp->certProgress = 0;
            $sp->sourceProgress = 2;
            $sp->surveyProgress = 0;
            $sp->save();
            SurveyProductSource::destroy($spSource->id);
            $sps = $sp->sps;
            return view('step_through.productSurveys.productEntry.sourceEntry', compact('sp','sps'));
        
    }

    public function removeCert(Request $request, $spCert){
        $spCert = SurveyProductCert::where('id', '=', $spCert)->first();
        
            $sp = $spCert->survey_product;
            $sp->submitted = 0;
            $sp->repackingProgress = 0;
            $sp->activeProgress = 0;
            $sp->certProgress = 2;
            $sp->surveyProgress = 25;
            $sp->save();
            SurveyProductCert::destroy($spCert->id);
            //$sps = $sp->sps;
            return view('step_through.productSurveys.productEntry.certsEntry' , compact('sp'));
        
    }    

    public function addSourceAmount(Request $request, SurveyProduct $sp, SurveyProductSource $sps){
        $sps->proportionOfProduct = Null;
        $sps->save();
        return view('step_through.productSurveys.productEntry.source.addSourceAmount', compact('sp', 'sps'));
    }

    public function certsEntry(Request $request, SurveyProduct $sp){
        return view('step_through.productSurveys.productEntry.certsEntry', compact('sp'));
    }

    public function sourceEntryForm(Request $request, surveyProduct $sp = Null, $source = Null)
    {
        $countries= Country::orderby('typeSort')->orderBy('sortOrder')->get();
        $faoAreas = FaoMajorFishingArea::all();
        if(is_null($source)){
            if($sp->product->seafoodCategory->category != "Mixed") {
                $sfs = SeafoodSpecies::where('seafoodSpecies.fk_seafoodCategory_id','=', $sp->product->fk_seafoodCategory_id)
                ->orderBy('seafoodSpecies.seafoodSpecies','asc')
                ->get();            
            } else {
                $sfCats = SeafoodCategories::orderBy('seafoodCategories.category','asc')->get();
            }
        } else {
            $mySource = Source::where('id',$source)->first();
            $sfs = SeafoodSpecies::where('seafoodSpecies.fk_seafoodCategory_id','=', $mySource->fk_seafoodCategory_id)
                ->orderBy('seafoodSpecies.seafoodSpecies','asc')
                ->get();
            /*
            $methCats = methodCategories::where('wild_farmed','=', $mySource->wild_farmed)
                ->orderBy('methodCategories.methodCategory','asc')
                ->get();
            $hMeths = harvestMethods::where('fk_methodCategory_id','=', $mySource->method_category_id)
                ->orderBy('harvestMethods.harvestMethod','asc')
                ->get();
            */
            $hMeths = harvestMethods::where('wild_farmed','=', $mySource->wild_farmed)
                ->orderBy('harvestMethods.fk_methodCategory_id','asc')
                ->orderBy('harvestMethods.order','asc')
                ->get();
            
            $certTypes = CertificationType::where('wild_farmed','=', $mySource->wild_farmed)
                ->orderBy('certification_types.certType','asc')
                ->get();
            if($sp->product->fk_seafoodCategory_id == "9999"){
                $sfCats = SeafoodCategories::orderBy('seafoodCategories.category','asc')->get();
            }
            
        }
        return view('step_through.productSurveys.productEntry.source.sourceFormOnly', compact('mySource', 'sfs', 'sfCats', 'countries','sp', 'methCats', 'hMeths', 'certTypes', 'faoAreas'));    
    }
    public function sourcesList(Request $request, SurveyProduct $sp){
        $sps = $sp->sps;
        return view('step_through.productSurveys.productEntry.source.sourcesList', compact('sp','sps'));
    }

    public function completeSources(Request $request, SurveyProduct $sp){
        if($sp->sourceProgress == 2){
            $sp->sourceProgress = 1;
            $sp->certProgress = 2;
            $sp->surveyProgress = 25;
            $sp->save();
        }
        return view('step_through.productSurveys.productEntry.certsEntry' , compact('sp'));
    }

    public function progressBar(Request $request, SurveyProduct $sp, $section = Null) {
        return view('step_through.productSurveys.productEntry.progressBar', compact('sp','section'));
    }

    public function certsList(Request $request, SurveyProduct $sp) {
            $certs = $sp->certification;
            return view('step_through.productSurveys.productEntry.certs.certsList', compact('sp','certs')); 
        
    }
    
    public function certSelect(Request $request, SurveyProduct $sp) {

        return view('step_through.productSurveys.productEntry.certs.certSelect', compact('sp'));
    }

    public function addCertsQuestion(Request $request, SurveyProduct $sp) {

        return view('step_through.productSurveys.productEntry.certs.addCertsQuestion', compact('sp'));
    }

    

    public function newCertEntry(Request $request, SurveyProduct $sp) {
        $cert_types=CertificationType::orderBy('wild_farmed')
            ->orderBy('order')
            ->get();
        $certs = Certification::where('fk_vendor_id', '=', $sp->fk_vendor_id)
        ->get();
//        $unAssignedCerts = unAssignedCerts($sp);
        return view('step_through.productSurveys.productEntry.certs.newCertEntry', compact('sp','certs', 'cert_types'));
    }

    public function newCertForm(Request $request, SurveyProduct $sp , $cert = Null) {
        $cert_types=CertificationType::orderBy('wild_farmed')
            ->orderBy('order')
            ->get();

        if(!is_null($cert)){
            $myCert = Certification::where('id','=', $cert)->first();
        }
        return view('step_through.productSurveys.productEntry.certs.certEntryForm', compact('sp','cert_types', 'myCert'));
    }

    public function doneWithCerts(Request $request, SurveyProduct $sp){
        $sp->certProgress = 1;
        $sp->surveyProgress = 50;
        $sp->repackingProgress = 2;
        $sp->save();
        return view('step_through.productSurveys.productEntry.repackingEntry' , compact('sp'));
    }

    public function reOpenCerts(Request $request, SurveyProduct $sp){
        $sp->certProgress = 2;
        $sp->surveyProgress = 25;
        $sp->repackingProgress = 0;
        $sp->activeProgress = 0;
        $sp->submitted = 0;
        $sp->save();
        return view('step_through.productSurveys.productEntry.certsEntry' , compact('sp'));
    }

    public function updateProgressBar(Request $request, SurveyProduct $sp, $section = Null){
        
        $progressDots = array(
            "sources" => $sp->sourceProgress,
            "certs" => $sp->certProgress,
            "repacking" => $sp->repackingProgress,
            "active" => $sp->activeProgress,
            "submit" => $sp->submitted,
        );
        If(is_null($section)){
            if (1==2) {
            } elseif ($sp->sourceProgress ==2) {
                $section = "sources";                # code...
            } elseif ($sp->certProgress ==2) {
                $section = "certs"  ;              # code...
            } elseif ($sp->repackingProgress ==2) {
                $section = "repacking";               # code...
            } elseif ($sp->sactiveProgress ==2) {
                $section = "active" ;               # code...
            } elseif ($sp->submitted ==2) {
                $section = "submit" ;               # code...
            }
        }
        return view('step_through.productSurveys.productEntry.updateProgressBar' , compact('progressDots', 'sp', 'section'));
    }

    public function repackingAnswer(Request $request, SurveyProduct $sp, $repacking){

        $sp->repack = $repacking;
        $sp->repackingProgress = 1;
        $sp->activeProgress = 2;
        $sp->surveyProgress = 75;
        $sp->save();
        return view('step_through.productSurveys.productEntry.activeEntry' , compact('sp'));
    }


    public function repackingStatus(Request $request, SurveyProduct $sp){
        if(is_null($sp->repack)){
            echo "";
        } else {
            return view('step_through.productSurveys.productEntry.repacking.repackingStatus' , compact('sp'));
        }
    }

    public function reOpenRepacking(Request $request, SurveyProduct $sp){
        $sp->repack = Null;
        $sp->repackingProgress = 2;
        $sp->surveyProgress = 50;
        $sp->activeProgress = 0;
        $sp->submitted = 0;
        $sp->save();
        return view('step_through.productSurveys.productEntry.repackingEntry' , compact('sp'));
    }

    public function activeStatus(Request $request, SurveyProduct $sp){
        if(is_null($sp->active)){
            echo "";
        } else {
            return view('step_through.productSurveys.productEntry.active.activeStatus' , compact('sp'));
        }
    }

    public function reOpenActive(Request $request, SurveyProduct $sp){
        $sp->active = Null;
        $sp->activeProgress = 2;
        $sp->surveyProgress = 75;
        $sp->submitted = 0;
        $sp->save();
        return view('step_through.productSurveys.productEntry.ActiveEntry' , compact('sp'));
    }

    public function activeAnswer(Request $request, SurveyProduct $sp, $active){

        $sp->active = $active;
        $sp->activeProgress = 1;
        $sp->submitted = 2;
        $sp->surveyProgress = 100;
        $sp->save();
        return view('step_through.productSurveys.productEntry.submitEntry' , compact('sp'));
    }

    public function submitStatus(Request $request, SurveyProduct $sp){
        if(is_null($sp->submitted)){
            echo "";
        } else {
            return view('step_through.productSurveys.productEntry.submit.submitStatus' , compact('sp'));
        }
    }


    public function submitSurveyProduct(Request $request, SurveyProduct $sp){

        $sp->submitted = 1;
        $sp->save();
        $survey = $sp->survey;
        $completedProducts = Null;
        $prodCount = count($survey->sp);
        $completedProducts = SurveyProduct::where('surveyProducts.fk_survey_id', "=" , $survey->id)
                    ->where('surveyProducts.submitted', "=" , 1)
                    ->get();
        $percentComplete = round(count($completedProducts) / $prodCount , 2);
        $survey->progress = $percentComplete;
        $survey->save();

        return view('step_through.productSurveys.productEntry.completedEntry' , compact('sp'));
    }

public function reOpenSources(Request $request, SurveyProduct $sp){
        $sp->active = Null;
        $sp->repackingProgress = 0;
        $sp->certProgress = 0;
        $sp->activeProgress = 0;
        $sp->sourceProgress = 2;
        $sp->surveyProgress = 0;
        $sp->submitted = 0;
        $sp->save();
        $sps = $sp->sps;
        return view('step_through.productSurveys.productEntry.sourceEntry' , compact('sp', 'sps'));
    }

public function onToCertifications(Request $request, SurveyProduct $sp){

        return view('step_through.productSurveys.productEntry.source.onToCertifications' , compact('sp'));
    }


public function onToSubmit(Request $request, SurveyProduct $sp){
        
        $sp->activeProgress = 1;
        $sp->surveyProgress = 100;
        $sp->submitted = 2;
        $sp->save();
        $survey = $sp->survey;
        $completedProducts = Null;
        $prodCount = count($survey->sp);
        $completedProducts = SurveyProduct::where('surveyProducts.fk_survey_id', "=" , $survey->id)
                    ->where('surveyProducts.submitted', "=" , 1)
                    ->get();
        $percentComplete = round(count($completedProducts) / $prodCount , 2);
        $survey->progress = $percentComplete;
        $survey->save();
        return view('step_through.productSurveys.productEntry.submitEntry' , compact('sp'));
    }

public function onToActive(Request $request, SurveyProduct $sp){
        $sp->repackingProgress = 1;
        $sp->activeProgress = 2;
        $sp->surveyProgress = 75;
        $sp->save();
        return view('step_through.productSurveys.productEntry.activeEntry' , compact('sp'));
    }



public function reOpenSubmit(Request $request, SurveyProduct $sp){
        $sp->submitted = 2;
        $sp->save();
        $survey = $sp->survey;
        $completedProducts = Null;
        $prodCount = count($survey->sp);
        $completedProducts = SurveyProduct::where('surveyProducts.fk_survey_id', "=" , $survey->id)
                    ->where('surveyProducts.submitted', "=" , 1)
                    ->get();
        $percentComplete = round(count($completedProducts) / $prodCount , 2);
        $survey->progress = $percentComplete;
        $survey->save();
        return view('step_through.productSurveys.productEntry.submitEntry' , compact('sp'));
    }

public function completedEntry(Request $request, SurveyProduct $sp){
        //Check first to see if any products withing the same category are still incomplete
        $sfCat = $sp->product->fk_seafoodCategory_id;
        $sps = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
            ->where("products.fk_seafoodCategory_id",'=', $sfCat )
            ->where('surveyProducts.fk_survey_id', "=" , $sp->survey->id)
            ->orderBy('products.retailerProductCode','asc')
            ->select('surveyProducts.*')
            ->get();
        $nextSp = Null;
        foreach ($sps as $sp) {
          if(is_null($nextSp)){
            if($sp->submitted != 1){
                $nextSp = $sp;
            }
          }
        }
        if(!is_null($nextSp)){
            return view('step_through.productSurveys.productEntry.completed.completedQuestion' , compact('nextSp', 'sp'));
        } else {
            $sps = ordSurveyProducts($sp->survey);
            $inclCats = array();
            foreach ($sps as $sp) {
                array_push($inclCats, $sp->product->seafoodCategory);
            }
            $inclCats = array_unique($inclCats);
            $surveyCats = array();
            foreach($inclCats as $inclCat ){
                $catProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
                ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
                ->where('surveyProducts.fk_survey_id', "=" , $sp->fk_survey_id)
                ->get();
                $finishedCatProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
                ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
                ->where('surveyProducts.fk_survey_id', "=" , $sp->fk_survey_id)
                ->where('surveyProducts.submitted', "=" , 1)
                ->get();
                $percentComplete = count($finishedCatProducts) / count($catProducts);
                //build an array with a row for each seafoodcat (columns are seafoodCategory.id, seafoodCategory.category, count of products and precent of products oi that ctegory marked as complete)
                array_push($surveyCats, array($inclCat->id ,$inclCat->category , count($catProducts), $percentComplete));
            }
            $nextCat = Null;
            foreach ($surveyCats as $surveyCat) {
                if(is_null($nextCat)){
                    if($surveyCat[3] < 1){
                        $nextCat = SeafoodCategories::where('id','=', $surveyCat[0])
                        ->first();
                    }
                }
            }
            if(!is_null($nextCat)){
                return view('step_through.productSurveys.productEntry.completed.completedQuestion' , compact('nextCat', 'sp'));
            } else {
                return view('step_through.productSurveys.productEntry.completed.completedQuestion' , compact('sp'));
            }
        }

    }

public function thrSurveyHome(Request $request, TraceHrSurvey $thrSurvey){
        $survey = $thrSurvey;
        return view('step_through.thrSurveys.surveyForm' , compact('survey') );
    }





public function thrClear(Request $request, TraceHrSurvey $thrSurvey)
    {
        $t = time() - 18000;
        $thrSurvey->last_saved = date("m-d-y h:i a", $t).' EST';
        $thrSurvey->save();
        $thsqs = TraceHrSurveyQuestion::where('trace_hr_survey_questions.trace_hr_survey_id', '=' , $thrSurvey->id)->get();
        if(count($thsqs) > 0){
        foreach ($thsqs as $thsq) 
            {
            $ex_ans = TraceHrAnswer::where('trace_hr_answers.survey_questions_id', "=", $thsq->id)->get();
            foreach ($ex_ans as $ex_an){
                TraceHrAnswer::destroy($ex_an->id);
                }
            }
        }
      $survey=$thrSurvey;

      $survclear = 1;
      if(Gate::allows('thr-check',$survey))
      {
        return view('step_through.thrSurveys.surveyFormOnly',compact('survey', 'survclear'));
      }
      else {
        return "Perhaps there is something wrong with thr-check?";
      }
    }


public function thrSave(Request $request, TraceHrSurvey $thrSurvey, $submit = Null)
    {

      if(Gate::allows('thr-check',$thrSurvey))
      {
        $t = time() - 18000;
        $thrSurvey->last_saved = date("m-d-y h:i a", $t).' EST';
        $thrSurvey->save();
        $thsqs = TraceHrSurveyQuestion::where('trace_hr_survey_questions.trace_hr_survey_id', '=' , $thrSurvey->id)->get();
        if(count($thsqs) > 0){
        foreach ($thsqs as $thsq) 
            {
            $ex_ans = TraceHrAnswer::where('trace_hr_answers.survey_questions_id', "=", $thsq->id)->get();
            foreach ($ex_ans as $ex_an){
                TraceHrAnswer::destroy($ex_an->id);
                }
            }
        }


        $jdata=$request->json('survey_response');
        if(count($jdata) > 0){
        foreach ($jdata as $arr){
          $ans = new TraceHrAnswer;
          $ans->survey_questions_id = intval($arr['sq_id']);
          if(array_key_exists('additional_info', $arr))
          {
            $ans->additional_info = $arr['additional_info'];
          }
          else {
            $ans->response = $arr['response'];
          }
          $ans->ovs_downloadFlag=1;
          $ans->save();
          $ans->question_id = $ans->survey_question->trace_hr_question_id;
          $ans->save();
            }
        }
        if (!is_null($submit)) {
            $thrSurvey->complete = 1;
            $thrSurvey->save();

        }
        /*$thr->complete=1;*/    
        echo "1";
      }
      else {
       // return "No submitting for you!";
      }
    }
    
    public function thrSurveyForm(Request $request, TraceHrSurvey $thrSurvey)
    {
      $survey=$thrSurvey;
      if(Gate::allows('thr-check',$survey))
      {
        return view('step_through.thrSurveys.surveySummary',compact('survey'));
      }
      else {
        return "Perhaps there is something wrong with thr-check?";
      }
    }

    public function thrSurveySubmittedAlert(Request $request)
    {
        return view('step_through.thrSurveys.submittedAlert');
    }

    public function thrSurveyCompletedAlert(Request $request)
    {
        return view('step_through.thrSurveys.completedAlert');
    }



    public function thrSurveySummaryAfterSave(Request $request, TraceHrSurvey $thrSurvey)
    {
      $survey = $thrSurvey;
      $success = 1;
      if(Gate::allows('thr-check',$survey))
      {
        return view('step_through.thrSurveys.surveySummary',compact('survey', 'success'));
      } else {
        return "Perhaps there is something wrong with thr-check?";
      }
    }


    public function thrSubmit(Request $request, TraceHrSurvey $thrSurvey)
    {
      $survey=$thrSurvey;
      if(Gate::allows('thr-check',$survey))
      {
      $survey->complete = 1;
      $survey->save();
      
        return view('step_through.thrSurveys.surveyForm',compact('survey'));
      }
      else {
        //return "Perhaps there is something wrong with thr-check?";
      }
    }

    public function completeSurvey(Request $request, Survey $survey)
    {
      $survey->complete = 1;
      $survey->save();
      $vendor = $survey->vendor;
      $sps = ordSurveyProducts($survey);
        $inclCats = array();
        foreach ($sps as $sp) {
            array_push($inclCats, $sp->product->seafoodCategory);
        }
        $inclCats = array_unique($inclCats);
        $surveyCats = array();
        foreach($inclCats as $inclCat ){
            $catProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
            ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
            ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
            ->get();
            $finishedCatProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
            ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
            ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
            ->where('surveyProducts.submitted', "=" , 1)
            ->get();
            $percentComplete = 100 * round(count($finishedCatProducts) / count($catProducts), 2);
            //build an array with a row for each seafoodcat (columns are seafoodCategory.id, seafoodCategory.category, count of products and precent of products oi that ctegory marked as complete)
            array_push($surveyCats, array($inclCat->id ,$inclCat->category , count($catProducts), $percentComplete));
        }
        //someday there will be logic here where the last active state of the survey is stored somewhere and reterieved
        // 
        return view('step_through.productSurveys.surveyCatList', compact ('vendor', 'survey', 'surveyCats') );


            
    }



    
    public function setMethCat(Request $request){
    /*    if(!is_null($wf)){
            $methCats = methodCategories::where('wild_farmed','=', $wf)
                ->orderBy('methodCategories.methodCategory','asc')
                ->get();
            }
        return view ('step_through.productSurveys.productEntry.source.updateLists.methCatList', compact('methCats'));


        */
    //Where this used to grab the WF and then pick only the list of categories that apply.  Now it just blanks out the MEthod Caegory box
        return view ('step_through.productSurveys.productEntry.source.updateLists.methCatList');
    }

    public function setMethCatByGear(Request $request, harvestMethods $specMeth){
        /*$methCats = methodCategories::where('wild_farmed','=', $specMeth->wild_farmed)
                ->orderBy('methodCategories.methodCategory','asc')
                ->get();
            
        return view ('step_through.productSurveys.productEntry.source.updateLists.methCatList', compact('methCats', 'specMeth'));
        */
        return view ('step_through.productSurveys.productEntry.source.updateLists.methCatList', compact('specMeth'));
        
    }



    public function setSpecMethByWF(Request $request, $wf = Null){
         if(!is_null($wf)){
            $wildFarmed =  strval($wf);
            $specMeths = harvestMethods::where('wild_farmed','=', $wildFarmed)
           ->orderBy('harvestMethods.fk_methodCategory_id','asc')
           ->orderBy('harvestMethods.order','asc')
           ->get();
            }
        
        return view ('step_through.productSurveys.productEntry.source.updateLists.methList', compact('specMeths'));
    }

    public function setSpecMeth(Request $request, $methCat = Null){
        if(!is_null($methCat)){
            $specMeths = harvestMethods::where('fk_methodCategory_id','=', $methCat)
                ->orderBy('harvestMethods.order','asc')
                ->get();
            }
        
        
        return view ('step_through.productSurveys.productEntry.source.updateLists.methList', compact('specMeths'));
    }

    public function setCertList(Request $request, $wf = Null){
        if(!is_null($wf)){
            $certTypes = CertificationType::where('wild_farmed','=', $wf)
                ->orderBy('certification_types.certType','asc')
                ->get();
            }
        return view ('step_through.productSurveys.productEntry.source.updateLists.certList', compact('certTypes'));
    }

    public function setSpeciesList(Request $request, SeafoodCategories $sfCat = Null){
        $seafood = Null;
        if(!is_null($sfCat)){
            $seafood = SeafoodSpecies::where('seafoodSpecies.fk_seafoodCategory_id','=', $sfCat->id)
           ->orderBy('seafoodSpecies.seafoodSpecies','asc')
           ->get();
            }
        return view ('step_through.productSurveys.productEntry.source.updateLists.speciesList', compact('seafood', 'sfCat'));
    }


    public function catCompleted(Request $request, Survey $survey, SeafoodCategories $sfCat){
        $sps = ordSurveyProducts($survey);
            $inclCats = array();
            foreach ($sps as $sp) {
                array_push($inclCats, $sp->product->seafoodCategory);
            }
            $inclCats = array_unique($inclCats);
            $surveyCats = array();
            foreach($inclCats as $inclCat ){
                $catProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
                ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
                ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
                ->get();
                $finishedCatProducts = SurveyProduct::join('products','surveyProducts.fk_product_id','=','products.id')
                ->where("products.fk_seafoodCategory_id",'=', $inclCat->id )
                ->where('surveyProducts.fk_survey_id', "=" , $survey->id)
                ->where('surveyProducts.submitted', "=" , 1)
                ->get();
                $percentComplete = count($finishedCatProducts) / count($catProducts);
                //build an array with a row for each seafoodcat (columns are seafoodCategory.id, seafoodCategory.category, count of products and precent of products oi that ctegory marked as complete)
                array_push($surveyCats, array($inclCat->id ,$inclCat->category , count($catProducts), $percentComplete));
            }
            $nextCat = Null;
            foreach ($surveyCats as $surveyCat) {
                if(is_null($nextCat)){
                    if($surveyCat[3] < 1){
                        $nextCat = SeafoodCategories::where('id','=', $surveyCat[0])
                        ->first();
                    }
                }
            }
            if(!is_null($nextCat)){
                return view('step_through.productSurveys.surveyCatCompleted' , compact('sfCat', 'nextCat', 'survey'));
            } else {
                return view('step_through.productSurveys.surveyCatCompleted' , compact('sfCat' , 'survey'));
            }

    }

    public function setLastOpened(Request $request, Survey $survey){
        $t = time() - 18000;
        $survey->lastOpened = date("m-d-y h:i a", $t).' EST';
        $survey->save();
    } 



}
