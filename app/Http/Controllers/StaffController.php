<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gate;
use App\Http\Requests;
use App\ngo;
use App\Retailer;
use App\Vendor;
use App\survey;
use \Redirect;
use App\User;
class StaffController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth',['except'=>'welcome']);
    }

    public function home(Request $request)
    {
        $user = $request->user(); //get the user
        $org = $user->org; // get the organization this user belogns to (ngo, retailer, vendor)
        $results=getTabsScripts($user);
        $tabs=$results['tabs']; $loadComp=$results['loadComp'];
        return view('home',compact('org','tabs','loadComp','user'));
    }
    public function info(Request $request, ngo $ngo)
    {
      if(Gate::allows('org-check',$ngo))
        {
          $org=$ngo; //the general.info blade is expecting an org variable
          $type='ngo';
          return view('general.info',compact('org','type'));
        }
      else
      {
        return Redirect::back();
      }
            }
    //custom function used to pass various PHP objects and see them dumped out into a simple
    //webpage
    public function message(Request $request, ngo $ngo)
    {
        $message=var_dump($request);
        return view('errors.custom',compact('message'));
    }
    public function retailerList(Request $request)
    {
        $retailers=Retailer::where('show',1)->get();
        return view('ngo.parts.retailerList',compact('retailers'));
    }
    public function vendorList(Request $request)
    {
        $vendors=Vendor::where('show',1)->get();
        return view('ngo.parts.vendorList',compact('vendors'));
    }
    public function surveyList(Request $request)
    {
        $surveys=Survey::all();
        return view('ngo.parts.surveyList',compact('surveys'));
    }
    public function users(Request $request, User $user)
    {
      if($user->admin)
      {
        $users=User::all();
        //return var_dump($users);
        $admin=$user;
        
        return view('ngo.parts.user_list',compact('users','admin'));
      }
      else {
        return "<div class='alert alert-warning'>
        <strong>Adminsitrator access required</strong>
        </div>";
      }
    }
}
