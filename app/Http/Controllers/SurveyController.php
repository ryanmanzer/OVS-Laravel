<?php

namespace App\Http\Controllers;

use DB;
use Psy\CodeCleaner\AssignThisVariablePass;
use Gate;
use App\survey;
Use App\Retailer;
use App\Vendor;
use App\Product;
use App\SurveyProductSource;
use Illuminate\Http\Request;
use \Redirect;
use App\Http\Requests;
use App\SurveyProductCert;
use App\SurveyProduct;
Use App\SeafoodSpecies;

class SurveyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth',['except'=>'welcome']);
    }

    //FUNCTION THAT HANLDES UPDATING SURVEY STATUS
    public function updateStatus(Request $request, Survey $survey)
    {
      if(Gate::allows('survey-edit',$survey))
        {
          $survey->surveyStatus = $request->surveyStatus;
          $survey->ovs_downloadFlag=1;
          $survey->modified_by = $request->user()->id;
          if($request->surveyStatus =="Assigned to Vendor")
          {
              $survey->dateIssued = date("Y-m-d");
          }
          $survey->save();
          return back();
        }
      else
      {
        return Redirect::back();
      }
    }
    //FUNCTION FOR LOADING THE NEW SURVEY FORM
    public function newSurvey(Request $request)
    {
        //return "new Survey funtion called!";
        $user=$request->user;
        $retailers=Retailer::all();
        $vendors=Vendor::all();
        return view('survey.new',compact('user','retailers','vendors'));
    }
    //FUNCTION TO SHOW ALL PRODUCTS ASSIGNED TO A SPECIFIC SURVEY
    //DUE TO REOGRGANIZATION OF CONCEPTS, THIS WILL NOW
    // RETURN THE INSTANCES OF THE SURVEYPRODUCT TABLE ASSOCIATED WITH THIS
    //SURVEY

    public function products(Request $request,Survey $survey, $open_sp = NULL)
    {
      if(Gate::allows('survey-view',$survey))
        {
          $user = $request->user();
          $sp_list=ordSurveyProducts($survey); //this wil return a collection of surveyProduct records
          $done=$survey->complete;
          if(is_null($open_sp))
          {
          return view('survey.parts.productsTable',compact('sp_list','survey','user','done'));
          } else {
          // otherwise you need to pass all the data needed to open the assign source  
            $osp = SurveyProduct::where('id', $open_sp)->first();
            $sps=$osp->sps; //returning the surveyproductsource records assiciated with this survey product          
            $sps_ids=$sps->pluck('fk_source_id')->toArray();
            $sfCat=$osp->product->seafoodCategory->id;
            
          //getting all potential sources for this vendor
            $sources=$user->org->sources()
            ->where([
              ['show','=',1],
              ['fk_seafoodCategory_id','=',$sfCat],
              ])->get();
            //getting all potential certifications for this vendor
            $vendor_certs=$user->org->certifications;
            $prod_certs=$osp->certification;//getting collection of certifications for this product
            $cert_ids=$prod_certs->pluck('certification_id')->toArray();
            
          return view('survey.parts.productsTable',compact('sp_list', 'survey', 'sps','sources','user','vendor_certs','prod_certs','done','sps_ids','cert_ids', 'osp'));
          }        
        }
          
      else
      {
        return Redirect::back();
      }
    }
    //FUNCTION TO LOAD THE FORM TO DISPLAY/ASSIGN SOURCES FOR PRODUCTS
    public function assign(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
        {
          $user=$request->user();
          $sps=$sp->sps; //returning the surveyproductsource records assiciated with this survey product
          $sps_ids=$sps->pluck('fk_source_id')->toArray();
          $sfCat=$sp->product->seafoodCategory->id;
          //getting all potential sources for this vendor
          $sources=$user->org->sources()
          ->where([
            ['show','=',1],
            ['fk_seafoodCategory_id','=',$sfCat],
            ])->get();
            //getting all potential certifications for this vendor
            $vendor_certs=$user->org->certifications;
            $prod_certs=$sp->certification;//getting collection of certifications for this product
            $cert_ids=$prod_certs->pluck('certification_id')->toArray();
            $done=$sp->survey->complete; //indicating if survey is completed by vendor
          //returning the view in survey/parts/productSources.blade.php with the variables
          //for surveyProducts, SurveyProductSources, and Sources/
          return view('survey.parts.productSources',compact('sp','sps','sources','user','vendor_certs','prod_certs','done','sps_ids','cert_ids'));

        }
      else
      {
        return Redirect::back();
      }
    }

    //FUNCTION TO DELETE A SPECIFIC SURVEYPRODUCTSOURCE RECORD
    public function destroySPS(Request $request, SurveyProductSource $sps)
    {
      if(Gate::allows('sps-edit',$sps))
        {
          $sp=$sps->sp;$sp->ovs_downloadFlag=1;$sp->save();
          SurveyProductSource::destroy($sps->id); //destroying this specific record
          echo '1';
        }
      else
      {
        echo '0';
        /*return Redirect::back();*/
      }
    }
    //function to create a SurveyProductSource record assigned to a specific SurveyProduct
    public function assignSPS(Request $request, SurveyProduct $sp)
    {
      $this->validate($request,[
        'Proportion_Of_Product'=>'required',
        'Source'=>'required',
      ]);
      if(Gate::allows('sp-edit',$sp))
        {

          $pop = $request->Proportion_Of_Product/100;
          $prodPer=sourcePercent($sp->product);

          if($pop == 0) //if the user didn't specify a percent of product
          {
            errorMessage('Please assign a percent of product for this source.');
          }
          elseif($pop > 1) //if they try to specify more than 100%
          {
            errorMessage('Please do not assign more than 100% for any source.');
          }
          elseif($pop+$prodPer > 1)
          {
            $remain=(1-$prodPer) * 100;
            errorMessage('This would allocate more than 100% to this product.');
          }
          else
          {
            $sp->ovs_downloadFlag=1; $sp->save();
            $sps = new SurveyProductSource();
            $sps->fk_source_id = $request->Source;
            $sps->proportionOfProduct= $pop;
            $sps->fk_product_id = $sp->fk_product_id;
            $sps->fk_survey_id = $sp->fk_survey_id;
            $sps->surveyProducts_id = $sp->id;
            $sps->created_by = $request->user()->id;
            $sps->ovs_downloadFlag=1;
            $sps->fk_vendor_id = $sp->fk_vendor_id;
            $sps->save();
            flashMessage('Source Added to product','success');
          }

          echo "1";
        }
      else
      {
        echo "0";
      }

    }
    //Function used to return a read-only view of product sourcing for
    //NGO staff to view.
    public function viewSources(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
        {
          $user=$request->user();
          $sps=$sp->sps;
          return view('survey.parts.productSources',compact('user','sps',"sp"));
        }
      else
      {
        echo "0";
      }
    }
    public function assignCert(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
      {
        $new_cert= new SurveyProductCert();
        $new_cert->survey_product_id=$sp->id;
        $new_cert->certification_id=$request->certification_id;
        $new_cert->vendor_id=$sp->fk_vendor_id;
        $new_cert->ovs_downloadFlag=1;
        $new_cert->save();
        flashMessage('Certification assigned to product','success');
        echo "1";
      }
      else {
        echo "0";
      }
    }
    public function removeCert(Request $request, $id)
    {
      $spc=SurveyProductCert::find($id);
      $sp=$spc->survey_product;
      if(Gate::allows('sp-edit',$sp))
      {
        SurveyProductCert::destroy($spc->id);
        echo "1";
      }
      else {
        flashMessage('Something appears to have gone wrong','danger');
      }
    }
    public function assignRepack(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
      {
        $sp->repack=$request->repack;
        $sp->ovs_downloadFlag=1;
        $sp->save();
        echo "1";
      }
      else {
        echo "0";
      }
    }
    public function updatePercent(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
      {
        $percent = sourcePercent($sp->product)*100 .' %';
        echo $percent;
      }
      else {
        echo "0";
      }
    }    

    public function newModalTitle(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
      {
        $msg = 'Enter New '.$sp->product->seafoodCategory->category.' Source Information';
        echo $msg;
      }
      else {
        echo "0";
      }
    } 

    public function sfCatGroup(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
      {
        return view('source.parts.sfCatForm',compact('sp'));
      }
      else {
        echo "0";
      }
    } 

    public function sfGroup(Request $request, SurveyProduct $sp)
    {
      if(Gate::allows('sp-edit',$sp))
      {
        $sfs = SeafoodSpecies::all();
        return view('source.parts.sfGroup',compact('sp', 'sfs'));
      }
      else {
        echo "0";
      }
    } 


}
