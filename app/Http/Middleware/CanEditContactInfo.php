<?php

namespace App\Http\Middleware;

use Closure;

class CanEditContactInfo
{
    /**
     * Handle an incoming request. This middleware is used for all editing of
     * contact information.  It verifies the user should be able to edit this
     * information.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $org)
    {
      switch ($org)
      {
        case 'vendor':
          $id=$request->vendor->id;
          break;
        case 'retailer':
          $id=$request->retailer->id;
          break;
        case 'ngo':
          $id=$request->ngo->id;
          break;
        default:
          return response()->view('errors.custom',['message'=>'Uh uh uh....bad user!']);
          break;
      }
      if (!$request->user()->admin)
      {
        if ($request->user()->org_id == $id and $request->user()->user_type == $org)
        {

            return $next($request);
        }
        else
        {
          return response()->view('errors.custom',['message'=>'Uh un uh...bad user!']);
        }
      }
      else
      {
        return $next($request);
      }

    }
}
?>
