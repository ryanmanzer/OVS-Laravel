<?php

namespace App\Http\Middleware;

use Closure;

class SurveyRecordsCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $recType)
    {
        // We let
        return view('errors.custom',['message'=>var_dump($request)])
        if(! $request->user()->user_type == "ngo")
        {
          if ($request->user()->user_type=="retailer")
          {
            switch ($recType)
            {
              case 'survey':
                $id=$request->survey->fk_retailer_id;
                break;
              case 'sp':
                $id=$request->sp->fk_retailer_id;
                break;
              default:
                errorMessage('Invalid request made');
                return redirect('/');
                break;
            }
          }
          elseif($request->user()->user_type=="vendor");
          {
            switch ($recType)
            {
              case 'survey':
                $id=$request->survey->fk_vendor_id;
                break;
              case 'sps':
                $id=$request->sps->source->fk_vendor_id;
                break;
              case 'sp':
                $id=$request->sp->fk_vendor_id;
              default:
                $message= "No valid record type passed in to SurveyRecordsCheck middelware";
                return response()->view('errors.custom',compact('message'));
                break;
            }
            //IF THE ID DOESN'T MATCH THE VENDOR ID OF THE USER, THEY GET SENT HOME
            if(! $id == $request->user()->org_id)
            {
              //return view('errors.custom',['message'=>'Vendor ID does not match record to be modified!']);
              errorMessage('Invalied request made');
              return redirect('/');
            }
          }

        }
        return $next($request);
    }
}
