<?php

namespace App\Http\Middleware;

use Closure;

class AdminRequired
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $user=$request->user(); // returns user account if signed in, otherwise returns null
        if($user && $user->admin==1) {
            return $next($request);
        }
        abort(404, 'Administrator Privileges are required for this action.');
    }
}
