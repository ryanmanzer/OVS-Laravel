<?php

namespace App\Http\Middleware;

use Closure;
/*
* This middleware is used to check if one vendor is trying to modify records
* of another vendor
*/
class VendorCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $recType)
    {

      switch ($recType) {
        case 'vendor':
          $id=$request->vendor->id;
          break;
        case 'source':
          $id=$request->vendor->id;
          break;
        default:
          # code...
          break;
      }
      if(! $request->user()->admin)
      {
        // If they are NGO users then we don't worry
        if($request->user()->user_type=="vendor")
        { //We process vendors and retailers differently as far as what they
          //can see

            return->response()->view('errors.custom',['message'=>'user is vendor. test Id val is ' . $id);
            return $next($request);
        }
        elseif ($request->user()->user_type=="retailer")
        {
          # code...

          return $next($request);
        }
      }
      else
      {
        $message="Users is of org type: " . $request->user()->user_type . " and
        admin status of " . $request->user()->admin;
        return->response()->view('errors.custom',['message'=>$message])
      }

    }
}
