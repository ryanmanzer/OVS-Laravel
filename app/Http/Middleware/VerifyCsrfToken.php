<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Closure;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];
    public function handle($request, Closure $next)
    {
      if (
          $this->isReading($request) ||
          $this->runningUnitTests() ||
          $this->shouldPassthrough($request) ||
          $this->tokensMatch($request)
          )
      {
        return $this->addCookieToResponse($request, $next($request));
      }
      //Error signal that certificate has expired (User is logged out)
      return json_encode(0);
    }
}
