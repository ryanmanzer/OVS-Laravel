<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyProduct extends Model
{
    /**
     * Table to which this model relates
     *
     * @var string
     */
    protected $table="surveyProducts";

    public function survey()
    {
        return $this->belongsTo('App\survey','fk_survey_id');
    }

    public function product()
    {
        return $this->belongsTo('App\Product','fk_product_id');
    }
    public function sps()
    {
      return $this->hasMany('App\SurveyProductSource','surveyProducts_id');
    }
    public function certification()
    {
      return $this->hasMany("App\SurveyProductCert",'survey_product_id');
    }
    public function vendor()
    {
      return $this->belongsTo('App\Vendor','fk_vendor_id');
    }

}
