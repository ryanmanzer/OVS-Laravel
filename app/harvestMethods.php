<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class harvestMethods extends Model
{
    /**
     * The table associated with this model
     *
     * @var string
     */
    
    protected $table="harvestMethods";
    
    public function category()
    {
        return $this->belongsTo('App\methodCategories','fk_methodCategory_id');
    }
}