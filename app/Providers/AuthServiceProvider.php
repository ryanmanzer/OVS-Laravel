<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);
        //provides universal record access to Fishwise staff users
        $gate->before(function($user,$ability)
        {
          if ($user->org->name == "FishWise")
          {
            return true;
          }
        });
        //This abiility determines if a user can view/edit records based on the organization passed in.
        $gate->define('org-check', function($user, $org)
        {
          return $user->org_id == $org->id;
        });
        //This ability defines a users ability to modify a Source
        $gate->define('source-check',function($user,$source)
        {
          return $user->org_id == $source->fk_vendor_id;
        });
        //Determines who can view a survey
        $gate->define('survey-view',function($user,$survey)
        {
          if ($user->user_type=="vendor")
          {
              return $user->org_id == $survey->fk_vendor_id;
          }
          elseif ($user->user_type=="retailer")
          {
            return $user->org_id ==$survey->fk_retailer_id;
          }
        });
        //Determining who can edit details about the survey -- at present this only makes sense for vendors
        $gate->define('survey-edit',function($user,$survey){
          return $user->org_id == $survey->fk_vendor_id;
        });
        //Determining who can edit details of survey products or survey product sources
        $gate->define('sp-edit',function($user,$sp){
          if ($user->user_type == "vendor") {
            return $user->org_id  == $sp->fk_vendor_id;
          } else {
            return false;
          }
        });
        //checking to make sure only the vendor associated with the correct source can edit this record.
        $gate->define('sps-edit',function($user,$sps){
          return $user->org_id == $sps->source->fk_vendor_id;
        });
        //checking to ensure a retailer has a reason to be viewing info related
        //to a vendor
        $gate->define('retailer-vendor',function($user,$vendor){
          $recs=$vendor->surveys()->where('fk_retailer_id',$user->org_id);
          if($recs->count()>0)
          {
            return true;
          }
          else
          {
            return false;
          }

        });
        $gate->define('product-check',function($user,$product)
        {
          return $user->org_id == $product->fk_vendor_id;
        });
        $gate->define('cert-check',function($user,$cert){
          if($user->user_type=='vendor')
          {
            return $user->org_id == $cert->fk_vendor_id;
          }
          return false;
        });
        //Checks to ensure the only people who can view/edit the Trace/HR survey are associated with the vendor it's assigned to.
        $gate->define('thr-check',function($user,$thr){
          return $user->org_id == $thr->fk_vendor_id;
        });

    }
}
