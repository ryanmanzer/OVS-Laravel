<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class methodCategories extends Model
{
    /**
     * The table this model relates to
     *
     * @var string
     */
    protected $table="methodCategories";
    
    public function methods()
    {
        return $this->hasMany('App\harvestMethods','fk_methodCategory_id');    
    }
}
