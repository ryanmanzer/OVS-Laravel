<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Vendor extends Model
{
    protected $fillable=['name','contact_name','contact_email','contact_phone',
                         'add_street_1','add_street_2','add_city','add_state_prov',
                         'add_country'];

    public function surveys()
    {
        return $this->hasMany('App\survey', 'fk_vendor_id');
    }

    public function sources()
    {
        return $this->hasMany('App\Source','fk_vendor_id');
    }

    public function products()
    {
        return $this->hasMany('App\Product','fk_vendor_id');
    }

    public function addSource(Source $source)
    {
        return $this->sources()->save($source);
    }
    public function THR_surveys()
    {
      return $this->hasMany('App\TraceHrSurvey','fk_vendor_id')->orderBy('survey_type','desc');
    }
    public function certifications()
    {
      return $this->hasMany('App\Certification','fk_vendor_id');
    }
    public function addCert(Certification $cert)
    {
      return $this->certifications()->save($cert);
    }
}
