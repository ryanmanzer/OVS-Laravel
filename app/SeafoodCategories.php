<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeafoodCategories extends Model
{
    /**
     * Table this model relates to
     *
     * @var string
     */
    protected $table="seafoodCategories";
    
    public function seafood()
    {
        return $this->hasMany('App\SeafoodSpecies','fk_seafoodCategory_id','id');    
    }
    public function sources()
    {
        return $this->hasMany('App\Source','fk_seafoodCategory_id','id');
    }
}
