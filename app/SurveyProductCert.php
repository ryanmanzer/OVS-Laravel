<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SurveyProductCert extends Model
{
  //MODEL FOR THE SURVEY PRODUCT CERTIFICATIONS TABLE THAT LINKS VENDOR CERTIFIIONS WITH SURVEY PRODUCT RECORDS

  public function certification()
  {
    return $this->belongsTo('App\Certification','certification_id');
  }
  public function survey_product()
  {
    return $this->belongsTo('App\SurveyProduct');
  }

}
