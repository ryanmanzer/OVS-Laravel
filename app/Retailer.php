<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Retailer extends Model
{
    public function products()
    {
        return $this->hasMany('App\Product','fk_retailer_id');
    }

    public function surveys()
    {
        return $this->hasMany('App\survey', 'fk_retailer_id');
    }
    public function vendors()
    {
      return $this->belongsToMany('App\Vendor','surveys','fk_retailer_id','fk_vendor_id');
    }
}
