<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FaoMajorFishingArea extends Model
{
    /**
     * The table associated with this model
     *
     * @var string
 */
    
    protected $table="faoMajorFishingAreas";
    
    
}