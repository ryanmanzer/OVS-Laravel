<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public $with = ['sources']; //determines what relationships should be automatically loaded with this model
    /*
     * FUNCTIONS THAT DEFINE RELATIONSHIP TO OTHER TABLES
     */
    public function retailer()
    {
        return $this->belongsTo('App\Retailer','fk_retailer_id');
    }

    public function vendor()
    {
        return $this->belongsTo('App\Vendor','fk_vendor_id');
    }
    public function sps()
    {
        return $this->hasMany('App\SurveyProductSource','fk_product_id');
    }
    public function sources()
    {
        return $this->belongsToMany('App\Source','surveyProductSources','fk_product_id','fk_source_id');
    }
    public function seafoodCategory()
    {
        return $this->belongsTo('App\SeafoodCategories','fk_seafoodCategory_id');
    }
    public function surveys()
    {
      return $this->belongsToMany('App\survey',"surveyProducts", 'fk_product_id', 'fk_survey_id');
    }
    public function sp()
    {
      return $this->hasMany('App\SurveyProduct','fk_product_id');
    }
}
