<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraceHrSurvey extends Model
{
    // Defining the relatshionships bewteen the various tables used to build Traceablility and Human Rights surveys.
    public function questions()
    {
      //Oddly enough, the second argument in the belongsToMany relationship, even if it is defined in Laravel as a model, needs to be the actual table name for the union table.
      return $this->belongsToMany('App\TraceHrQuestion','trace_hr_survey_questions', 'trace_hr_survey_id','trace_hr_question_id');
    }
    public function surveyquestions()
    {
      return $this->hasMany('App\TraceHrSurveyQuestion');
    }
    //Function returns the answers associated with this survey RECORD
    public function answers()
    {
      return $this->belongsToMany('App\TraceHrAnswer','trace_hr_survey_questions','trace_hr_survey_id','trace_hr_question_id');
    }
    //Returns the vendor records this survey record belongs to
    public function vendor()
    {
      return $this->belongsTo('App\Vendor','fk_vendor_id');
    }
}
