<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'completed_tutorial', 'login_count'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    //Return the related organization
    public function org()
    {
        switch($this->user_type){
            case "ngo":
                return $this->belongsTo('App\ngo','org_id');
                break;
            case "vendor":
                return $this->belongsTo('App\Vendor','org_id');
                break;
            case "retailer":
                return $this->belongsTo('App\Retailer','org_id');
                break;
        }

    }

    public function getCompletedTutorialsAttribute() {
        if (!$this->attributes['completed_tutorials']) {
            return [];
        }
        return json_decode($this->attributes['completed_tutorials']);
    }

    public function setCompletedTutorialsAttribute($tutorials_array) {
        $this->attributes['completed_tutorials'] = json_encode($tutorials_array);
    }

    public function completeTutorial($tutorial_name) {
        $tutorials_array = $this->completed_tutorials;
        $tutorials_array[] = $tutorial_name;
        $this->completed_tutorials = array_unique($tutorials_array);
        $this->save();
    }

    public function hasCompletedTutorial($tutorial_name) {
        return in_array($tutorial_name, $this->completed_tutorials);
    }
}
