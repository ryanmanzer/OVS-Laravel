<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CertificationType extends Model
{
    // Certification Type maps to database table of approved certification types issued by various organizations and recognized by FishWise
}
