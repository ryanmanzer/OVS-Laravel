<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Source extends Model
{
    //public $with = ['harvestMethod','seafoodSpecies'];

    protected $fillable = ['fk_vendor_id', 'fk_seafoodSpecies_id','fk_harvestMethod_id','supplierCompany',
                           'contactName','contactEmail','contactPhone','fk_coolCountry_id',
                           'fk_harvestCountry_id','harvestRegion','faoMajorFishingArea',
                           'fk_flagOfVesselCountry_id','certification_types_id','certificationNumber',
                           'vendorComments','showSource','createdBy','scientificName','wild_farmed','method_category_id','fk_seafoodCategory_id','highSeasName', 'fip_name'];
    public function vendor()
    {
        return $this->belongsTo('App\Vendor','fk_vendor_id');
    }

    public function seafoodSpecies()
    {
        return $this->belongsTo('App\SeafoodSpecies','fk_seafoodSpecies_id','id');
    }

    public function sps()
    {
        return $this->hasMany('App\SurveyProductSource','fk_source_id');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product','surveyProductSources','fk_product_id','fk_source_id');
    }

    public function methodCategory()
    {
        return $this->belongsTo('App\methodCategories','method_category_id');
    }

    public function harvestMethod()
    {
        return $this->belongsTo('App\harvestMethods','fk_harvestMethod_id');
    }
    public function COOL()
    {
        return $this->belongsTo('App\Country','fk_coolCountry_id');
    }

    public function seafoodCategory()
    {
        return $this->belongsTo('App\SeafoodCategories','fk_seafoodCategory_id');
    }
    public function catchCountry()
    {
        return $this->belongsTo('App\Country','fk_harvestCountry_id');
    }
    public function vesselFlag()
    {
        return $this->belongsTo('App\Country','fk_flagOfVesselCountry_id');
    }
    public function certification()
    {
      return $this->belongsTo('App\CertificationType','certification_types_id');
    }
}
