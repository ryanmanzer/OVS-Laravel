<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SeafoodSpecies extends Model
{
    /**
     * Table to which this model relates
     *
     * @var string
     */
    protected $table="seafoodSpecies";
    
    public function category()
    {
        return $this->belongsTo('App\SeafoodCategories','fk_seafoodCategory_id');
    }
}
