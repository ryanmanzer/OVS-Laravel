<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certification extends Model
{
    //
    protected $fillable=['certification_type_id','certification_number','description','repacker'];
    
    public function certType()
    {
      return $this->belongsTo("App\CertificationType",'certification_type_id');
    }
}
