<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraceHrSurveyQuestion extends Model
{
    //
    protected $table="trace_hr_survey_questions";

    public function answer()
    {
      if($this->question->single_answer)
      {
        return $this->hasOne('App\TraceHrAnswer','survey_questions_id');
      }
      else
      {
        return $this->hasMany('App\TraceHrAnswer','survey_questions_id');
      }

    }
    public function question()
    {
      return $this->hasOne('App\TraceHrQuestion','id','trace_hr_question_id');
    }
    public function survey()
    {
      return $this->belongsTo('App\TraceHrSurvey','trace_hr_survey_id');
    }
}
