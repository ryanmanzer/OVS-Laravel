<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TraceHrAnswer extends Model
{
    public function survey_question()
    {
    	return $this->belongsTo('App\TraceHrSurveyQuestion','survey_questions_id');
    }
}
