<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDownloadFlagSources extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn('sources','ovs_downloadFlag')) {
          Schema::table('sources',function($table){
            $table->dropColumn('ovs_downloadFlag');
          });
        }
        Schema::table('sources',function($table){
          $table->integer('ovs_downloadFlag')->default(0);
        });
        if (Schema::hasColumn('surveyProducts','ovs_downloadFlag')) {
          Schema::table('surveyProducts',function($table){
            $table->dropColumn('ovs_downloadFlag');
          });
        }
        Schema::table('surveyProducts',function($table){
          $table->integer('ovs_downloadFlag')->default(0);
        });
        if (Schema::hasColumn('surveyProductSources','ovs_downloadFlag')) {
          Schema::table('surveyProductSources',function($table){
            $table->dropColumn('ovs_downloadFlag');
          });
        }
        Schema::table('surveyProductSources',function($table){
          $table->integer('ovs_downloadFlag')->default(0);
        });
        if (Schema::hasColumn('seafoodSpecies','ovs_downloadFlag')) {
          Schema::table('seafoodSpecies',function($table){
            $table->dropColumn('ovs_downloadFlag');
          });
        }
        Schema::table('seafoodSpecies',function($table){
          $table->integer('ovs_downloadFlag')->default(0);
        });
        if(!Schema::hasColumn('seafoodCategories','ovs_downloadFlag')){
          Schema::table('seafoodCategories',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
        if(!Schema::hasColumn('retailers','ovs_downloadFlag')){
          Schema::table('retailers',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
        if(!Schema::hasColumn('vendors','ovs_downloadFlag')){
          Schema::table('vendors',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
        if(!Schema::hasColumn('users','ovs_downloadFlag')){
          Schema::table('users',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
        if(!Schema::hasColumn('products','ovs_downloadFlag')){
          Schema::table('products',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
        if(!Schema::hasColumn('methodCategories','ovs_downloadFlag')){
          Schema::table('methodCategories',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
        if(!Schema::hasColumn('harvestMethods','ovs_downloadFlag')){
          Schema::table('harvestMethods',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
        if(!Schema::hasColumn('countries','ovs_downloadFlag')){
          Schema::table('countries',function($table){
            $table->integer('ovs_downloadFlag')->default(0);
          });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
