<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpsTabe extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('survey_product_sources', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('fk_product_id')->index();
            $table->integer('fk_source_id')->index();
            $table->integer('fk_survey_id')->index();
            $table->float('percent');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('survey_product_sources');
    }
}
