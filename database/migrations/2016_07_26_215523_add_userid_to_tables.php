<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUseridToTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('retailers', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('fish', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('fish_categories', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('gear_types', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('products', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('sources', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('survey_products', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('survey_product_sources', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('surveys', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('users', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
        Schema::table('vendors', function($table){
            $table->integer('created_by');
            $table->integer('modified_by');
           
                      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
