<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateVendorTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('vendors', function($table){
            $table->renameColumn('address_street_1','add_street_1');
            $table->renameColumn('address_street_2','add_street_2');
            $table->renameColumn('address_city','add_city');
            $table->renameColumn('address_state_provence','address_state_prov');
            $table->renameColumn('address_country','add_country');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
