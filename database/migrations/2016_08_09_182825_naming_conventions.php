<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class NamingConventions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::rename('fish','seafoodSpecies');
    /*    Schema::table('seafoodSpecies',function($table){
           $table->renameColumn('created_by','createdBy');
           $table->renameColumn('modified_by','modifiedBy');
           $table->renameColumn('fish_category_id','fk_seafoodCategory_id');
           $table->renameColumn('sci_name','scientificName')->nullable();
           $table->renameColumn('fish_name','seafoodSpecies');
           $table->string('seafoodSpeciesForDisplay')->nullable();
           $table->smallInteger('excludeFromDropdown')->default(0);
        }); */
        //Schema::rename('fish_categories','seafoodCategories');
       /* Schema::table('seafoodCategories',function($table){
            $table->renameColumn('created_by','createdBy');
            $table->renameColumn('modified_by','modifiedBy');
            $table->string('seafoodCategoryForDisplay')->nullable();
            $table->smallInteger('excludeFromDropdown')->default(0);
        });
        Schema::rename('gear_types','harvestMethods');
        Schema::table('harvestMethods',function($table){
           $table->renameColumn('gear_type','harvestMethod');
           $table->dropColumn('gear_category');
           $table->integer('fk_methodCategory_id')->nullable();
           $table->string('harvestMethodForDisplay')->nullable();
           $table->smallInteger('excludeFromDropdown')->default(0);
        });
        Schema::create('methodCategories', function (Blueprint $table) {
           $table->string('methodCategory');
           $table->string('methodCategoryForDisplay')->nullable();
           $table->smallInteger('excludeFromDropdown')->default(0);
        });
        Schema::create('countries',function(Blueprint $table){
           $table->string('ISO2')->nullable();
           $table->string('ISO3')->nullable();
           $table->string('countryName');
           $table->smallInteger('excludeFromDropdown');
        }); 
        Schema::table('vendors',function($table){
           $table->text('fishwiseComments')->nullable(); 
        });
        Schema::table('retailers',function($table){
           $table->text('fishwiseComments')->nullable(); 
        }); 
        Schema::table('surveys',function($table){
           $table->renameColumn('survey_date','dateIssued');
           $table->date('dateDue')->nullable();
           $table->renameColumn('survey_status','surveyStatus');
           $table->text('retailerComments')->nullable();
           $table->text('fishwiseComments')->nullable();
        });
        Schema::table('products',function($table){
           $table->mediumInteger('fk_hvdb_id')->nullable();
           $table->string('retailerProductDescription')->nullable();
           $table->string('retailerProductCode')->nullable();
           $table->string('retailerProductBrand')->nullable();
           $table->string('fk_seafoodCategory_id')->nullable();
           $table->string('vendorProductCode')->nullable();
           $table->string('vendorCoCType')->nullable();
           $table->string('vendorCoCNumber')->nullable();
        }); 
        Schema::table('sources',function($table){
            $table->smallInteger('createdBy');
            $table->smallInteger('modifiedBy');
            $table->renameColumn('source_company','supplierCompany');
            $table->renameColumn('contact_name','contactName');
            $table->renameColumn('contact_email','contactEmail');
            $table->renameColumn('contact_phone','contactPhone');
            $table->renameColumn('fk_gear_id','fk_harvestMethod_id');
            $table->dropColumn('COOL');
            $table->mediumInteger('fk_coolCountry_id');
            $table->mediumInteger('fk_harvestCountry_id');
            $table->string('harvestRegion');
            $table->string('faoMajorFishingArea');
            $table->mediumInteger('fk_flagOfVesselCountry_id');
            $table->string('certificationType');
            $table->string('certificationNumber');
            $table->text('vendorComments');
            $table->renameColumn('show_source','showSource');
        }); */
        Schema::rename('survey_products','surveyProducts');
        Schema::table('surveyProducts',function($table){
           $table->mediumInteger('fk_retailer_id');
           $table->mediumInteger('fk_vendor_id');
           $table->smallInteger('markedComplete');
        });
        Schema::rename('survey_product_sources','surveyProductSources');
        Schema::table('surveyProductSources',function($table){
           $table->mediumInteger('fk_retailer_id');
           $table->mediumInteger('fk_vendor_id');
           $table->renameColumn('percent','proportionOfProduct');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
    }
}
