<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShow extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sources',function($table){
            $table->renameColumn('showSource','show');    
        });
        Schema::table('vendors',function($table){
            $table->integer('show')->default(0);
        });
        Schema::table('retailers',function($table){
           $table->integer('show')->default(0); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
