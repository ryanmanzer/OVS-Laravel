<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSourceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sources',function($table){
            
            $table->renameColumn('fk_fish_id','fk_seafoodSpecies_id');
            $table->renameColumn('fk_gear_id','fk_harvestMethod_id');

            $table->renameColumn('show_source','showSource');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
