<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeOrientations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema:create('orientations', function(Blueprint $table){
          $table->increments('id');
          $table->timestamps();
          //TODO: Create necessary fields to house orientations photo paths and captions
          $table->string('img_path');
          $table->string('caption');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
          Schema::drop('orientations');
    }
}
