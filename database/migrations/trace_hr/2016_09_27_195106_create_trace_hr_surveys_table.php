<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTraceHrSurveysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trace_hr_surveys', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer("created_by")->default(0);
            $table->integer("modified_by")->default(0);
            $table->date("survey_date")->nullable();
            $table->string('survey_type')->nullable();  
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('trace_hr_surveys');
    }
}
