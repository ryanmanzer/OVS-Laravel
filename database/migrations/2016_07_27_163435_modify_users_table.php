<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifyUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(Schema::hasTable('users')){
            Schema::table('users',function($table){
                $table->string('user_type');
                $table->integer('org_id');
                $table->tinyInteger('admin');
                $table->tinyInteger('approved_user');
            });
            
        }
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if(Schema::hasTable('users')){
            Schema::table('users',function($table){
                $table->dropColumn(['user_type','admin','org_id','approved_user' ]);
            });
        }
    }
}
