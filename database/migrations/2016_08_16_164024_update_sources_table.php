<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSourcesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('sources',function($table){
           // $table->integer('fk_seafoodCategory_id');
            $table->string('scientificName');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('sources',function($table){
            $table->dropColumn('fk_seafoodCategory_id');
        });
        
    }
}
