<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGearTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gear_types', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string("gear_type");
            $table->string("gear_category");
            $table->string("wild_farmed");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gear_types');
    }
}
