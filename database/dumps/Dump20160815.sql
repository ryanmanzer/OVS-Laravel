-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: homestead
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2
CREATE SCHEMA IF NOT EXISTS homestead;
USE homestead;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `ISO2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISO3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10218 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES ('AF','AFG','Afghanistan',0,10003,NULL,NULL),('AL','ALB','Albania',0,10004,NULL,NULL),('DZ','DZA','Algeria',0,10005,NULL,NULL),('AD','AND','Andorra',0,10006,NULL,NULL),('AO','AGO','Angola',0,10007,NULL,NULL),('AG','ATG','Antigua and Barbuda',0,10008,NULL,NULL),('AR','ARG','Argentina',0,10009,NULL,NULL),('AM','ARM','Armenia',0,10010,NULL,NULL),('AU','AUS','Australia',0,10011,NULL,NULL),('AT','AUT','Austria',0,10012,NULL,NULL),('AZ','AZE','Azerbaijan',0,10013,NULL,NULL),('BS','BHS','Bahamas, The',0,10014,NULL,NULL),('BH','BHR','Bahrain',0,10015,NULL,NULL),('BD','BGD','Bangladesh',0,10016,NULL,NULL),('BB','BRB','Barbados',0,10017,NULL,NULL),('BY','BLR','Belarus',0,10018,NULL,NULL),('BE','BEL','Belgium',0,10019,NULL,NULL),('BZ','BLZ','Belize',0,10020,NULL,NULL),('BJ','BEN','Benin',0,10021,NULL,NULL),('BT','BTN','Bhutan',0,10022,NULL,NULL),('BO','BOL','Bolivia',0,10023,NULL,NULL),('BA','BIH','Bosnia and Herzegovina',0,10024,NULL,NULL),('BW','BWA','Botswana',0,10025,NULL,NULL),('BR','BRA','Brazil',0,10026,NULL,NULL),('BN','BRN','Brunei',0,10027,NULL,NULL),('BG','BGR','Bulgaria',0,10028,NULL,NULL),('BF','BFA','Burkina Faso',0,10029,NULL,NULL),('BI','BDI','Burundi',0,10030,NULL,NULL),('KH','KHM','Cambodia',0,10031,NULL,NULL),('CM','CMR','Cameroon',0,10032,NULL,NULL),('CA','CAN','Canada',0,10033,NULL,NULL),('CV','CPV','Cape Verde',0,10034,NULL,NULL),('CF','CAF','Central African Republic',0,10035,NULL,NULL),('TD','TCD','Chad',0,10036,NULL,NULL),('CL','CHL','Chile',0,10037,NULL,NULL),('CN','CHN','China, Peoples Republic of',0,10038,NULL,NULL),('CO','COL','Colombia',0,10039,NULL,NULL),('KM','COM','Comoros',0,10040,NULL,NULL),('CD','COD','Congo, (Congo Â Kinshasa)',0,10041,NULL,NULL),('CG','COG','Congo, (Congo Â Brazzaville)',0,10042,NULL,NULL),('CR','CRI','Costa Rica',0,10043,NULL,NULL),('CI','CIV','Cote dIvoire (Ivory Coast)',0,10044,NULL,NULL),('HR','HRV','Croatia',0,10045,NULL,NULL),('CU','CUB','Cuba',0,10046,NULL,NULL),('CY','CYP','Cyprus',0,10047,NULL,NULL),('CZ','CZE','Czech Republic',0,10048,NULL,NULL),('DK','DNK','Denmark',0,10049,NULL,NULL),('DJ','DJI','Djibouti',0,10050,NULL,NULL),('DM','DMA','Dominica',0,10051,NULL,NULL),('DO','DOM','Dominican Republic',0,10052,NULL,NULL),('EC','ECU','Ecuador',0,10053,NULL,NULL),('EG','EGY','Egypt',0,10054,NULL,NULL),('SV','SLV','El Salvador',0,10055,NULL,NULL),('GQ','GNQ','Equatorial Guinea',0,10056,NULL,NULL),('ER','ERI','Eritrea',0,10057,NULL,NULL),('EE','EST','Estonia',0,10058,NULL,NULL),('ET','ETH','Ethiopia',0,10059,NULL,NULL),('FJ','FJI','Fiji',0,10060,NULL,NULL),('FI','FIN','Finland',0,10061,NULL,NULL),('FR','FRA','France',0,10062,NULL,NULL),('GA','GAB','Gabon',0,10063,NULL,NULL),('GM','GMB','Gambia, The',0,10064,NULL,NULL),('GE','GEO','Georgia',0,10065,NULL,NULL),('DE','DEU','Germany',0,10066,NULL,NULL),('GH','GHA','Ghana',0,10067,NULL,NULL),('GR','GRC','Greece',0,10068,NULL,NULL),('GD','GRD','Grenada',0,10069,NULL,NULL),('GT','GTM','Guatemala',0,10070,NULL,NULL),('GN','GIN','Guinea',0,10071,NULL,NULL),('GW','GNB','Guinea-Bissau',0,10072,NULL,NULL),('GY','GUY','Guyana',0,10073,NULL,NULL),('HT','HTI','Haiti',0,10074,NULL,NULL),('HN','HND','Honduras',0,10075,NULL,NULL),('HU','HUN','Hungary',0,10076,NULL,NULL),('IS','ISL','Iceland',0,10077,NULL,NULL),('IN','IND','India',0,10078,NULL,NULL),('ID','IDN','Indonesia',0,10079,NULL,NULL),('IR','IRN','Iran',0,10080,NULL,NULL),('IQ','IRQ','Iraq',0,10081,NULL,NULL),('IE','IRL','Ireland',0,10082,NULL,NULL),('IL','ISR','Israel',0,10083,NULL,NULL),('IT','ITA','Italy',0,10084,NULL,NULL),('JM','JAM','Jamaica',0,10085,NULL,NULL),('JP','JPN','Japan',0,10086,NULL,NULL),('JO','JOR','Jordan',0,10087,NULL,NULL),('KZ','KAZ','Kazakhstan',0,10088,NULL,NULL),('KE','KEN','Kenya',0,10089,NULL,NULL),('KI','KIR','Kiribati',0,10090,NULL,NULL),('KP','PRK','Korea, North',0,10091,NULL,NULL),('KR','KOR','Korea, South',0,10092,NULL,NULL),('KW','KWT','Kuwait',0,10093,NULL,NULL),('KG','KGZ','Kyrgyzstan',0,10094,NULL,NULL),('LA','LAO','Laos',0,10095,NULL,NULL),('LV','LVA','Latvia',0,10096,NULL,NULL),('LB','LBN','Lebanon',0,10097,NULL,NULL),('LS','LSO','Lesotho',0,10098,NULL,NULL),('LR','LBR','Liberia',0,10099,NULL,NULL),('LY','LBY','Libya',0,10100,NULL,NULL),('LI','LIE','Liechtenstein',0,10101,NULL,NULL),('LT','LTU','Lithuania',0,10102,NULL,NULL),('LU','LUX','Luxembourg',0,10103,NULL,NULL),('MK','MKD','Macedonia',0,10104,NULL,NULL),('MG','MDG','Madagascar',0,10105,NULL,NULL),('MW','MWI','Malawi',0,10106,NULL,NULL),('MY','MYS','Malaysia',0,10107,NULL,NULL),('MV','MDV','Maldives',0,10108,NULL,NULL),('ML','MLI','Mali',0,10109,NULL,NULL),('MT','MLT','Malta',0,10110,NULL,NULL),('MH','MHL','Marshall Islands',0,10111,NULL,NULL),('MR','MRT','Mauritania',0,10112,NULL,NULL),('MU','MUS','Mauritius',0,10113,NULL,NULL),('MX','MEX','Mexico',0,10114,NULL,NULL),('FM','FSM','Micronesia',0,10115,NULL,NULL),('MD','MDA','Moldova',0,10116,NULL,NULL),('MC','MCO','Monaco',0,10117,NULL,NULL),('MN','MNG','Mongolia',0,10118,NULL,NULL),('ME','MNE','Montenegro',0,10119,NULL,NULL),('MA','MAR','Morocco',0,10120,NULL,NULL),('MZ','MOZ','Mozambique',0,10121,NULL,NULL),('MM','MMR','Myanmar (Burma)',0,10122,NULL,NULL),('NA','NAM','Namibia',0,10123,NULL,NULL),('NR','NRU','Nauru',0,10124,NULL,NULL),('NP','NPL','Nepal',0,10125,NULL,NULL),('NL','NLD','Netherlands',0,10126,NULL,NULL),('NZ','NZL','New Zealand',0,10127,NULL,NULL),('NI','NIC','Nicaragua',0,10128,NULL,NULL),('NE','NER','Niger',0,10129,NULL,NULL),('NG','NGA','Nigeria',0,10130,NULL,NULL),('NO','NOR','Norway',0,10131,NULL,NULL),('OM','OMN','Oman',0,10132,NULL,NULL),('PK','PAK','Pakistan',0,10133,NULL,NULL),('PW','PLW','Palau',0,10134,NULL,NULL),('PA','PAN','Panama',0,10135,NULL,NULL),('PG','PNG','Papua New Guinea',0,10136,NULL,NULL),('PY','PRY','Paraguay',0,10137,NULL,NULL),('PE','PER','Peru',0,10138,NULL,NULL),('PH','PHL','Philippines',0,10139,NULL,NULL),('PL','POL','Poland',0,10140,NULL,NULL),('PT','PRT','Portugal',0,10141,NULL,NULL),('QA','QAT','Qatar',0,10142,NULL,NULL),('RO','ROU','Romania',0,10143,NULL,NULL),('RU','RUS','Russia',0,10144,NULL,NULL),('RW','RWA','Rwanda',0,10145,NULL,NULL),('KN','KNA','Saint Kitts and Nevis',0,10146,NULL,NULL),('LC','LCA','Saint Lucia',0,10147,NULL,NULL),('VC','VCT','Saint Vincent and the Grenadines',0,10148,NULL,NULL),('WS','WSM','Samoa',0,10149,NULL,NULL),('SM','SMR','San Marino',0,10150,NULL,NULL),('ST','STP','Sao Tome and Principe',0,10151,NULL,NULL),('SA','SAU','Saudi Arabia',0,10152,NULL,NULL),('SN','SEN','Senegal',0,10153,NULL,NULL),('RS','SRB','Serbia',0,10154,NULL,NULL),('SC','SYC','Seychelles',0,10155,NULL,NULL),('SL','SLE','Sierra Leone',0,10156,NULL,NULL),('SG','SGP','Singapore',0,10157,NULL,NULL),('SK','SVK','Slovakia',0,10158,NULL,NULL),('SI','SVN','Slovenia',0,10159,NULL,NULL),('SB','SLB','Solomon Islands',0,10160,NULL,NULL),('SO','SOM','Somalia',0,10161,NULL,NULL),('ZA','ZAF','South Africa',0,10162,NULL,NULL),('ES','ESP','Spain',0,10163,NULL,NULL),('LK','LKA','Sri Lanka',0,10164,NULL,NULL),('SD','SDN','Sudan',0,10165,NULL,NULL),('SR','SUR','Suriname',0,10166,NULL,NULL),('SZ','SWZ','Swaziland',0,10167,NULL,NULL),('SE','SWE','Sweden',0,10168,NULL,NULL),('CH','CHE','Switzerland',0,10169,NULL,NULL),('SY','SYR','Syria',0,10170,NULL,NULL),('TJ','TJK','Tajikistan',0,10171,NULL,NULL),('TZ','TZA','Tanzania',0,10172,NULL,NULL),('TH','THA','Thailand',0,10173,NULL,NULL),('TL','TLS','Timor-Leste (East Timor)',0,10174,NULL,NULL),('TG','TGO','Togo',0,10175,NULL,NULL),('TO','TON','Tonga',0,10176,NULL,NULL),('TT','TTO','Trinidad and Tobago',0,10177,NULL,NULL),('TN','TUN','Tunisia',0,10178,NULL,NULL),('TR','TUR','Turkey',0,10179,NULL,NULL),('TM','TKM','Turkmenistan',0,10180,NULL,NULL),('TV','TUV','Tuvalu',0,10181,NULL,NULL),('UG','UGA','Uganda',0,10182,NULL,NULL),('UA','UKR','Ukraine',0,10183,NULL,NULL),('AE','ARE','United Arab Emirates',0,10184,NULL,NULL),('GB','GBR','United Kingdom',0,10185,NULL,NULL),('US','USA','United States',0,10186,NULL,NULL),('UY','URY','Uruguay',0,10187,NULL,NULL),('UZ','UZB','Uzbekistan',0,10188,NULL,NULL),('VU','VUT','Vanuatu',0,10189,NULL,NULL),('VA','VAT','Vatican City',0,10190,NULL,NULL),('VE','VEN','Venezuela',0,10191,NULL,NULL),('VN','VNM','Vietnam',0,10192,NULL,NULL),('YE','YEM','Yemen',0,10193,NULL,NULL),('ZM','ZMB','Zambia',0,10194,NULL,NULL),('ZW','ZWE','Zimbabwe',0,10195,NULL,NULL),('GE','GEO','Abkhazia',0,10196,NULL,NULL),('TW','TWN','China, Republic of (Taiwan)',0,10197,NULL,NULL),('AZ','AZE','Nagorno-Karabakh',0,10198,NULL,NULL),('CY','CYP','Northern Cyprus',0,10199,NULL,NULL),('MD','MDA','Pridnestrovie (Transnistria)',0,10200,NULL,NULL),('SO','SOM','Somaliland',0,10201,NULL,NULL),('GE','GEO','South Ossetia',0,10202,NULL,NULL),('AU','AUS','Ashmore and Cartier Islands',0,10203,NULL,NULL),('CX','CXR','Christmas Island',0,10204,NULL,NULL),('CC','CCK','Cocos (Keeling) Islands',0,10205,NULL,NULL),('AU','AUS','Coral Sea Islands',0,10206,NULL,NULL),('HM','HMD','Heard Island and McDonald Islands',0,10207,NULL,NULL),('NF','NFK','Norfolk Island',0,10208,NULL,NULL),('NC','NCL','New Caledonia',0,10209,NULL,NULL),('PF','PYF','French Polynesia',0,10210,NULL,NULL),('YT','MYT','Mayotte',0,10211,NULL,NULL),('GP','GLP','Saint Barthelemy',0,10212,NULL,NULL),('GP','GLP','Saint Martin',0,10213,NULL,NULL),('PM','SPM','Saint Pierre and Miquelon',0,10214,NULL,NULL),('WF','WLF','Wallis and Futuna',0,10215,NULL,NULL),('TF','ATF','French Southern and Antarctic Lands',0,10216,NULL,NULL),('PF','PYF','Clipperton Island',0,10217,NULL,NULL);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harvestMethods`
--

DROP TABLE IF EXISTS `harvestMethods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harvestMethods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `harvestMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wild_farmed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_methodCategory_id` int(11) DEFAULT NULL,
  `harvestMethodForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harvestMethods`
--

LOCK TABLES `harvestMethods` WRITE;
/*!40000 ALTER TABLE `harvestMethods` DISABLE KEYS */;
INSERT INTO `harvestMethods` VALUES (108,NULL,NULL,'Pond, closed','Farmed',1,1,1,NULL,0),(109,NULL,NULL,'Pond, diversion','Farmed',1,1,1,NULL,0),(110,NULL,NULL,'Pond, frequent exchange','Farmed',1,1,1,NULL,0),(111,NULL,NULL,'Pond, infrequent exchange','Farmed',1,1,1,NULL,0),(113,NULL,NULL,'Pond, barrage','Farmed',1,1,1,NULL,0),(114,NULL,NULL,'Basin catch','Farmed',1,1,1,NULL,0),(115,NULL,NULL,'Bottom culture','Farmed',1,1,2,NULL,0),(116,NULL,NULL,'Off-bottom, culture','Farmed',1,1,2,NULL,0),(117,NULL,NULL,'Mixed, culture','Farmed',1,1,2,NULL,0),(118,NULL,NULL,'Bag culture','Farmed',1,1,2,NULL,0),(120,NULL,NULL,'Longline, culture','Farmed',1,1,2,NULL,0),(121,NULL,NULL,'Raft, culture','Farmed',1,1,2,NULL,0),(122,NULL,NULL,'Stakes','Farmed',1,1,2,NULL,0),(123,NULL,NULL,'Collector','Farmed',1,1,3,NULL,0),(124,NULL,NULL,'Collector-Seed','Farmed',1,1,3,NULL,0),(125,NULL,NULL,'Dam','Farmed',1,1,3,NULL,0),(126,NULL,NULL,'Farming - Integrated','Farmed',1,1,3,NULL,0),(127,NULL,NULL,'Hatchery','Farmed',1,1,3,NULL,0),(128,NULL,NULL,'Lagoon','Farmed',1,1,3,NULL,0),(129,NULL,NULL,'Lake','Farmed',1,1,3,NULL,0),(130,NULL,NULL,'Paddy Field','Farmed',1,1,3,NULL,0),(131,NULL,NULL,'Ranching','Farmed',1,1,3,NULL,0),(132,NULL,NULL,'Silo','Farmed',1,1,3,NULL,0),(133,NULL,NULL,'Tank, closed','Farmed',1,1,4,NULL,0),(134,NULL,NULL,'Cage, fixed','Farmed',1,1,5,NULL,0),(135,NULL,NULL,'Cage, floating','Farmed',1,1,5,NULL,0),(136,NULL,NULL,'Pen (general)','Farmed',1,1,6,NULL,0),(137,NULL,NULL,'Raceway (general)','Farmed',1,1,7,NULL,0),(138,NULL,NULL,'Towed dredges','Wild',1,1,8,NULL,0),(139,NULL,NULL,'Hand dredges','Wild',1,1,8,NULL,0),(140,NULL,NULL,'Mechanized dredges','Wild',1,1,8,NULL,0),(141,NULL,NULL,'Dredges (unspecified)','Wild',1,1,8,NULL,0),(142,NULL,NULL,'Cast nets','Wild',1,1,9,NULL,0),(143,NULL,NULL,'Cover pots/Lantern nets','Wild',1,1,9,NULL,0),(144,NULL,NULL,'Falling gear (unspecified)','Wild',1,1,9,NULL,0),(145,NULL,NULL,'Set gillnets','Wild',1,1,10,NULL,0),(146,NULL,NULL,'Drift gillnets (driftnets)','Wild',1,1,10,NULL,0),(147,NULL,NULL,'Encircling gillnets','Wild',1,1,10,NULL,0),(148,NULL,NULL,'Fixed gillnets (on stakes)','Wild',1,1,10,NULL,0),(149,NULL,NULL,'Trammel nets','Wild',1,1,10,NULL,0),(150,NULL,NULL,'Combined gillnets - trammel nets','Wild',1,1,10,NULL,0),(151,NULL,NULL,'Gillnets and entangling nets (unspecified)','Wild',1,1,10,NULL,0),(152,NULL,NULL,'Handlines and hand-operated pole-and lines','Wild',1,1,11,NULL,0),(153,NULL,NULL,'Mechanized lines and pole-and-lines','Wild',1,1,11,NULL,0),(154,NULL,NULL,'Set longlines','Wild',1,1,11,NULL,0),(155,NULL,NULL,'Drifting longlines','Wild',1,1,11,NULL,0),(156,NULL,NULL,'Trolling lines','Wild',1,1,11,NULL,0),(157,NULL,NULL,'Bottom longlines','Wild',1,1,11,NULL,0),(158,NULL,NULL,'Pelagic longline','Wild',1,1,11,NULL,0),(159,NULL,NULL,'Buoy gear','Wild',1,1,11,NULL,0),(161,NULL,NULL,'Longlines (unspecified)','Wild',1,1,11,NULL,0),(162,NULL,NULL,'Hooks and lines (unspecified)','Wild',1,1,11,NULL,0),(163,NULL,NULL,'Greenstick','Wild',1,1,11,NULL,0),(164,NULL,NULL,'Vertical lines','Wild',1,1,11,NULL,0),(165,NULL,NULL,'Portable lift nets','Wild',1,1,12,NULL,0),(166,NULL,NULL,'Boat-operated lift nets','Wild',1,1,12,NULL,0),(167,NULL,NULL,'Shore-operated stationary lift nets','Wild',1,1,12,NULL,0),(168,NULL,NULL,'Lift nets (unspecified)','Wild',1,1,12,NULL,0),(169,NULL,NULL,'Harpoons','Wild',1,1,11,NULL,0),(170,NULL,NULL,'Hand implements','Wild',1,1,13,NULL,0),(171,NULL,NULL,'Diving','Wild',1,1,13,NULL,0),(173,NULL,NULL,'Pumps','Wild',1,1,13,NULL,0),(174,NULL,NULL,'Electric Fishing','Wild',1,1,13,NULL,0),(175,NULL,NULL,'Pushnets','Wild',1,1,13,NULL,0),(176,NULL,NULL,'Scoopnets','Wild',1,1,13,NULL,0),(177,NULL,NULL,'Drive-in nets','Wild',1,1,13,NULL,0),(178,NULL,NULL,'Gear not listed','Wild',1,1,13,NULL,0),(179,NULL,NULL,'Pushed butterfly net','Wild',1,1,13,NULL,0),(180,NULL,NULL,'Pushed skimmer net','Wild',1,1,13,NULL,0),(181,NULL,NULL,'Beach seines','Wild',1,1,14,NULL,0),(182,NULL,NULL,'Boat seines','Wild',1,1,14,NULL,0),(183,NULL,NULL,'Seine nets (unspecified)','Wild',1,1,14,NULL,0),(184,NULL,NULL,'Purse seines','Wild',1,1,15,NULL,0),(185,NULL,NULL,'Dolphin set purse seine','Wild',1,1,15,NULL,0),(186,NULL,NULL,'Floating Object purse seine (FAD)','Wild',1,1,15,NULL,0),(187,NULL,NULL,'Unassociated purse seine (non-FAD)','Wild',1,1,15,NULL,0),(189,NULL,NULL,'Surrounding nets without purse lines','Wild',1,1,15,NULL,0),(190,NULL,NULL,'Lampara','Wild',1,1,15,NULL,0),(191,NULL,NULL,'Surrounding nets (unspecified)','Wild',1,1,15,NULL,0),(192,NULL,NULL,'Stationary uncovered pound nets','Wild',1,1,16,NULL,0),(193,NULL,NULL,'Pots','Wild',1,1,16,NULL,0),(194,NULL,NULL,'Fyke nets','Wild',1,1,16,NULL,0),(195,NULL,NULL,'Stow nets','Wild',1,1,16,NULL,0),(196,NULL,NULL,'Barriers, fences, weiers, corrals, etc.','Wild',1,1,16,NULL,0),(197,NULL,NULL,'Aerial traps','Wild',1,1,16,NULL,0),(198,NULL,NULL,'Crab Rings','Wild',1,1,16,NULL,0),(199,NULL,NULL,'Traps (unspecified)','Wild',1,1,16,NULL,0),(200,NULL,NULL,'Bottom trawls','Wild',1,1,17,NULL,0),(201,NULL,NULL,'Midwater trawls','Wild',1,1,17,NULL,0),(202,NULL,NULL,'Semipelagic trawls','Wild',1,1,17,NULL,0),(203,NULL,NULL,'Otter trawls','Wild',1,1,17,NULL,0),(204,NULL,NULL,'Trawls (unspecified)','Wild',1,1,17,NULL,0),(206,NULL,NULL,'Beam trawls','Wild',1,1,17,NULL,0),(207,NULL,NULL,'Single boat bottom otter trawls','Wild',1,1,17,NULL,0),(208,NULL,NULL,'Twin bottom otter trawls','Wild',1,1,17,NULL,0),(209,NULL,NULL,'Multiple bottom otter trawls','Wild',1,1,17,NULL,0),(210,NULL,NULL,'Bottom pair trawls','Wild',1,1,17,NULL,0),(211,NULL,NULL,'Single boat midwater otter trawls','Wild',1,1,17,NULL,0),(212,NULL,NULL,'Midwater pair trawls','Wild',1,1,17,NULL,0),(213,NULL,NULL,'Magdalena - Artisanal bottom trawl','Wild',1,1,17,NULL,0),(214,NULL,NULL,'Gear Type Unknown','Wild',1,1,18,NULL,0);
/*!40000 ALTER TABLE `harvestMethods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `methodCategories`
--

DROP TABLE IF EXISTS `methodCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `methodCategories` (
  `methodCategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `methodCategoryForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `wild_farmed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `methodcategories_wild_farmed_index` (`wild_farmed`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `methodCategories`
--

LOCK TABLES `methodCategories` WRITE;
/*!40000 ALTER TABLE `methodCategories` DISABLE KEYS */;
INSERT INTO `methodCategories` VALUES ('Pond',NULL,0,1,NULL,NULL,'Farmed'),('Shellfish',NULL,0,2,NULL,NULL,'Farmed'),('Other',NULL,0,3,NULL,NULL,'Farmed'),('Tank',NULL,0,4,NULL,NULL,'Farmed'),('Cage',NULL,0,5,NULL,NULL,'Farmed'),('Pen',NULL,0,6,NULL,NULL,'Farmed'),('Raceway',NULL,0,7,NULL,NULL,'Farmed'),('Dredges',NULL,0,8,NULL,NULL,'Wild'),('Falling Gear',NULL,0,9,NULL,NULL,'Wild'),('Gillnets and Entangling Nets',NULL,0,10,NULL,NULL,'Wild'),('Hooks and Lines',NULL,0,11,NULL,NULL,'Wild'),('Lift nets',NULL,0,12,NULL,NULL,'Wild'),('Miscellaneous gear',NULL,0,13,NULL,NULL,'Wild'),('Seine nets',NULL,0,14,NULL,NULL,'Wild'),('Surrounding nets',NULL,0,15,NULL,NULL,'Wild'),('Traps',NULL,0,16,NULL,NULL,'Wild'),('Trawls',NULL,0,17,NULL,NULL,'Wild'),('Gear Type Unknown',NULL,0,18,NULL,NULL,'Wild');
/*!40000 ALTER TABLE `methodCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_07_25_171245_create_survey_table',1),('2016_07_25_192936_make_vendor_table',1),('2016_07_25_214644_create_product_table',1),('2016_07_25_215102_create_sp_tabe',1),('2016_07_25_215333_create_sps_tabe',1),('2016_07_25_221030_create_source_table',1),('2016_07_25_224323_create_gear_types_table',1),('2016_07_25_224613_create_fish_table',1),('2016_07_25_224800_create_fish_categories_table',1),('2016_07_26_180742_create_retailers_table',1),('2016_07_26_180932_update_retailer_table',1),('2016_07_26_215523_add_userid_to_tables',1),('2016_07_27_163435_modify_users_table',1),('2016_07_27_175947_create_ngos_table',1),('2016_08_09_170213_modify_fish',2),('2016_08_09_182825_naming_conventions',3),('2016_08_10_001224_fix_ids',4),('2016_08_10_173200_update_method_categories',5);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ngos`
--

DROP TABLE IF EXISTS `ngos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ngos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_street_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_street_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_state_prov` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_post_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ngos`
--

LOCK TABLES `ngos` WRITE;
/*!40000 ALTER TABLE `ngos` DISABLE KEYS */;
INSERT INTO `ngos` VALUES (1,'2016-08-09 16:21:23','2016-08-09 16:21:54','FishWise','Ryan Manzer','r.manzer@fishwise.org','831-460-6123','500 Seabright Ave',NULL,NULL,'CA',NULL,'USA',1,1);
/*!40000 ALTER TABLE `ngos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `product_upc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_vendor_id` int(11) NOT NULL,
  `fk_retailer_id` int(11) NOT NULL,
  `product_brand` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `product_description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_hvdb_id` mediumint(9) DEFAULT NULL,
  `retailerProductDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailerProductCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailerProductBrand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_seafoodCategory_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendorProductCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendorCoCType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendorCoCNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retailers`
--

DROP TABLE IF EXISTS `retailers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retailers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fishwiseComments` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retailers`
--

LOCK TABLES `retailers` WRITE;
/*!40000 ALTER TABLE `retailers` DISABLE KEYS */;
/*!40000 ALTER TABLE `retailers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seafoodCategories`
--

DROP TABLE IF EXISTS `seafoodCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seafoodCategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `seafoodCategoryForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seafoodCategories`
--

LOCK TABLES `seafoodCategories` WRITE;
/*!40000 ALTER TABLE `seafoodCategories` DISABLE KEYS */;
INSERT INTO `seafoodCategories` VALUES (1,NULL,NULL,'Abalone',1,1,NULL,0),(2,NULL,NULL,'Alligator',1,1,NULL,0),(3,NULL,NULL,'Amberjack',1,1,NULL,0),(4,NULL,NULL,'Anchovy',1,1,NULL,0),(5,NULL,NULL,'Artic Char',1,1,NULL,0),(6,NULL,NULL,'Barramundi',1,1,NULL,0),(7,NULL,NULL,'Bass',1,1,NULL,0),(8,NULL,NULL,'Blackfish',1,1,NULL,0),(9,NULL,NULL,'Bluefish',1,1,NULL,0),(10,NULL,NULL,'Bonito',1,1,NULL,0),(11,NULL,NULL,'Bream',1,1,NULL,0),(12,NULL,NULL,'Buffalo Fish',1,1,NULL,0),(13,NULL,NULL,'Butter Fish',1,1,NULL,0),(14,NULL,NULL,'Carp',1,1,NULL,0),(15,NULL,NULL,'Catfish',1,1,NULL,0),(16,NULL,NULL,'Caviar',1,1,NULL,0),(17,NULL,NULL,'Clam',1,1,NULL,0),(18,NULL,NULL,'Cobia',1,1,NULL,0),(19,NULL,NULL,'Cod',1,1,NULL,0),(20,NULL,NULL,'Conch',1,1,NULL,0),(21,NULL,NULL,'Corvina',1,1,NULL,0),(22,NULL,NULL,'Crab',1,1,NULL,0),(23,NULL,NULL,'Crawfish',1,1,NULL,0),(24,NULL,NULL,'Croaker',1,1,NULL,0),(25,NULL,NULL,'Cuttlefish',1,1,NULL,0),(26,NULL,NULL,'Eel',1,1,NULL,0),(27,NULL,NULL,'Escolar',1,1,NULL,0),(28,NULL,NULL,'Flatfish',1,1,NULL,0),(29,NULL,NULL,'Flying Fish',1,1,NULL,0),(30,NULL,NULL,'Grouper',1,1,NULL,0),(31,NULL,NULL,'Haddock',1,1,NULL,0),(32,NULL,NULL,'Hake',1,1,NULL,0),(33,NULL,NULL,'Herring',1,1,NULL,0),(34,NULL,NULL,'Kampachi',1,1,NULL,0),(35,NULL,NULL,'Kingklip',1,1,NULL,0),(36,NULL,NULL,'Langostino',1,1,NULL,0),(37,NULL,NULL,'Lingcod',1,1,NULL,0),(38,NULL,NULL,'Lobster',1,1,NULL,0),(39,NULL,NULL,'Lutefisk',1,1,NULL,0),(40,NULL,NULL,'Mackerel',1,1,NULL,0),(41,NULL,NULL,'Mahi Mahi',1,1,NULL,0),(42,NULL,NULL,'Marlin',1,1,NULL,0),(43,NULL,NULL,'Mixed',1,1,NULL,0),(44,NULL,NULL,'Monkfish',1,1,NULL,0),(45,NULL,NULL,'Mullet',1,1,NULL,0),(46,NULL,NULL,'Mussel',1,1,NULL,0),(47,NULL,NULL,'Nile Perch',1,1,NULL,0),(48,NULL,NULL,'Not Seafood',1,1,NULL,0),(49,NULL,NULL,'Octopus',1,1,NULL,0),(50,NULL,NULL,'Opah',1,1,NULL,0),(51,NULL,NULL,'Orange Roughy',1,1,NULL,0),(52,NULL,NULL,'Oyster',1,1,NULL,0),(53,NULL,NULL,'Pangasius',1,1,NULL,0),(54,NULL,NULL,'Parrot Fish',1,1,NULL,0),(55,NULL,NULL,'Perch',1,1,NULL,0),(56,NULL,NULL,'Pike',1,1,NULL,0),(57,NULL,NULL,'Pollock',1,1,NULL,0),(58,NULL,NULL,'Pomfret',1,1,NULL,0),(59,NULL,NULL,'Pompano',1,1,NULL,0),(60,NULL,NULL,'Porgy',1,1,NULL,0),(61,NULL,NULL,'Rockfish',1,1,NULL,0),(62,NULL,NULL,'Rover, Golden',1,1,NULL,0),(63,NULL,NULL,'Salmon',1,1,NULL,0),(64,NULL,NULL,'Sardine',1,1,NULL,0),(65,NULL,NULL,'Scad, Bigeye',1,1,NULL,0),(66,NULL,NULL,'Scallop',1,1,NULL,0),(67,NULL,NULL,'Sculpin',1,1,NULL,0),(68,NULL,NULL,'Seabob, Atlantic',1,1,NULL,0),(69,NULL,NULL,'Seaweed',1,1,NULL,0),(70,NULL,NULL,'Shad',1,1,NULL,0),(71,NULL,NULL,'Shark',1,1,NULL,0),(72,NULL,NULL,'Sheephead',1,1,NULL,0),(73,NULL,NULL,'Sheepshead',1,1,NULL,0),(74,NULL,NULL,'Shrimp',1,1,NULL,0),(75,NULL,NULL,'Skate',1,1,NULL,0),(76,NULL,NULL,'Smelt',1,1,NULL,0),(77,NULL,NULL,'Snapper',1,1,NULL,0),(78,NULL,NULL,'Soldierfish',1,1,NULL,0),(79,NULL,NULL,'Spearfish',1,1,NULL,0),(80,NULL,NULL,'Squid',1,1,NULL,0),(81,NULL,NULL,'Squirrelfish',1,1,NULL,0),(82,NULL,NULL,'Sturgeon',1,1,NULL,0),(83,NULL,NULL,'Swordfish',1,1,NULL,0),(84,NULL,NULL,'Tautog',1,1,NULL,0),(85,NULL,NULL,'Tilapia',1,1,NULL,0),(86,NULL,NULL,'Tilefish',1,1,NULL,0),(87,NULL,NULL,'Toothfish',1,1,NULL,0),(88,NULL,NULL,'Trout',1,1,NULL,0),(89,NULL,NULL,'Tuna',1,1,NULL,0),(90,NULL,NULL,'Wahoo',1,1,NULL,0),(91,NULL,NULL,'Walleye',1,1,NULL,0),(92,NULL,NULL,'Whelk',1,1,NULL,0),(93,NULL,NULL,'Whitefish',1,1,NULL,0),(94,NULL,NULL,'Whiting',1,1,NULL,0),(95,NULL,NULL,'Wrasse',1,1,NULL,0);
/*!40000 ALTER TABLE `seafoodCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seafoodSpecies`
--

DROP TABLE IF EXISTS `seafoodSpecies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seafoodSpecies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seafoodSpecies` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scientificName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_seafoodCategory_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `seafoodSpeciesForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seafoodSpecies`
--

LOCK TABLES `seafoodSpecies` WRITE;
/*!40000 ALTER TABLE `seafoodSpecies` DISABLE KEYS */;
INSERT INTO `seafoodSpecies` VALUES (7,NULL,NULL,'Abalone','Haliotis spp.','1',1,1,NULL,0),(8,NULL,NULL,'Alligator','Alligator mississippiensis','2',1,1,NULL,0),(9,NULL,NULL,'Amberjack','Seriola dumerili','3',1,1,NULL,0),(10,NULL,NULL,'Anchovy','Engraulis spp.','4',1,1,NULL,0),(11,NULL,NULL,'Arctic Char','Salvelinus alpinus','5',1,1,NULL,0),(12,NULL,NULL,'Barramundi','Lates calcarifer','6',1,1,NULL,0),(13,NULL,NULL,'Bass','','7',1,1,NULL,0),(14,NULL,NULL,'Bass, Silver Sheephead','Aplodinotus rafinesque','7',1,1,NULL,0),(15,NULL,NULL,'Bass, White','Morone chrysops','7',1,1,NULL,0),(16,NULL,NULL,'Bass, Striped','Morone saxatilis','7',1,1,NULL,0),(17,NULL,NULL,'Bluefish','Pomatomus saltatrix','9',1,1,NULL,0),(18,NULL,NULL,'Bonito','Sarda spp.','10',1,1,NULL,0),(19,NULL,NULL,'Bream, Sea','Family Sparidae','11',1,1,NULL,0),(20,NULL,NULL,'Buffalo Fish','Ictiobius spp.','12',1,1,NULL,0),(21,NULL,NULL,'Butter Fish','Family Stromateidae','13',1,1,NULL,0),(22,NULL,NULL,'Carp','Family Cyprinidae','14',1,1,NULL,0),(23,NULL,NULL,'Catfish','','15',1,1,NULL,0),(24,NULL,NULL,'Pangasius','Pangasius bocourti\nPangasius hypophthalamus','53',1,1,NULL,0),(25,NULL,NULL,'Catfish, Channel','Ictalurus punctatus','15',1,1,NULL,0),(26,NULL,NULL,'Caviar, carp roe','','16',1,1,NULL,0),(27,NULL,NULL,'Clam','','17',1,1,NULL,0),(28,NULL,NULL,'Clam, Littleneck','Mercenaria mercenaria','17',1,1,NULL,0),(29,NULL,NULL,'Clam, Mahogany','Arctica islandica','17',1,1,NULL,0),(30,NULL,NULL,'Clam, Manila','Venerupis philippinarum','17',1,1,NULL,0),(31,NULL,NULL,'Clam, Quahog','Mercenaria mercenaria','17',1,1,NULL,0),(32,NULL,NULL,'Clam, Northern Quahog','Mercenaria mercenaria','17',1,1,NULL,0),(33,NULL,NULL,'Clam, Razor','Siliqua patula','17',1,1,NULL,0),(34,NULL,NULL,'Clam, Surf','Spisula solida','17',1,1,NULL,0),(35,NULL,NULL,'Clam, Venus','Venus spp.','17',1,1,NULL,0),(36,NULL,NULL,'Cobia','Rachycentron canadum','18',1,1,NULL,0),(37,NULL,NULL,'Cod','Gadus spp.','19',1,1,NULL,0),(38,NULL,NULL,'Cod, Atlantic','Gadus morhua','19',1,1,NULL,0),(39,NULL,NULL,'Cod, Black','Notothenia microlepidota','19',1,1,NULL,0),(40,NULL,NULL,'Lingcod','Ophiodon elongatus','37',1,1,NULL,0),(41,NULL,NULL,'Cod, Pacific','Gadus macrocephalus','19',1,1,NULL,0),(42,NULL,NULL,'Conch','Strombus spp.','20',1,1,NULL,0),(43,NULL,NULL,'Corvina','Cilus gilberti','21',1,1,NULL,0),(44,NULL,NULL,'Crab','','22',1,1,NULL,0),(45,NULL,NULL,'Crab, Blue','Callinectes sapidus','22',1,1,NULL,0),(46,NULL,NULL,'Crab, Blue Swimming','Portunus pelagicus','22',1,1,NULL,0),(47,NULL,NULL,'Crab, Dungeness','Metacarcinus magister','22',1,1,NULL,0),(48,NULL,NULL,'Crab, Jonah','Cancer borealis','22',1,1,NULL,0),(49,NULL,NULL,'Crab, King','Lithodes aequispinus\nParalithodes camtschaticus\nParalithodes platypus','22',1,1,NULL,0),(50,NULL,NULL,'Crab, Red Atlantic','Chaceon quinquedens','22',1,1,NULL,0),(51,NULL,NULL,'Crab, Rock','Cancer productus','22',1,1,NULL,0),(52,NULL,NULL,'Crab, Shrimp','','22',1,1,NULL,0),(53,NULL,NULL,'Crab, Snow','Chionoecetes opilio','22',1,1,NULL,0),(54,NULL,NULL,'Crab, Stone','Menippe mercenaria','22',1,1,NULL,0),(55,NULL,NULL,'Crab, Softshell','Callinectes sapidus\nPortunus trituberculatus\nCharybdis japonica\nCarcinus aestuarii','22',1,1,NULL,0),(56,NULL,NULL,'Crawfish','Family Astacoidea\nFamily Parastacoidea','23',1,1,NULL,0),(57,NULL,NULL,'Croaker','Family Sciaenidae','24',1,1,NULL,0),(58,NULL,NULL,'Cusk Eel','Genypterus blacodes\nGenypterus capensis','26',1,1,NULL,0),(59,NULL,NULL,'Eel','Anguilla japonica\nAnguilla anguilla\nAnguilla rostrata ','26',1,1,NULL,0),(60,NULL,NULL,'Flounder','Paralichthys albigutta\nParalichthys lethostigma\nPseudopleuronectes americanus\nPlatichthys flesus\nParalichthys olivaceus','28',1,1,NULL,0),(61,NULL,NULL,'Flounder, Yellowtail','','28',1,1,NULL,0),(62,NULL,NULL,'Fluke','Paralichthys dentatus','28',1,1,NULL,0),(63,NULL,NULL,'Grouper','Family Serranidae subfamily Epinephellinae','30',1,1,NULL,0),(64,NULL,NULL,'Haddock','Melanogrammus aeglefinus','31',1,1,NULL,0),(65,NULL,NULL,'Hake','Merluccius productus','32',1,1,NULL,0),(66,NULL,NULL,'Hake, Argentinean','Meluccius hubbsi','32',1,1,NULL,0),(67,NULL,NULL,'Hake, Pacific','Merluccius productus','32',1,1,NULL,0),(68,NULL,NULL,'Hake, Red','Urophycis chuss','32',1,1,NULL,0),(69,NULL,NULL,'Halibut, California','Paralichthys californicus','28',1,1,NULL,0),(70,NULL,NULL,'Halibut, Atlantic','Hippoglossus hippoglossus','28',1,1,NULL,0),(71,NULL,NULL,'Halibut, Pacific','Hippoglossus stenolepis','28',1,1,NULL,0),(72,NULL,NULL,'Herring','Clupea spp.','33',1,1,NULL,0),(73,NULL,NULL,'Kingklip','Genypterus capensis','35',1,1,NULL,0),(74,NULL,NULL,'Langostino','Cervimunida johni\nMunida gregaria\nPleurocondes monodon','36',1,1,NULL,0),(75,NULL,NULL,'Ling, Common','Molva molva','19',1,1,NULL,0),(76,NULL,NULL,'Lobster','Panuliris interruptus','38',1,1,NULL,0),(77,NULL,NULL,'Lobster, American','Homarus americanus','38',1,1,NULL,0),(78,NULL,NULL,'Lobster, Caribbean','Panuliris argus','38',1,1,NULL,0),(79,NULL,NULL,'Lutefisk','','39',1,1,NULL,0),(80,NULL,NULL,'Mackerel, Spanish','Scomberomorus maculatus','40',1,1,NULL,0),(81,NULL,NULL,'Mackerel','Family Scombridae','40',1,1,NULL,0),(82,NULL,NULL,'Mackerel, King','Scomberomorus cavalla','40',1,1,NULL,0),(83,NULL,NULL,'Mahi mahi','Coryphaena hippurus','41',1,1,NULL,0),(84,NULL,NULL,'Marlin','Makaira spp\nTetrapturus spp.','42',1,1,NULL,0),(85,NULL,NULL,'Marlin, Blue','Makaira mazara\nMakaira nigricans','42',1,1,NULL,0),(86,NULL,NULL,'Marlin, Black','Makaira indica','42',1,1,NULL,0),(87,NULL,NULL,'Marlin, Striped','Tetrapturus audax','42',1,1,NULL,0),(88,NULL,NULL,'Marlin, White','Tetrapturus albidus','42',1,1,NULL,0),(89,NULL,NULL,'Mixed','','43',1,1,NULL,0),(90,NULL,NULL,'Monkfish','Lophius piscatorius\nLophius caulinaris\nLophius americanus\nLophius budegassa','44',1,1,NULL,0),(91,NULL,NULL,'Mullet','Family Mugilidae\nFamily Mullidae','45',1,1,NULL,0),(92,NULL,NULL,'Mussel','Mytilus californianus\nPerna canaliculus\nMytilus edulis','46',1,1,NULL,0),(93,NULL,NULL,'Octopus','Order Octopoda','49',1,1,NULL,0),(94,NULL,NULL,'Opah','Lampris guttatus\nLampris immaculatus','50',1,1,NULL,0),(95,NULL,NULL,'Orange Roughy','Hoplostethus atlanticus','51',1,1,NULL,0),(96,NULL,NULL,'Oyster','Family Ostreidae','52',1,1,NULL,0),(97,NULL,NULL,'Parrot Fish','Family Scaridae','54',1,1,NULL,0),(98,NULL,NULL,'Perch','Perca spp.','55',1,1,NULL,0),(99,NULL,NULL,'Redfish, Ocean Perch','Sebastes norvegicus ','61',1,1,NULL,0),(100,NULL,NULL,'Perch, White','Morone americana','55',1,1,NULL,0),(101,NULL,NULL,'Perch, Yellow','Perca flavescens','55',1,1,NULL,0),(102,NULL,NULL,'Pike, Northern','Esox lucius','56',1,1,NULL,0),(103,NULL,NULL,'Pike, Walleye','Sander vitreus','56',1,1,NULL,0),(104,NULL,NULL,'Pollock, Atlantic','Pollachius pollachius','57',1,1,NULL,0),(105,NULL,NULL,'Pollock','Pollachius pollachius\nPollachius virens','57',1,1,NULL,0),(106,NULL,NULL,'Pollock, Alaska','Theragra chalcogramma','57',1,1,NULL,0),(107,NULL,NULL,'Pompano','Trachinotus spp.','59',1,1,NULL,0),(108,NULL,NULL,'Porgy','Family Sparidae','60',1,1,NULL,0),(109,NULL,NULL,'Rockfish','Sebastes spp.','61',1,1,NULL,0),(110,NULL,NULL,'Salmon','Salmo spp.\nOncorhynchus spp.','63',1,1,NULL,0),(111,NULL,NULL,'Salmon, Atlantic','Salmo salar','63',1,1,NULL,0),(112,NULL,NULL,'Salmon, Coho','Oncoryhnchus kisutch','63',1,1,NULL,0),(113,NULL,NULL,'Salmon, King','Oncoryhnchus tshawytscha','63',1,1,NULL,0),(114,NULL,NULL,'Salmon, Chum','Oncoryhnchus keta','63',1,1,NULL,0),(115,NULL,NULL,'Salmon, Pink','Oncorhynchus gorbuscha','63',1,1,NULL,0),(116,NULL,NULL,'Salmon, Sockeye','Oncoryhnchus nerka','63',1,1,NULL,0),(117,NULL,NULL,'Sanddab','Citharichthys sordidus','28',1,1,NULL,0),(118,NULL,NULL,'Sardine','Sardinops spp\nSardina spp.','64',1,1,NULL,0),(119,NULL,NULL,'Scallop','Family Pectinidae','66',1,1,NULL,0),(120,NULL,NULL,'Scallop, Bay','Argopecten irradians','66',1,1,NULL,0),(121,NULL,NULL,'Scallop, Sea','Placopecten magellanicus','66',1,1,NULL,0),(122,NULL,NULL,'Seabass','Family Serranidae','7',1,1,NULL,0),(123,NULL,NULL,'Seabass, Black','Centropristis striata','7',1,1,NULL,0),(124,NULL,NULL,'Seabass, Chilean','Dissostichus eleginoides','87',1,1,NULL,0),(125,NULL,NULL,'Seabass, European','Dicentrarchus labrax','7',1,1,NULL,0),(126,NULL,NULL,'Shad','Alosa spp.','70',1,1,NULL,0),(127,NULL,NULL,'Shark','','71',1,1,NULL,0),(128,NULL,NULL,'Shark, Longfin Mako','Isurus paucus','71',1,1,NULL,0),(129,NULL,NULL,'Shark, Shortfin Mako','Isurus oxyrinchus','71',1,1,NULL,0),(130,NULL,NULL,'Shark, Soupfin','Galeorhinus galeus','71',1,1,NULL,0),(131,NULL,NULL,'Shark, Common Thresher','Alopias vulpinus','71',1,1,NULL,0),(132,NULL,NULL,'Shark, Bigeye Thresher','Alopias superciliosus','71',1,1,NULL,0),(133,NULL,NULL,'Shark, Pelagic Thresher','Alopias pelagicus','71',1,1,NULL,0),(134,NULL,NULL,'Sheephead','Semicossyphus pulcher','72',1,1,NULL,0),(135,NULL,NULL,'Sheepshead','Archosargus probatocephalus','73',1,1,NULL,0),(136,NULL,NULL,'Shrimp','Order Caridea','74',1,1,NULL,0),(137,NULL,NULL,'Shrimp, Freshwater','Macrobrachium rosenbergii\nMacrobrachium spp.','74',1,1,NULL,0),(138,NULL,NULL,'Skate','Family Rajidae','75',1,1,NULL,0),(139,NULL,NULL,'Smelt','Family Osmeridae','76',1,1,NULL,0),(140,NULL,NULL,'Smelt, Lake','Family Osmeridae','76',1,1,NULL,0),(141,NULL,NULL,'Snail','','48',1,1,NULL,0),(142,NULL,NULL,'Snapper','Family Lutjanidae','77',1,1,NULL,0),(143,NULL,NULL,'Snapper, Brazilian','Lutjanus alexandrei','77',1,1,NULL,0),(144,NULL,NULL,'Snapper, Red','Lutjanus campechanus','77',1,1,NULL,0),(145,NULL,NULL,'Snapper, Silk','Lutjanus vivanus','77',1,1,NULL,0),(146,NULL,NULL,'Snapper, Vermilion','Rhomboplites aurorubens','77',1,1,NULL,0),(147,NULL,NULL,'Snapper, Yellowtail','Ocyurus chrysurus','77',1,1,NULL,0),(148,NULL,NULL,'Sole','Family Soleidae','28',1,1,NULL,0),(149,NULL,NULL,'Sole, Dover','Solea solea\nSolea vulgaris','28',1,1,NULL,0),(150,NULL,NULL,'Sole, English','Parophrys vetulus','28',1,1,NULL,0),(151,NULL,NULL,'Sole, Grey','Glyptocephalus cynoglossus','28',1,1,NULL,0),(152,NULL,NULL,'Sole, Lemon','Microstomus kitt','28',1,1,NULL,0),(153,NULL,NULL,'Sole, Pacific','Microstomus pacificus','28',1,1,NULL,0),(154,NULL,NULL,'Sole, Petrale','Eopsetta jordani','28',1,1,NULL,0),(155,NULL,NULL,'Sole, Rex','Glyptocephalus zachirus','28',1,1,NULL,0),(156,NULL,NULL,'Sole, Rock','Lepidopsetta bilineata','28',1,1,NULL,0),(157,NULL,NULL,'Sole, Yellowfin','Limanda aspera','28',1,1,NULL,0),(158,NULL,NULL,'Spearfish','Tetrapturus angustirostris','79',1,1,NULL,0),(159,NULL,NULL,'Croaker, Spot','Leiostomus xanthurus','24',1,1,NULL,0),(160,NULL,NULL,'Squid','Order Teuthida','80',1,1,NULL,0),(161,NULL,NULL,'Sturgeon','Family Acipenseridae','82',1,1,NULL,0),(162,NULL,NULL,'Swordfish','Xiphias gladius','83',1,1,NULL,0),(163,NULL,NULL,'Tautog','Tautoga onitis','84',1,1,NULL,0),(164,NULL,NULL,'Tilapia','Genus Orechromis\nGenus Sarotherodon\nGenus Tilapia','85',1,1,NULL,0),(165,NULL,NULL,'Tilefish','Family Malacanthidae','86',1,1,NULL,0),(166,NULL,NULL,'Trout','Family Salmonidae, Subfamily Salmoninae','88',1,1,NULL,0),(167,NULL,NULL,'Trout, Golden','Oncoryhnchus mykiss aguabonita','88',1,1,NULL,0),(168,NULL,NULL,'Trout, Lake','Salvelinus namaycush','88',1,1,NULL,0),(169,NULL,NULL,'Trout, Rainbow','Oncoryhnchus mykiss','88',1,1,NULL,0),(170,NULL,NULL,'Trout, Red','','88',1,1,NULL,0),(171,NULL,NULL,'Trout, Sea','Salmo trutta\nOncoryhnchus mykiss irideus','88',1,1,NULL,0),(172,NULL,NULL,'Trout, Steelhead','Oncoryhnchus mykiss irideus','88',1,1,NULL,0),(173,NULL,NULL,'Tuna','Genus Thunnus','89',1,1,NULL,0),(174,NULL,NULL,'Tuna, Albacore','Thunnus alalunga','89',1,1,NULL,0),(175,NULL,NULL,'Tuna, Dogtooth','Gymnosarda unicolor','89',1,1,NULL,0),(176,NULL,NULL,'Tuna, Tongol','Thunnus tonggol','89',1,1,NULL,0),(177,NULL,NULL,'Tuna, Atlantic Bluefin','Thunnus thynnus','89',1,1,NULL,0),(178,NULL,NULL,'Tuna, Southern Bluefin','Thunnus maccoyii','89',1,1,NULL,0),(179,NULL,NULL,'Tuna, Pacific Bluefin','Thunnus orientalis','89',1,1,NULL,0),(180,NULL,NULL,'Tuna, Blackfin','Thunnus atlanticus','89',1,1,NULL,0),(181,NULL,NULL,'Tuna, Skipjack','Katsuwonus pelamis','89',1,1,NULL,0),(182,NULL,NULL,'Tuna, Yellowfin','Thunnus albacares','89',1,1,NULL,0),(183,NULL,NULL,'Turbot','Scopthalmus maximus','28',1,1,NULL,0),(184,NULL,NULL,'Wahoo','Acanthocybium solandri','90',1,1,NULL,0),(185,NULL,NULL,'Walleye','Sander vitreus','91',1,1,NULL,0),(186,NULL,NULL,'Whelk','Family Buccinidae','92',1,1,NULL,0),(187,NULL,NULL,'Whiting','Merluccius bilinearis','94',1,1,NULL,0),(188,NULL,NULL,'Kampachi','Seriola rivoliana','34',1,1,NULL,0),(189,NULL,NULL,'Blackfish','Tautoga onitis','8',1,1,NULL,0),(190,NULL,NULL,'Toothfish','Dissostichus mawsoni','87',1,1,NULL,0),(191,NULL,NULL,'Cod, Rock','Lotella rhacina','19',1,1,NULL,0),(192,NULL,NULL,'Crab, Three-Spotted Swimming','Portunus sanguinolentus','22',1,1,NULL,0),(193,NULL,NULL,'Crab, White Swimming','Portunus trituberculatus','22',1,1,NULL,0),(194,NULL,NULL,'Croaker, Yellow','Larimichthys polyactis','24',1,1,NULL,0),(195,NULL,NULL,'Cuttlefish','','25',1,1,NULL,0),(196,NULL,NULL,'Escolar','Lepidocybium flavobrunneum','27',1,1,NULL,0),(197,NULL,NULL,'Jobfish','Pristipomoides spp.','77',1,1,NULL,0),(198,NULL,NULL,'Snapper, Hawaiian','Pristipomoides filamentosus','77',1,1,NULL,0),(199,NULL,NULL,'Mackerel Scad','Decapterus macarellus','40',1,1,NULL,0),(200,NULL,NULL,'Rockfish, Pacific Ocean Perch','Sebastes alutus','61',1,1,NULL,0),(201,NULL,NULL,'Redfish','Sebastes fasciatus\nSebastes mentella','61',1,1,NULL,0),(202,NULL,NULL,'Rover, Golden','Erythrocles scintillans','62',1,1,NULL,0),(203,NULL,NULL,'Scad, Bigeye','Selar crumenophthalmus','65',1,1,NULL,0),(204,NULL,NULL,'Seabob, Atlantic','Xiphopenaeus kroyeri','68',1,1,NULL,0),(205,NULL,NULL,'Snapper, Crimson','Lutjanus erythropterus','77',1,1,NULL,0),(206,NULL,NULL,'Snapper, Goldband','Pristipomoides Multidens','77',1,1,NULL,0),(207,NULL,NULL,'Snapper, Johns','Lutjanus Johnii','77',1,1,NULL,0),(208,NULL,NULL,'Snapper, Lavender','Pristipomoides sieboldii','77',1,1,NULL,0),(209,NULL,NULL,'Snapper, Lehi','Aphareus rutilans','77',1,1,NULL,0),(210,NULL,NULL,'Snapper, Long-Tail Red','Etelis coruscans','77',1,1,NULL,0),(211,NULL,NULL,'Snapper, Mangrove','Lutjanus griseus','77',1,1,NULL,0),(212,NULL,NULL,'Snapper, Pinjalo','Pinjalo spp.','77',1,1,NULL,0),(213,NULL,NULL,'Snapper, Short-Tail Red','Etelis carbunculus','77',1,1,NULL,0),(214,NULL,NULL,'Snapper, Scarlet','Lutjanus sanguineus\nLutjanus malabaricus','77',1,1,NULL,0),(215,NULL,NULL,'Soldierfish','','78',1,1,NULL,0),(216,NULL,NULL,'Spearfish, Shortbill','Tetrapturus angustirostris','79',1,1,NULL,0),(217,NULL,NULL,'Squirrelfish','Holocentridae','81',1,1,NULL,0),(218,NULL,NULL,'Tuna, Bigeye','Thunnus obesus','89',1,1,NULL,0),(219,NULL,NULL,'Whitefish, Lake','Coregonus clupeaformis','93',1,1,NULL,0),(220,NULL,NULL,'Whitefish, Round','Prosopium cylindraceum','93',1,1,NULL,0),(221,NULL,NULL,'Whitefish','','93',1,1,NULL,0),(222,NULL,NULL,'Halibut','','28',1,1,NULL,0),(223,NULL,NULL,'Drum, Red','Sciaenops ocellatus','7',1,1,NULL,0),(224,NULL,NULL,'Capelin','Mallotus villosus','76',1,1,NULL,0),(225,NULL,NULL,'Clam, New Zealand Cockle','Austrovenus stutchburyi','17',1,1,NULL,0),(226,NULL,NULL,'Clam, Hardshell','Meretrix lyrata','17',1,1,NULL,0),(227,NULL,NULL,'Crab, Sandy Swimming','Liocarcinus depurator','22',1,1,NULL,0),(228,NULL,NULL,'Crab, Atlantic Rock','Cancer irroratus','22',1,1,NULL,0),(229,NULL,NULL,'Crawfish, Red Swamp','Procambarus clarkii','23',1,1,NULL,0),(230,NULL,NULL,'Croaker, Atlantic','Micropogonias undulatus','24',1,1,NULL,0),(231,NULL,NULL,'Flounder, Arrowtooth','Reinhardtius stomias\nAtheresthes stomias','28',1,1,NULL,0),(232,NULL,NULL,'Plaice','Pleuronectes spp.','28',1,1,NULL,0),(233,NULL,NULL,'Flying Fish','Cheilopogon agoo','29',1,1,NULL,0),(234,NULL,NULL,'Goatfish','Family Mullidae','45',1,1,NULL,0),(235,NULL,NULL,'Hake, White','Urophycis tenuis','32',1,1,NULL,0),(236,NULL,NULL,'Jack, Yellowtail','Seriola lalandi dorsalis','3',1,1,NULL,0),(237,NULL,NULL,'Octopus, Common','Octopus vulgaris','49',1,1,NULL,0),(238,NULL,NULL,'Pomfret','family Bramidae','58',1,1,NULL,0),(239,NULL,NULL,'Scallop, Patagonian','Zygochlamys patagonica','66',1,1,NULL,0),(240,NULL,NULL,'Seabass, White','Atractoscion nobilis','24',1,1,NULL,0),(241,NULL,NULL,'Smelt, Ocean','Family Osmeridae','76',1,1,NULL,0),(242,NULL,NULL,'Smelt, Rainbow','Osmerus mordax','76',1,1,NULL,0),(243,NULL,NULL,'Snapper, Lane','Lutjanus synagris','77',1,1,NULL,0),(244,NULL,NULL,'Sole, Northern Rock','Lutjanus synagris','28',1,1,NULL,0),(245,NULL,NULL,'Sturgeon, Atlantic','Acipenser oxyrinchus oxyrinchus','82',1,1,NULL,0),(246,NULL,NULL,'Crab, Southern King','Lithodes santolla','22',1,1,NULL,0),(247,NULL,NULL,'Scallop, Peruvian','Argopecten purpuratus','66',1,1,NULL,0),(248,NULL,NULL,'Snapper, Cubera','Lutjanus cyanopterus','77',1,1,NULL,0),(249,NULL,NULL,'Hogfish','Lachnolaimus maximus','95',1,1,NULL,0),(250,NULL,NULL,'Prawn, Giant Tiger','Penaeus monodon','74',1,1,NULL,0),(251,NULL,NULL,'Lobster, Tristan da Cunha Rock','Jasus tristani','38',1,1,NULL,0),(252,NULL,NULL,'Cabezon','Scorpaenichthys marmoratus','67',1,1,NULL,0),(253,NULL,NULL,'Seaweed','','69',1,1,NULL,0),(254,NULL,NULL,'Nile Perch','Lates niloticus','47',1,1,NULL,0),(255,NULL,NULL,'Sablefish','Anoplopoma fimbria','',1,1,NULL,0),(256,NULL,NULL,'Black cod','Anoplopoma fimbria','',1,1,NULL,0);
/*!40000 ALTER TABLE `seafoodSpecies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources`
--

DROP TABLE IF EXISTS `sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fk_vendor_id` int(11) NOT NULL,
  `fk_fish_id` int(11) NOT NULL,
  `fk_harvestMethod_id` int(11) NOT NULL,
  `supplierCompany` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactPhone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `COOL` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show_source` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `fk_coolCountry_id` mediumint(9) NOT NULL,
  `fk_harvestCountry_id` mediumint(9) NOT NULL,
  `harvestRegion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `faoMajorFishingArea` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_flagOfVesselCountry_id` mediumint(9) NOT NULL,
  `certificationType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `certificationNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendorComments` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sources_fk_vendor_id_index` (`fk_vendor_id`),
  KEY `sources_fk_fish_id_index` (`fk_fish_id`),
  KEY `sources_fk_gear_id_index` (`fk_harvestMethod_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources`
--

LOCK TABLES `sources` WRITE;
/*!40000 ALTER TABLE `sources` DISABLE KEYS */;
/*!40000 ALTER TABLE `sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyProductSources`
--

DROP TABLE IF EXISTS `surveyProductSources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyProductSources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fk_product_id` int(11) NOT NULL,
  `fk_source_id` int(11) NOT NULL,
  `fk_survey_id` int(11) NOT NULL,
  `proportionOfProduct` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_retailer_id` mediumint(9) NOT NULL,
  `fk_vendor_id` mediumint(9) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_product_sources_fk_product_id_index` (`fk_product_id`),
  KEY `survey_product_sources_fk_source_id_index` (`fk_source_id`),
  KEY `survey_product_sources_fk_survey_id_index` (`fk_survey_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyProductSources`
--

LOCK TABLES `surveyProductSources` WRITE;
/*!40000 ALTER TABLE `surveyProductSources` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyProductSources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyProducts`
--

DROP TABLE IF EXISTS `surveyProducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyProducts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fk_survey_id` int(11) NOT NULL,
  `fk_product_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_retailer_id` mediumint(9) NOT NULL,
  `fk_vendor_id` mediumint(9) NOT NULL,
  `markedComplete` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_products_fk_survey_id_index` (`fk_survey_id`),
  KEY `survey_products_fk_product_id_index` (`fk_product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyProducts`
--

LOCK TABLES `surveyProducts` WRITE;
/*!40000 ALTER TABLE `surveyProducts` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveyProducts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `surveyStatus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_retailer_id` int(10) unsigned NOT NULL,
  `fk_vendor_id` int(10) unsigned NOT NULL,
  `dateIssued` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `dateDue` date DEFAULT NULL,
  `retailerComments` text COLLATE utf8_unicode_ci,
  `fishwiseComments` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `surveys_fk_retailer_id_index` (`fk_retailer_id`),
  KEY `surveys_fk_vendor_id_index` (`fk_vendor_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `org_id` int(11) NOT NULL,
  `admin` tinyint(4) NOT NULL,
  `approved_user` tinyint(4) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Ryan Manzer','r.manzer@fishwise.org','$2y$10$WwG2p7Xal4UG4pVsKMp/8uGA5JMLbmFuN/wf21k51.YlFnXJu6C8a','3ZdFcBh9D62A5zTKxM70cwNnQFydCXZ6BlPAkZDGVcPaG8YHS9DTwHK1Fb6d','2016-08-08 16:23:30','2016-08-11 22:10:05',1,1,'ngo',1,1,0),(2,'John Doe','jd@boringname.net','$2y$10$rmbd5P/7KTrbUOfWACG1BeDT2/RjGDAasNPP8Y6svaRTKSNsqslzC','LvOAJpmnJHM8EEya2WZEknmXkO0dPnlHkQBOF6rsU5yGaiFoTPax0rtmlihO','2016-08-09 00:46:42','2016-08-11 22:08:32',1,1,'vendor',1,0,0),(4,'Nat Robinson','n.robinson@fishwise.org','$2y$10$JisZkd4lTEAOLKfFDw3coegiv7U.PcwEuZyHV0MXogPGpyHopwtXS','UP355syACxq2ufd171DJZ6h4ZPmcirHKw5IU4ymjiVZ5vyeK4Wir0pBvmOUl','2016-08-09 16:17:42','2016-08-09 17:29:47',1,1,'ngo',1,1,0),(5,'Jane Doe','jd@missing.person','$2y$10$li4zfvp/6kdR.eBtdKw1tO3GdEP03z5CcV.3wwH2Zg4aaiAflSbQ.',NULL,'2016-08-09 16:28:59','2016-08-09 16:28:59',1,1,'vendor',2,0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `vendor_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_street_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_street_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_state_provence` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fishwiseComments` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendors`
--

LOCK TABLES `vendors` WRITE;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
INSERT INTO `vendors` VALUES (1,'2016-08-09 00:50:23','2016-08-09 00:50:23','Tasty Fish','John Doe','jd@boringname.net','(123) 456-7890','123 Boring Ln.','','San Fransisco','CA','','',0,0,NULL),(2,'2016-08-09 16:25:27','2016-08-09 16:25:27','Yum Fish','Jane Doe','jd@missing.person','','123 Street St','','Seattle','WA','USA','',1,1,NULL);
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-15  8:37:44
