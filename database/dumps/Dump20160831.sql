-- MySQL dump 10.13  Distrib 5.7.12, for osx10.9 (x86_64)
--
-- Host: 127.0.0.1    Database: homestead
-- ------------------------------------------------------
-- Server version	5.7.13-0ubuntu0.16.04.2

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `countries`
--
USE homestead;
DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `countries` (
  `ISO2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ISO3` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `countryName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL,
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10218 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` (`ISO2`, `ISO3`, `countryName`, `excludeFromDropdown`, `id`, `created_at`, `updated_at`) VALUES ('AF','AFG','Afghanistan',0,10003,NULL,NULL),('AL','ALB','Albania',0,10004,NULL,NULL),('DZ','DZA','Algeria',0,10005,NULL,NULL),('AD','AND','Andorra',0,10006,NULL,NULL),('AO','AGO','Angola',0,10007,NULL,NULL),('AG','ATG','Antigua and Barbuda',0,10008,NULL,NULL),('AR','ARG','Argentina',0,10009,NULL,NULL),('AM','ARM','Armenia',0,10010,NULL,NULL),('AU','AUS','Australia',0,10011,NULL,NULL),('AT','AUT','Austria',0,10012,NULL,NULL),('AZ','AZE','Azerbaijan',0,10013,NULL,NULL),('BS','BHS','Bahamas, The',0,10014,NULL,NULL),('BH','BHR','Bahrain',0,10015,NULL,NULL),('BD','BGD','Bangladesh',0,10016,NULL,NULL),('BB','BRB','Barbados',0,10017,NULL,NULL),('BY','BLR','Belarus',0,10018,NULL,NULL),('BE','BEL','Belgium',0,10019,NULL,NULL),('BZ','BLZ','Belize',0,10020,NULL,NULL),('BJ','BEN','Benin',0,10021,NULL,NULL),('BT','BTN','Bhutan',0,10022,NULL,NULL),('BO','BOL','Bolivia',0,10023,NULL,NULL),('BA','BIH','Bosnia and Herzegovina',0,10024,NULL,NULL),('BW','BWA','Botswana',0,10025,NULL,NULL),('BR','BRA','Brazil',0,10026,NULL,NULL),('BN','BRN','Brunei',0,10027,NULL,NULL),('BG','BGR','Bulgaria',0,10028,NULL,NULL),('BF','BFA','Burkina Faso',0,10029,NULL,NULL),('BI','BDI','Burundi',0,10030,NULL,NULL),('KH','KHM','Cambodia',0,10031,NULL,NULL),('CM','CMR','Cameroon',0,10032,NULL,NULL),('CA','CAN','Canada',0,10033,NULL,NULL),('CV','CPV','Cape Verde',0,10034,NULL,NULL),('CF','CAF','Central African Republic',0,10035,NULL,NULL),('TD','TCD','Chad',0,10036,NULL,NULL),('CL','CHL','Chile',0,10037,NULL,NULL),('CN','CHN','China, Peoples Republic of',0,10038,NULL,NULL),('CO','COL','Colombia',0,10039,NULL,NULL),('KM','COM','Comoros',0,10040,NULL,NULL),('CD','COD','Congo, (Congo Â Kinshasa)',0,10041,NULL,NULL),('CG','COG','Congo, (Congo Â Brazzaville)',0,10042,NULL,NULL),('CR','CRI','Costa Rica',0,10043,NULL,NULL),('CI','CIV','Cote dIvoire (Ivory Coast)',0,10044,NULL,NULL),('HR','HRV','Croatia',0,10045,NULL,NULL),('CU','CUB','Cuba',0,10046,NULL,NULL),('CY','CYP','Cyprus',0,10047,NULL,NULL),('CZ','CZE','Czech Republic',0,10048,NULL,NULL),('DK','DNK','Denmark',0,10049,NULL,NULL),('DJ','DJI','Djibouti',0,10050,NULL,NULL),('DM','DMA','Dominica',0,10051,NULL,NULL),('DO','DOM','Dominican Republic',0,10052,NULL,NULL),('EC','ECU','Ecuador',0,10053,NULL,NULL),('EG','EGY','Egypt',0,10054,NULL,NULL),('SV','SLV','El Salvador',0,10055,NULL,NULL),('GQ','GNQ','Equatorial Guinea',0,10056,NULL,NULL),('ER','ERI','Eritrea',0,10057,NULL,NULL),('EE','EST','Estonia',0,10058,NULL,NULL),('ET','ETH','Ethiopia',0,10059,NULL,NULL),('FJ','FJI','Fiji',0,10060,NULL,NULL),('FI','FIN','Finland',0,10061,NULL,NULL),('FR','FRA','France',0,10062,NULL,NULL),('GA','GAB','Gabon',0,10063,NULL,NULL),('GM','GMB','Gambia, The',0,10064,NULL,NULL),('GE','GEO','Georgia',0,10065,NULL,NULL),('DE','DEU','Germany',0,10066,NULL,NULL),('GH','GHA','Ghana',0,10067,NULL,NULL),('GR','GRC','Greece',0,10068,NULL,NULL),('GD','GRD','Grenada',0,10069,NULL,NULL),('GT','GTM','Guatemala',0,10070,NULL,NULL),('GN','GIN','Guinea',0,10071,NULL,NULL),('GW','GNB','Guinea-Bissau',0,10072,NULL,NULL),('GY','GUY','Guyana',0,10073,NULL,NULL),('HT','HTI','Haiti',0,10074,NULL,NULL),('HN','HND','Honduras',0,10075,NULL,NULL),('HU','HUN','Hungary',0,10076,NULL,NULL),('IS','ISL','Iceland',0,10077,NULL,NULL),('IN','IND','India',0,10078,NULL,NULL),('ID','IDN','Indonesia',0,10079,NULL,NULL),('IR','IRN','Iran',0,10080,NULL,NULL),('IQ','IRQ','Iraq',0,10081,NULL,NULL),('IE','IRL','Ireland',0,10082,NULL,NULL),('IL','ISR','Israel',0,10083,NULL,NULL),('IT','ITA','Italy',0,10084,NULL,NULL),('JM','JAM','Jamaica',0,10085,NULL,NULL),('JP','JPN','Japan',0,10086,NULL,NULL),('JO','JOR','Jordan',0,10087,NULL,NULL),('KZ','KAZ','Kazakhstan',0,10088,NULL,NULL),('KE','KEN','Kenya',0,10089,NULL,NULL),('KI','KIR','Kiribati',0,10090,NULL,NULL),('KP','PRK','Korea, North',0,10091,NULL,NULL),('KR','KOR','Korea, South',0,10092,NULL,NULL),('KW','KWT','Kuwait',0,10093,NULL,NULL),('KG','KGZ','Kyrgyzstan',0,10094,NULL,NULL),('LA','LAO','Laos',0,10095,NULL,NULL),('LV','LVA','Latvia',0,10096,NULL,NULL),('LB','LBN','Lebanon',0,10097,NULL,NULL),('LS','LSO','Lesotho',0,10098,NULL,NULL),('LR','LBR','Liberia',0,10099,NULL,NULL),('LY','LBY','Libya',0,10100,NULL,NULL),('LI','LIE','Liechtenstein',0,10101,NULL,NULL),('LT','LTU','Lithuania',0,10102,NULL,NULL),('LU','LUX','Luxembourg',0,10103,NULL,NULL),('MK','MKD','Macedonia',0,10104,NULL,NULL),('MG','MDG','Madagascar',0,10105,NULL,NULL),('MW','MWI','Malawi',0,10106,NULL,NULL),('MY','MYS','Malaysia',0,10107,NULL,NULL),('MV','MDV','Maldives',0,10108,NULL,NULL),('ML','MLI','Mali',0,10109,NULL,NULL),('MT','MLT','Malta',0,10110,NULL,NULL),('MH','MHL','Marshall Islands',0,10111,NULL,NULL),('MR','MRT','Mauritania',0,10112,NULL,NULL),('MU','MUS','Mauritius',0,10113,NULL,NULL),('MX','MEX','Mexico',0,10114,NULL,NULL),('FM','FSM','Micronesia',0,10115,NULL,NULL),('MD','MDA','Moldova',0,10116,NULL,NULL),('MC','MCO','Monaco',0,10117,NULL,NULL),('MN','MNG','Mongolia',0,10118,NULL,NULL),('ME','MNE','Montenegro',0,10119,NULL,NULL),('MA','MAR','Morocco',0,10120,NULL,NULL),('MZ','MOZ','Mozambique',0,10121,NULL,NULL),('MM','MMR','Myanmar (Burma)',0,10122,NULL,NULL),('NA','NAM','Namibia',0,10123,NULL,NULL),('NR','NRU','Nauru',0,10124,NULL,NULL),('NP','NPL','Nepal',0,10125,NULL,NULL),('NL','NLD','Netherlands',0,10126,NULL,NULL),('NZ','NZL','New Zealand',0,10127,NULL,NULL),('NI','NIC','Nicaragua',0,10128,NULL,NULL),('NE','NER','Niger',0,10129,NULL,NULL),('NG','NGA','Nigeria',0,10130,NULL,NULL),('NO','NOR','Norway',0,10131,NULL,NULL),('OM','OMN','Oman',0,10132,NULL,NULL),('PK','PAK','Pakistan',0,10133,NULL,NULL),('PW','PLW','Palau',0,10134,NULL,NULL),('PA','PAN','Panama',0,10135,NULL,NULL),('PG','PNG','Papua New Guinea',0,10136,NULL,NULL),('PY','PRY','Paraguay',0,10137,NULL,NULL),('PE','PER','Peru',0,10138,NULL,NULL),('PH','PHL','Philippines',0,10139,NULL,NULL),('PL','POL','Poland',0,10140,NULL,NULL),('PT','PRT','Portugal',0,10141,NULL,NULL),('QA','QAT','Qatar',0,10142,NULL,NULL),('RO','ROU','Romania',0,10143,NULL,NULL),('RU','RUS','Russia',0,10144,NULL,NULL),('RW','RWA','Rwanda',0,10145,NULL,NULL),('KN','KNA','Saint Kitts and Nevis',0,10146,NULL,NULL),('LC','LCA','Saint Lucia',0,10147,NULL,NULL),('VC','VCT','Saint Vincent and the Grenadines',0,10148,NULL,NULL),('WS','WSM','Samoa',0,10149,NULL,NULL),('SM','SMR','San Marino',0,10150,NULL,NULL),('ST','STP','Sao Tome and Principe',0,10151,NULL,NULL),('SA','SAU','Saudi Arabia',0,10152,NULL,NULL),('SN','SEN','Senegal',0,10153,NULL,NULL),('RS','SRB','Serbia',0,10154,NULL,NULL),('SC','SYC','Seychelles',0,10155,NULL,NULL),('SL','SLE','Sierra Leone',0,10156,NULL,NULL),('SG','SGP','Singapore',0,10157,NULL,NULL),('SK','SVK','Slovakia',0,10158,NULL,NULL),('SI','SVN','Slovenia',0,10159,NULL,NULL),('SB','SLB','Solomon Islands',0,10160,NULL,NULL),('SO','SOM','Somalia',0,10161,NULL,NULL),('ZA','ZAF','South Africa',0,10162,NULL,NULL),('ES','ESP','Spain',0,10163,NULL,NULL),('LK','LKA','Sri Lanka',0,10164,NULL,NULL),('SD','SDN','Sudan',0,10165,NULL,NULL),('SR','SUR','Suriname',0,10166,NULL,NULL),('SZ','SWZ','Swaziland',0,10167,NULL,NULL),('SE','SWE','Sweden',0,10168,NULL,NULL),('CH','CHE','Switzerland',0,10169,NULL,NULL),('SY','SYR','Syria',0,10170,NULL,NULL),('TJ','TJK','Tajikistan',0,10171,NULL,NULL),('TZ','TZA','Tanzania',0,10172,NULL,NULL),('TH','THA','Thailand',0,10173,NULL,NULL),('TL','TLS','Timor-Leste (East Timor)',0,10174,NULL,NULL),('TG','TGO','Togo',0,10175,NULL,NULL),('TO','TON','Tonga',0,10176,NULL,NULL),('TT','TTO','Trinidad and Tobago',0,10177,NULL,NULL),('TN','TUN','Tunisia',0,10178,NULL,NULL),('TR','TUR','Turkey',0,10179,NULL,NULL),('TM','TKM','Turkmenistan',0,10180,NULL,NULL),('TV','TUV','Tuvalu',0,10181,NULL,NULL),('UG','UGA','Uganda',0,10182,NULL,NULL),('UA','UKR','Ukraine',0,10183,NULL,NULL),('AE','ARE','United Arab Emirates',0,10184,NULL,NULL),('GB','GBR','United Kingdom',0,10185,NULL,NULL),('US','USA','United States',0,10186,NULL,NULL),('UY','URY','Uruguay',0,10187,NULL,NULL),('UZ','UZB','Uzbekistan',0,10188,NULL,NULL),('VU','VUT','Vanuatu',0,10189,NULL,NULL),('VA','VAT','Vatican City',0,10190,NULL,NULL),('VE','VEN','Venezuela',0,10191,NULL,NULL),('VN','VNM','Vietnam',0,10192,NULL,NULL),('YE','YEM','Yemen',0,10193,NULL,NULL),('ZM','ZMB','Zambia',0,10194,NULL,NULL),('ZW','ZWE','Zimbabwe',0,10195,NULL,NULL),('GE','GEO','Abkhazia',0,10196,NULL,NULL),('TW','TWN','China, Republic of (Taiwan)',0,10197,NULL,NULL),('AZ','AZE','Nagorno-Karabakh',0,10198,NULL,NULL),('CY','CYP','Northern Cyprus',0,10199,NULL,NULL),('MD','MDA','Pridnestrovie (Transnistria)',0,10200,NULL,NULL),('SO','SOM','Somaliland',0,10201,NULL,NULL),('GE','GEO','South Ossetia',0,10202,NULL,NULL),('AU','AUS','Ashmore and Cartier Islands',0,10203,NULL,NULL),('CX','CXR','Christmas Island',0,10204,NULL,NULL),('CC','CCK','Cocos (Keeling) Islands',0,10205,NULL,NULL),('AU','AUS','Coral Sea Islands',0,10206,NULL,NULL),('HM','HMD','Heard Island and McDonald Islands',0,10207,NULL,NULL),('NF','NFK','Norfolk Island',0,10208,NULL,NULL),('NC','NCL','New Caledonia',0,10209,NULL,NULL),('PF','PYF','French Polynesia',0,10210,NULL,NULL),('YT','MYT','Mayotte',0,10211,NULL,NULL),('GP','GLP','Saint Barthelemy',0,10212,NULL,NULL),('GP','GLP','Saint Martin',0,10213,NULL,NULL),('PM','SPM','Saint Pierre and Miquelon',0,10214,NULL,NULL),('WF','WLF','Wallis and Futuna',0,10215,NULL,NULL),('TF','ATF','French Southern and Antarctic Lands',0,10216,NULL,NULL),('PF','PYF','Clipperton Island',0,10217,NULL,NULL);
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `harvestMethods`
--

DROP TABLE IF EXISTS `harvestMethods`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `harvestMethods` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `harvestMethod` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `wild_farmed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_methodCategory_id` int(11) DEFAULT NULL,
  `harvestMethodForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  `fk_FM_id` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=215 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `harvestMethods`
--

LOCK TABLES `harvestMethods` WRITE;
/*!40000 ALTER TABLE `harvestMethods` DISABLE KEYS */;
INSERT INTO `harvestMethods` (`id`, `created_at`, `updated_at`, `harvestMethod`, `wild_farmed`, `created_by`, `modified_by`, `fk_methodCategory_id`, `harvestMethodForDisplay`, `excludeFromDropdown`, `fk_FM_id`) VALUES (108,NULL,NULL,'Pond, closed','Farmed',1,1,1,NULL,0,NULL),(109,NULL,NULL,'Pond, diversion','Farmed',1,1,1,NULL,0,NULL),(110,NULL,NULL,'Pond, frequent exchange','Farmed',1,1,1,NULL,0,NULL),(111,NULL,NULL,'Pond, infrequent exchange','Farmed',1,1,1,NULL,0,NULL),(113,NULL,NULL,'Pond, barrage','Farmed',1,1,1,NULL,0,NULL),(114,NULL,NULL,'Basin catch','Farmed',1,1,1,NULL,0,NULL),(115,NULL,NULL,'Bottom culture','Farmed',1,1,2,NULL,0,NULL),(116,NULL,NULL,'Off-bottom, culture','Farmed',1,1,2,NULL,0,NULL),(117,NULL,NULL,'Mixed, culture','Farmed',1,1,2,NULL,0,NULL),(118,NULL,NULL,'Bag culture','Farmed',1,1,2,NULL,0,NULL),(120,NULL,NULL,'Longline, culture','Farmed',1,1,2,NULL,0,NULL),(121,NULL,NULL,'Raft, culture','Farmed',1,1,2,NULL,0,NULL),(122,NULL,NULL,'Stakes','Farmed',1,1,2,NULL,0,NULL),(123,NULL,NULL,'Collector','Farmed',1,1,3,NULL,0,NULL),(124,NULL,NULL,'Collector-Seed','Farmed',1,1,3,NULL,0,NULL),(125,NULL,NULL,'Dam','Farmed',1,1,3,NULL,0,NULL),(126,NULL,NULL,'Farming - Integrated','Farmed',1,1,3,NULL,0,NULL),(127,NULL,NULL,'Hatchery','Farmed',1,1,3,NULL,0,NULL),(128,NULL,NULL,'Lagoon','Farmed',1,1,3,NULL,0,NULL),(129,NULL,NULL,'Lake','Farmed',1,1,3,NULL,0,NULL),(130,NULL,NULL,'Paddy Field','Farmed',1,1,3,NULL,0,NULL),(131,NULL,NULL,'Ranching','Farmed',1,1,3,NULL,0,NULL),(132,NULL,NULL,'Silo','Farmed',1,1,3,NULL,0,NULL),(133,NULL,NULL,'Tank, closed','Farmed',1,1,4,NULL,0,NULL),(134,NULL,NULL,'Cage, fixed','Farmed',1,1,5,NULL,0,NULL),(135,NULL,NULL,'Cage, floating','Farmed',1,1,5,NULL,0,NULL),(136,NULL,NULL,'Pen (general)','Farmed',1,1,6,NULL,0,NULL),(137,NULL,NULL,'Raceway (general)','Farmed',1,1,7,NULL,0,NULL),(138,NULL,NULL,'Towed dredges','Wild',1,1,8,NULL,0,NULL),(139,NULL,NULL,'Hand dredges','Wild',1,1,8,NULL,0,NULL),(140,NULL,NULL,'Mechanized dredges','Wild',1,1,8,NULL,0,NULL),(141,NULL,NULL,'Dredges (unspecified)','Wild',1,1,8,NULL,0,NULL),(142,NULL,NULL,'Cast nets','Wild',1,1,9,NULL,0,NULL),(143,NULL,NULL,'Cover pots/Lantern nets','Wild',1,1,9,NULL,0,NULL),(144,NULL,NULL,'Falling gear (unspecified)','Wild',1,1,9,NULL,0,NULL),(145,NULL,NULL,'Set gillnets','Wild',1,1,10,NULL,0,NULL),(146,NULL,NULL,'Drift gillnets (driftnets)','Wild',1,1,10,NULL,0,NULL),(147,NULL,NULL,'Encircling gillnets','Wild',1,1,10,NULL,0,NULL),(148,NULL,NULL,'Fixed gillnets (on stakes)','Wild',1,1,10,NULL,0,NULL),(149,NULL,NULL,'Trammel nets','Wild',1,1,10,NULL,0,NULL),(150,NULL,NULL,'Combined gillnets - trammel nets','Wild',1,1,10,NULL,0,NULL),(151,NULL,NULL,'Gillnets and entangling nets (unspecified)','Wild',1,1,10,NULL,0,NULL),(152,NULL,NULL,'Handlines and hand-operated pole-and lines','Wild',1,1,11,NULL,0,NULL),(153,NULL,NULL,'Mechanized lines and pole-and-lines','Wild',1,1,11,NULL,0,NULL),(154,NULL,NULL,'Set longlines','Wild',1,1,11,NULL,0,NULL),(155,NULL,NULL,'Drifting longlines','Wild',1,1,11,NULL,0,NULL),(156,NULL,NULL,'Trolling lines','Wild',1,1,11,NULL,0,NULL),(157,NULL,NULL,'Bottom longlines','Wild',1,1,11,NULL,0,NULL),(158,NULL,NULL,'Pelagic longline','Wild',1,1,11,NULL,0,NULL),(159,NULL,NULL,'Buoy gear','Wild',1,1,11,NULL,0,NULL),(161,NULL,NULL,'Longlines (unspecified)','Wild',1,1,11,NULL,0,NULL),(162,NULL,NULL,'Hooks and lines (unspecified)','Wild',1,1,11,NULL,0,NULL),(163,NULL,NULL,'Greenstick','Wild',1,1,11,NULL,0,NULL),(164,NULL,NULL,'Vertical lines','Wild',1,1,11,NULL,0,NULL),(165,NULL,NULL,'Portable lift nets','Wild',1,1,12,NULL,0,NULL),(166,NULL,NULL,'Boat-operated lift nets','Wild',1,1,12,NULL,0,NULL),(167,NULL,NULL,'Shore-operated stationary lift nets','Wild',1,1,12,NULL,0,NULL),(168,NULL,NULL,'Lift nets (unspecified)','Wild',1,1,12,NULL,0,NULL),(169,NULL,NULL,'Harpoons','Wild',1,1,11,NULL,0,NULL),(170,NULL,NULL,'Hand implements','Wild',1,1,13,NULL,0,NULL),(171,NULL,NULL,'Diving','Wild',1,1,13,NULL,0,NULL),(173,NULL,NULL,'Pumps','Wild',1,1,13,NULL,0,NULL),(174,NULL,NULL,'Electric Fishing','Wild',1,1,13,NULL,0,NULL),(175,NULL,NULL,'Pushnets','Wild',1,1,13,NULL,0,NULL),(176,NULL,NULL,'Scoopnets','Wild',1,1,13,NULL,0,NULL),(177,NULL,NULL,'Drive-in nets','Wild',1,1,13,NULL,0,NULL),(178,NULL,NULL,'Gear not listed','Wild',1,1,13,NULL,0,NULL),(179,NULL,NULL,'Pushed butterfly net','Wild',1,1,13,NULL,0,NULL),(180,NULL,NULL,'Pushed skimmer net','Wild',1,1,13,NULL,0,NULL),(181,NULL,NULL,'Beach seines','Wild',1,1,14,NULL,0,NULL),(182,NULL,NULL,'Boat seines','Wild',1,1,14,NULL,0,NULL),(183,NULL,NULL,'Seine nets (unspecified)','Wild',1,1,14,NULL,0,NULL),(184,NULL,NULL,'Purse seines','Wild',1,1,15,NULL,0,NULL),(185,NULL,NULL,'Dolphin set purse seine','Wild',1,1,15,NULL,0,NULL),(186,NULL,NULL,'Floating Object purse seine (FAD)','Wild',1,1,15,NULL,0,NULL),(187,NULL,NULL,'Unassociated purse seine (non-FAD)','Wild',1,1,15,NULL,0,NULL),(189,NULL,NULL,'Surrounding nets without purse lines','Wild',1,1,15,NULL,0,NULL),(190,NULL,NULL,'Lampara','Wild',1,1,15,NULL,0,NULL),(191,NULL,NULL,'Surrounding nets (unspecified)','Wild',1,1,15,NULL,0,NULL),(192,NULL,NULL,'Stationary uncovered pound nets','Wild',1,1,16,NULL,0,NULL),(193,NULL,NULL,'Pots','Wild',1,1,16,NULL,0,NULL),(194,NULL,NULL,'Fyke nets','Wild',1,1,16,NULL,0,NULL),(195,NULL,NULL,'Stow nets','Wild',1,1,16,NULL,0,NULL),(196,NULL,NULL,'Barriers, fences, weiers, corrals, etc.','Wild',1,1,16,NULL,0,NULL),(197,NULL,NULL,'Aerial traps','Wild',1,1,16,NULL,0,NULL),(198,NULL,NULL,'Crab Rings','Wild',1,1,16,NULL,0,NULL),(199,NULL,NULL,'Traps (unspecified)','Wild',1,1,16,NULL,0,NULL),(200,NULL,NULL,'Bottom trawls','Wild',1,1,17,NULL,0,NULL),(201,NULL,NULL,'Midwater trawls','Wild',1,1,17,NULL,0,NULL),(202,NULL,NULL,'Semipelagic trawls','Wild',1,1,17,NULL,0,NULL),(203,NULL,NULL,'Otter trawls','Wild',1,1,17,NULL,0,NULL),(204,NULL,NULL,'Trawls (unspecified)','Wild',1,1,17,NULL,0,NULL),(206,NULL,NULL,'Beam trawls','Wild',1,1,17,NULL,0,NULL),(207,NULL,NULL,'Single boat bottom otter trawls','Wild',1,1,17,NULL,0,NULL),(208,NULL,NULL,'Twin bottom otter trawls','Wild',1,1,17,NULL,0,NULL),(209,NULL,NULL,'Multiple bottom otter trawls','Wild',1,1,17,NULL,0,NULL),(210,NULL,NULL,'Bottom pair trawls','Wild',1,1,17,NULL,0,NULL),(211,NULL,NULL,'Single boat midwater otter trawls','Wild',1,1,17,NULL,0,NULL),(212,NULL,NULL,'Midwater pair trawls','Wild',1,1,17,NULL,0,NULL),(213,NULL,NULL,'Magdalena - Artisanal bottom trawl','Wild',1,1,17,NULL,0,NULL),(214,NULL,NULL,'Gear Type Unknown','Wild',1,1,18,NULL,0,NULL);
/*!40000 ALTER TABLE `harvestMethods` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `methodCategories`
--

DROP TABLE IF EXISTS `methodCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `methodCategories` (
  `methodCategory` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `methodCategoryForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `wild_farmed` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_FM_id` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `methodcategories_wild_farmed_index` (`wild_farmed`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `methodCategories`
--

LOCK TABLES `methodCategories` WRITE;
/*!40000 ALTER TABLE `methodCategories` DISABLE KEYS */;
INSERT INTO `methodCategories` (`methodCategory`, `methodCategoryForDisplay`, `excludeFromDropdown`, `id`, `created_at`, `updated_at`, `wild_farmed`, `fk_FM_id`) VALUES ('Pond',NULL,0,1,NULL,NULL,'Farmed',NULL),('Shellfish',NULL,0,2,NULL,NULL,'Farmed',NULL),('Other',NULL,0,3,NULL,NULL,'Farmed',NULL),('Tank',NULL,0,4,NULL,NULL,'Farmed',NULL),('Cage',NULL,0,5,NULL,NULL,'Farmed',NULL),('Pen',NULL,0,6,NULL,NULL,'Farmed',NULL),('Raceway',NULL,0,7,NULL,NULL,'Farmed',NULL),('Dredges',NULL,0,8,NULL,NULL,'Wild',NULL),('Falling Gear',NULL,0,9,NULL,NULL,'Wild',NULL),('Gillnets and Entangling Nets',NULL,0,10,NULL,NULL,'Wild',NULL),('Hooks and Lines',NULL,0,11,NULL,NULL,'Wild',NULL),('Lift nets',NULL,0,12,NULL,NULL,'Wild',NULL),('Miscellaneous gear',NULL,0,13,NULL,NULL,'Wild',NULL),('Seine nets',NULL,0,14,NULL,NULL,'Wild',NULL),('Surrounding nets',NULL,0,15,NULL,NULL,'Wild',NULL),('Traps',NULL,0,16,NULL,NULL,'Wild',NULL),('Trawls',NULL,0,17,NULL,NULL,'Wild',NULL),('Gear Type Unknown',NULL,0,18,NULL,NULL,'Wild',NULL);
/*!40000 ALTER TABLE `methodCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`migration`, `batch`) VALUES ('2014_10_12_000000_create_users_table',1),('2014_10_12_100000_create_password_resets_table',1),('2016_07_25_171245_create_survey_table',1),('2016_07_25_192936_make_vendor_table',1),('2016_07_25_214644_create_product_table',1),('2016_07_25_215102_create_sp_tabe',1),('2016_07_25_215333_create_sps_tabe',1),('2016_07_25_221030_create_source_table',1),('2016_07_25_224323_create_gear_types_table',1),('2016_07_25_224613_create_fish_table',1),('2016_07_25_224800_create_fish_categories_table',1),('2016_07_26_180742_create_retailers_table',1),('2016_07_26_180932_update_retailer_table',1),('2016_07_26_215523_add_userid_to_tables',1),('2016_07_27_163435_modify_users_table',1),('2016_07_27_175947_create_ngos_table',1),('2016_08_09_170213_modify_fish',2),('2016_08_09_182825_naming_conventions',3),('2016_08_10_001224_fix_ids',4),('2016_08_10_173200_update_method_categories',5),('2016_08_16_002928_update_source_table',6),('2016_08_16_164024_update_sources_table',7),('2016_08_16_195736_add_sci_name',8),('2016_08_16_203053_add_rfmo',9),('2016_08_17_195840_update_vendor_table',10),('2016_08_17_200358_update_vendor_table2',11),('2016_08_17_200704_update_vendor_table3',12),('2016_08_17_214809_update_retailer',13),('2016_08_17_221142_add_show',14),('2016_08_22_153437_update_sps',15);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ngos`
--

DROP TABLE IF EXISTS `ngos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ngos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_street_1` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_street_2` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_state_prov` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_post_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `add_country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_by` int(11) DEFAULT NULL,
  `modified_by` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ngos`
--

LOCK TABLES `ngos` WRITE;
/*!40000 ALTER TABLE `ngos` DISABLE KEYS */;
INSERT INTO `ngos` (`id`, `created_at`, `updated_at`, `name`, `contact_name`, `contact_email`, `contact_phone`, `add_street_1`, `add_street_2`, `add_city`, `add_state_prov`, `add_post_code`, `add_country`, `created_by`, `modified_by`) VALUES (1,'2016-08-09 16:21:23','2016-08-09 16:21:54','FishWise','Ryan Manzer','r.manzer@fishwise.org','831-460-6123','500 Seabright Ave',NULL,NULL,'CA',NULL,'USA',1,1);
/*!40000 ALTER TABLE `ngos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`),
  KEY `password_resets_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES ('r.manzer@fishwise.org','a61af475c9131a3ace73ea802936c0733e02e43197648287db5dc8b40716ea51','2016-08-24 21:19:54');
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `products`
--

DROP TABLE IF EXISTS `products`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fk_vendor_id` int(11) NOT NULL,
  `fk_retailer_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_hvdb_id` mediumint(9) DEFAULT NULL,
  `retailerProductDescription` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailerProductCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `retailerProductBrand` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_seafoodCategory_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendorProductCode` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendorCoCType` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `vendorCoCNumber` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_seafoodSpecies_id` mediumint(9) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=367 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `products`
--

LOCK TABLES `products` WRITE;
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `created_at`, `updated_at`, `fk_vendor_id`, `fk_retailer_id`, `created_by`, `modified_by`, `fk_hvdb_id`, `retailerProductDescription`, `retailerProductCode`, `retailerProductBrand`, `fk_seafoodCategory_id`, `vendorProductCode`, `vendorCoCType`, `vendorCoCNumber`, `fk_seafoodSpecies_id`) VALUES (1,NULL,'2016-08-18 15:51:38',1,1,0,0,NULL,'CARP NATURAL FIL 2LB',NULL,NULL,'14','12346',NULL,NULL,22),(2,NULL,NULL,2,2,0,0,NULL,'ANCHOVY FIL 6OZ',NULL,NULL,NULL,'12346',NULL,NULL,NULL),(3,NULL,NULL,1,1,1,1,NULL,'LOUISIANA CRAWFISH CKD IQF',NULL,'Crawfish','23','05598',NULL,NULL,56),(4,NULL,NULL,1,1,1,1,NULL,'SPINY LOBSTER TAILS 20 OZ & UP',NULL,'Lobster','38','05450',NULL,NULL,78),(5,NULL,NULL,1,1,1,1,NULL,'SPINY LOBSTER TAILS 5 OZ',NULL,'Lobster','38','05452',NULL,NULL,78),(6,NULL,NULL,1,1,1,1,NULL,'SPINY LOBSTER TAILS 8 OZ',NULL,'Lobster','38','05454',NULL,NULL,78),(7,NULL,NULL,1,1,1,1,NULL,'SPINY LOBSTER TAILS 12/14 OZ',NULL,'Lobster','38','05456',NULL,NULL,78),(8,NULL,NULL,1,1,1,1,NULL,'SPINY LOBSTERS TAILS 16-20 OZ',NULL,'Lobster','38','05458',NULL,NULL,78),(9,NULL,NULL,1,1,1,1,NULL,'BREADED SCALLOPS RETAIL BOX',NULL,'Scallop','66','05466',NULL,NULL,120),(10,NULL,NULL,1,1,1,1,NULL,'GREEN LIP MUSSELS IQF 2 LB',NULL,'Mussel','46','05469',NULL,NULL,92),(11,NULL,NULL,1,1,1,1,NULL,'POLLOCK IQF 1 LB BAG 2/4 CT',NULL,'Pollock','57','05672',NULL,NULL,106),(12,NULL,NULL,1,1,1,1,NULL,'CRAB CAKE IMITATION MARYLAND 4 Z',NULL,'Pollock','57','06131',NULL,NULL,45),(13,NULL,NULL,1,1,1,1,NULL,'WHITING H&G FRZ',NULL,'Hake','32','06663',NULL,NULL,67),(14,NULL,NULL,1,1,1,1,NULL,'RAINBOW TROUT NATURAL FIL 10 LB',NULL,'Trout','88','05264',NULL,NULL,169),(15,NULL,NULL,1,1,1,1,NULL,'RAINBOW TROUT NATURAL FIL 5 LB',NULL,'Trout','88','05265',NULL,NULL,169),(16,NULL,NULL,1,1,1,1,NULL,'RAINBOW TROUT PARMESAN CRUSTED',NULL,'Trout','88','05778',NULL,NULL,169),(17,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE FARMED 7-13 10#',NULL,'Catfish','15','05160',NULL,NULL,25),(18,NULL,NULL,1,1,1,1,NULL,'CATFISH NUGGETS 15 LB',NULL,'Catfish','15','05162',NULL,NULL,25),(19,NULL,NULL,1,1,1,1,NULL,'CATFISH FRESH FIL 5-12 OZ  15 LB',NULL,'Catfish','15','05167',NULL,NULL,25),(20,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE BULK 7/13 OZ',NULL,'Catfish','15','05169',NULL,NULL,25),(21,NULL,NULL,1,1,1,1,NULL,'CATFISH NUGGETS IQF 10 LB',NULL,'Catfish','15','05260',NULL,NULL,25),(22,NULL,NULL,1,1,1,1,NULL,'CATFISH NUGGET ICE PACK',NULL,'Catfish','15','05261',NULL,NULL,25),(23,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE IQF  7-9Z',NULL,'Catfish','15','05902',NULL,NULL,25),(24,NULL,NULL,1,1,1,1,NULL,'CATFISH WHL FARMED IQF 11-13 15#',NULL,'Catfish','15','05904',NULL,NULL,25),(25,NULL,NULL,1,1,1,1,NULL,'CATFISH WHL FARMED IQF 15-17 15#',NULL,'Catfish','15','05910',NULL,NULL,25),(26,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE 9/11 IQF BREADED',NULL,'Catfish','15','05911',NULL,NULL,25),(27,NULL,NULL,1,1,1,1,NULL,'IQF SOUTHRN BRD 4 OZ CATFISH FLT',NULL,'Catfish','15','05913',NULL,NULL,25),(28,NULL,NULL,1,1,1,1,NULL,'CATFISH FIL IQF  7-9Z',NULL,'Catfish','15','05916',NULL,NULL,25),(29,NULL,NULL,1,1,1,1,NULL,'BREADED CATFISH NUGGETS',NULL,'Catfish','15','05930',NULL,NULL,25),(30,NULL,NULL,1,1,1,1,NULL,'CATFISH FILLETS FARMED 5/12 10#',NULL,'Catfish','15','05932',NULL,NULL,25),(31,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE FARMED 7-9 15#',NULL,'Catfish','15','05946',NULL,NULL,25),(32,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE FARMED 11-13 15#',NULL,'Catfish','15','05948',NULL,NULL,25),(33,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE FARMED 15-17 15#',NULL,'Catfish','15','05950',NULL,NULL,25),(34,NULL,NULL,1,1,1,1,NULL,'CATFISH WHOLE 32/UP',NULL,'Catfish','15','05958',NULL,NULL,25),(35,NULL,NULL,1,1,1,1,NULL,'HALIBUT FILLETS  10#',NULL,'Flatfish','28','05496',NULL,NULL,71),(36,NULL,NULL,1,1,1,1,NULL,'HALIBUT WHOLE STEAK RDY 1-3 FISH',NULL,'Flatfish','28','05497',NULL,NULL,71),(37,NULL,NULL,1,1,1,1,NULL,'SALMON ARCTIC KETA FIL CHUM 25LB',NULL,'Salmon','63','05506',NULL,NULL,114),(38,NULL,NULL,1,1,1,1,NULL,'SALMON WILD KING FILLETS 25#',NULL,'Salmon','63','05508',NULL,NULL,113),(39,NULL,NULL,1,1,1,1,NULL,'SALMON - COHO FILLET  25#',NULL,'Salmon','63','05592',NULL,NULL,112),(40,NULL,NULL,1,1,1,1,NULL,'SALMON SOCKEYE WHOLE 25#',NULL,'Salmon','63','05593',NULL,NULL,116),(41,NULL,NULL,1,1,1,1,NULL,'SALMON SOCKEYE FIL 25LB',NULL,'Salmon','63','05595',NULL,NULL,116),(42,NULL,NULL,1,1,1,1,NULL,'SOCKEYE SALMON FIL IQF VP 1-3 LB',NULL,'Salmon','63','06845',NULL,NULL,116),(43,NULL,NULL,1,1,1,1,NULL,'OYSTERS COUNT 1 GALLON',NULL,'Oyster','52','06553',NULL,NULL,96),(44,NULL,NULL,1,1,1,1,NULL,'OYSTERS STANDARD  1 GALLON',NULL,'Oyster','52','06557',NULL,NULL,96),(45,NULL,NULL,1,1,1,1,NULL,'OYSTERS SELECT  1 GALLON',NULL,'Oyster','52','06559',NULL,NULL,96),(46,NULL,NULL,1,1,1,1,NULL,'SOFTSHELL CRAB/CLEANED PRIME FRZ',NULL,'Crab','22','05194',NULL,NULL,45),(47,NULL,NULL,1,1,1,1,NULL,'OYSTERS STANDARD  8 OZ',NULL,'Oyster','52','06558',NULL,NULL,96),(48,NULL,NULL,1,1,1,1,NULL,'OYSTERS SELECT - 8 OZ PACK',NULL,'Oyster','52','06560',NULL,NULL,96),(49,NULL,NULL,1,1,1,1,NULL,'OYSTERS  SELECT - 16 OZ PACK',NULL,'Oyster','52','06561',NULL,NULL,96),(50,NULL,NULL,1,1,1,1,NULL,'OYSTERS STANDARD - 16 OZ PACK',NULL,'Oyster','52','06567',NULL,NULL,96),(51,NULL,NULL,1,1,1,1,NULL,'SUMMER HERB CRUSTED COD 5-6Z',NULL,'Cod','19','05784',NULL,NULL,41),(52,NULL,NULL,1,1,1,1,NULL,'MEDITERRAN CRUSTED SALMON 5-6Z',NULL,'Salmon','63','05785',NULL,NULL,115),(53,NULL,NULL,1,1,1,1,NULL,'IQF COD LOINS 6 OZ (PACIFIC)',NULL,'Cod','19','06658',NULL,NULL,41),(54,NULL,NULL,1,1,1,1,NULL,'BLUE STAR LUMP CRAB MEAT PCH',NULL,'Crab','22','06134',NULL,NULL,46),(55,NULL,NULL,1,1,1,1,NULL,'BLUE STAR JBO LUMP CRAB MEAT PCH',NULL,'Crab','22','06135',NULL,NULL,46),(56,NULL,NULL,1,1,1,1,NULL,'BLUE STAR SUP LUMP CRAB MEAT PCH',NULL,'Crab','22','06136',NULL,NULL,46),(57,NULL,NULL,1,1,1,1,NULL,'BLUE STAR CLAW CRAB MEAT PCH',NULL,'Crab','22','06137',NULL,NULL,46),(58,NULL,NULL,1,1,1,1,NULL,'BLUE STAR LUMP CRAB MEAT 12/1 LB',NULL,'Crab','22','06140',NULL,NULL,46),(59,NULL,NULL,1,1,1,1,NULL,'MUSSELS (PRINCE EDWARD ISLANDS)',NULL,'Mussel','46','06223',NULL,NULL,92),(60,NULL,NULL,1,1,1,1,NULL,'OYSTERS IN SHELL/PRINCE EDW ISL',NULL,'Oyster','52','06571',NULL,NULL,96),(61,NULL,NULL,1,1,1,1,NULL,'PERCH-YELLOW BFLY FLT 10#',NULL,'Perch','55','05547',NULL,NULL,101),(62,NULL,NULL,1,1,1,1,NULL,'WALLEYE FIL SK-ON 12/16',NULL,'Walleye','91','05548',NULL,NULL,185),(63,NULL,NULL,1,1,1,1,NULL,'SMELTS IQF H&G',NULL,'Smelt','76','05721',NULL,NULL,140),(64,NULL,NULL,1,1,1,1,NULL,'WALLEYE FIL IQF 12-14 OZ',NULL,'Walleye','91','06681',NULL,NULL,185),(65,NULL,NULL,1,1,1,1,NULL,'WHL CKD FRZ AMER LOBSTER',NULL,'Lobster','38','05807',NULL,NULL,77),(66,NULL,NULL,1,1,1,1,NULL,'LOBSTER MEAT CKD CKL 12 OZ BAG',NULL,'Lobster','38','05810',NULL,NULL,77),(67,NULL,NULL,1,1,1,1,NULL,'MOREYS WHOLE SMOKED SALMON FRZN',NULL,'Salmon','63','06424',NULL,NULL,114),(68,NULL,NULL,1,1,1,1,NULL,'MOREYS SMK SALMON WHOLE 5 LB',NULL,'Salmon','63','06426',NULL,NULL,114),(69,NULL,NULL,1,1,1,1,NULL,'MOREYS SMK SALMON CHUNKS TP',NULL,'Salmon','63','06428',NULL,NULL,114),(70,NULL,NULL,1,1,1,1,NULL,'MOREYS CLASSIC SALMON FULL FILET',NULL,'Salmon','63','06450',NULL,NULL,114),(71,NULL,NULL,1,1,1,1,NULL,'MOREYS SMK SALMON CLASSIC PARTY',NULL,'Salmon','63','06454',NULL,NULL,114),(72,NULL,NULL,1,1,1,1,NULL,'MOREYS SMK SALMON PEPPERED PARTY',NULL,'Salmon','63','06455',NULL,NULL,114),(73,NULL,NULL,1,1,1,1,NULL,'MOREYS SMK NOVA LOX',NULL,'Salmon','63','06460',NULL,NULL,114),(74,NULL,NULL,1,1,1,1,NULL,'MOREYS PRESLI CLASS SMK SALMON',NULL,'Salmon','63','06465',NULL,NULL,114),(75,NULL,NULL,1,1,1,1,NULL,'MOREYS PRESLI PEPP SMK SALMON',NULL,'Salmon','63','06466',NULL,NULL,114),(76,NULL,NULL,1,1,1,1,NULL,'MOREYS CLASS SALMON VP',NULL,'Salmon','63','06474',NULL,NULL,114),(77,NULL,NULL,1,1,1,1,NULL,'MOREYS TOMATO BASIL SALMON VP',NULL,'Salmon','63','06475',NULL,NULL,114),(78,NULL,NULL,1,1,1,1,NULL,'MOREYS PEPP SALMON VP',NULL,'Salmon','63','06476',NULL,NULL,114),(79,NULL,NULL,1,1,1,1,NULL,'MOREYS CAJUN SALMON VP',NULL,'Salmon','63','06478',NULL,NULL,114),(80,NULL,NULL,1,1,1,1,NULL,'MOREYS ALASKAN SMK SALMON NUGGET',NULL,'Salmon','63','06479',NULL,NULL,114),(81,NULL,NULL,1,1,1,1,NULL,'MOREYS VARIETY SALMON VP',NULL,'Salmon','63','06482',NULL,NULL,114),(82,NULL,NULL,1,1,1,1,NULL,'OYSTERS IN THE SHELL/GOLD BAND',NULL,'Oyster','52','06572',NULL,NULL,96),(83,NULL,NULL,1,1,1,1,NULL,'MATLAW GOURMET STUFFED CLAM BULK',NULL,'Clam','17','06033',NULL,NULL,31),(84,NULL,NULL,1,1,1,1,NULL,'MATLAW GOURMET STUFD SCALLOP BUL',NULL,'Scallop','66','06034',NULL,NULL,120),(85,NULL,NULL,1,1,1,1,NULL,'MT COOK IVP KING SALMON FIL U/1#',NULL,'Salmon','63','06855',NULL,NULL,113),(86,NULL,NULL,1,1,1,1,NULL,'MT COOK IVP KING SALMON PORT 5Z',NULL,'Salmon','63','06857',NULL,NULL,113),(87,NULL,NULL,1,1,1,1,NULL,'SWORDFISH LOINS 5-8LB COV',NULL,'Swordfish','83','05620',NULL,NULL,162),(88,NULL,NULL,1,1,1,1,NULL,'SNAPPER FILLETS IVP',NULL,'Snapper','77','06886',NULL,NULL,NULL),(89,NULL,NULL,1,1,1,1,NULL,'SEA SCALLOPS DRY IQF 10/20 CT',NULL,'Scallop','66','06224',NULL,NULL,121),(90,NULL,NULL,1,1,1,1,NULL,'SEA SCALLOPS U/10    1 GAL',NULL,'Scallop','66','06225',NULL,NULL,121),(91,NULL,NULL,1,1,1,1,NULL,'BAY SCALLOPS DRY IQF 80/100',NULL,'Scallop','66','06226',NULL,NULL,120),(92,NULL,NULL,1,1,1,1,NULL,'BAY SCALLOPS PREV FROZEN-1 GAL',NULL,'Scallop','66','06228',NULL,NULL,120),(93,NULL,NULL,1,1,1,1,NULL,'SEA SCALLOPS DRY 10/20 CT 1 GAL',NULL,'Scallop','66','06230',NULL,NULL,121),(94,NULL,NULL,1,1,1,1,NULL,'FRESH LUTEFISK SKNLS BULK',NULL,'Cod','19','06241',NULL,NULL,75),(95,NULL,NULL,1,1,1,1,NULL,'LUTEFISK SKNLS  FILLET 12/1 3/4#',NULL,'Cod','19','06245',NULL,NULL,75),(96,NULL,NULL,1,1,1,1,NULL,'SHRIMP-WILD EZ PEEL USA 10/15 CT',NULL,'Shrimp','74','05310',NULL,NULL,136),(97,NULL,NULL,1,1,1,1,NULL,'SHRIMP-WILD EZ PEEL 16/20 1#BAG',NULL,'Shrimp','74','05317',NULL,NULL,136),(98,NULL,NULL,1,1,1,1,NULL,'SHRIMP WILD HDLESS 31/40 1# BAG',NULL,'Shrimp','74','05318',NULL,NULL,136),(99,NULL,NULL,1,1,1,1,NULL,'SHRIMP-WILD USA EZ PEEL 16/20 CT',NULL,'Shrimp','74','05320',NULL,NULL,136),(100,NULL,NULL,1,1,1,1,NULL,'SHRIMP WILD EZ PEEL USA 21-30CT',NULL,'Shrimp','74','05328',NULL,NULL,136),(101,NULL,NULL,1,1,1,1,NULL,'HADDOCK FIL SKNLS 10# (PREV FRZ)',NULL,'Haddock','31','06200',NULL,NULL,64),(102,NULL,NULL,1,1,1,1,NULL,'COD FILLET ALASKA PREV FRZ 10 LB',NULL,'Cod','19','06204',NULL,NULL,41),(103,NULL,NULL,1,1,1,1,NULL,'COD FILLET ALASKA PREV FRZ 5 LB',NULL,'Cod','19','06205',NULL,NULL,41),(104,NULL,NULL,1,1,1,1,NULL,'FLOUNDER FIL/EAST COAST 10 LB',NULL,'Flatfish','28','06218',NULL,NULL,61),(105,NULL,NULL,1,1,1,1,NULL,'TILAPIA LOINS IQF 3-4 OZ',NULL,'Tilapia','85','05413',NULL,NULL,164),(106,NULL,NULL,1,1,1,1,NULL,'SINGLETN CAJUN SMOKED SALMON 5LB',NULL,'Salmon','63','45636',NULL,NULL,106),(107,NULL,NULL,1,1,1,1,NULL,'SINGL ITALIAN SEAFOOD SALAD 5LB',NULL,'Pollock','57','45637',NULL,NULL,106),(108,NULL,NULL,1,1,1,1,NULL,'SINGLETON CAJUN KRAB SPREAD',NULL,'Pollock','57','45639',NULL,NULL,106),(109,NULL,NULL,1,1,1,1,NULL,'YELLOWFIN TUNA LOIN IVP 5-8#',NULL,'Tuna','89','05611',NULL,NULL,182),(110,NULL,NULL,1,1,1,1,NULL,'YELLOWFIN TUNA STEAKS IVP 5 OZ',NULL,'Tuna','89','05615',NULL,NULL,182),(111,NULL,NULL,1,1,1,1,NULL,'YELLOWFIN TUNA LOIN IVP 3-5#',NULL,'Tuna','89','05616',NULL,NULL,182),(112,NULL,NULL,1,1,1,1,NULL,'MAHI MAHI SKIN-ON FLT 3-5# IQF',NULL,'Mahi Mahi','41','05612',NULL,NULL,83),(113,NULL,NULL,1,1,1,1,NULL,'CLAMS LITTLE NECK 2.5 BUSHEL',NULL,'Clam','17','06106',NULL,NULL,226),(114,NULL,NULL,1,1,1,1,NULL,'CLAMS LITTLE NECK 8-10CT/1LB',NULL,'Clam','17','06110',NULL,NULL,226),(115,NULL,NULL,1,1,1,1,NULL,'CLAMS LITTLE NECK 8-10CT/1LB',NULL,'Clam','17','06112',NULL,NULL,226),(116,NULL,NULL,1,1,1,1,NULL,'CLAMS CHERRYSTONE 3CT/1LB',NULL,'Clam','17','06118',NULL,NULL,226),(117,NULL,NULL,1,1,1,1,NULL,'CLAMS CHERRYSTONE 3CT/1LB',NULL,'Clam','17','06120',NULL,NULL,226),(118,NULL,NULL,1,1,1,1,NULL,'ALASKAN SNOW LEGS IMITATION CRAB',NULL,'Mixed','43','45620',NULL,NULL,106),(119,NULL,NULL,1,1,1,1,NULL,'IQF WHOLE HALIBUT  10-20#',NULL,'Flatfish','28','05172',NULL,NULL,71),(120,NULL,NULL,1,1,1,1,NULL,'16/20 TAIL-ON SHRIMP RAW P&D',NULL,'Shrimp','74','05441',NULL,NULL,136),(121,NULL,NULL,1,1,1,1,NULL,'CRAB WHL CKD DUNGENESS',NULL,'Crab','22','05848',NULL,NULL,47),(122,NULL,NULL,1,1,1,1,NULL,'COCKTAIL CRAB CLAWS 21/25 CAPOFF',NULL,'Crab','22','06234',NULL,NULL,53),(123,NULL,NULL,1,1,1,1,NULL,'COD FILLET IQF 16-32 (TORSK)',NULL,'Cod','19','06657',NULL,NULL,41),(124,NULL,NULL,1,1,1,1,NULL,'CLAM STRIPS RAW MEAT',NULL,'Clam','17','06104',NULL,NULL,27),(125,NULL,NULL,1,1,1,1,NULL,'SMOKED SEASONED SALMON GIFT BOX',NULL,'Salmon','63','05513',NULL,NULL,115),(126,NULL,NULL,1,1,1,1,NULL,'WHOLE ALASKA KING CRAB',NULL,'Crab','22','06249',NULL,NULL,49),(127,NULL,NULL,1,1,1,1,NULL,'ALASKA GOLDEN KING CRAB 20/24 CT',NULL,'Crab','22','06255',NULL,NULL,49),(128,NULL,NULL,1,1,1,1,NULL,'L KEMP LOBSTER DELIGHT CHUNK',NULL,'Pollock','57','45647',NULL,NULL,106),(129,NULL,NULL,1,1,1,1,NULL,'L KEMP FRESH CRAB DELIGHT LEG',NULL,'Pollock','57','45650',NULL,NULL,106),(130,NULL,NULL,1,1,1,1,NULL,'L KEMP CRAB DEL CHESAPEAKE BAY',NULL,'Pollock','57','45652',NULL,NULL,106),(131,NULL,NULL,1,1,1,1,NULL,'L KEMP FRESH CRAB DELIGHT CHUNK',NULL,'Pollock','57','45653',NULL,NULL,106),(132,NULL,NULL,1,1,1,1,NULL,'L KEMP FRESH CRAB DELIGHT FLAKE',NULL,'Pollock','57','45656',NULL,NULL,106),(133,NULL,NULL,1,1,1,1,NULL,'TILAPIA FILLET 10 LB',NULL,'Tilapia','85','05556',NULL,NULL,164),(134,NULL,NULL,1,1,1,1,NULL,'TILAPIA FILLET  5 LB',NULL,'Tilapia','85','05557',NULL,NULL,164),(135,NULL,NULL,1,1,1,1,NULL,'TILAPIA WHOLE HEAD ON 10 LB',NULL,'Tilapia','85','05577',NULL,NULL,164),(136,NULL,NULL,1,1,1,1,NULL,'RAINBOW TROUT SUN DRI TOM/GARLIC',NULL,'Trout','88','05777',NULL,NULL,169),(137,NULL,NULL,1,1,1,1,NULL,'RAINBOW TROUT NAT WHL 12-16 OZ',NULL,'Trout','88','05267',NULL,NULL,169),(138,NULL,NULL,1,1,1,1,NULL,'MT COOK KING SALMON FIL IVP 2-3#',NULL,'Salmon','63','06856',NULL,NULL,113),(139,NULL,NULL,1,1,1,1,NULL,'MT COOK KING SALMON PORT IVP 10Z',NULL,'Salmon','63','06858',NULL,NULL,113),(140,NULL,NULL,1,1,1,1,NULL,'POLLOCK FILLETS IQF 4-6 OZ',NULL,'Pollock','57','05604',NULL,NULL,106),(141,NULL,NULL,1,1,1,1,NULL,'POLLOCK FILLETS IQF 2-4 OZ',NULL,'Pollock','57','05608',NULL,NULL,106),(142,NULL,NULL,1,1,1,1,NULL,'SB COOKED SHRIMP TAIL-ON 51/60CT',NULL,'Shrimp','74','05731',NULL,NULL,136),(143,NULL,NULL,1,1,1,1,NULL,'LOBSTER SEAFOOD STUFFING (CAKES)',NULL,'Lobster','38','06163',NULL,NULL,77),(144,NULL,NULL,1,1,1,1,NULL,'CRAB SEAFOOD STUFFING (CAKES)',NULL,'Crab','22','06164',NULL,NULL,45),(145,NULL,NULL,1,1,1,1,NULL,'ALASKA POLLOCK FIL IQF 4-6 OZ',NULL,'Pollock','57','05610',NULL,NULL,106),(146,NULL,NULL,1,1,1,1,NULL,'ALASKA KING CRAB LEGS 14/17',NULL,'Crab','22','06242',NULL,NULL,49),(147,NULL,NULL,1,1,1,1,NULL,'GOLDEN KING CRAB BITES',NULL,'Crab','22','06257',NULL,NULL,49),(148,NULL,NULL,1,1,1,1,NULL,'KETA SALMON FILLET IQF',NULL,'Salmon','63','06844',NULL,NULL,114),(149,NULL,NULL,1,1,1,1,NULL,'COHO SALMON MEAT',NULL,'Salmon','63','06854',NULL,NULL,112),(150,NULL,NULL,1,1,1,1,NULL,'HALIBUT CHEEKS 10#',NULL,'Flatfish','28','05494',NULL,NULL,71),(151,NULL,NULL,1,1,1,1,NULL,'HALIBUT WHOLE FRESH HEAD ON',NULL,'Flatfish','28','05498',NULL,NULL,71),(152,NULL,NULL,1,1,1,1,NULL,'SALMON SOCKEYE-WHOLE COPPER RVR',NULL,'Salmon','63','05502',NULL,NULL,116),(153,NULL,NULL,1,1,1,1,NULL,'SALMON SOCKEYE-FILLET COPPER RVR',NULL,'Salmon','63','05503',NULL,NULL,116),(154,NULL,NULL,1,1,1,1,NULL,'SALMON COPPER RVR KING WHOLE 80#',NULL,'Salmon','63','05504',NULL,NULL,113),(155,NULL,NULL,1,1,1,1,NULL,'SALMON KING FILLET COPPER RIVER',NULL,'Salmon','63','05507',NULL,NULL,113),(156,NULL,NULL,1,1,1,1,NULL,'HV/FM YELLOWFIN TUNA STEAKS',NULL,'Tuna','89','05868',NULL,NULL,182),(157,NULL,NULL,1,1,1,1,NULL,'HV/FM TILAPIA FILLET',NULL,'Tilapia','85','05870',NULL,NULL,164),(158,NULL,NULL,1,1,1,1,NULL,'HV/FM MAHI MAHI FILLETS',NULL,'Mahi Mahi','41','05872',NULL,NULL,83),(159,NULL,NULL,1,1,1,1,NULL,'HV/FM WILD SALMON FILLETS',NULL,'Salmon','63','05874',NULL,NULL,114),(160,NULL,NULL,1,1,1,1,NULL,'HV/FM FLOUNDER FILLETS',NULL,'Flatfish','28','05875',NULL,NULL,157),(161,NULL,NULL,1,1,1,1,NULL,'HV/FM PACIFIC COD FILLETS',NULL,'Cod','19','05876',NULL,NULL,41),(162,NULL,NULL,1,1,1,1,NULL,'NAT BLACK TIGER SHRIMP EZ 6/8CT',NULL,'Shrimp','74','05433',NULL,NULL,136),(163,NULL,NULL,1,1,1,1,NULL,'NAT BLACK TIGER SHRIMP EZ 8/12CT',NULL,'Shrimp','74','05434',NULL,NULL,136),(164,NULL,NULL,1,1,1,1,NULL,'SEA CUIS HNY CHPTLE SALMON BULK',NULL,'Salmon','63','05782',NULL,NULL,115),(165,NULL,NULL,1,1,1,1,NULL,'SALMON FLT-SWEET & SPICY RUB',NULL,'Salmon','63','05783',NULL,NULL,115),(166,NULL,NULL,1,1,1,1,NULL,'PARM CRUSTED TILAPIA/TUSCAN HERB',NULL,'Tilapia','85','05789',NULL,NULL,164),(167,NULL,NULL,1,1,1,1,NULL,'SEA CUIS POTATO CRUSTED COD BULK',NULL,'Cod','19','05790',NULL,NULL,41),(168,NULL,NULL,1,1,1,1,NULL,'TORTILLA CRSTD TILAPIA CHPLTE 5Z',NULL,'Tilapia','85','05795',NULL,NULL,164),(169,NULL,NULL,1,1,1,1,NULL,'COCONUT CRSTD TILAPIA W/MANGO 5Z',NULL,'Tilapia','85','05796',NULL,NULL,164),(170,NULL,NULL,1,1,1,1,NULL,'TUNA YELLOWFIN SAKU AAA IVP',NULL,'Tuna','89','05613',NULL,NULL,182),(171,NULL,NULL,1,1,1,1,NULL,'SHRIMP WILD P&D 40/50 CT',NULL,'Shrimp','74','05311',NULL,NULL,136),(172,NULL,NULL,1,1,1,1,NULL,'PIAZZA SHRIMP WILD P&D 31/40 CT',NULL,'Shrimp','74','05315',NULL,NULL,136),(173,NULL,NULL,1,1,1,1,NULL,'CRAWFISH TAIL MEAT CKD 100/150CT',NULL,'Crawfish','23','05421',NULL,NULL,56),(174,NULL,NULL,1,1,1,1,NULL,'SHRIMP BELIZE 45/55 P&D NAT T/0',NULL,'Shrimp','74','05760',NULL,NULL,136),(175,NULL,NULL,1,1,1,1,NULL,'SHRIMP BELIZE 51/60 P&D NAT T/O',NULL,'Shrimp','74','05761',NULL,NULL,136),(176,NULL,NULL,1,1,1,1,NULL,'SHRIMP BELIZE 51/60 CKD NAT T/OF',NULL,'Shrimp','74','05762',NULL,NULL,136),(177,NULL,NULL,1,1,1,1,NULL,'SHRIMP BELIZE 31/40 EZP NAT BULK',NULL,'Shrimp','74','05769',NULL,NULL,136),(178,NULL,NULL,1,1,1,1,NULL,'SALMON ATLANTIC PORTION 5Z FRESH',NULL,'Salmon','63','05279',NULL,NULL,111),(179,NULL,NULL,1,1,1,1,NULL,'SALMON ATLANTIC FIL FARM 35LB',NULL,'Salmon','63','05281',NULL,NULL,111),(180,NULL,NULL,1,1,1,1,NULL,'SALMON ATLANTIC FIL FARM 10LB',NULL,'Salmon','63','05282',NULL,NULL,111),(181,NULL,NULL,1,1,1,1,NULL,'HV/FM HERRING WINE SCE 8Z',NULL,'Herring','33','90775',NULL,NULL,72),(182,NULL,NULL,1,1,1,1,NULL,'HV/FM HERRING WINE SCE 22Z',NULL,'Herring','33','90777',NULL,NULL,72),(183,NULL,NULL,1,1,1,1,NULL,'HV/FM HERRING DILL SCE 8Z',NULL,'Herring','33','90778',NULL,NULL,72),(184,NULL,NULL,1,1,1,1,NULL,'OLSEN HERRING-PARTY PAIL 30 OZ',NULL,'Herring','33','90779',NULL,NULL,72),(185,NULL,NULL,1,1,1,1,NULL,'HY-VEE/FM HERRING CREAM SCE 8 OZ',NULL,'Herring','33','90781',NULL,NULL,72),(186,NULL,NULL,1,1,1,1,NULL,'FISH MKT HERRING WINE SAUCE 1/45',NULL,'Herring','33','90782',NULL,NULL,72),(187,NULL,NULL,1,1,1,1,NULL,'OLSEN HERRING WINE PREPK 16Z',NULL,'Herring','33','90802',NULL,NULL,72),(188,NULL,NULL,1,1,1,1,NULL,'VIKING HERRING WINE 10 LB',NULL,'Herring','33','90893',NULL,NULL,72),(189,NULL,NULL,1,1,1,1,NULL,'VIKING HERRING WINE 45 LB',NULL,'Herring','33','90894',NULL,NULL,72),(190,NULL,NULL,1,1,1,1,NULL,'VIKING CREAMED HERRING 10 LB',NULL,'Herring','33','90897',NULL,NULL,72),(191,NULL,NULL,1,1,1,1,NULL,'IVARS CLAM CHOWDER FRESH',NULL,'Clam','17','05182',NULL,NULL,31),(192,NULL,NULL,1,1,1,1,NULL,'IVARS WILD SMOKED SALMON CHOWDER',NULL,'Salmon','63','05183',NULL,NULL,114),(193,NULL,NULL,1,1,1,1,NULL,'RUBY BAY YUKON KETA KANDY',NULL,'Salmon','63','06399',NULL,NULL,114),(194,NULL,NULL,1,1,1,1,NULL,'BLUE HILL BAY SALMON BAKED HONEY',NULL,'Salmon','63','06398',NULL,NULL,111),(195,NULL,NULL,1,1,1,1,NULL,'BLUE HILL BAY SALMON BAKED BBQ',NULL,'Salmon','63','06397',NULL,NULL,111),(196,NULL,NULL,1,1,1,1,NULL,'BLUE HILL BAY SALMON BAKED HERB',NULL,'Salmon','63','06396',NULL,NULL,111),(197,NULL,NULL,1,1,1,1,NULL,'MATLAW CLAM BAC/CHS STUFFED',NULL,'Clam','17','06036',NULL,NULL,31),(198,NULL,NULL,2,2,1,1,NULL,'MATLAW CLAM CHORIZO GMT STUFFED',NULL,'Clam','17','06037',NULL,NULL,31),(199,NULL,NULL,2,2,1,1,NULL,'MATLAW CLAM BAC/CHS STUFFED',NULL,'Clam','17','06029',NULL,NULL,31),(200,NULL,NULL,2,2,1,1,NULL,'MATLAW CLAM CHORIZO STUFFED',NULL,'Clam','17','06028',NULL,NULL,31),(201,NULL,NULL,2,2,1,1,NULL,'SALMON SOCKEYE FILLETS PBO PRVFZ',NULL,'Salmon','63','05597',NULL,NULL,116),(202,NULL,NULL,2,2,1,1,NULL,'LOBSTER LIVE 6-9 LB',NULL,'Lobster','38','05799',NULL,NULL,77),(203,NULL,NULL,2,2,1,1,NULL,'AS COD HARVEST GRAIN ALASKAN',NULL,'Cod','19','05858',NULL,NULL,41),(204,NULL,NULL,2,2,1,1,NULL,'AS COD GLUTEN FREE ALASKAN CRISP',NULL,'Cod','19','05857',NULL,NULL,41),(205,NULL,NULL,2,2,1,1,NULL,'AS COD CRISPY BATTERED ALASKAN',NULL,'Cod','19','05856',NULL,NULL,41),(206,NULL,NULL,2,2,1,1,NULL,'AS POLLOCK BEER BATTERED ALASKA',NULL,'Pollock','57','05855',NULL,NULL,106),(207,NULL,NULL,2,2,1,1,NULL,'AS ALASKA FISH SANDWICH',NULL,'Pollock','57','05854',NULL,NULL,106),(208,NULL,NULL,2,2,1,1,NULL,'AS ALASKA FISH BITES',NULL,'Pollock','57','05853',NULL,NULL,106),(209,NULL,NULL,2,2,1,1,NULL,'AS SHRIMP BUTTERFLY CRUNCHY',NULL,'Shrimp','74','05861',NULL,NULL,136),(210,NULL,NULL,2,2,1,1,NULL,'AS SHRIMP BUTTERFLY COCONUT',NULL,'Shrimp','74','05862',NULL,NULL,136),(211,NULL,NULL,2,2,1,1,NULL,'AS SHRIMP POPCORN',NULL,'Shrimp','74','05863',NULL,NULL,136),(212,NULL,NULL,2,2,1,1,NULL,'AS PACIFIC SALMON FILLETS',NULL,'Salmon','63','05859',NULL,NULL,115),(213,NULL,NULL,2,2,1,1,NULL,'AS SALMON PINK FILLET TWNPK IVP',NULL,'Salmon','63','05865',NULL,NULL,115),(214,NULL,NULL,2,2,1,1,NULL,'AS SHRIMP SKEWER PDTO 31/40',NULL,'Shrimp','74','05867',NULL,NULL,136),(215,NULL,NULL,2,2,1,1,NULL,'BARRAMUNDI FLT FRSH(ALL NATURAL)',NULL,'Barramundi','6','05561',NULL,NULL,12),(216,NULL,NULL,2,2,1,1,NULL,'TUNA LOIN YELLOWFIN IVP 5-8#',NULL,'Tuna','89','05621',NULL,NULL,182),(217,NULL,NULL,2,2,1,1,NULL,'SALMON ALASKA SOCKEYE FIL IQF',NULL,'Salmon','63','06847',NULL,NULL,116),(218,NULL,NULL,2,2,1,1,NULL,'SOCKEYE SALMON PINWHEEL SPN/FETA',NULL,'Salmon','63','05371',NULL,NULL,116),(219,NULL,NULL,2,2,1,1,NULL,'LOBSTER LIVE 2-3 LB',NULL,'Lobster','38','05800',NULL,NULL,77),(220,NULL,NULL,2,2,1,1,NULL,'LOBSTER LIVE 1.75 LB',NULL,'Lobster','38','05802',NULL,NULL,77),(221,NULL,NULL,2,2,1,1,NULL,'LOBSTER LIVE 1.25 LB',NULL,'Lobster','38','05806',NULL,NULL,77),(222,NULL,NULL,2,2,1,1,NULL,'MOREYS COD TUSCAN MEDLEY',NULL,'Cod','19','06047',NULL,NULL,38),(223,NULL,NULL,2,2,1,1,NULL,'MOREYS WOOD ROASTED SALMON',NULL,'Salmon','63','06048',NULL,NULL,114),(224,NULL,NULL,2,2,1,1,NULL,'MOREYS SALMON SEASONED GRILL',NULL,'Salmon','63','06049',NULL,NULL,114),(225,NULL,NULL,2,2,1,1,NULL,'MOREYS TILAPIA SEASONED',NULL,'Tilapia','85','06050',NULL,NULL,164),(226,NULL,NULL,2,2,1,1,NULL,'31/40 TAIL-OFF P&D RAW SHRIMP',NULL,'Shrimp','74','05419',NULL,NULL,136),(227,NULL,NULL,2,2,1,1,NULL,'51/60 TAIL-OFF P&D RAW SHRIMP',NULL,'Shrimp','74','05420',NULL,NULL,136),(228,NULL,NULL,2,2,1,1,NULL,'SHRIMP RAW HEADON 21/25CT USA 1#',NULL,'Shrimp','74','05440',NULL,NULL,136),(229,NULL,NULL,2,2,1,1,NULL,'SHRIMP CKD TAIL ON 30/40CT 2LB',NULL,'Shrimp','74','05660',NULL,NULL,136),(230,NULL,NULL,2,2,1,1,NULL,'SEA BEST PACIFIC COD FILLETS 1#',NULL,'Cod','19','05671',NULL,NULL,41),(231,NULL,NULL,2,2,1,1,NULL,'SHRIMP HALF TRAY 7Z/4Z SAUCE',NULL,'Shrimp','74','05688',NULL,NULL,136),(232,NULL,NULL,2,2,1,1,NULL,'TRIDENT POLLOCK OVEN RDY BULK 2Z',NULL,'Pollock','57','05476',NULL,NULL,106),(233,NULL,NULL,2,2,1,1,NULL,'TRIDENT FISH BRD HIDDEN TREAS',NULL,'Pollock','57','05477',NULL,NULL,106),(234,NULL,NULL,2,2,1,1,NULL,'TRIDENT ALASKA COD 10 GRAIN BRD',NULL,'Cod','19','05851',NULL,NULL,41),(235,NULL,NULL,2,2,1,1,NULL,'TRIDENT ALASKAN SALMON BITES',NULL,'Salmon','63','05852',NULL,NULL,115),(236,NULL,NULL,2,2,1,1,NULL,'TRIDENT BREAD POLLOCK FISH',NULL,'Pollock','57','05889',NULL,NULL,106),(237,NULL,NULL,2,2,1,1,NULL,'TRIDENT BAJA BREADED FILLETS',NULL,'Cod','19','05899',NULL,NULL,41),(238,NULL,NULL,2,2,1,1,NULL,'TRIDENT ALASKAN POLLOCK BURGERS',NULL,'Pollock','57','06094',NULL,NULL,106),(239,NULL,NULL,2,2,1,1,NULL,'TRIDENT ALASKAN SALMON BURGERS',NULL,'Salmon','63','06095',NULL,NULL,115),(240,NULL,NULL,2,2,1,1,NULL,'TRIDENT ALASKAN FISH & CHIPS',NULL,'Pollock','57','06096',NULL,NULL,106),(241,NULL,NULL,2,2,1,1,NULL,'L KEMP CRAB DEL SRCHA SEAFD ROLL',NULL,'Pollock','57','45658',NULL,NULL,106),(242,NULL,NULL,2,2,1,1,NULL,'L KEMP SALMON DEL HERB CRM CHS',NULL,'Salmon','63','45659',NULL,NULL,115),(243,NULL,NULL,2,2,1,1,NULL,'L KEMP SALMON DELIGHT SNACKS',NULL,'Salmon','63','45660',NULL,NULL,115),(244,NULL,NULL,2,2,1,1,NULL,'OB SHRIMP CKD CLDWTR USA 250-350',NULL,'Shrimp','74','05657',NULL,NULL,136),(245,NULL,NULL,2,2,1,1,NULL,'ROCK LOBSTER TAILS CLDWTR 3-4 OZ',NULL,'Lobster','38','05445',NULL,NULL,NULL),(246,NULL,NULL,2,2,1,1,NULL,'SC PARMESAN CRUSTED TILAPIA TRAY',NULL,'Tilapia','85','05893',NULL,NULL,164),(247,NULL,NULL,2,2,1,1,NULL,'SC POTATO CRUSTED COD TRAY',NULL,'Cod','19','05894',NULL,NULL,37),(248,NULL,NULL,2,2,1,1,NULL,'SC TORTILLA CRUSTED TILAPIA',NULL,'Tilapia','85','05895',NULL,NULL,164),(249,NULL,NULL,2,2,1,1,NULL,'SC MEDITERAN CRUSTED SALMON-TRAY',NULL,'Salmon','63','05896',NULL,NULL,115),(250,NULL,NULL,2,2,1,1,NULL,'YELLOWFIN TUNA LOIN IVP 8-12#',NULL,'Tuna','89','05609',NULL,NULL,182),(251,NULL,NULL,2,2,1,1,NULL,'SALMON WH ATLANTIC 11-13#  1/50#',NULL,'Salmon','63','05280',NULL,NULL,111),(252,NULL,NULL,2,2,1,1,NULL,'ANCIENT GRAINS MAHI MAHI 5Z',NULL,'Mahi Mahi','41','05786',NULL,NULL,83),(253,NULL,NULL,2,2,1,1,NULL,'MACADAMIA CRUSTED MAHI 5Z',NULL,'Mahi Mahi','41','05787',NULL,NULL,83),(254,NULL,NULL,2,2,1,1,NULL,'MATLAW CLAM SMALL CASINO',NULL,'Clam','17','06030',NULL,NULL,31),(255,NULL,NULL,2,2,1,1,NULL,'MATLAW CLAM NEW ENGLND LRG STUFF',NULL,'Clam','17','06031',NULL,NULL,31),(256,NULL,NULL,2,2,1,1,NULL,'SQUID UNCLEANED PRODUCT OF USA',NULL,'Squid','80','05465',NULL,NULL,160),(257,NULL,NULL,2,2,1,1,NULL,'SEA BEST BREADED SHRIMP 10Z RETL',NULL,'Shrimp','74','05468',NULL,NULL,136),(258,NULL,NULL,2,2,1,1,NULL,'BREADED POLLOCK SQUARES 4 OZ',NULL,'Pollock','57','05475',NULL,NULL,106),(259,NULL,NULL,2,2,1,1,NULL,'SHRIMP NATURAL CKD 26/30 CT  2LB',NULL,'Shrimp','74','05653',NULL,NULL,136),(260,NULL,NULL,2,2,1,1,NULL,'MOREYS NOVA LOX SALMON SM ROSE P',NULL,'Salmon','63','06459',NULL,NULL,114),(261,NULL,NULL,2,2,1,1,NULL,'MOREYS SM SALMON SNDRI TOM&BASIL',NULL,'Salmon','63','06456',NULL,NULL,114),(262,NULL,NULL,2,2,1,1,NULL,'TRIDENT ALASKAN SALMON BURGER 4Z',NULL,'Salmon','63','06097',NULL,NULL,115),(263,NULL,NULL,2,2,1,1,NULL,'ALASKA SNOW CRAB CLUSTER 8/UP',NULL,'Crab','22','06250',NULL,NULL,53),(264,NULL,NULL,2,2,1,1,NULL,'SEA SCALLOPS IQF DRY U/10',NULL,'Scallop','66','06246',NULL,NULL,121),(265,NULL,NULL,2,2,1,1,NULL,'SOCKEYE SALMON POS',NULL,'Salmon','63','06814',NULL,NULL,116),(266,NULL,NULL,2,2,1,1,NULL,'HALIBUT FLT IVP 10#',NULL,'Flatfish','28','05501',NULL,NULL,71),(267,NULL,NULL,2,2,1,1,NULL,'SALMON COHO BURGER MEAT FRZ',NULL,'Salmon','63','06849',NULL,NULL,112),(268,NULL,NULL,2,2,1,1,NULL,'SOCKEYE SALMON MEAT  4/11.2 LB',NULL,'Salmon','63','06851',NULL,NULL,116),(269,NULL,NULL,2,2,1,1,NULL,'SOCKEYE SALMON MEAT 8/3 LB',NULL,'Salmon','63','06853',NULL,NULL,116),(270,NULL,NULL,2,2,1,1,NULL,'SALMON KING WHL COPPER RVR 2 PC',NULL,'Salmon','63','05505',NULL,NULL,113),(271,NULL,NULL,2,2,1,1,NULL,'SALMON ARCTIC KETA FIL CHUM 10#',NULL,'Salmon','63','05514',NULL,NULL,114),(272,NULL,NULL,2,2,1,1,NULL,'SALMON - COHO FILLET  12#',NULL,'Salmon','63','05589',NULL,NULL,112),(273,NULL,NULL,2,2,1,1,NULL,'SALMON SOCKEYE FIL 10LB',NULL,'Salmon','63','05596',NULL,NULL,116),(274,NULL,NULL,2,2,1,1,NULL,'OB COD PORTIONS IQF 2 OZ',NULL,'Cod','19','05412',NULL,NULL,41),(275,NULL,NULL,2,2,1,1,NULL,'SHRIMP SALAD COOKED 4Z RTL',NULL,'Shrimp','74','05665',NULL,NULL,136),(276,NULL,NULL,2,2,1,1,NULL,'HV/FM ALASKA SOCKEYE SALMON FLTS',NULL,'Salmon','63','05877',NULL,NULL,116),(277,NULL,NULL,2,2,1,1,NULL,'HV/FM KETA SALMON FILLETS 24Z',NULL,'Salmon','63','05890',NULL,NULL,114),(278,NULL,NULL,2,2,1,1,NULL,'SINGLETON JALAPENO KRAB DIP',NULL,'Pollock','57','45638',NULL,NULL,106),(279,NULL,NULL,2,2,1,1,NULL,'FLOUNDER WHOLE  10#',NULL,'Flatfish','28','05528',NULL,NULL,NULL),(280,NULL,NULL,2,2,1,1,NULL,'SALT COD 12/1 LB WOODEN BOX',NULL,'Cod','19','06416',NULL,NULL,41),(281,NULL,NULL,2,2,1,1,NULL,'SOFT SHELL CLAMS FRESH',NULL,'Clam','17','06114',NULL,NULL,NULL),(282,NULL,NULL,2,2,1,1,NULL,'SWORDFISH LOINS 5-8LB COV',NULL,'Swordfish','83','05618',NULL,NULL,NULL),(283,NULL,NULL,2,2,1,1,NULL,'BUTTERFLY SNAPPER FILLETS IVP',NULL,'Snapper','77','06885',NULL,NULL,NULL),(284,NULL,NULL,2,2,1,1,NULL,'SOFTSHELL CRAB CLEAN JUMBO FRZ',NULL,'Crab','22','05193',NULL,NULL,45),(285,NULL,NULL,2,2,1,1,NULL,'SOFT SHELL CRABS HOTEL',NULL,'Crab','22','05196',NULL,NULL,45),(286,NULL,NULL,2,2,1,1,NULL,'OE COD PORTIONS 2#',NULL,'Cod','19','05834',NULL,NULL,41),(287,NULL,NULL,2,2,1,1,NULL,'OE PINK SALMON FILLETS',NULL,'Salmon','63','05836',NULL,NULL,114),(288,NULL,NULL,2,2,1,1,NULL,'OE SALMON PORTIONS 4 OZ',NULL,'Salmon','63','05843',NULL,NULL,115),(289,NULL,NULL,2,2,1,1,NULL,'OE TILAPIA PORTIONS 4 OZ',NULL,'Tilapia','85','05844',NULL,NULL,164),(290,NULL,NULL,2,2,1,1,NULL,'ROCK LOBSTER TAILS CLDWTR 2-3 OZ',NULL,'Lobster','38','05449',NULL,NULL,NULL),(291,NULL,NULL,2,2,1,1,NULL,'NAT BLACK TIGER SHRIMP EZ 6/8 CT',NULL,'Shrimp','74','05435',NULL,NULL,136),(292,NULL,NULL,2,2,1,1,NULL,'SHRIMP HEAD-ON 8/12 BLK TGR NATU',NULL,'Shrimp','74','05495',NULL,NULL,136),(293,NULL,NULL,2,2,1,1,NULL,'NATURAL SHRIMP PLATTER 16 OZ',NULL,'Shrimp','74','05697',NULL,NULL,136),(294,NULL,NULL,2,2,1,1,NULL,'SHRIMP-WILD HDLS USA 16/20 1#BAG',NULL,'Shrimp','74','05316',NULL,NULL,136),(295,NULL,NULL,2,2,1,1,NULL,'SHRIMP WILD USA HDLS 51/70 CT 1#',NULL,'Shrimp','74','05319',NULL,NULL,136),(296,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP BATTERED FISH TENDER',NULL,'Pollock','57','06905',NULL,NULL,106),(297,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP BREADED FISH STICKS',NULL,'Pollock','57','06906',NULL,NULL,NULL),(298,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP XL FISH STICKS',NULL,'Pollock','57','06907',NULL,NULL,106),(299,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP BEER BATTER FILLETS',NULL,'Pollock','57','06908',NULL,NULL,106),(300,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP BATTERED FILLETS',NULL,'Pollock','57','06909',NULL,NULL,106),(301,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP ORIG FISH SANDWICH',NULL,'Pollock','57','06910',NULL,NULL,106),(302,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP LT & CRISPY FILLETS',NULL,'Pollock','57','06911',NULL,NULL,106),(303,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP BREADED FISH STICKS',NULL,'Pollock','57','06912',NULL,NULL,106),(304,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP BREADED FILLETS',NULL,'Pollock','57','06913',NULL,NULL,106),(305,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP SALMON BLACK PEPPER',NULL,'Salmon','63','06914',NULL,NULL,115),(306,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP TILAPIA PARM GARLIC',NULL,'Tilapia','85','06915',NULL,NULL,164),(307,NULL,NULL,2,2,1,1,NULL,'VAN DE KAMP COD GARLIC HERB',NULL,'Cod','19','06916',NULL,NULL,38),(308,NULL,NULL,2,2,1,1,NULL,'SHRIMP BELIZE 60/80 CKD NAT T/OF',NULL,'Shrimp','74','05759',NULL,NULL,136),(309,NULL,NULL,2,2,1,1,NULL,'SHRIMP BELIZE 41/50 EZP NAT BULK',NULL,'Shrimp','74','05770',NULL,NULL,136),(310,NULL,NULL,2,2,1,1,NULL,'TUNA LOINS YELLOWFIN IVP 3/5#',NULL,'Tuna','89','05622',NULL,NULL,182),(311,NULL,NULL,2,2,1,1,NULL,'TUNA STEAKS YELLOWFIN IVP 4Z',NULL,'Tuna','89','05623',NULL,NULL,182),(312,NULL,NULL,2,2,1,1,NULL,'VERLASSO SMOKED SALMON',NULL,'Salmon','63','06388',NULL,NULL,111),(313,NULL,NULL,2,2,1,1,NULL,'BLUE HILL BAY SMK SALMON GRAVLAX',NULL,'Salmon','63','06389',NULL,NULL,111),(314,NULL,NULL,2,2,1,1,NULL,'BLUE HILL BAY SMK SALMON PSTRAMI',NULL,'Salmon','63','06390',NULL,NULL,111),(315,NULL,NULL,2,2,1,1,NULL,'SOCKEYE SALMON MEAT',NULL,'Salmon','63','06860',NULL,NULL,116),(316,NULL,NULL,2,2,1,1,NULL,'ALASKA KING SALMON SPOON MEAT',NULL,'Salmon','63','06861',NULL,NULL,116),(317,NULL,NULL,2,2,1,1,NULL,'HV SOCKEYE PINWHEELS CLD SMK',NULL,'Salmon','63','06380',NULL,NULL,116),(318,NULL,NULL,2,2,1,1,NULL,'HV SOCKEYE SALMON 3 CLD SMK',NULL,'Salmon','63','06381',NULL,NULL,116),(319,NULL,NULL,2,2,1,1,NULL,'HV ATLANTIC SALMON 4 CLD SMK',NULL,'Salmon','63','06382',NULL,NULL,111),(320,NULL,NULL,2,2,1,1,NULL,'NAT PACIFIC COD LOINS 10-15 Z',NULL,'Cod','19','06659',NULL,NULL,41),(321,NULL,NULL,2,2,1,1,NULL,'NAT PACIFIC COD LOINS 6 Z',NULL,'Cod','19','06660',NULL,NULL,41),(322,NULL,NULL,2,2,1,1,NULL,'NAT PACIFIC COD LOINS 15-20 Z',NULL,'Cod','19','06670',NULL,NULL,41),(323,NULL,NULL,2,2,1,1,NULL,'SALMON SOCKEYE WHOLE H&G IVP FRZ',NULL,'Salmon','63','06843',NULL,NULL,116),(324,NULL,NULL,2,2,1,1,NULL,'LOBSTER LIVE CHICKS 1 LB',NULL,'Lobster','38','05804',NULL,NULL,77),(325,NULL,NULL,2,2,1,1,NULL,'TUNA LOINS AHI PREV FRZ',NULL,'Tuna','89','05600',NULL,NULL,182),(326,NULL,NULL,2,2,1,1,NULL,'SEAPAK POPCORN SHRIMP',NULL,'Seabob, Atlantic','68','06926',NULL,NULL,204),(327,NULL,NULL,2,2,1,1,NULL,'SEAPAK JUMBO BUTTERFLY SHRIMP',NULL,'Shrimp','74','06927',NULL,NULL,136),(328,NULL,NULL,2,2,1,1,NULL,'SEAPAK BUTTERFLY SHRIMP FAM PACK',NULL,'Shrimp','74','06929',NULL,NULL,136),(329,NULL,NULL,2,2,1,1,NULL,'SEAPAK JUMBO COCONUT SHRIMP FAM',NULL,'Shrimp','74','06930',NULL,NULL,136),(330,NULL,NULL,2,2,1,1,NULL,'SEAPAK POPCORN SHRIMP FAM PAK',NULL,'Seabob, Atlantic','68','06931',NULL,NULL,204),(331,NULL,NULL,2,2,1,1,NULL,'SEAPAK BUTTERFLY SHRIMP CLUB PAK',NULL,'Shrimp','74','06932',NULL,NULL,136),(332,NULL,NULL,2,2,1,1,NULL,'GORTONS SMRT CRUNCHY FISH STICKS',NULL,'Pollock','57','06942',NULL,NULL,106),(333,NULL,NULL,2,2,1,1,NULL,'GORTONS SMART CRNCH FISH FILLET',NULL,'Pollock','57','06943',NULL,NULL,106),(334,NULL,NULL,2,2,1,1,NULL,'GORTONS RSTD GAR ITAL HERB FLTS',NULL,'Pollock','57','06944',NULL,NULL,106),(335,NULL,NULL,2,2,1,1,NULL,'GORTONS BREAD FISH STICK VAL PK',NULL,'Pollock','57','06945',NULL,NULL,106),(336,NULL,NULL,2,2,1,1,NULL,'GORTONS SMPL BAKE SALMON GAR/BTR',NULL,'Salmon','63','06946',NULL,NULL,115),(337,NULL,NULL,2,2,1,1,NULL,'GORTONS BKD TILAPIA W/SEASONING',NULL,'Tilapia','85','06947',NULL,NULL,164),(338,NULL,NULL,2,2,1,1,NULL,'GORTONS SKLT CRISP TILAPIA SEASN',NULL,'Tilapia','85','06948',NULL,NULL,164),(339,NULL,NULL,2,2,1,1,NULL,'GORTONS SKLT CRISP TILAPIA GAR/H',NULL,'Tilapia','85','06949',NULL,NULL,164),(340,NULL,NULL,2,2,1,1,NULL,'GORTONS POTATO CRNCH FISH FILLET',NULL,'Pollock','57','06950',NULL,NULL,106),(341,NULL,NULL,2,2,1,1,NULL,'GORTONS GOLDEN FISH FILLET',NULL,'Pollock','57','06951',NULL,NULL,106),(342,NULL,NULL,2,2,1,1,NULL,'GORTONS LARGE POPCORN SHRIMP',NULL,'Seabob, Atlantic','68','06952',NULL,NULL,204),(343,NULL,NULL,2,2,1,1,NULL,'GORTONS GRILL FILLET GAR/BUTTER',NULL,'Pollock','57','06953',NULL,NULL,106),(344,NULL,NULL,2,2,1,1,NULL,'GORTONS CAJUN GRILLED FILLET',NULL,'Pollock','57','06954',NULL,NULL,106),(345,NULL,NULL,2,2,1,1,NULL,'GORTONS CLASSIC GRILLED SALMON',NULL,'Salmon','63','06955',NULL,NULL,115),(346,NULL,NULL,2,2,1,1,NULL,'GORTONS SIGNATURE GRLLED TILAPIA',NULL,'Tilapia','85','06956',NULL,NULL,164),(347,NULL,NULL,2,2,1,1,NULL,'GORTONS GRILLED TILAPIA FILLETS',NULL,'Tilapia','85','06957',NULL,NULL,164),(348,NULL,NULL,2,2,1,1,NULL,'WALLEYE (WHOLE)',NULL,'Walleye','91','05578',NULL,NULL,NULL),(349,NULL,NULL,2,2,1,1,NULL,'SEA BEST TILAPIA FILLETS 2 LB',NULL,'Tilapia','85','05417',NULL,NULL,164),(350,NULL,NULL,2,2,1,1,NULL,'SEA BEST SWAI FILLETS 2 LB',NULL,'Pangasius','53','05461',NULL,NULL,24),(351,NULL,NULL,2,2,1,1,NULL,'Prem. White Raw EZ Peel Shrimp 16/20 ct.',NULL,'Shrimp','74','05484',NULL,NULL,136),(352,NULL,NULL,2,2,1,1,NULL,'Prem. White Raw EZ Peel Shrimp 26/30 ct.',NULL,'Shrimp','74','05485',NULL,NULL,136),(353,NULL,NULL,2,2,1,1,NULL,'Prem. White Raw EZ Peel Shrimp 51/60 ct.',NULL,'Shrimp','74','05486',NULL,NULL,136),(354,NULL,NULL,2,2,1,1,NULL,'Fully Cooked Tail-on Shrimp 51/60 ct. ',NULL,'Shrimp','74','05487',NULL,NULL,136),(355,NULL,NULL,2,2,1,1,NULL,'Fully Cooked Tail-on Shrimp 26/30 ct.',NULL,'Shrimp','74','05488',NULL,NULL,136),(356,NULL,NULL,2,2,1,1,NULL,'Fully Cooked Tail-on Shrimp 26/30 ct.',NULL,'Shrimp','74','05652',NULL,NULL,136),(357,NULL,NULL,2,2,1,1,NULL,'CKD SALAD SHRIMP 100/150 P&D - FARM RAISED - TREATED BAP 2 STAR',NULL,'Shrimp','74','05663',NULL,NULL,136),(358,NULL,NULL,2,2,1,1,NULL,'HV Cooked Tail/on Shrimp - 26/30 Count - NEEDLE DEVEINED',NULL,'Shrimp','74','05734',NULL,NULL,136),(359,NULL,NULL,2,2,1,1,NULL,'HV Raw Quick Peel Shrimp - 51/60 Count',NULL,'Shrimp','74','05737',NULL,NULL,136),(360,NULL,NULL,2,2,1,1,NULL,'HV Cooked Tail/on Shrimp - 26/30 Count - NEEDLE DEVEINED',NULL,'Shrimp','74','05764',NULL,NULL,136),(361,NULL,NULL,2,2,1,1,NULL,'HV Cooked Tail/on Shrimp - 16/20 Count - NEEDLE DEVEINED',NULL,'Shrimp','74','05766',NULL,NULL,136),(362,NULL,NULL,2,2,1,1,NULL,'HV BATTERED FISH FILLETS',NULL,'Pollock','57','05878',NULL,NULL,106),(363,NULL,NULL,2,2,1,1,NULL,'HV SHRIMP CRUNCHY POPCORN',NULL,'Shrimp','74','05879',NULL,NULL,136),(364,NULL,NULL,2,2,1,1,NULL,'HV BEER BATTERED FISH FILLETS',NULL,'Pollock','57','05880',NULL,NULL,106),(365,NULL,NULL,2,2,1,1,NULL,'HV SHRIMP JUMBO BUTTERFLY',NULL,'Shrimp','74','05882',NULL,NULL,136),(366,NULL,NULL,2,2,1,1,NULL,'HV BREADED FISH FILLET',NULL,'Pollock','57','05883',NULL,NULL,106);
/*!40000 ALTER TABLE `products` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `retailers`
--

DROP TABLE IF EXISTS `retailers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `retailers` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fishwiseComments` text COLLATE utf8_unicode_ci,
  `Scale` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `show` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `retailers`
--

LOCK TABLES `retailers` WRITE;
/*!40000 ALTER TABLE `retailers` DISABLE KEYS */;
INSERT INTO `retailers` (`id`, `created_at`, `updated_at`, `name`, `contact_name`, `contact_phone`, `contact_email`, `created_by`, `modified_by`, `fishwiseComments`, `Scale`, `show`) VALUES (1,NULL,'2016-08-17 21:51:09','Off Target','Tor Jansen','123-456-7890','tj@bigswede.com',0,0,NULL,'National',1),(2,NULL,'2016-08-17 21:51:13','LowBee','Phillip Marlowe','789-456+1230','phil@private.eye',0,0,NULL,'Regional',1);
/*!40000 ALTER TABLE `retailers` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seafoodCategories`
--

DROP TABLE IF EXISTS `seafoodCategories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seafoodCategories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `seafoodCategoryForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=96 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seafoodCategories`
--

LOCK TABLES `seafoodCategories` WRITE;
/*!40000 ALTER TABLE `seafoodCategories` DISABLE KEYS */;
INSERT INTO `seafoodCategories` (`id`, `created_at`, `updated_at`, `category`, `createdBy`, `modifiedBy`, `seafoodCategoryForDisplay`, `excludeFromDropdown`) VALUES (1,NULL,NULL,'Abalone',1,1,NULL,0),(2,NULL,NULL,'Alligator',1,1,NULL,0),(3,NULL,NULL,'Amberjack',1,1,NULL,0),(4,NULL,NULL,'Anchovy',1,1,NULL,0),(5,NULL,NULL,'Artic Char',1,1,NULL,0),(6,NULL,NULL,'Barramundi',1,1,NULL,0),(7,NULL,NULL,'Bass',1,1,NULL,0),(8,NULL,NULL,'Blackfish',1,1,NULL,0),(9,NULL,NULL,'Bluefish',1,1,NULL,0),(10,NULL,NULL,'Bonito',1,1,NULL,0),(11,NULL,NULL,'Bream',1,1,NULL,0),(12,NULL,NULL,'Buffalo Fish',1,1,NULL,0),(13,NULL,NULL,'Butter Fish',1,1,NULL,0),(14,NULL,NULL,'Carp',1,1,NULL,0),(15,NULL,NULL,'Catfish',1,1,NULL,0),(16,NULL,NULL,'Caviar',1,1,NULL,0),(17,NULL,NULL,'Clam',1,1,NULL,0),(18,NULL,NULL,'Cobia',1,1,NULL,0),(19,NULL,NULL,'Cod',1,1,NULL,0),(20,NULL,NULL,'Conch',1,1,NULL,0),(21,NULL,NULL,'Corvina',1,1,NULL,0),(22,NULL,NULL,'Crab',1,1,NULL,0),(23,NULL,NULL,'Crawfish',1,1,NULL,0),(24,NULL,NULL,'Croaker',1,1,NULL,0),(25,NULL,NULL,'Cuttlefish',1,1,NULL,0),(26,NULL,NULL,'Eel',1,1,NULL,0),(27,NULL,NULL,'Escolar',1,1,NULL,0),(28,NULL,NULL,'Flatfish',1,1,NULL,0),(29,NULL,NULL,'Flying Fish',1,1,NULL,0),(30,NULL,NULL,'Grouper',1,1,NULL,0),(31,NULL,NULL,'Haddock',1,1,NULL,0),(32,NULL,NULL,'Hake',1,1,NULL,0),(33,NULL,NULL,'Herring',1,1,NULL,0),(34,NULL,NULL,'Kampachi',1,1,NULL,0),(35,NULL,NULL,'Kingklip',1,1,NULL,0),(36,NULL,NULL,'Langostino',1,1,NULL,0),(37,NULL,NULL,'Lingcod',1,1,NULL,0),(38,NULL,NULL,'Lobster',1,1,NULL,0),(39,NULL,NULL,'Lutefisk',1,1,NULL,0),(40,NULL,NULL,'Mackerel',1,1,NULL,0),(41,NULL,NULL,'Mahi Mahi',1,1,NULL,0),(42,NULL,NULL,'Marlin',1,1,NULL,0),(43,NULL,NULL,'Mixed',1,1,NULL,0),(44,NULL,NULL,'Monkfish',1,1,NULL,0),(45,NULL,NULL,'Mullet',1,1,NULL,0),(46,NULL,NULL,'Mussel',1,1,NULL,0),(47,NULL,NULL,'Nile Perch',1,1,NULL,0),(48,NULL,NULL,'Not Seafood',1,1,NULL,0),(49,NULL,NULL,'Octopus',1,1,NULL,0),(50,NULL,NULL,'Opah',1,1,NULL,0),(51,NULL,NULL,'Orange Roughy',1,1,NULL,0),(52,NULL,NULL,'Oyster',1,1,NULL,0),(53,NULL,NULL,'Pangasius',1,1,NULL,0),(54,NULL,NULL,'Parrot Fish',1,1,NULL,0),(55,NULL,NULL,'Perch',1,1,NULL,0),(56,NULL,NULL,'Pike',1,1,NULL,0),(57,NULL,NULL,'Pollock',1,1,NULL,0),(58,NULL,NULL,'Pomfret',1,1,NULL,0),(59,NULL,NULL,'Pompano',1,1,NULL,0),(60,NULL,NULL,'Porgy',1,1,NULL,0),(61,NULL,NULL,'Rockfish',1,1,NULL,0),(62,NULL,NULL,'Rover, Golden',1,1,NULL,0),(63,NULL,NULL,'Salmon',1,1,NULL,0),(64,NULL,NULL,'Sardine',1,1,NULL,0),(65,NULL,NULL,'Scad, Bigeye',1,1,NULL,0),(66,NULL,NULL,'Scallop',1,1,NULL,0),(67,NULL,NULL,'Sculpin',1,1,NULL,0),(68,NULL,NULL,'Seabob, Atlantic',1,1,NULL,0),(69,NULL,NULL,'Seaweed',1,1,NULL,0),(70,NULL,NULL,'Shad',1,1,NULL,0),(71,NULL,NULL,'Shark',1,1,NULL,0),(72,NULL,NULL,'Sheephead',1,1,NULL,0),(73,NULL,NULL,'Sheepshead',1,1,NULL,0),(74,NULL,NULL,'Shrimp',1,1,NULL,0),(75,NULL,NULL,'Skate',1,1,NULL,0),(76,NULL,NULL,'Smelt',1,1,NULL,0),(77,NULL,NULL,'Snapper',1,1,NULL,0),(78,NULL,NULL,'Soldierfish',1,1,NULL,0),(79,NULL,NULL,'Spearfish',1,1,NULL,0),(80,NULL,NULL,'Squid',1,1,NULL,0),(81,NULL,NULL,'Squirrelfish',1,1,NULL,0),(82,NULL,NULL,'Sturgeon',1,1,NULL,0),(83,NULL,NULL,'Swordfish',1,1,NULL,0),(84,NULL,NULL,'Tautog',1,1,NULL,0),(85,NULL,NULL,'Tilapia',1,1,NULL,0),(86,NULL,NULL,'Tilefish',1,1,NULL,0),(87,NULL,NULL,'Toothfish',1,1,NULL,0),(88,NULL,NULL,'Trout',1,1,NULL,0),(89,NULL,NULL,'Tuna',1,1,NULL,0),(90,NULL,NULL,'Wahoo',1,1,NULL,0),(91,NULL,NULL,'Walleye',1,1,NULL,0),(92,NULL,NULL,'Whelk',1,1,NULL,0),(93,NULL,NULL,'Whitefish',1,1,NULL,0),(94,NULL,NULL,'Whiting',1,1,NULL,0),(95,NULL,NULL,'Wrasse',1,1,NULL,0);
/*!40000 ALTER TABLE `seafoodCategories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seafoodSpecies`
--

DROP TABLE IF EXISTS `seafoodSpecies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seafoodSpecies` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `seafoodSpecies` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `scientificName` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_seafoodCategory_id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createdBy` int(11) NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `seafoodSpeciesForDisplay` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excludeFromDropdown` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=257 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seafoodSpecies`
--

LOCK TABLES `seafoodSpecies` WRITE;
/*!40000 ALTER TABLE `seafoodSpecies` DISABLE KEYS */;
INSERT INTO `seafoodSpecies` (`id`, `created_at`, `updated_at`, `seafoodSpecies`, `scientificName`, `fk_seafoodCategory_id`, `createdBy`, `modifiedBy`, `seafoodSpeciesForDisplay`, `excludeFromDropdown`) VALUES (7,NULL,NULL,'Abalone','Haliotis spp.','1',1,1,NULL,0),(8,NULL,NULL,'Alligator','Alligator mississippiensis','2',1,1,NULL,0),(9,NULL,NULL,'Amberjack','Seriola dumerili','3',1,1,NULL,0),(10,NULL,NULL,'Anchovy','Engraulis spp.','4',1,1,NULL,0),(11,NULL,NULL,'Arctic Char','Salvelinus alpinus','5',1,1,NULL,0),(12,NULL,NULL,'Barramundi','Lates calcarifer','6',1,1,NULL,0),(13,NULL,NULL,'Bass','','7',1,1,NULL,0),(14,NULL,NULL,'Bass, Silver Sheephead','Aplodinotus rafinesque','7',1,1,NULL,0),(15,NULL,NULL,'Bass, White','Morone chrysops','7',1,1,NULL,0),(16,NULL,NULL,'Bass, Striped','Morone saxatilis','7',1,1,NULL,0),(17,NULL,NULL,'Bluefish','Pomatomus saltatrix','9',1,1,NULL,0),(18,NULL,NULL,'Bonito','Sarda spp.','10',1,1,NULL,0),(19,NULL,NULL,'Bream, Sea','Family Sparidae','11',1,1,NULL,0),(20,NULL,NULL,'Buffalo Fish','Ictiobius spp.','12',1,1,NULL,0),(21,NULL,NULL,'Butter Fish','Family Stromateidae','13',1,1,NULL,0),(22,NULL,NULL,'Carp','Family Cyprinidae','14',1,1,NULL,0),(23,NULL,NULL,'Catfish','','15',1,1,NULL,0),(24,NULL,NULL,'Pangasius','Pangasius bocourti\nPangasius hypophthalamus','53',1,1,NULL,0),(25,NULL,NULL,'Catfish, Channel','Ictalurus punctatus','15',1,1,NULL,0),(26,NULL,NULL,'Caviar, carp roe','','16',1,1,NULL,0),(27,NULL,NULL,'Clam','','17',1,1,NULL,0),(28,NULL,NULL,'Clam, Littleneck','Mercenaria mercenaria','17',1,1,NULL,0),(29,NULL,NULL,'Clam, Mahogany','Arctica islandica','17',1,1,NULL,0),(30,NULL,NULL,'Clam, Manila','Venerupis philippinarum','17',1,1,NULL,0),(31,NULL,NULL,'Clam, Quahog','Mercenaria mercenaria','17',1,1,NULL,0),(32,NULL,NULL,'Clam, Northern Quahog','Mercenaria mercenaria','17',1,1,NULL,0),(33,NULL,NULL,'Clam, Razor','Siliqua patula','17',1,1,NULL,0),(34,NULL,NULL,'Clam, Surf','Spisula solida','17',1,1,NULL,0),(35,NULL,NULL,'Clam, Venus','Venus spp.','17',1,1,NULL,0),(36,NULL,NULL,'Cobia','Rachycentron canadum','18',1,1,NULL,0),(37,NULL,NULL,'Cod','Gadus spp.','19',1,1,NULL,0),(38,NULL,NULL,'Cod, Atlantic','Gadus morhua','19',1,1,NULL,0),(39,NULL,NULL,'Cod, Black','Notothenia microlepidota','19',1,1,NULL,0),(40,NULL,NULL,'Lingcod','Ophiodon elongatus','37',1,1,NULL,0),(41,NULL,NULL,'Cod, Pacific','Gadus macrocephalus','19',1,1,NULL,0),(42,NULL,NULL,'Conch','Strombus spp.','20',1,1,NULL,0),(43,NULL,NULL,'Corvina','Cilus gilberti','21',1,1,NULL,0),(44,NULL,NULL,'Crab','','22',1,1,NULL,0),(45,NULL,NULL,'Crab, Blue','Callinectes sapidus','22',1,1,NULL,0),(46,NULL,NULL,'Crab, Blue Swimming','Portunus pelagicus','22',1,1,NULL,0),(47,NULL,NULL,'Crab, Dungeness','Metacarcinus magister','22',1,1,NULL,0),(48,NULL,NULL,'Crab, Jonah','Cancer borealis','22',1,1,NULL,0),(49,NULL,NULL,'Crab, King','Lithodes aequispinus\nParalithodes camtschaticus\nParalithodes platypus','22',1,1,NULL,0),(50,NULL,NULL,'Crab, Red Atlantic','Chaceon quinquedens','22',1,1,NULL,0),(51,NULL,NULL,'Crab, Rock','Cancer productus','22',1,1,NULL,0),(52,NULL,NULL,'Crab, Shrimp','','22',1,1,NULL,0),(53,NULL,NULL,'Crab, Snow','Chionoecetes opilio','22',1,1,NULL,0),(54,NULL,NULL,'Crab, Stone','Menippe mercenaria','22',1,1,NULL,0),(55,NULL,NULL,'Crab, Softshell','Callinectes sapidus\nPortunus trituberculatus\nCharybdis japonica\nCarcinus aestuarii','22',1,1,NULL,0),(56,NULL,NULL,'Crawfish','Family Astacoidea\nFamily Parastacoidea','23',1,1,NULL,0),(57,NULL,NULL,'Croaker','Family Sciaenidae','24',1,1,NULL,0),(58,NULL,NULL,'Cusk Eel','Genypterus blacodes\nGenypterus capensis','26',1,1,NULL,0),(59,NULL,NULL,'Eel','Anguilla japonica\nAnguilla anguilla\nAnguilla rostrata ','26',1,1,NULL,0),(60,NULL,NULL,'Flounder','Paralichthys albigutta\nParalichthys lethostigma\nPseudopleuronectes americanus\nPlatichthys flesus\nParalichthys olivaceus','28',1,1,NULL,0),(61,NULL,NULL,'Flounder, Yellowtail','','28',1,1,NULL,0),(62,NULL,NULL,'Fluke','Paralichthys dentatus','28',1,1,NULL,0),(63,NULL,NULL,'Grouper','Family Serranidae subfamily Epinephellinae','30',1,1,NULL,0),(64,NULL,NULL,'Haddock','Melanogrammus aeglefinus','31',1,1,NULL,0),(65,NULL,NULL,'Hake','Merluccius productus','32',1,1,NULL,0),(66,NULL,NULL,'Hake, Argentinean','Meluccius hubbsi','32',1,1,NULL,0),(67,NULL,NULL,'Hake, Pacific','Merluccius productus','32',1,1,NULL,0),(68,NULL,NULL,'Hake, Red','Urophycis chuss','32',1,1,NULL,0),(69,NULL,NULL,'Halibut, California','Paralichthys californicus','28',1,1,NULL,0),(70,NULL,NULL,'Halibut, Atlantic','Hippoglossus hippoglossus','28',1,1,NULL,0),(71,NULL,NULL,'Halibut, Pacific','Hippoglossus stenolepis','28',1,1,NULL,0),(72,NULL,NULL,'Herring','Clupea spp.','33',1,1,NULL,0),(73,NULL,NULL,'Kingklip','Genypterus capensis','35',1,1,NULL,0),(74,NULL,NULL,'Langostino','Cervimunida johni\nMunida gregaria\nPleurocondes monodon','36',1,1,NULL,0),(75,NULL,NULL,'Ling, Common','Molva molva','19',1,1,NULL,0),(76,NULL,NULL,'Lobster','Panuliris interruptus','38',1,1,NULL,0),(77,NULL,NULL,'Lobster, American','Homarus americanus','38',1,1,NULL,0),(78,NULL,NULL,'Lobster, Caribbean','Panuliris argus','38',1,1,NULL,0),(79,NULL,NULL,'Lutefisk','','39',1,1,NULL,0),(80,NULL,NULL,'Mackerel, Spanish','Scomberomorus maculatus','40',1,1,NULL,0),(81,NULL,NULL,'Mackerel','Family Scombridae','40',1,1,NULL,0),(82,NULL,NULL,'Mackerel, King','Scomberomorus cavalla','40',1,1,NULL,0),(83,NULL,NULL,'Mahi mahi','Coryphaena hippurus','41',1,1,NULL,0),(84,NULL,NULL,'Marlin','Makaira spp\nTetrapturus spp.','42',1,1,NULL,0),(85,NULL,NULL,'Marlin, Blue','Makaira mazara\nMakaira nigricans','42',1,1,NULL,0),(86,NULL,NULL,'Marlin, Black','Makaira indica','42',1,1,NULL,0),(87,NULL,NULL,'Marlin, Striped','Tetrapturus audax','42',1,1,NULL,0),(88,NULL,NULL,'Marlin, White','Tetrapturus albidus','42',1,1,NULL,0),(89,NULL,NULL,'Mixed','','43',1,1,NULL,0),(90,NULL,NULL,'Monkfish','Lophius piscatorius\nLophius caulinaris\nLophius americanus\nLophius budegassa','44',1,1,NULL,0),(91,NULL,NULL,'Mullet','Family Mugilidae\nFamily Mullidae','45',1,1,NULL,0),(92,NULL,NULL,'Mussel','Mytilus californianus\nPerna canaliculus\nMytilus edulis','46',1,1,NULL,0),(93,NULL,NULL,'Octopus','Order Octopoda','49',1,1,NULL,0),(94,NULL,NULL,'Opah','Lampris guttatus\nLampris immaculatus','50',1,1,NULL,0),(95,NULL,NULL,'Orange Roughy','Hoplostethus atlanticus','51',1,1,NULL,0),(96,NULL,NULL,'Oyster','Family Ostreidae','52',1,1,NULL,0),(97,NULL,NULL,'Parrot Fish','Family Scaridae','54',1,1,NULL,0),(98,NULL,NULL,'Perch','Perca spp.','55',1,1,NULL,0),(99,NULL,NULL,'Redfish, Ocean Perch','Sebastes norvegicus ','61',1,1,NULL,0),(100,NULL,NULL,'Perch, White','Morone americana','55',1,1,NULL,0),(101,NULL,NULL,'Perch, Yellow','Perca flavescens','55',1,1,NULL,0),(102,NULL,NULL,'Pike, Northern','Esox lucius','56',1,1,NULL,0),(103,NULL,NULL,'Pike, Walleye','Sander vitreus','56',1,1,NULL,0),(104,NULL,NULL,'Pollock, Atlantic','Pollachius pollachius','57',1,1,NULL,0),(105,NULL,NULL,'Pollock','Pollachius pollachius\nPollachius virens','57',1,1,NULL,0),(106,NULL,NULL,'Pollock, Alaska','Theragra chalcogramma','57',1,1,NULL,0),(107,NULL,NULL,'Pompano','Trachinotus spp.','59',1,1,NULL,0),(108,NULL,NULL,'Porgy','Family Sparidae','60',1,1,NULL,0),(109,NULL,NULL,'Rockfish','Sebastes spp.','61',1,1,NULL,0),(110,NULL,NULL,'Salmon','Salmo spp.\nOncorhynchus spp.','63',1,1,NULL,0),(111,NULL,NULL,'Salmon, Atlantic','Salmo salar','63',1,1,NULL,0),(112,NULL,NULL,'Salmon, Coho','Oncoryhnchus kisutch','63',1,1,NULL,0),(113,NULL,NULL,'Salmon, King','Oncoryhnchus tshawytscha','63',1,1,NULL,0),(114,NULL,NULL,'Salmon, Chum','Oncoryhnchus keta','63',1,1,NULL,0),(115,NULL,NULL,'Salmon, Pink','Oncorhynchus gorbuscha','63',1,1,NULL,0),(116,NULL,NULL,'Salmon, Sockeye','Oncoryhnchus nerka','63',1,1,NULL,0),(117,NULL,NULL,'Sanddab','Citharichthys sordidus','28',1,1,NULL,0),(118,NULL,NULL,'Sardine','Sardinops spp\nSardina spp.','64',1,1,NULL,0),(119,NULL,NULL,'Scallop','Family Pectinidae','66',1,1,NULL,0),(120,NULL,NULL,'Scallop, Bay','Argopecten irradians','66',1,1,NULL,0),(121,NULL,NULL,'Scallop, Sea','Placopecten magellanicus','66',1,1,NULL,0),(122,NULL,NULL,'Seabass','Family Serranidae','7',1,1,NULL,0),(123,NULL,NULL,'Seabass, Black','Centropristis striata','7',1,1,NULL,0),(124,NULL,NULL,'Seabass, Chilean','Dissostichus eleginoides','87',1,1,NULL,0),(125,NULL,NULL,'Seabass, European','Dicentrarchus labrax','7',1,1,NULL,0),(126,NULL,NULL,'Shad','Alosa spp.','70',1,1,NULL,0),(127,NULL,NULL,'Shark','','71',1,1,NULL,0),(128,NULL,NULL,'Shark, Longfin Mako','Isurus paucus','71',1,1,NULL,0),(129,NULL,NULL,'Shark, Shortfin Mako','Isurus oxyrinchus','71',1,1,NULL,0),(130,NULL,NULL,'Shark, Soupfin','Galeorhinus galeus','71',1,1,NULL,0),(131,NULL,NULL,'Shark, Common Thresher','Alopias vulpinus','71',1,1,NULL,0),(132,NULL,NULL,'Shark, Bigeye Thresher','Alopias superciliosus','71',1,1,NULL,0),(133,NULL,NULL,'Shark, Pelagic Thresher','Alopias pelagicus','71',1,1,NULL,0),(134,NULL,NULL,'Sheephead','Semicossyphus pulcher','72',1,1,NULL,0),(135,NULL,NULL,'Sheepshead','Archosargus probatocephalus','73',1,1,NULL,0),(136,NULL,NULL,'Shrimp','Order Caridea','74',1,1,NULL,0),(137,NULL,NULL,'Shrimp, Freshwater','Macrobrachium rosenbergii\nMacrobrachium spp.','74',1,1,NULL,0),(138,NULL,NULL,'Skate','Family Rajidae','75',1,1,NULL,0),(139,NULL,NULL,'Smelt','Family Osmeridae','76',1,1,NULL,0),(140,NULL,NULL,'Smelt, Lake','Family Osmeridae','76',1,1,NULL,0),(141,NULL,NULL,'Snail','','48',1,1,NULL,0),(142,NULL,NULL,'Snapper','Family Lutjanidae','77',1,1,NULL,0),(143,NULL,NULL,'Snapper, Brazilian','Lutjanus alexandrei','77',1,1,NULL,0),(144,NULL,NULL,'Snapper, Red','Lutjanus campechanus','77',1,1,NULL,0),(145,NULL,NULL,'Snapper, Silk','Lutjanus vivanus','77',1,1,NULL,0),(146,NULL,NULL,'Snapper, Vermilion','Rhomboplites aurorubens','77',1,1,NULL,0),(147,NULL,NULL,'Snapper, Yellowtail','Ocyurus chrysurus','77',1,1,NULL,0),(148,NULL,NULL,'Sole','Family Soleidae','28',1,1,NULL,0),(149,NULL,NULL,'Sole, Dover','Solea solea\nSolea vulgaris','28',1,1,NULL,0),(150,NULL,NULL,'Sole, English','Parophrys vetulus','28',1,1,NULL,0),(151,NULL,NULL,'Sole, Grey','Glyptocephalus cynoglossus','28',1,1,NULL,0),(152,NULL,NULL,'Sole, Lemon','Microstomus kitt','28',1,1,NULL,0),(153,NULL,NULL,'Sole, Pacific','Microstomus pacificus','28',1,1,NULL,0),(154,NULL,NULL,'Sole, Petrale','Eopsetta jordani','28',1,1,NULL,0),(155,NULL,NULL,'Sole, Rex','Glyptocephalus zachirus','28',1,1,NULL,0),(156,NULL,NULL,'Sole, Rock','Lepidopsetta bilineata','28',1,1,NULL,0),(157,NULL,NULL,'Sole, Yellowfin','Limanda aspera','28',1,1,NULL,0),(158,NULL,NULL,'Spearfish','Tetrapturus angustirostris','79',1,1,NULL,0),(159,NULL,NULL,'Croaker, Spot','Leiostomus xanthurus','24',1,1,NULL,0),(160,NULL,NULL,'Squid','Order Teuthida','80',1,1,NULL,0),(161,NULL,NULL,'Sturgeon','Family Acipenseridae','82',1,1,NULL,0),(162,NULL,NULL,'Swordfish','Xiphias gladius','83',1,1,NULL,0),(163,NULL,NULL,'Tautog','Tautoga onitis','84',1,1,NULL,0),(164,NULL,NULL,'Tilapia','Genus Orechromis\nGenus Sarotherodon\nGenus Tilapia','85',1,1,NULL,0),(165,NULL,NULL,'Tilefish','Family Malacanthidae','86',1,1,NULL,0),(166,NULL,NULL,'Trout','Family Salmonidae, Subfamily Salmoninae','88',1,1,NULL,0),(167,NULL,NULL,'Trout, Golden','Oncoryhnchus mykiss aguabonita','88',1,1,NULL,0),(168,NULL,NULL,'Trout, Lake','Salvelinus namaycush','88',1,1,NULL,0),(169,NULL,NULL,'Trout, Rainbow','Oncoryhnchus mykiss','88',1,1,NULL,0),(170,NULL,NULL,'Trout, Red','','88',1,1,NULL,0),(171,NULL,NULL,'Trout, Sea','Salmo trutta\nOncoryhnchus mykiss irideus','88',1,1,NULL,0),(172,NULL,NULL,'Trout, Steelhead','Oncoryhnchus mykiss irideus','88',1,1,NULL,0),(173,NULL,NULL,'Tuna','Genus Thunnus','89',1,1,NULL,0),(174,NULL,NULL,'Tuna, Albacore','Thunnus alalunga','89',1,1,NULL,0),(175,NULL,NULL,'Tuna, Dogtooth','Gymnosarda unicolor','89',1,1,NULL,0),(176,NULL,NULL,'Tuna, Tongol','Thunnus tonggol','89',1,1,NULL,0),(177,NULL,NULL,'Tuna, Atlantic Bluefin','Thunnus thynnus','89',1,1,NULL,0),(178,NULL,NULL,'Tuna, Southern Bluefin','Thunnus maccoyii','89',1,1,NULL,0),(179,NULL,NULL,'Tuna, Pacific Bluefin','Thunnus orientalis','89',1,1,NULL,0),(180,NULL,NULL,'Tuna, Blackfin','Thunnus atlanticus','89',1,1,NULL,0),(181,NULL,NULL,'Tuna, Skipjack','Katsuwonus pelamis','89',1,1,NULL,0),(182,NULL,NULL,'Tuna, Yellowfin','Thunnus albacares','89',1,1,NULL,0),(183,NULL,NULL,'Turbot','Scopthalmus maximus','28',1,1,NULL,0),(184,NULL,NULL,'Wahoo','Acanthocybium solandri','90',1,1,NULL,0),(185,NULL,NULL,'Walleye','Sander vitreus','91',1,1,NULL,0),(186,NULL,NULL,'Whelk','Family Buccinidae','92',1,1,NULL,0),(187,NULL,NULL,'Whiting','Merluccius bilinearis','94',1,1,NULL,0),(188,NULL,NULL,'Kampachi','Seriola rivoliana','34',1,1,NULL,0),(189,NULL,NULL,'Blackfish','Tautoga onitis','8',1,1,NULL,0),(190,NULL,NULL,'Toothfish','Dissostichus mawsoni','87',1,1,NULL,0),(191,NULL,NULL,'Cod, Rock','Lotella rhacina','19',1,1,NULL,0),(192,NULL,NULL,'Crab, Three-Spotted Swimming','Portunus sanguinolentus','22',1,1,NULL,0),(193,NULL,NULL,'Crab, White Swimming','Portunus trituberculatus','22',1,1,NULL,0),(194,NULL,NULL,'Croaker, Yellow','Larimichthys polyactis','24',1,1,NULL,0),(195,NULL,NULL,'Cuttlefish','','25',1,1,NULL,0),(196,NULL,NULL,'Escolar','Lepidocybium flavobrunneum','27',1,1,NULL,0),(197,NULL,NULL,'Jobfish','Pristipomoides spp.','77',1,1,NULL,0),(198,NULL,NULL,'Snapper, Hawaiian','Pristipomoides filamentosus','77',1,1,NULL,0),(199,NULL,NULL,'Mackerel Scad','Decapterus macarellus','40',1,1,NULL,0),(200,NULL,NULL,'Rockfish, Pacific Ocean Perch','Sebastes alutus','61',1,1,NULL,0),(201,NULL,NULL,'Redfish','Sebastes fasciatus\nSebastes mentella','61',1,1,NULL,0),(202,NULL,NULL,'Rover, Golden','Erythrocles scintillans','62',1,1,NULL,0),(203,NULL,NULL,'Scad, Bigeye','Selar crumenophthalmus','65',1,1,NULL,0),(204,NULL,NULL,'Seabob, Atlantic','Xiphopenaeus kroyeri','68',1,1,NULL,0),(205,NULL,NULL,'Snapper, Crimson','Lutjanus erythropterus','77',1,1,NULL,0),(206,NULL,NULL,'Snapper, Goldband','Pristipomoides Multidens','77',1,1,NULL,0),(207,NULL,NULL,'Snapper, Johns','Lutjanus Johnii','77',1,1,NULL,0),(208,NULL,NULL,'Snapper, Lavender','Pristipomoides sieboldii','77',1,1,NULL,0),(209,NULL,NULL,'Snapper, Lehi','Aphareus rutilans','77',1,1,NULL,0),(210,NULL,NULL,'Snapper, Long-Tail Red','Etelis coruscans','77',1,1,NULL,0),(211,NULL,NULL,'Snapper, Mangrove','Lutjanus griseus','77',1,1,NULL,0),(212,NULL,NULL,'Snapper, Pinjalo','Pinjalo spp.','77',1,1,NULL,0),(213,NULL,NULL,'Snapper, Short-Tail Red','Etelis carbunculus','77',1,1,NULL,0),(214,NULL,NULL,'Snapper, Scarlet','Lutjanus sanguineus\nLutjanus malabaricus','77',1,1,NULL,0),(215,NULL,NULL,'Soldierfish','','78',1,1,NULL,0),(216,NULL,NULL,'Spearfish, Shortbill','Tetrapturus angustirostris','79',1,1,NULL,0),(217,NULL,NULL,'Squirrelfish','Holocentridae','81',1,1,NULL,0),(218,NULL,NULL,'Tuna, Bigeye','Thunnus obesus','89',1,1,NULL,0),(219,NULL,NULL,'Whitefish, Lake','Coregonus clupeaformis','93',1,1,NULL,0),(220,NULL,NULL,'Whitefish, Round','Prosopium cylindraceum','93',1,1,NULL,0),(221,NULL,NULL,'Whitefish','','93',1,1,NULL,0),(222,NULL,NULL,'Halibut','','28',1,1,NULL,0),(223,NULL,NULL,'Drum, Red','Sciaenops ocellatus','7',1,1,NULL,0),(224,NULL,NULL,'Capelin','Mallotus villosus','76',1,1,NULL,0),(225,NULL,NULL,'Clam, New Zealand Cockle','Austrovenus stutchburyi','17',1,1,NULL,0),(226,NULL,NULL,'Clam, Hardshell','Meretrix lyrata','17',1,1,NULL,0),(227,NULL,NULL,'Crab, Sandy Swimming','Liocarcinus depurator','22',1,1,NULL,0),(228,NULL,NULL,'Crab, Atlantic Rock','Cancer irroratus','22',1,1,NULL,0),(229,NULL,NULL,'Crawfish, Red Swamp','Procambarus clarkii','23',1,1,NULL,0),(230,NULL,NULL,'Croaker, Atlantic','Micropogonias undulatus','24',1,1,NULL,0),(231,NULL,NULL,'Flounder, Arrowtooth','Reinhardtius stomias\nAtheresthes stomias','28',1,1,NULL,0),(232,NULL,NULL,'Plaice','Pleuronectes spp.','28',1,1,NULL,0),(233,NULL,NULL,'Flying Fish','Cheilopogon agoo','29',1,1,NULL,0),(234,NULL,NULL,'Goatfish','Family Mullidae','45',1,1,NULL,0),(235,NULL,NULL,'Hake, White','Urophycis tenuis','32',1,1,NULL,0),(236,NULL,NULL,'Jack, Yellowtail','Seriola lalandi dorsalis','3',1,1,NULL,0),(237,NULL,NULL,'Octopus, Common','Octopus vulgaris','49',1,1,NULL,0),(238,NULL,NULL,'Pomfret','family Bramidae','58',1,1,NULL,0),(239,NULL,NULL,'Scallop, Patagonian','Zygochlamys patagonica','66',1,1,NULL,0),(240,NULL,NULL,'Seabass, White','Atractoscion nobilis','24',1,1,NULL,0),(241,NULL,NULL,'Smelt, Ocean','Family Osmeridae','76',1,1,NULL,0),(242,NULL,NULL,'Smelt, Rainbow','Osmerus mordax','76',1,1,NULL,0),(243,NULL,NULL,'Snapper, Lane','Lutjanus synagris','77',1,1,NULL,0),(244,NULL,NULL,'Sole, Northern Rock','Lutjanus synagris','28',1,1,NULL,0),(245,NULL,NULL,'Sturgeon, Atlantic','Acipenser oxyrinchus oxyrinchus','82',1,1,NULL,0),(246,NULL,NULL,'Crab, Southern King','Lithodes santolla','22',1,1,NULL,0),(247,NULL,NULL,'Scallop, Peruvian','Argopecten purpuratus','66',1,1,NULL,0),(248,NULL,NULL,'Snapper, Cubera','Lutjanus cyanopterus','77',1,1,NULL,0),(249,NULL,NULL,'Hogfish','Lachnolaimus maximus','95',1,1,NULL,0),(250,NULL,NULL,'Prawn, Giant Tiger','Penaeus monodon','74',1,1,NULL,0),(251,NULL,NULL,'Lobster, Tristan da Cunha Rock','Jasus tristani','38',1,1,NULL,0),(252,NULL,NULL,'Cabezon','Scorpaenichthys marmoratus','67',1,1,NULL,0),(253,NULL,NULL,'Seaweed','','69',1,1,NULL,0),(254,NULL,NULL,'Nile Perch','Lates niloticus','47',1,1,NULL,0),(255,NULL,NULL,'Sablefish','Anoplopoma fimbria','',1,1,NULL,0),(256,NULL,NULL,'Black cod','Anoplopoma fimbria','',1,1,NULL,0);
/*!40000 ALTER TABLE `seafoodSpecies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sources`
--

DROP TABLE IF EXISTS `sources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fk_vendor_id` int(11) NOT NULL,
  `fk_seafoodSpecies_id` int(11) NOT NULL,
  `fk_harvestMethod_id` int(11) DEFAULT NULL,
  `supplierCompany` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactEmail` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contactPhone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `show` int(11) NOT NULL,
  `createdBy` int(11) NOT NULL,
  `modifiedBy` int(11) NOT NULL,
  `fk_coolCountry_id` mediumint(9) NOT NULL,
  `fk_harvestCountry_id` mediumint(9) NOT NULL,
  `harvestRegion` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `faoMajorFishingArea` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_flagOfVesselCountry_id` mediumint(9) NOT NULL,
  `certificationType` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `certificationNumber` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `vendorComments` text COLLATE utf8_unicode_ci NOT NULL,
  `supplierCompant` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `fk_seafoodCategory_id` int(11) NOT NULL,
  `scientificName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `highSeasName` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sources_fk_vendor_id_index` (`fk_vendor_id`),
  KEY `sources_fk_fish_id_index` (`fk_seafoodSpecies_id`),
  KEY `sources_fk_gear_id_index` (`fk_harvestMethod_id`)
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sources`
--

LOCK TABLES `sources` WRITE;
/*!40000 ALTER TABLE `sources` DISABLE KEYS */;
INSERT INTO `sources` (`id`, `created_at`, `updated_at`, `fk_vendor_id`, `fk_seafoodSpecies_id`, `fk_harvestMethod_id`, `supplierCompany`, `contactName`, `contactEmail`, `contactPhone`, `show`, `createdBy`, `modifiedBy`, `fk_coolCountry_id`, `fk_harvestCountry_id`, `harvestRegion`, `faoMajorFishingArea`, `fk_flagOfVesselCountry_id`, `certificationType`, `certificationNumber`, `vendorComments`, `supplierCompant`, `fk_seafoodCategory_id`, `scientificName`, `highSeasName`) VALUES (1,NULL,'2016-08-25 20:42:03',1,22,156,'Bob\'s Fish','Bob Boberts','bob@boberts.bob','',0,0,0,10186,10186,'','',0,'','','',NULL,14,'',''),(2,NULL,'2016-08-15 19:00:35',2,10,184,'Bob\'s Fish','Bob Boberts','bob@boberts.bob','',1,0,0,10186,10186,'','',0,'','','',NULL,4,'',''),(3,'2016-08-16 16:16:31','2016-08-25 22:53:53',1,69,157,'Ryan\'s Tasty Fish','Ryan','ryan@iswaycool.duh','',0,2,0,10186,10186,'Monterey Bay','',10077,'','','',NULL,28,'',''),(4,'2016-08-16 16:19:26','2016-08-16 19:22:44',1,116,184,'Ryan\'s Tasty Fish','Ryan','ryan@iswaycool.duh','',1,2,0,10186,10186,'Alaska','',10029,'','','',NULL,63,'',''),(5,'2016-08-16 16:49:53','2016-08-23 16:33:46',1,70,157,'Bob\'s Fish','Bob','bob@bobert.bob','',1,2,0,10186,10186,'','',10086,'','','',NULL,0,'',''),(6,'2016-08-16 22:52:26','2016-08-24 16:51:49',1,69,157,'Ryan\'s Tasty Fish','Ryan','ryan@iswaycool.duh','',0,2,0,10186,10186,'Monterey Bay','',10077,'','','',NULL,0,'',''),(9,'2016-08-17 17:57:14','2016-08-24 17:18:21',1,69,157,'Ryan\'s Tasty Fish','Ryan','ryan@iswaycool.duh','',0,2,2,10186,10186,'Monterey Bay','',10075,'','','',NULL,0,'Paralichthys californicus',''),(11,'2016-08-22 22:05:20','2016-08-22 22:05:20',1,77,199,'Louie\'s Lobster','Louie','louielouie@ohbaby.wegottago','',1,2,0,10083,10083,'Golan Heights','',10122,'','','',NULL,0,'',''),(12,'2016-08-22 22:18:35','2016-08-22 22:18:35',1,78,193,'Sammy\'s Shellfish','Sammy','Sam@iam.seuss','',1,2,0,10014,10014,'','',10131,'','','',NULL,0,'',''),(13,'2016-08-23 21:59:56','2016-08-23 21:59:56',1,56,199,'Cajun Joe\'s Fish','Cajun Joe','joe@cajun.gumbo','',1,2,0,10186,10186,'Loisiana','',10116,'','','',NULL,0,'',''),(14,'2016-08-23 23:06:10','2016-08-24 17:25:58',1,120,171,'Nifty Fish','Nate the Neat','nifty@unitednathans.neat','',0,2,0,10086,10086,'Hokkaido','',10199,'','','',NULL,0,'',''),(15,'2016-08-24 16:51:49','2016-08-24 17:20:16',1,69,157,'Ryan\'s Tasty Fish','Ryan','ryan@iswaycool.duh','',0,2,2,10186,10186,'Monterey Bay','',10082,'','','',NULL,0,'',''),(16,'2016-08-24 17:25:58','2016-08-24 17:26:08',1,120,171,'Nifty Fish','Nate the Neat','nifty@unitednathans.neat','',0,2,2,10086,10086,'Hokkaido','',10199,'','12345','',NULL,0,'',''),(17,'2016-08-24 17:26:08','2016-08-24 17:26:08',1,120,171,'Nifty Fish','Nate the Neat','nifty@unitednathans.neat','',1,2,2,10086,10086,'Hokkaido','',10199,'','','',NULL,0,'',''),(18,'2016-08-24 17:31:12','2016-08-24 17:31:12',1,67,169,'Hake Is Cake','Grandmaster Melle Mel','mel@furious.five','',1,2,0,10186,10186,'','',10073,'','','',NULL,0,'',''),(19,'2016-08-24 18:39:02','2016-08-24 18:39:02',1,92,120,'Hake Is Cake','Grandmaster Melle Mel','mel@furious.five','',1,2,0,10186,10186,'','',0,'','','',NULL,0,'',''),(20,'2016-08-24 19:45:34','2016-08-24 19:51:22',1,118,183,'Testy Test','Mr Testy','test@test.testing','',0,2,0,10038,10038,'','',10131,'','','',NULL,0,'',''),(21,'2016-08-24 20:05:37','2016-08-24 20:05:37',1,106,182,'Peter\'s Pollock','Peter Salmon','pete@oddname.isntit','',1,2,0,10186,10186,'Alaska','',10144,'','','',NULL,0,'',''),(22,'2016-08-24 20:12:42','2016-08-24 20:12:42',1,169,145,'Freshwater Fun','Bill Phillis','billphill@aol.com','',1,2,0,10186,10186,'Oregon','',0,'','','',NULL,0,'',''),(23,'2016-08-24 20:14:04','2016-08-24 20:16:53',1,23,137,'Freshwater Fun','Bill Phillis','billphill@aol.com','',0,2,0,10144,10144,'','',0,'','','',NULL,0,'',''),(24,'2016-08-24 20:16:53','2016-08-25 20:42:16',1,23,137,'Freshwater Fun','Bill Phillis','billphill@aol.com','',0,2,2,10144,10144,'','',10017,'','','',NULL,0,'',''),(25,'2016-08-25 20:42:03','2016-08-25 20:42:03',1,22,156,'Bob\'s Fish','Bob Boberts','bob@boberts.bob','',1,2,2,10186,10186,'','',0,'','','',NULL,0,'',''),(26,'2016-08-25 20:42:16','2016-08-25 20:42:16',1,23,137,'Freshwater Fun','Bill Phillis','billphill@aol.com','',1,2,2,10144,10144,'','',10017,'','','',NULL,0,'',''),(27,'2016-08-25 22:53:53','2016-08-25 22:53:53',1,69,157,'Ryan\'s Tasty Fish','Ryan','ryan@iswaycool.duh','',1,2,2,10186,10186,'Monterey Bay','',10077,'','','',NULL,0,'','');
/*!40000 ALTER TABLE `sources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyProductSources`
--

DROP TABLE IF EXISTS `surveyProductSources`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyProductSources` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fk_product_id` int(11) NOT NULL,
  `fk_source_id` int(11) NOT NULL,
  `fk_survey_id` int(11) NOT NULL,
  `proportionOfProduct` double NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_retailer_id` mediumint(9) NOT NULL,
  `fk_vendor_id` mediumint(9) NOT NULL,
  `surveyProducts_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_product_sources_fk_product_id_index` (`fk_product_id`),
  KEY `survey_product_sources_fk_source_id_index` (`fk_source_id`),
  KEY `survey_product_sources_fk_survey_id_index` (`fk_survey_id`),
  KEY `surveyproductsources_surveyproducts_id_index` (`surveyProducts_id`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyProductSources`
--

LOCK TABLES `surveyProductSources` WRITE;
/*!40000 ALTER TABLE `surveyProductSources` DISABLE KEYS */;
INSERT INTO `surveyProductSources` (`id`, `created_at`, `updated_at`, `fk_product_id`, `fk_source_id`, `fk_survey_id`, `proportionOfProduct`, `created_by`, `modified_by`, `fk_retailer_id`, `fk_vendor_id`, `surveyProducts_id`) VALUES (0,'2016-08-22 18:52:22','2016-08-22 19:06:57',0,1,0,0.5,0,0,0,0,0),(5,'2016-08-22 22:16:59','2016-08-22 22:16:59',5,11,2,1,0,0,0,0,4),(6,'2016-08-22 22:20:39','2016-08-22 22:20:39',6,11,2,0.25,0,0,0,0,5),(7,'2016-08-22 22:20:47','2016-08-22 22:20:47',6,12,2,0.75,0,0,0,0,5),(9,'2016-08-23 18:37:57','2016-08-23 18:37:57',0,11,0,0,0,0,0,0,0),(14,'2016-08-23 20:33:14','2016-08-23 20:33:14',4,11,2,0.8,0,0,0,0,3),(15,'2016-08-23 20:33:34','2016-08-23 20:33:34',4,12,2,0.2,0,0,0,0,3),(17,'2016-08-23 22:02:26','2016-08-23 22:02:26',3,13,2,1,0,0,0,0,2),(18,'2016-08-23 22:07:37','2016-08-23 22:07:37',7,11,2,1,0,0,0,0,6),(19,'2016-08-23 22:24:17','2016-08-23 22:24:17',8,12,2,1,0,0,0,0,7),(21,'2016-08-24 19:51:09','2016-08-24 19:51:09',1,1,2,1,0,0,0,0,1),(22,'2016-08-24 19:52:12','2016-08-24 19:52:12',9,17,2,1,0,0,0,0,8),(23,'2016-08-24 19:52:27','2016-08-24 19:52:27',10,19,2,1,0,0,0,0,9),(25,'2016-08-24 20:08:46','2016-08-24 20:08:46',12,21,2,1,0,0,0,0,11),(26,'2016-08-24 20:09:45','2016-08-24 20:09:45',13,18,2,1,0,0,0,0,12),(27,'2016-08-24 20:12:57','2016-08-24 20:12:57',14,22,2,1,0,0,0,0,13),(28,'2016-08-24 20:13:07','2016-08-24 20:13:07',15,22,2,1,0,0,0,0,14),(29,'2016-08-24 20:13:20','2016-08-24 20:13:20',16,22,2,1,0,0,0,0,15),(30,'2016-08-24 20:14:17','2016-08-24 20:14:17',17,23,2,1,0,0,0,0,16),(31,'2016-08-24 20:14:36','2016-08-24 20:14:36',18,23,2,1,0,0,0,0,17),(32,'2016-08-24 20:14:46','2016-08-24 20:14:46',19,23,2,1,0,0,0,0,18),(33,'2016-08-24 20:14:56','2016-08-24 20:14:56',20,23,2,1,0,0,0,0,19),(34,'2016-08-24 20:15:06','2016-08-24 20:15:06',21,23,2,1,0,0,0,0,20),(35,'2016-08-24 20:15:39','2016-08-24 20:15:39',11,21,2,1,0,0,0,0,10);
/*!40000 ALTER TABLE `surveyProductSources` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveyProducts`
--

DROP TABLE IF EXISTS `surveyProducts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveyProducts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `fk_survey_id` int(11) NOT NULL,
  `fk_product_id` int(11) NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fk_retailer_id` mediumint(9) NOT NULL,
  `fk_vendor_id` mediumint(9) NOT NULL,
  `markedComplete` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `survey_products_fk_survey_id_index` (`fk_survey_id`),
  KEY `survey_products_fk_product_id_index` (`fk_product_id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveyProducts`
--

LOCK TABLES `surveyProducts` WRITE;
/*!40000 ALTER TABLE `surveyProducts` DISABLE KEYS */;
INSERT INTO `surveyProducts` (`id`, `created_at`, `updated_at`, `fk_survey_id`, `fk_product_id`, `created_by`, `modified_by`, `fk_retailer_id`, `fk_vendor_id`, `markedComplete`) VALUES (1,NULL,NULL,2,1,1,1,1,1,0),(2,NULL,NULL,2,3,1,1,1,1,0),(3,NULL,NULL,2,4,1,1,1,1,0),(4,NULL,NULL,2,5,1,1,1,1,0),(5,NULL,NULL,2,6,1,1,1,1,0),(6,NULL,NULL,2,7,1,1,1,1,0),(7,NULL,NULL,2,8,1,1,1,1,0),(8,NULL,NULL,2,9,1,1,1,1,0),(9,NULL,NULL,2,10,1,1,1,1,0),(10,NULL,NULL,2,11,1,1,1,1,0),(11,NULL,NULL,2,12,1,1,1,1,0),(12,NULL,NULL,2,13,1,1,1,1,0),(13,NULL,NULL,2,14,1,1,1,1,0),(14,NULL,NULL,2,15,1,1,1,1,0),(15,NULL,NULL,2,16,1,1,1,1,0),(16,NULL,NULL,2,17,1,1,1,1,0),(17,NULL,NULL,2,18,1,1,1,1,0),(18,NULL,NULL,2,19,1,1,1,1,0),(19,NULL,NULL,2,20,1,1,1,1,0),(20,NULL,NULL,2,21,1,1,1,1,0);
/*!40000 ALTER TABLE `surveyProducts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `surveys`
--

DROP TABLE IF EXISTS `surveys`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `surveys` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `surveyStatus` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `fk_retailer_id` int(10) unsigned NOT NULL,
  `fk_vendor_id` int(10) unsigned NOT NULL,
  `dateIssued` date NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `dateDue` date DEFAULT NULL,
  `retailerComments` text COLLATE utf8_unicode_ci,
  `fishwiseComments` text COLLATE utf8_unicode_ci,
  PRIMARY KEY (`id`),
  KEY `surveys_fk_retailer_id_index` (`fk_retailer_id`),
  KEY `surveys_fk_vendor_id_index` (`fk_vendor_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `surveys`
--

LOCK TABLES `surveys` WRITE;
/*!40000 ALTER TABLE `surveys` DISABLE KEYS */;
INSERT INTO `surveys` (`id`, `created_at`, `updated_at`, `surveyStatus`, `fk_retailer_id`, `fk_vendor_id`, `dateIssued`, `created_by`, `modified_by`, `dateDue`, `retailerComments`, `fishwiseComments`) VALUES (2,'2016-08-18 18:49:55','2016-08-24 21:05:49','Submitted to Fishwise',1,1,'2016-08-18',1,0,NULL,NULL,NULL);
/*!40000 ALTER TABLE `surveys` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `user_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `org_id` int(11) NOT NULL,
  `admin` tinyint(4) NOT NULL,
  `approved_user` tinyint(4) NOT NULL,
  `resetNeeded` tinyint(4) DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `created_by`, `modified_by`, `user_type`, `org_id`, `admin`, `approved_user`, `resetNeeded`) VALUES (1,'Ryan Manzer','r.manzer@fishwise.org','$2y$10$WwG2p7Xal4UG4pVsKMp/8uGA5JMLbmFuN/wf21k51.YlFnXJu6C8a','LVoJ9KpRk2NrG0cgi90GYe3t6KvSF4k4GWqA7RtGRvQwk0STUlvjG0prJsrp','2016-08-08 16:23:30','2016-08-31 19:25:07',1,1,'ngo',1,1,0,0),(2,'John Doe','jd@boringname.net','$2y$10$rmbd5P/7KTrbUOfWACG1BeDT2/RjGDAasNPP8Y6svaRTKSNsqslzC','K3Mg6eu0Ir4QDeRcB0m1iahYerxyelyaeibpyHDjWO7c4z0vgQzhYeAk4odS','2016-08-09 00:46:42','2016-08-31 18:56:09',1,1,'vendor',1,0,0,0),(4,'Nat Robinson','n.robinson@fishwise.org','$2y$10$JisZkd4lTEAOLKfFDw3coegiv7U.PcwEuZyHV0MXogPGpyHopwtXS','UP355syACxq2ufd171DJZ6h4ZPmcirHKw5IU4ymjiVZ5vyeK4Wir0pBvmOUl','2016-08-09 16:17:42','2016-08-09 17:29:47',1,1,'ngo',1,1,0,0),(5,'Jane Doe','jd@missing.person','$2y$10$li4zfvp/6kdR.eBtdKw1tO3GdEP03z5CcV.3wwH2Zg4aaiAflSbQ.','4aQD8UhaAA2M8P0uQMNR6uFMUnHfdlEwc5DpPeDmiePpxIZ6NZOt5MAjna8X','2016-08-09 16:28:59','2016-08-31 18:30:41',1,1,'vendor',2,0,0,0),(6,'Tom Jones','tj@funky.town','$2y$10$zJQra9wr0eHn78PMgi9Ff.GK7.rq3lC3lXnSsRUtS3Ycxu6woUQ7i','xfmm3GnfxwyAK9loMCWTXPRFERPvsYuDQ5BVxMoXPh9KE7PtAO2WlNyqO9Zy',NULL,'2016-08-25 20:28:05',0,0,'retailer',1,0,0,0),(7,'Chaka Khan','j.doe@boringname.net','$2y$10$Z1oEnn2lzKIqHHjDHhR1TOTkSrSCiRdV1I8GGGx3K.IFJYYLbOpkO',NULL,NULL,NULL,0,0,'retailer',2,0,0,0),(8,'Elsie Tanadjaja','e.tanadjaja@fishwise.org','$2y$10$Rw7iuoQ4dkIcwCaTaBXnI.lyJ1VfMFXwrbMa284.uS3TsUqAadshi',NULL,'2016-08-31 19:26:54','2016-08-31 19:26:54',0,0,'ngo',1,0,0,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vendors`
--

DROP TABLE IF EXISTS `vendors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vendors` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `contact_phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `add_street_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `add_street_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `add_city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `add_state_prov` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `add_country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address_postal_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_by` int(11) NOT NULL,
  `modified_by` int(11) NOT NULL,
  `fishwiseComments` text COLLATE utf8_unicode_ci,
  `show` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vendors`
--

LOCK TABLES `vendors` WRITE;
/*!40000 ALTER TABLE `vendors` DISABLE KEYS */;
INSERT INTO `vendors` (`id`, `created_at`, `updated_at`, `name`, `contact_name`, `contact_email`, `contact_phone`, `add_street_1`, `add_street_2`, `add_city`, `add_state_prov`, `add_country`, `address_postal_code`, `created_by`, `modified_by`, `fishwiseComments`, `show`) VALUES (1,'2016-08-09 00:50:23','2016-08-09 00:50:23','Tasty Fish','John Doe','jd@boringname.net','(123) 456-7890','123 Boring Ln.','','San Fransisco','CA','','',0,0,NULL,1),(2,'2016-08-09 16:25:27','2016-08-09 16:25:27','Yum Fish','Jane Doe','jd@missing.person','','123 Street St','','Seattle','WA','USA','',1,1,NULL,1);
/*!40000 ALTER TABLE `vendors` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-08-31 12:28:57
