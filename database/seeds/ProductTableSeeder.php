<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //ADDING PRODUCTS TO PRODUCTS TABLE
        $carpID=DB::select('select id from seafoodSpecies where seafoodSpecies=?',['Carp']);
        $carpID=$carpID[0]->id;
        DB::table('products')->insert([
           'fk_vendor_id'=>1,
           'fk_retailer_id'=>1,
           'retailerProductDescription'=>"CARP NATURAL FIL 2LB",
           'retailerProductCode'=> '055447',
           'fk_seafoodSpecies_id'=>$carpID,
        ]);
       $carpID=DB::select('select id from seafoodSpecies where seafoodSpecies=?',['Anchovy']);
       $carpID=$carpID[0]->id;
        DB::table('products')->insert([
           'fk_vendor_id'=>2,
           'fk_retailer_id'=>2,
           'retailerProductDescription'=>"ANCHOVY FIL 6OZ",
           'retailerProductCode'=> '123456',
           'fk_seafoodSpecies_id'=>$carpID,
        ]);
    }
}
