<?php

use Illuminate\Database\Seeder;

class VendorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SEEDING TWO VENDOR RECORDS
        DB::table('vendors')->insert([
           'vendor_name'=>'Tasty Fish',
           'contact_name'=>'John Doe',
           'contact_email'=>'j.doe@boringname.net',
           
        ]);
        DB::table('vendors')->insert([
           'vendor_name'=>'Yum Fish',
           'contact_name'=>'Jane Doe',
           'contact_email'=>'jane.doe@missingpersons.net',
        ]);
    }
}
