<?php

use Illuminate\Database\Seeder;

class SourcesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SEEDING TWO SOURCE RECORDS
        $carpID=DB::select('select * from seafoodSpecies where seafoodSpecies=?',['Carp']);
        $gearID=DB::select('select * from harvestMethods where harvestMethod=?',['Trolling lines']);
        $vendID=DB::select('select * from vendors where vendor_name=?',['Tasty Fish']);
        $countryID=DB::select('select * from countries where countryName=?',['United States']);
        $carpID=$carpID[0]->id;$gearID=$gearID[0]->id;$vendID=$vendID[0]->id;$countryID=$countryID[0]->id;
        DB::table('sources')->insert([
            'fk_vendor_id'=>$vendID,
            'fk_fish_id'=>$carpID,
            'fk_harvestMethod_id'=>$gearID,
            'supplierCompany' => "Bob's Fish",
            'contactName' => 'Bob Boberts',
            'contactEmail' => 'bob@boberts.bob',
            'fk_coolCountry_id' => $countryID,
            'fk_harvestCountry_id' => $countryID,
            
        ]);
        $carpID=DB::select('select * from seafoodSpecies where seafoodSpecies=?',['Anchovy']);
        $gearID=DB::select('select * from harvestMethods where harvestMethod=?',['Purse seines']);
        $vendID=DB::select('select * from vendors where vendor_name=?',['Yum Fish']);
        $carpID=$carpID[0]->id;$gearID=$gearID[0]->id;$vendID=$vendID[0]->id;
        DB::table('sources')->insert([
            'fk_vendor_id'=>$vendID,
            'fk_fish_id'=>$carpID,
            'fk_harvestMethod_id'=>$gearID,
            'supplierCompany' => "Bob's Fish",
            'contactName' => 'Bob Boberts',
            'contactEmail' => 'bob@boberts.bob',
            'fk_coolCountry_id' => $countryID,
            'fk_harvestCountry_id' => $countryID,
        ]);
    }
}
