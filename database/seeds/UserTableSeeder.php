<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SEEDING TWO NGO USERS <-- UNCOMMENT THIS SECTION IF WE WANT TO REPLACE THE ENTIRE DB
     /*   DB::table('users')->insert([
           'name' => 'Ryan Manzer',
           'email' => 'r.manzer@fishwise.org',
           'password' => bcrypt('password'),
           'user_type' => 'ngo',
           'org_id' => 1,
           'admin' => 1,
        ]);
        
        DB::table('users')->insert([
           'name' => 'Not Robinson',
           'email' => 'n.robinson@fishwise.org',
           'password' => bcrypt('password'),
           'user_type' => 'ngo',
           'org_id' => 1,
           'admin' => 1,
        ]);
        //SEEDING TWO VENDOR USERS
        DB::table('users')->insert([
           'name' => 'John Doe',
           'email' => 'j.doe@boringname.net',
           'password' => bcrypt('password'),
           'user_type' => 'vendor',
           'org_id' => 1,
           'admin' => 0,
        ]);
        DB::table('users')->insert([
           'name' => 'Jane Doe',
           'email' => 'jane.doe@missingpersons.net',
           'password' => bcrypt('password'),
           'user_type' => 'vendor',
           'org_id' => 2,
           'admin' => 0,
        ]); */
        //SEEDING TWO RETAILER USERS
        DB::table('users')->insert([
           'name' => 'Tom Jones',
           'email' => 'tj@funky.town',
           'password' => bcrypt('password'),
           'user_type' => 'retailer',
           'org_id' => 1,
           'admin' => 0,
        ]);
        DB::table('users')->insert([
           'name' => 'Chaka Khan',
           'email' => 'j.doe@boringname.net',
           'password' => bcrypt('password'),
           'user_type' => 'retailer',
           'org_id' => 2,
           'admin' => 0,
        ]);
    }
}
