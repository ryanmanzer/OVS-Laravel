<?php

use Illuminate\Database\Seeder;

class RetailerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //SEEDING TWO SEAFOOD RETAILERS
        DB::table('retailers')->insert([
           'name'=>'Off Target',
           'contact_name'=>'Tor Jansen',
           'contact_phone'=>'123-456-7890',
           'contact_email'=>'tj@bigswede.com'
        ]);
        DB::table('retailers')->insert([
           'name'=>'LowBee',
           'contact_name'=>'Phillip Marlowe',
           'contact_phone'=>'789-456-1230',
           'contact_email'=>'phil@private.eye'
        ]);
    }
}
