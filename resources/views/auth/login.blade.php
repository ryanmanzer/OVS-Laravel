@extends('layouts.app')

@section('content')
<div class="container-fluid body">
    <div class="jumbotron" style= "margin-top: 15px;">
        <h2>Welcome to the FishWise {{env('APP_NAME','Online Vendor Surveys')}} application!</h2>

        <blockquote class="blockquote-reverse">
          <p>Access to this application is only provided to authorized users.  If you
          have been granted access to the OVS system then please log in below.</p>
        
        </blockquote>

    </div>
<div class="row" id = "unsupportedBrowser" hidden>
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
        <div class="panel-heading">Unsupported Browser</div>
        <div class="panel-body">
               Currently the Fishwise Online vendor survey does not support the following browsers:<br>
               <li>Safari</li>
               <li>Internet Explorer</li>
               <br>
               The recommended browsers for using this site are Chrome (by Google) and Firefox (by Mozilla).<br>
               
               <li>To download and install Chrome:<a href="https://www.google.com/chrome/browser/desktop/index.html"><img src="/css/images/logos/google_chrome_icon.png" class="img-responsive" width="26" height="26" style="display: inline-block"/>Google Chrome Install Site</a></li>
               
               <li>To download and install Firefox:<a href="https://www.mozilla.org/en-US/firefox/new/"><img src="/css/images/logos/firefox_icon.png" class="img-responsive" width="26" height="26" style="display: inline-block"/>Firefox Install Site</a></li>
               <br>
               We apologize for the inconvenience and will be working in the future to make our system compatible with all browsers.<br><br>
               - The FishWise Online Vendor Survey development team
         </div>
    </div>
</div>
</div>
<div class="row" id = "main_body">
        <div class="mywarning">
      <p class="mywarning" style="font-size: 12px;">
        For best results, use this site with <a href="https://www.google.com/chrome/browser/desktop/index.html">Chrome</a>
        or <a href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a> browsers.
      </p>
    </div>

        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>
                <div class="panel-body">
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password">

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-btn fa-sign-in"></i> Login
                                </button>

                                <a class="btn btn-link" href="{{ url('/password/reset') }}">Forgot Your Password?</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<div id="unsupportedMobile">
    <div class="panel panel-default">
        <div class="panel-heading">Mobile Browsing Not Supported</div>
        <div class="panel-body">
            Currently the Fishwise Online vendor survey does not support use on mobile devices.<br><br>
            We apologize for the inconvenience and will be working in the future to make our system compatible with all browsers.<br><br>
            - The FishWise Online Vendor Survey development team
        </div>
    </div>
</div>
<script>

function checkBrowser(){
    
    //check for Safari
        var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';
    // Safari <= 9 "[object HTMLElementConstructor]" 
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;
    // Chrome 1+
        var isChrome = !!window.chrome && !!window.chrome.webstore;
    // Blink engine detection
        var isBlink = (isChrome || isOpera) && !!window.CSS;
        if (isSafari) {
 
           document.getElementById("main_body").hidden = true;
           document.getElementById("unsupportedBrowser").hidden = false;

        } else if (isIE) {
           document.getElementById("main_body").hidden = true;
           document.getElementById("unsupportedBrowser").hidden = false;
        }/* else if (isChrome) {
            $("#main_body").html("NOT SUPPORTED");
            alert("Chrome IS NOT SUPPORTED");
            $.ajax({ 
                type :'GET', 
                url: '/unsupported_browser/Chrome', 
                success: function(response){
                    $("#main_body").html(response); 
                }
            });
        } */

    } 

checkBrowser();

</script>
@endsection
