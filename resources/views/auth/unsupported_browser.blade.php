@if(Isset($browser))
  
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-default">
    	<div class="panel-heading">Unsupported Browser</div>
        <div class="panel-body">
               Currently the Fishwise Online vendor survey does not support the following browsers:<br>
               <li>Safari</li>
               <li>INternet Explorer</li>
               <br>
               The recommended browsers for using this site are Chrome (by Google) and Firefox (by Mozilla).<br>
               
               <li>To download and install Chrome:<br><a href="https://www.google.com/chrome/browser/desktop/index.html"><img src="/css/images/logos/google_chrome_icon.png" class="img-responsive" width="26" height="26"/>Google Chrome Install Site</a></li>
               <li>To download and install Firefox:<br><a href="https://www.mozilla.org/en-US/firefox/new/"><img src="/css/images/logos/firefox_icon.png" class="img-responsive" width="26" height="26"/>Firefox Install Site</a></li>
               <br>
               We apologize for the inconvenience and will be wrking in the future to make our system compatible with all browsers.<br><br>
               - The FishWise Online Vendor Survey development team
         </div>
    </div>
</div>

@else
  BROWSER NOT SET!!!
@endif