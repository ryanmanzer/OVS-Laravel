@extends('layouts.app')

@section('content')

<div class="container-fluid body">
  <div class="jumbotron">
    <div class="row">
      <div class="col-sm-8">
        <h1>Coming Soon - FishWise Online Vendor Survey</h1>
      </div>
      <div class="col-sm-4">
        <img src="css/images/logos/Fishwise_logo.jpg" class="img-rounded" width="368" height="124"
        style="" />
      </div>
    </div>


    <p>
      This site will soon be hosting the FishWise Online Vendor Survey.  At that
      time users will be able to log in interact with the system.  We anticipate
      this system will be available in October.
    </p>
  </div>
</div>
@endsection
