@extends('layouts.app')
<script>
    //jQuery(document).ready(function)
</script>
@section('content')
<div class="container-fluid body">
    <h2>Edit Vendor Info for {{$vendor->name}}</h2>
    <form role="form" method="POST" action="/vendor/{{$vendor->id}}">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
                <h4 class="alert alert-info">Company</h4>
                <div class="form-group">
                    <label for="name">Vendor Name: </label>
                    <input type="text" id="name" name="name" value="{{$vendor->name }}">
                </div>
                <h4>Point of Contact</h4>
                <div class="form-group">
                    <label for="contact_name">Enter Contact Name: </label>
                    <input type="text" id="contact_name" name="contact_name" value="{{$vendor->contact_name}}">
                </div>
                <div class="form-group">
                    <label for="contact_email">Enter Contact Email: </label>
                    <input type="text" id="contact_email" name="contact_email" value="{{$vendor->contact_email}}">
                </div>
                <div class="form-group">
                    <label for="contact_phone">Enter Contact Phane Number: </label>
                    <input type="text" id="contact_phone" name="contact_phone" value="{{$vendor->contact_phone}}">
                </div>
        <hr>
        <div class="form-group submit-button">
            <button class="btn btn-info" type="submit">Update Info</button>
        </div>
    </form>
    @if(strcmp($user->user_type,'ngo')==0)
        <div class="form-group cancel-button">
          <button class="btn btn-default getAJAX" tag="#vendors"
          url="">Cancel</button>
        </div>
    @else
      <form action="/vendor" method="get">
          <div class="form-group cancel-button">
              <button class="btn btn-default">Cancel</button>
          </div>
      </form>
    @endif
</div>

@stop
