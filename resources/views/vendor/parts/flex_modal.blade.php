<!-- FLEX MODAL INNARDS -->
<div id="flexModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- building modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span class="fa fa-times-circle fa-lg"></span></button>
                <h4 class="modal-title" id="flexTitle">Delete Source Record</h4>
            </div>
            <div class="modal-body">
               <h3 id="flexMessage">Are you sure you want to delete this record?</h3>
               <button class="btn btn-info btn-lg" id="flexYes" data-dismiss="modal">Yes</button>
               <button class="btn btn-secondary btn-lg" id="flexNo" data-dismiss="modal">No</button>
               <button class="btn btn-secondary btn-lg" id="flexOk" data-dismiss="modal">OK</button>
            </div>
        </div>
    </div>
</div>


