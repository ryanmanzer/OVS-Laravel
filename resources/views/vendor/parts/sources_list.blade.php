<!-- view.vendor.parts.sources_list.blade.php
 This blade will be passed a $sources variable that contains all sources for a specific
 vendor.
 -->
 <p></p>

@if(isset($sources))
  <div class="header">
      <h3>Seafood Sources</h3>
  </div>
<div id="mysources" class="table sources_container">
<input type="hidden" id="vendor_id" value="{{$vendor->id}}">
<table class="table table-hover ">
    <thead>
        <tr>
            <th>Company</th>
            <th>Seafood Common Name</th>
            <th>Wild/Farmed</th>
            <th>Harvest Method</th>
            <th>Catch Country</th>
            <th>Catch Region</th>
            <th>COOL</th>
            <th>
              Certification Type
            </th>
            <th></th>
            <th colspan="3">
              <button class="btn btn-sm btn-info assignModal" 
              {{-- data-toggle="modal" data-target="#newSource" --}}
            ><i class="fa fa-plus" aria-hidden="true"></i>Add New Source</button>
          </th>
        </tr>
    </thead>
    <tbody class="source_table">
      @if(count($sources) > 0)  
        @foreach($sources as $source)
        {!! catTable($source,12) !!}
          <tr id="sourcesummary_{{$source->id}}">
          @include('vendor.parts.source_row')
          </tr>
          @if($source->id == $edit_source->id)
            <tr id="sourcerow_{{$source->id}}" class="outputrow" style="background-color:#f9f9f9;">
          @else 
            <tr id="sourcerow_{{$source->id}}" class="outputrow" style="display: none; background-color:#f9f9f9;">
          @endif
              <td colspan="12">
                <div id =  "{{$source->id}}_sps_count_div">
                  <input type="hidden" id="{{$source->id}}_sps_count" value ="{{count($source->sps)}}">
                </div>
                <div id="source_{{$source->id}}" class="myoutput">
                  @if($source->id == $edit_source->id)
                    @include('source.edit')
                  @endif
                </div>
              </td>
            </tr>
        @endforeach
      @else
        <tr><td colspan = 12>No seafoof sources have been defined yet.  Add new Sources here or during the survey process</td></tr>
      @endif

    </tbody>
 </table>
{{$sources->links()}}
</div>
@else
    <h3>Sources varaible not set</h3>
@endif
<script>
  var tag='#sources';
  var url="/vendor/{{$vendor->id}}/sources";
  /*editSourceHandler("div.myoutput",tag,url);*/
  //inlineAJAX();
  //assignFormClose('div.myoutput');
  //myPages("div.sources_container","#sources");
  $("button.delete").off("click");
  $("button.assignModal").off("click");
  $("button.dupSource").off("click");
  $("button.sourceDetails").off("click");
  $("button.sourceEdit").off("click");

  $("button.delete").click(sourceDelete);
  $("button.assignModal").click(newSourceModal);
  $("button.dupSource").click(duplicateSource);
  $("button.sourceDetails").click(sourceDetailsButton);
  $("button.sourceEdit").click(sourceEditButton);
  function newSourceModal()
  {
    newSourceHandler(tag,url);
  }

</script>
