
@if(isset($cert_types)) <!-- ONLY MAKE THIS VISIBLE IF WE HAVE THE CERTIFICATES LOADED TO BUILD THE FORM -->

    <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span class="fa fa-times-circle fa-lg"></span></button>
                <h4 class="modal-title" id="addCertTitle">Add New Certification</h4>
          </div>
          <div class="modal-body">
            <form class="form form-default addCert" url="/vendor/{{$vendor->id}}/certs" tag="#vendor_certs" method="">
              {{csrf_field()}}
              <div class='form-group'>
                <label for='#cert_type'>Certification Type: </label><p class="source_err" id="certification_type_id" style = "display: inline-block; margin-bottom: 5px"></p>
                <select name="certification_type_id" id="cert_type" class="selectpicker form-control" >
                <option value>--select Certification Type--</option>
                @foreach ($cert_types as $mycert)
                  <option value="{{$mycert->id}}">
                  {{$mycert->certType}}
                  </option>
                @endforeach
                </select>
              </div>
              <div class='form-group'>
                <label for='#cert_num'>Certification Number</label><p class="source_err" id="certification_number" style = "display: inline-block; margin-bottom: 5px"></p>
                <input type="text" class='form-control' name="certification_number" id="cert_num" value="" />
              </div>
              <div class='form-group'>
                <label for='desc'>Certification Description (e.g. species, scope, etc)</label><p class="source_err" id="description" style = "display: inline-block; margin-bottom: 5px"></p>
                <input type="text" class='form-control' name="description" id="desc" value="" />
              </div>
              <div class="form-group">
                <button class="btn btn-primary addCert" type="submit" name="button"><i class="fa fa-plus-square" aria-hidden="true"></i>Add Cert</button>
              </div>
            </form>
          </div>
        </div>
    </div>
<script>
  selectPicker();
  function selectPicker()
  {
    $('.selectpicker').selectpicker({});
  }
</script>
@endif
