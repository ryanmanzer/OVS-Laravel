<!-- resources/views/vendor/parts/products.blade.php -->
<div class="page-header">
  <h3>Seafood Products</h3>
</div>

<table class="table table-striped">
  <thead>
    <tr>
      <th>
        Retailer
      </th>
      <th>

      </th>
      <th>
        Product Description
      </th>
      <th>
      </th>
    </tr>
  </thead>
  <tbody>
    @foreach($products as $product)
        {!! catTable($product,4) !!}
        <tr>
          <td>
            {{$product->retailer->name or 'retailer'}}
          </td>
          <td>

          </td>
          <td>
            {{$product->retailerProductDescription or 'Description'}}
          </td>
          <td style="display: none;">
            @if($product->sources->count()>0)
              <button url="/vps/{{$product->id}}/show" tag="#products"
                class="btn btn-small btn-info getAJAX"
              ><i class="fa fa-search" aria-hidden="true"></i> View Sources</button>
            @endif
          </td>
        </tr>
    @endforeach

  </tbody>
</table>
