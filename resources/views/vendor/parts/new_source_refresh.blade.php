
<div class="modal-dialog">
        <!-- building modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><icon class="fa fa-times-circle fa-lg"></icon></button>
                <h4 class="modal-title" id = "newSourceTitle">
                @if(isset($sp))
                    @if($sp->product->seafoodCategory->category == "Mixed")
                        <h4 class="modal-title" id = "newSourceTitle">Enter New Source Information</h4>
                    @else   
                        <h4 class="modal-title" id = "newSourceTitle">Enter New {{$sp->product->seafoodCategory->category}} Source Information</h4>
                    @endif
                @else
                    <h4 class="modal-title" id = "newSourceTitle">Enter New Source Information</h4>
                @endif 
            </div>
            <div class="modal-body">
                <form id="addsource" role="form" method="GET" url="/vendor/{{$vendor->id}}/addsource">
                {{ csrf_field() }}
                    <div class="row">
                        <div class="col-sm-4 ">    
                          @include('vendor.parts.newSource.col1')
                        </div>
                        <div class="col-sm-4 ">
                            @include('vendor.parts.newSource.col2')
                        </div>
                        <div class="col-sm-4 ">
                           @include('vendor.parts.newSource.col3')
                        </div>
                    </div>
                    <div class="form-group">
                      <button type="submit" style="float: right; padding-right: 5px;" class="btn btn-primary">Add Source</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    <script>
  function selectPicker()
  {
    $('.selectpicker').selectpicker({});
  }
  selectPicker();

  $("#newSource").modal('show');
    </script>