
  <div class="header">
      <h3>Vendor Certifications</h3>
  </div>
        <table class="table table-hover">
          <thead>
            <th>
              Certification Type
            </th>
            <th>
              Certification Number
            </th>
            <th>
              Certification Description (e.g. species, scope, etc)            </th>
            <th>

            </th>
          </thead>
          <tbody>
      @if(isset($certs) and count($certs)>0)
            @foreach($certs as $cert)
            <tr>
              <td>
                {{$cert->certType->certType}}
              </td>
              <td>
                {{$cert->certification_number}}
              </td>
              <td>
                {{$cert->description}}
              </td>
              <td>
                <button class="btn btn-danger removeCert" url = "/vendor/{{$vendor->id}}/certs" check-url="confirmRemoveCert/{{$cert->id}}" data-url="/removeCert/{{$cert->id}}" data-id = "{{$cert->id}}" tag="#vendor_certs"><i class="fa fa-trash" aria-hidden="true"></i> Remove Certification</button>
              </td>
            </tr>
          @endforeach
      @else
      <tr><td colspan = 4>-- No vendor-level certifications have been assigned yet --</td></tr>
      @endif
          </tbody>
        </table>
   <hr />

{{-- @include('/vendor/parts/add_cert') --}}
<button class="btn btn-info assignCertModal" {{-- data-toggle="modal" data-target="#newSource" --}}><i class="fa fa-plus" aria-hidden="true"></i>Add New Certification</button>

<script>
$("button.assignCertModal").off("click");
$("button.assignCertModal").click(newCertModal);
$("button.removeCert").off("click");
$("button.removeCert").click(removeCert);
</script>
