<!-- view.vendor.parts.survey_list.blade.php
 This blade will be passed a $surveys variable that contains all surveys for a specific
 vendor.
 -->
 <div class="header">
    <h3>Product Surveys</h3>
</div>
<div class="table">
 <table class="table table-striped">
    <thead>
        <tr>
            <th>Date of Survey</th>
            <th>
              Survey Due Date
            </th>
            <th>Survey Status</th>
            <th>Retailer</th>
            <th>Number of Products</th>
            <th>
              Percent Complete
            </th>
            <th></th>
            <th>
              @if($user->user_type == "retailer")
                <button url="/retailer/{{$user->org_id}}/vendors" class="btn btn-info myright getAJAX"
                  tag="#vendors">Back</button>
              @endif
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach($surveys as $survey)
            <tr>
               <td>{{$survey->dateIssued or ''}}</td>
               <td>
                 {{$survey->dateDue or ''}}
               </td>
               <td>{{$survey->surveyStatus or ''}}</td>
               <td>{{$survey->retailer->name or ''}}</td>
               <td>{{count($survey->sp)}}</td>
               <td>
                 {{intval(perComplete($survey)*100)}} %   {!! getBadge(perComplete($survey)) !!}
               </td>
               <td>
                 @if($user->user_type != "vendor")
                   <button class="btn btn=small btn-warning openSurvey"
                   url="/survey/{{$survey->id}}/products" tag="#surveys"
                   >View Products</button>
                 @else
                     <button class="btn btn-small btn-warning openSurvey"
                     url="/survey/{{$survey->id}}/products" tag="#surveys"
                     ><i class="fa fa-search" aria-hidden="true"></i> View Products</button>
                @endif
               </td>
               <td>
                 @if(perComplete($survey)==1 and $user->user_type=="vendor" and !$survey->complete)
                   <form style="display: inline;" action="/survey/{{$survey->id}}/status" method="post">
                     {{ csrf_field() }}
                     <input type="hidden" name="surveyStatus" value="Submitted to Fishwise" />
                     <button class="btn bt-sm-primary" type="submit"><i class="fa fa-check" aria-hidden="true"></i> Submit Survey</button>
                   </form>
                 @endif
               </td>
            </tr>
        @endforeach
    </tbody>
 </table>
</div>
<script>
  $('button.openSurvey').off("click");
  $('button.openSurvey').click(getAJAX);
</script>

