<div class="form-group">
    <label for="wf">Wild or Farmed*: </label><p class="source_err" id="wf" style = "display: inline-block; margin-bottom: 5px"></p>
    <select name="wf" id="wf" class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10>
        <option value selected="selected">--select Wild or Farmed--</option>
        <option value="Wild">Wild</option>
        <option value="Farmed">Farmed</option>
    </select>
</div>

<div class="form-group">
    <label for="method_cat">Harvest Method Category*: </label><p class="source_err" id="method_cat" style = "display: inline-block; margin-bottom: 5px"></p>
    <select class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10 type="text" name="method_cat" id="methodCat"
    autocomplete="off">
    <option value selected="selected" disabled="disabled">--no Wild Farmed selected yet--</option>
    </select>
    
</div>
<div class="form-group">
    <label for="gear_type">Specific Method*: </label><p class="source_err" id="gear_type" style = "display: inline-block; margin-bottom: 5px"></p>
    <select name="gear_type" id="gear_type" class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10
    autocomplete="off">
      <option value selected="selected" disabled="disabled">--no Method Category selected yet--</option>
  </select>
</div>
<div class="form-group">
    <label for="catch_country">Catch Country*: </label><p class="source_err" id="catch_country" style = "display: inline-block; margin-bottom: 5px"></p>
    <select class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10  name="catch_country" id='catch_country'  data-live-search="true" autocomplete="off"
    value="">
    <option value selected="selected">--select Catch Country--</option>
      @if(isset($countries))
          @foreach($countries as $country)
              <option value="{{$country->id}}" >{{$country->countryName}}</option>
              @if ($country->sortOrder == 13)
                  <option disabled>──────────</option>
              @endif
          @endforeach
      @endif
  </select>
    
</div>
<div class="form-group">
    <label for="catch_region">Catch Region*: </label><p class="source_err" id="harvestRegion" style = "display: inline-block; margin-bottom: 5px"></p>
    <input type="text" class="form-control" name="harvestRegion" autocomplete="off">
    
</div>
 
<div class="form-group">
    <label for="cool">Country of Origin Labeling (COOL)*:</label><p class="source_err" id="cool" style = "display: inline-block; margin-bottom: 5px"></p>
    <select class="form-control dropdown selectpicker" name="cool" id="COOL" data-dropup-auto = false data-size= 10 data-live-search="true" autocomplete="off">
    <option value selected="selected">--select COOL Country--</option>    
      @if(isset($countries))
          @foreach($countries as $country)
              <option value="{{$country->id}}" >{{$country->countryName}}</option>
          @if ($country->sortOrder == 13)
                  <option disabled>──────────</option>
          @endif    
          @endforeach
      @endif
    </select>
    
</div>
