<div class="form-group">
    <label for="FAO_MFA">FAO Major Fishing Area: </label>
    <input type="text" class="form-control" name="faoMajorFishingArea" autocomplete="off">
</div>
<div class="form-group">
    <label for="RFMO">RFMO/High Seas Name</label>
    <input type="text" class="form-control" name="highSeasName" autocomplete="off">
</div>
<div class="form-group">
    <label for="vessel_flag">Flag of Vessel</label>
    <select class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10 ame="vessel_flag" id="v_flag"  data-live-search="true" autocomplete="off">
    <option value selected="selected">--select Vessel Flag Country--</option>        
    @if(isset($countries))
        @foreach($countries as $country)
            <option value="{{$country->id}}" >{{$country->countryName}}</option>
            @if ($country->sortOrder == 13)
              <option disabled>──────────</option>
            @endif
        @endforeach
        
    @endif
  </select>
</div>
<div class="form-group">
    <label for="cert_type">Certification Type</label>
    <select class="form-control" name="certification_types_id" id="certType">
      <option selected="selected" disabled="disabled" value>--no Wild Farmed selected yet--</option>
    </select>
</div>
<div class="form-group">
    <label for="cert_num">Certification Number</label>
    <input type="text" class="form-control" id="cert_num" name="certificationNumber"
    autocomplete="off">
</div>
<div class='form-group'>
  <label for='Comments'>Comments: </label>
  <textarea name="vendorComments" class="form-control" autocomplete="off"></textarea>
</div>
