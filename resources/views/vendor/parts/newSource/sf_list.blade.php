@if(isset($seafood))
    <option selected="selected" disabled="disabled">--select Seafood Common Name--</option>
    @foreach($seafood as $sf)
        <option value="{{$sf->id}}" >
          {{$sf->seafoodSpecies}}
        </option>
    @endforeach
@endif
