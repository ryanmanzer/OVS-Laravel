<div class="form-group">
    <label for="supplierCompany">Supplier*: </label><p class="source_err" id="supplierCompany" style = "display: inline-block; margin-bottom: 5px"></p>
    <input type="text" class="form-control" id = "formStart" name="supplierCompany"
    autocomplete="off" >
    
</div>
<div class="form-group">
    <label for="contact_name">Contact Name: </label>
    <input type="text" class="form-control" name="contactName" autocomplete="off">

</div>
<div class="form-group">
    <label for="contact_email">Contact Email: </label>
    <input type="text" class="form-control" name="contactEmail" autocomplete="off">
</div>
@if(!isset($sp))
  <div class="form-group" id = "seafoodCatGroup">
    <label for="seafood_cat">Seafood Category*: </label><p class="source_err" id="seafoodCategory" style = "display: inline-block; margin-bottom: 5px"></p>
    <input type="text" id = "seafoodCatText" class="form-control" value="" disabled="disabled" style = "display:none">
    <select name="seafoodCategory" class="form-control" id="seafood_cat"
    autocomplete="off" value="">
    <option value selected="selected">--select Seafood Category--</option>
    @if(isset($sfCat))
      @foreach($sfCat as $cat)
        <option value="{{$cat->id}}" >{{$cat->category}}</option>
      @endforeach
    @else
        <option value="Seafood Categories not set"></option>
    @endif
   </select>   
  </div>
  <div class="form-group" id = "sfGroup">
    <label for="_fish" >Seafood Common Name*: </label><p class="source_err" id="seafood" style = "display: inline-block; margin-bottom: 5px"></p> 
      
    <select class="form-control " name="seafood" id = "_fish" autocomplete="off"
    value="">
    <option value selected="selected" >--no Seafood Category selected yet--</option>
    </select>
  </div>
@else
  <div class="form-group">
      <label for="seafood_cat">Seafood Category: </label>
      <input type="text" class="form-control" value="{{$sp->product->seafoodCategory->category}}" disabled="disabled">
      <select name="seafoodCategory" class="form-control seafoodCategory" id="seafood_cat" autocomplete="off" style="display: none;">
      <option  selected="selected" value="{{$sp->product->fk_seafoodCategory_id}}"></option>
      </select>
  </div>
  <div class="form-group">
    <label for="_fish" >Seafood Common Name*: </label><p class="source_err" id="seafood" style = "display: inline-block; margin-bottom: 5px"></p>  
      <select class="form-control seafoodSpecies" name="seafood" autocomplete="off" id="_fish">
      @if($sp->product->seafoodCategory->category == "Mixed")
        <option value selected="selected" disabled="disabled">--no Seafood Category selected yet--</option>
      @else
        <option value selected="selected">--select Seafood Common Name--</option>
        @if(isset($sfs))
          @foreach($sfs as $sf)
                <option value="{{$sf->id}}" >{{$sf->seafoodSpecies}}</option>
          @endforeach
        @endif
      @endif
    </select>     
  </div>
@endif
  <div class="form-group">
    <label for="sci_nam">Seafood Latin Name*: </label><p class="source_err" id="scientificName" style = "display: inline-block; margin-bottom: 5px"></p>
    <input type="text" class="form-control" name="scientificName" autocomplete="off">
  </div>
<p>* = Required field</p>