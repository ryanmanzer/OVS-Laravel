<!-- resources/vendor/parts/trace_hr/surveyForm.blade.php -->
<!-- BUILDING HEADER AND SUBMISSION MESSAGE SECTION -->
@if(isset($survey))
</div>
<div class="page-header col-sm-10 col-xs-12" style = "margin-top: 0px; padding-left: 0px;">
  <h3 style="float:left">{{$survey->survey_type}} Survey for {{$survey->survey_date}}</h3>
  
  <button class="btn btn-sm btn-info myright backToSurveys"
  url="/vendor/{{$survey->fk_vendor_id}}/thr"
  tag="#trace_hr" style="margin-top: 15px;"><i class="fa fa-arrow-left" aria-hidden="true" ></i>Back To Survey List</button>
</div>
<div class="message" style="clear:both">
  @if($survey->complete)
    @include('messages.survey_submit')
  @endif
</div>
<!-- BUILDING FORM SECTION: THIS WILL USE SEVERAL COMPONENT BLADES -->
</div><form class="trace_hr_survey" url="/thrsurveys/{{$survey->id}}" data-url="/vendor/{{$survey->fk_vendor_id}}/thr" tag="#trace_hr" style="
    padding-left: 5px;">
  {{ csrf_field() }}
  @foreach($survey->surveyquestions as $sq)
    @if(!$sq->question->is_followUp)
        <label for='{{$sq->id}}'>{{$sq->question->question_text}} </label>
        @if($sq->question->single_answer)
          @include('vendor.parts.trace_hr.form_parts.yesno', ['sq' => $sq,'survey'=>$survey])
        @elseif($sq->question->is_fill_in)
          @include('vendor.parts.trace_hr.form_parts.open_response',['sq' => $sq,'survey'=>$survey])
        @else
          @include('vendor.parts.trace_hr.form_parts.checkbox', ['sq' => $sq,'survey'=>$survey])
        @endif
    @endif
    <br>
  @endforeach
    <div class="form-group" style="padding-bottom: 20px;">
    <button class="btn btn-primary submit-button"
      @if($survey->complete)
        disabled="true"
      @endif
      type="submit">Save Answers</button>
    <button class="btn btn-default cancel-button"
        @if($survey->complete)
          disabled="true"
        @endif
        type="reset" name="myreset" class="formReset">Clear Survey</button>
    </div>
</form>
@endif

<script type="text/javascript">
$(init);
function init()
{
  assignTraceHrSubmit("form.trace_hr_survey");
  assignFollowUp("input.hasFollowUp");
  hideFollowUp("input.noFollowUp");
  $("button[name='myreset']").click(function(){
    loadComponent('/thrsurveys/2/clear','#trace_hr');
  });
  $("button.backToSurveys").off("click");
  $("button.backToSurveys").click(getAJAX);
}
</script>
</div>
