<!-- resources/views/vendor/parts/trace_hr/survey_list.blade.php -->
  <div class="header">
      <h3>Traceability and Human Rights Surveys</h3>
  </div>
@if(isset($thrSurveys))
<table class="table table-hover">
  <thead>
    <th>
      Survey Date
    </th>
    <th>
      Survey Type
    </th>
    <th>
      # of Questions
    </th>
    <th>
      Survey Status
    </th>
    <th>
    </th>
  </thead>
  <tbody>
    @foreach($thrSurveys as $survey)
      <tr>
        <td>
          {{$survey->survey_date}}
        </td>
        <td>
          {{$survey->survey_type}}
        </td>
        <td>
          {{thr_qcount($survey)}}
        </td>
        <td>
          @if($survey->complete)
            Survey Completed
          @else
            Survey Not Completed
          @endif
        </td>
        <td>
          <button class="btn btn-info btn-sm openSurvey"
          url="/thrsurveys/{{$survey->id}}" tag="#trace_hr" name="viewSurvey" val="{{$survey->id or 1}}">
            <i class="fa fa-list-ol" aria-hidden="true"></i> View/Complete Survey</button>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
<script>
$("button.openSurvey").off("click");
$("button.openSurvey").click(getAJAX);

</script>
@endif
