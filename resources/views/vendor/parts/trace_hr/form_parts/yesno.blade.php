{{-- Loop through each answer to this question --}}
{{-- survclear is a variable = 1 if this is from a clear survey request, otherwise not set --}}
@foreach (ans_map($sq->question) as $key => $value)
  <div class="radio">
    <label>
    <input type="radio" name="sq_{{$sq->id}}" id="{{$sq->id}}" value="{{$key}}"
    @if(isset($survclear))
    @elseif(!is_null($sq->answer))
      @if($sq->answer->response == $key)
        checked="true"          
      @endif
    @endif
    @if(!is_null($sq->question->followUp_trigger))
      @if($sq->question->followUp_trigger == $key)
        class="hasFollowUp" tag="#follow_up_{{$sq->question->id}}"
      @else
        class="noFollowUp" tag="#follow_up_{{$sq->question->id}}"
      @endif
    @endif
    @if($survey->complete)
       disabled="true" 
    @endif
    >{{$value}}</label>
  </div>
  @if(get_follow_up($sq->question,$survey->id))
    @if($sq->question->followUp_trigger == $key)
      <div class="">
        @include('vendor.parts.trace_hr.form_parts.followup',['sq'=>$sq,'survey'=>$survey,'fus'=>get_follow_up($sq->question,$survey->id)])
      </div>
    </div>
    @endif
  @endif
@endforeach
