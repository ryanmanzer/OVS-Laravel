@foreach (ans_map($sq->question) as $key => $value)
  <div class='form-group'>
    <input type="checkbox" name="sq_{{$sq->id}}" id="{{$sq->id}}"
    value="{{$key}}"
    @if(isset($survclear))
    @elseif(!is_null($sq->answer))
        @foreach($sq->answer as $ans)
          @if ($ans->response == $key)
            checked="true"
          @endif
        @endforeach
    @endif      
    @if(!is_null($sq->question->followUp_trigger))
      @if($sq->question->followUp_trigger == $key)
        class="hasFollowUp" tag="#follow_up_{{$sq->question->id}}"
      @endif
    @endif
    @if($survey->complete)
      disabled="true"
    @endif
    /> {{$value}}
  </div>
  @if(get_follow_up($sq->question,$survey->id))
    @if($sq->question->followUp_trigger == $key)

          @include('vendor.parts.trace_hr.form_parts.followup',['sq'=>$sq,'survey'=>$survey,'fus'=>get_follow_up($sq->question,$survey->id)])

    @endif
  @endif
@endforeach
<div class="form-group">
    <input type="text" class="form-control" id="{{$sq->id}}"
    @if(isset($survclear))
      placeholder="Additional info..."
    @else
      @if(!is_null($sq->answer))
        @foreach($sq->answer as $ans)
          @if(!is_null($ans->additional_info))
            value="{{$ans->additional_info}}"
          @endif
        @endforeach
      @endif
      placeholder="Additional info..."
    @endif
    @if($survey->complete)
        disabled="true"
    @endif
     name="additional_info">
</div>
