<h4>Point of Contact</h4>
<ul>
<li class="no-style">Contact Name: {{$org->contact_name}}</li>
<li class="no-style">Contact Phone: {{$org->contact_phone}}</li>
<li class="no-style">Contact Email: {{$org->contact_email}}</li>
</ul>
<p></p>
<button class="btn btn-sm btn-primary editContactInfo" url="/{{$type}}/{{$org->id}}/edit" tag="#contact_info"><i class="fa fa-pencil" aria-hidden="true"></i>Edit Contact Info</button>
<script>
	$("button.editContactInfo").off("click");
	$("button.editContactInfo").click(getAJAX);
</script>