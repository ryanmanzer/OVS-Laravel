        <form id = "contactInfo" tag="#contact_info" url="{{$user->user_type}}/{{$user->org_id}}/contactInfo" data-url="/{{$user->user_type}}/{{$user->org_id}}">
        {{ csrf_field() }}
        <h4>Point of Contact</h4>
        <div class="form-group">
          <label class="control-label" for="contact_name">Contact Name: </label>
          <input type="text" class="form-control" name="contact_name" id="contact_name" value="{{$org->contact_name or ''}}">
        </div>
        <div class='form-group'>
          <label class="control-label" for='contact_phone'>Contact Phone: </label>
          <input type="text" class='form-control' name="contact_phone" id="contact_phone" value="{{$org->contact_phone or ''}}" />
        </div>
        <div class='form-group'>
          <label class="control-label" for='contact_email'>Contact Email: </label>
          <input type="text" class='form-control' name="contact_email" id="contact_email" value="{{$org->contact_email or ''}}" />
        </div>
        <hr/>
        </form>
        <button class="btn btn-sm btn-primary updateContactInfo">Save Updates</button>
        <button class = "btn btn-sm btn-default cancelContactInfo" tag="#contact_info" url="{{$user->user_type}}/{{$user->org_id}}/contactInfo"> Cancel </button>  
   
</form>          

   <script>
                 $("button.updateContactInfo").off("click");
              $("button.updateContactInfo").click(updateContactInfo);

              $("button.cancelContactInfo").off("click");
              $("button.cancelContactInfo").click(getAJAX);
    </script>