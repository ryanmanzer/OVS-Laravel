@if(isset($source))
                
                <td>{{$source->supplierCompany or ''}}</td>
                <td>{{$source->seafoodSpecies->seafoodSpecies or ''}}</td>
                <td>{{$source->harvestMethod->wild_farmed or ''}}</td>
                <td>{{$source->harvestMethod->harvestMethod or ''}}</td>
                <td>{{$source->catchCountry->countryName or ''}}</td>
                <td>{{$source->harvestRegion or ''}}</td>
                <td>{{$source->COOL->countryName or ''}}</td>
                <td>
                  {{$source->certification->certType or ''}}
                </td>
                <td>
                  <button class="btn btn-sm btn-info sourceDetails" tag="#sourcerow_{{$source->id}}"
                    url="source/{{$source->id}}/details">
                    <i class="fa fa-search" aria-hidden="true"></i> Details</button>
                </td>
                <td>
                  <button class="btn btn-sm btn-warning sourceEdit"
                  url="/source/{{$source->id}}/edit" tag="#sourcerow_{{$source->id}}">
                  <i class="fa fa-pencil" aria-hidden="true"></i> Edit</button>
                </td>
                <td>
                  <button class="btn btn-sm btn-warning dupSource"
                  url="/vendor/{{$source->fk_vendor_id}}/sources"
                  tag="#sources" sourceid="{{$source->id}}">
                  <i class="fa fa-plus" aria-hidden="true"></i> Duplicate</button>
                </td>
                <td>
                  <button id="{{$source->id}}"  class="btn btn-sm btn-danger delete"
                    data-toggle="modal" data-target="#delSource"><i class="fa fa-trash" aria-hidden="true"></i> Remove</button>
                </td>
         
  @endif