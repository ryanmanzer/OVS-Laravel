
@if(isset($cert_types)) <!-- ONLY MAKE THIS VISIBLE IF WE HAVE THE CERTIFICATES LOADED TO BUILD THE FORM -->
  <h4><strong>Add New Certification: </strong></h4>
  <div class="row">


    <div class="col-sm-8 col-sm-offset-0">

      <form class="form form-default addCert" url="/vendor/{{$vendor->id}}/certs" tag="#vendor_certs" method="">
        {{csrf_field()}}
        <div class='form-group'>
          <label for='#cert_type'>Certification Type: </label>
          <select name="certification_type_id" id="cert_type">
            @foreach ($cert_types as $mycert)
              <option value="{{$mycert->id}}">
                {{$mycert->certType}}
              </option>
            @endforeach
          </select>
        </div>
        <div class='form-group'>
          <label for='#cert_num'>Certification Number</label>
          <input type="text" class='form-control' name="certification_number" id="cert_num" value="" />
        </div>
        <div class='form-group'>
          <label for='desc'>Certification Description (e.g. species, scope, etc)</label>
          <input type="text" class='form-control' name="description" id="desc" value="" />
        </div>
        <div class="form-group">
          <button class="btn btn-primary addCert" type="submit" name="button"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Cert</button>
        </div>
      </form>
    </div>
  </div>
@endif
