<!-- resources/views/vendor/parts/productSources.blade.php -->


<div class="container-fluid body">
@include('messages.flash')
@if(isset($product))
    <div id="sourceTable">
        <h4>Sources assigned for <strong>{{$product->product->retailerProductDescription or ''}}</strong></h4>
        <form  method="get">
            @if(strcmp($user->user_type,'ngo')==0)
              <button url="/survey/{{$sp->survey->id}}/products" class="btn btn-info myright getAJAX"
                tag="#surveys">Back</button>
            @else
              <button url="/survey/{{$sp->survey->id}}/products" class="btn btn-info myright getAJAX"
                tag="#surveys">Back</button>
            @endif
        </form>
        @if($sps->count() > 0)
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Company</th>
                    <th>Seafood</th>
                    <th>Harvest Method</th>
                    <th>COOL</th>
                    <th>Percent of Product</th>
                    <th>
                      Survey Date
                    </th>
                    <th>

                    </th>
                </tr>
            </thead>
            <tbody>
              @foreach($sps as $source)
                  <tr>
                      <td>{{$source->source->supplierCompany or ''}}</td>
                      <td>{{$source->source->seafoodSpecies->seafoodSpecies or ''}}</td>
                      <td>{{$source->source->harvestMethod->harvestMethod or ''}}</td>
                      <td>{{$source->source->COOL->countryName or ''}}</td>
                      <td>{{$source->proportionOfProduct*100}} %</td>
                      <td>
                        {{$source->survey->dateIssued}}
                      </td>
                      <td>
                        @if(strcmp($user->user_type,'ngo')==0)
                          <!-- NGO staff do not have this capacity in the Web Application -->
                        @else
                          <button id="{{$source->id}}" class='btn btn-danger delSource'
                            data-toggle="modal" data-target="#delSource">Delete Source</button>
                        @endif
                      </td>
                  </tr>
              @endforeach
            </tbody>
        </table>
        @else
            <div class="alert alert-info"><strong>No Sources Yet Assigned</strong></div>
        @endif
    </div>
    <hr>
</div>
@endif
<div id="modal">

</div>
