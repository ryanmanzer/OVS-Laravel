<div style="margin-top: 10px">
    <div class="panel panel-info col-sm-6 col-xs-12" style = "border: none">
        <div class="panel panel-heading">{{ $org->name}} Contact Info</div>
        <div class="panel panel-body" id = "contact_info" style="border: none">
            @include('vendor.parts.accountInfo.show_contact_info')
        </div>
    </div>
    <div class="panel panel-info col-sm-6 col-xs-12" style = "border: none">
        <div class="panel panel-heading">{{ $org->name}} User Accounts</div>
        <div class="panel panel-body" id = "user_accounts" style="border: none">
            <table class="table table-striped"><tbody>
                <thead><tr><th>Name</th><th>Email</th></thead>
                    @if(isset($user_accounts))
                        @if(count($user_accounts) > 0)
                            @foreach($user_accounts as $user_account)
                                <tr><td>{{$user_account->name}}</td><td>{{$user_account->email}}</td></tr>
                            @endforeach
                        @else
                            <tr><td colspan = 2>There are no user accounts for this vendor</td></tr>
                        @endif
                    @endif
                </tbody></table>
        </div>
    </div> 
<div>