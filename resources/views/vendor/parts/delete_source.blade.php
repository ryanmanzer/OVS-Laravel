<!-- resources/views/vendor/parts/modals.blade.php -->

<!-- building modal window for defining a new source -->

<!-- building modal Delete window -->
<div id="delSource" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- building modal content -->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span class="fa fa-times-circle fa-lg"></span></button>
                <h4 class="modal-title" id="delTitle">Delete Source Record</h4>
            </div>
            <div class="modal-body">
               <h3 id="delMessage">Are you sure you want to delete this record?</h3>
               <button class="btn btn-info btn-lg" id="delYes" data-dismiss="modal">Yes</button>
               <button class="btn btn-secondary btn-lg" id="delNo" data-dismiss="modal">No</button>
            </div>
        </div>
    </div>
</div>
