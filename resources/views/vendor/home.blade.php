@extends('layouts.app')
@section('scripts')
    <script>
        
        loadTutorial('main');

        $('.btn-tutorial').click(function() {
            if($('.tutorial-carousel-wrapper.hidden').length > 0){
            $('.tutorial_close').show();
            $('.tutorial-carousel-wrapper').removeClass('hidden');
            }
        })
        
    </script>
<!--
<script>
//Taking a stab at incoroporating AJAX queries into laravel blades.  This is tricky since
//so much more of what is going on is being rendered on the server.  Hard to handle the traffic
// in my head.
    
    $(init);
    //INITIALIZING OUR PAGE
    function init(){
        loadComponent('/vendor/{{$vendor->id}}/info',"#account_info");
        loadComponent("/vendor/{{$vendor->id}}/surveys","#surveys");
        loadComponent("/vendor/{{$vendor->id}}/sources",'#sources');
        loadComponent("/vendor/{{$vendor->id}}/thr",'#trace_hr');
        loadComponent("/vendor/{{$vendor->id}}/certs",'#vendor_certs');
        //loadComponent("/vendor/{{$vendor->id}}/products",'#products');
        //loadComponent("/test/form","#test");
        //assignGET();
        //assignEditFunctions("div#sources"); //assigns the OnChange functions to the select elements in the Edit Source form.
        $("select#seafood_cat").change(getSF);
        $("select#wf").change(getWF);
        $("select#methodCat").change(getMeth);*/
        //Hiding the Source Details panel when a user naviagtes to a different tab.
        $("ul.nav-tabs li a").click(function(){
          $("#mysource_details").hide();
        })
        //$("div#newSource button.close").click(formReset);
    
    }

    //WE NEED THIS HERE SO IT CAN BE CALLED BY ANOTHER JQUERY FUNCTION
    function loadSources()
    {
        loadComponent("/vendor/{{$vendor->id}}/sources",'#sources');
    }

</script>
-->
@stop
@section('content')

<div class="container-fluid body" style = "min-height: 100%; padding-top: 20px; margin-bottom: 10px">
    <div class="row">
        <div class="col-sm-12" id="general_data" style="margin-bottom: 300px;">
          <!--<button class="btn btn-success btn-lg btn-block viewSurveys" url = "/vendor/{{$vendor->id}}/surveysHome" tag = "#general_data" style = "vertical-align: middle; margin-left: auto; margin-right: auto;">View My Current Surveys</button>-->
          <script>
            //$('button.viewSurveys').off("click");
            //$('button.viewSurveys').click(getAJAX);
            loadComponent("/{{$vendor->id}}/splash","#general_data");
            $('a#userNameNavBar').html('{{$vendor->name}}');
          </script>
<!--
          <button class="btn btn-success btn-sm btn-block stepSplash" url = "/{{$vendor->id}}/splash" tag = "#general_data">StepThrough Survey</button>
           <script>
          $('button.stepSplash').off("click")
$('button.stepSplash').click(getAJAX)
</script>
-->
            {{-- @include('messages.flash') --}}
            {{-- @include('errors.errlist') --}}
            <!--
            <ul class="nav nav-tabs">
              <li class="active"><a aria-expanded="true" data-toggle="tab" href="#surveys">Product Surveys</a></li>
              <li><a data-toggle="tab" href="#sources" aria-expanded="false">Seafood Sources </a></li>
              <li><a href="#vendor_certs" data-toggle="tab" aria-expaneded="false">Certifications</a></li>
              <li><a href="#trace_hr" data-toggle="tab" aria-expanded="false">Traceability/Human Rights Surveys</a></li>
              <li><a href="#account_info" data-toggle="tab" aria-expanded="false">Vendor Account Info</a></li>
            <!-- PRODUCTS TAB TO BE USED AFTER THE FIRST ROUND AS A WAY TO SHOW
                VENDORS WHAT THEY HAD PREVIOUSLY SOLD RETAILERS AND THE SOUCRES
                THEY USED.  EVENTUALLY THIS COULD INCLUDE THE 'MEETS POLICY' CHECK
              <li>
                <a data-toggle="tab" href="#products">Products</a>
              </li>
            
            </ul>
            -->
{{--  <div class="tab-content" style="overflow: scroll"></div>  --}}
{{--
            <div class="tab-content">
              <!-- Div where souce table will be rendered -->
              <div id="surveys" class="tab-pane fade in active" style="padding-bottom: 20px;">
              </div>
              <!-- Div where Vendor certifications will be rendered -->
              <div class="tab-pane fade" id="sources" style="padding-bottom: 20px;">
              </div>
              <!-- Div where product surveys will be rendered -->
              <div id="vendor_certs" class="tab-pane fade"  style="padding-bottom: 20px;">
              </div>
              <!-- Div where Traceability/Human Rights surveys will be rendered -->
              <div id="trace_hr" class="tab-pane fade" style="padding-bottom: 20px;">
              </div>
              <div class="tab-pane fade" id="account_info"  style="padding-bottom: 20px;">

              </div>

              <!-- Div where Vendor Products would be rendered if we did that
              <div id="products" class="tab-pane fade table">
              </div>
            -->
            </div>
            --}}
        </div>
    </div>

</div>

@include('/vendor/parts/modals')
@stop
