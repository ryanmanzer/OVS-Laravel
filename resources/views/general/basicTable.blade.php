<!--
This is a basic template for building tables.  It requires two variables, $data and $ fields.
Data will either be an associatve array or an Eloquent collection.  Fields will need to be an
associative array.  The keys will be the field names as they are to be displayed in the
table header and the values will be the field names used in the data array or collection.
-->

@if(isset($collection) and isset($fields))
<!-- Converting Eloquent collection to array -->
    <table class="table table-striped">
        <thead>
            <tr>
                @foreach($fields as $key=>$value)
                    <th>{{$key}}</th>
                @endforeach
            </tr>
        </thead>
        <tbody>
            @foreach($collection as $row)
                <tr>
                    @foreach($fields as $key=>$value)
                        <td>{{$row->$value}}</td>
                    @endforeach
                </tr>
            @endforeach
        </tbody>
    </table>
@endif