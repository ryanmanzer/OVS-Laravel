<div class="panel panel-info">
    <div class="panel panel-heading">{{ $org->name}} Contact Info</div>
    <div class="panel panel-body">
      <form action="/{{$user->user_type}}/{{$user->org_id}}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        {{ method_field('PATCH') }}
        <h4>Point of Contact</h4>
        <div class="form-group">
          <label class="control-label" for="contact_name">Contact Name: </label>
          <input type="text" class="form-control" name="contact_name" id="contact_name" value="{{$org->contact_name or ''}}">
        </div>
        <div class='form-group'>
          <label class="control-label" for='contact_phone'>Contact Phone: </label>
          <input type="text" class='form-control' name="contact_phone" id="contact_phone" value="{{$org->contact_phone or ''}}" />
        </div>
        <div class='form-group'>
          <label class="control-label" for='contact_email'>Contact Email: </label>
          <input type="text" class='form-control' name="contact_email" id="contact_email" value="{{$org->contact_email or ''}}" />
        </div>
        <hr />
        <div class="form-group">
          <button class="btn btn-small btn-info"
          type="submit"> <i class="fa fa-retweet" aria-hidden="true"></i> Update Info</button>
        </div>
        <div class='form-group'>
          <button tag="#contact_info" url="{{$user->user_type}}/{{$user->org_id}}/info"
            class="btn btn-small btn-default getAJAX"> Cancel </button>
        </div>
      </form>
    </div>
</div>
<datalist id="states_list">
	<option value="AL">
	<option value="AK">
	<option value="AZ">
	<option value="AR">
	<option value="CA">
	<option value="CO">
	<option value="CT">
	<option value="DE">
	<option value="FL">
	<option value="GA">
	<option value="HI">
	<option value="ID">
	<option value="IL">
	<option value="IN">
	<option value="IA">
	<option value="KS">
	<option value="KY">
	<option value="LA">
	<option value="ME">
	<option value="MD">
	<option value="MA">
	<option value="MI">
	<option value="MN">
	<option value="MS">
	<option value="MO">
	<option value="MT">
	<option value="NE">
	<option value="NV">
	<option value="NH">
	<option value="NJ">
	<option value="NM">
	<option value="NY">
	<option value="NC">
	<option value="ND">
	<option value="OH">
	<option value="OK">
	<option value="OR">
	<option value="PA">
	<option value="RI">
	<option value="SC">
	<option value="SD">
	<option value="TN">
	<option value="TX">
	<option value="UT">
	<option value="VT">
	<option value="VA">
	<option value="WA">
	<option value="WV">
	<option value="WI">
	<option value="WY">
</datalist>
<datalist id="countries_list">
      <option data-value="10003" value="Afghanistan"></option>
      <option data-value="10004" value="Albania"></option>
      <option data-value="10005" value="Algeria"></option>
      <option data-value="10006" value="Andorra"></option>
      <option data-value="10007" value="Angola"></option>
      <option data-value="10008" value="Antigua and Barbuda"></option>
      <option data-value="10009" value="Argentina"></option>
      <option data-value="10010" value="Armenia"></option>
      <option data-value="10011" value="Australia"></option>
      <option data-value="10012" value="Austria"></option>
      <option data-value="10013" value="Azerbaijan"></option>
      <option data-value="10014" value="Bahamas, The"></option>
      <option data-value="10015" value="Bahrain"></option>
      <option data-value="10016" value="Bangladesh"></option>
      <option data-value="10017" value="Barbados"></option>
      <option data-value="10018" value="Belarus"></option>
      <option data-value="10019" value="Belgium"></option>
      <option data-value="10020" value="Belize"></option>
      <option data-value="10021" value="Benin"></option>
      <option data-value="10022" value="Bhutan"></option>
      <option data-value="10023" value="Bolivia"></option>
      <option data-value="10024" value="Bosnia and Herzegovina"></option>
      <option data-value="10025" value="Botswana"></option>
      <option data-value="10026" value="Brazil"></option>
      <option data-value="10027" value="Brunei"></option>
      <option data-value="10028" value="Bulgaria"></option>
      <option data-value="10029" value="Burkina Faso"></option>
      <option data-value="10030" value="Burundi"></option>
      <option data-value="10031" value="Cambodia"></option>
      <option data-value="10032" value="Cameroon"></option>
      <option data-value="10033" value="Canada"></option>
      <option data-value="10034" value="Cape Verde"></option>
      <option data-value="10035" value="Central African Republic"></option>
      <option data-value="10036" value="Chad"></option>
      <option data-value="10037" value="Chile"></option>
      <option data-value="10038" value="China, Peoples Republic of"></option>
      <option data-value="10039" value="Colombia"></option>
      <option data-value="10040" value="Comoros"></option>
      <option data-value="10041" value="Congo, (Congo Â Kinshasa)"></option>
      <option data-value="10042" value="Congo, (Congo Â Brazzaville)"></option>
      <option data-value="10043" value="Costa Rica"></option>
      <option data-value="10044" value="Cote dIvoire (Ivory Coast)"></option>
      <option data-value="10045" value="Croatia"></option>
      <option data-value="10046" value="Cuba"></option>
      <option data-value="10047" value="Cyprus"></option>
      <option data-value="10048" value="Czech Republic"></option>
      <option data-value="10049" value="Denmark"></option>
      <option data-value="10050" value="Djibouti"></option>
      <option data-value="10051" value="Dominica"></option>
      <option data-value="10052" value="Dominican Republic"></option>
      <option data-value="10053" value="Ecuador"></option>
      <option data-value="10054" value="Egypt"></option>
      <option data-value="10055" value="El Salvador"></option>
      <option data-value="10056" value="Equatorial Guinea"></option>
      <option data-value="10057" value="Eritrea"></option>
      <option data-value="10058" value="Estonia"></option>
      <option data-value="10059" value="Ethiopia"></option>
      <option data-value="10060" value="Fiji"></option>
      <option data-value="10061" value="Finland"></option>
      <option data-value="10062" value="France"></option>
      <option data-value="10063" value="Gabon"></option>
      <option data-value="10064" value="Gambia, The"></option>
      <option data-value="10065" value="Georgia"></option>
      <option data-value="10066" value="Germany"></option>
      <option data-value="10067" value="Ghana"></option>
      <option data-value="10068" value="Greece"></option>
      <option data-value="10069" value="Grenada"></option>
      <option data-value="10070" value="Guatemala"></option>
      <option data-value="10071" value="Guinea"></option>
      <option data-value="10072" value="Guinea-Bissau"></option>
      <option data-value="10073" value="Guyana"></option>
      <option data-value="10074" value="Haiti"></option>
      <option data-value="10075" value="Honduras"></option>
      <option data-value="10076" value="Hungary"></option>
      <option data-value="10077" value="Iceland"></option>
      <option data-value="10078" value="India"></option>
      <option data-value="10079" value="Indonesia"></option>
      <option data-value="10080" value="Iran"></option>
      <option data-value="10081" value="Iraq"></option>
      <option data-value="10082" value="Ireland"></option>
      <option data-value="10083" value="Israel"></option>
      <option data-value="10084" value="Italy"></option>
      <option data-value="10085" value="Jamaica"></option>
      <option data-value="10086" value="Japan"></option>
      <option data-value="10087" value="Jordan"></option>
      <option data-value="10088" value="Kazakhstan"></option>
      <option data-value="10089" value="Kenya"></option>
      <option data-value="10090" value="Kiribati"></option>
      <option data-value="10091" value="Korea, North"></option>
      <option data-value="10092" value="Korea, South"></option>
      <option data-value="10093" value="Kuwait"></option>
      <option data-value="10094" value="Kyrgyzstan"></option>
      <option data-value="10095" value="Laos"></option>
      <option data-value="10096" value="Latvia"></option>
      <option data-value="10097" value="Lebanon"></option>
      <option data-value="10098" value="Lesotho"></option>
      <option data-value="10099" value="Liberia"></option>
      <option data-value="10100" value="Libya"></option>
      <option data-value="10101" value="Liechtenstein"></option>
      <option data-value="10102" value="Lithuania"></option>
      <option data-value="10103" value="Luxembourg"></option>
      <option data-value="10104" value="Macedonia"></option>
      <option data-value="10105" value="Madagascar"></option>
      <option data-value="10106" value="Malawi"></option>
      <option data-value="10107" value="Malaysia"></option>
      <option data-value="10108" value="Maldives"></option>
      <option data-value="10109" value="Mali"></option>
      <option data-value="10110" value="Malta"></option>
      <option data-value="10111" value="Marshall Islands"></option>
      <option data-value="10112" value="Mauritania"></option>
      <option data-value="10113" value="Mauritius"></option>
      <option data-value="10114" value="Mexico"></option>
      <option data-value="10115" value="Micronesia"></option>
      <option data-value="10116" value="Moldova"></option>
      <option data-value="10117" value="Monaco"></option>
      <option data-value="10118" value="Mongolia"></option>
      <option data-value="10119" value="Montenegro"></option>
      <option data-value="10120" value="Morocco"></option>
      <option data-value="10121" value="Mozambique"></option>
      <option data-value="10122" value="Myanmar (Burma)"></option>
      <option data-value="10123" value="Namibia"></option>
      <option data-value="10124" value="Nauru"></option>
      <option data-value="10125" value="Nepal"></option>
      <option data-value="10126" value="Netherlands"></option>
      <option data-value="10127" value="New Zealand"></option>
      <option data-value="10128" value="Nicaragua"></option>
      <option data-value="10129" value="Niger"></option>
      <option data-value="10130" value="Nigeria"></option>
      <option data-value="10131" value="Norway"></option>
      <option data-value="10132" value="Oman"></option>
      <option data-value="10133" value="Pakistan"></option>
      <option data-value="10134" value="Palau"></option>
      <option data-value="10135" value="Panama"></option>
      <option data-value="10136" value="Papua New Guinea"></option>
      <option data-value="10137" value="Paraguay"></option>
      <option data-value="10138" value="Peru"></option>
      <option data-value="10139" value="Philippines"></option>
      <option data-value="10140" value="Poland"></option>
      <option data-value="10141" value="Portugal"></option>
      <option data-value="10142" value="Qatar"></option>
      <option data-value="10143" value="Romania"></option>
      <option data-value="10144" value="Russia"></option>
      <option data-value="10145" value="Rwanda"></option>
      <option data-value="10146" value="Saint Kitts and Nevis"></option>
      <option data-value="10147" value="Saint Lucia"></option>
      <option data-value="10148" value="Saint Vincent and the Grenadines"></option>
      <option data-value="10149" value="Samoa"></option>
      <option data-value="10150" value="San Marino"></option>
      <option data-value="10151" value="Sao Tome and Principe"></option>
      <option data-value="10152" value="Saudi Arabia"></option>
      <option data-value="10153" value="Senegal"></option>
      <option data-value="10154" value="Serbia"></option>
      <option data-value="10155" value="Seychelles"></option>
      <option data-value="10156" value="Sierra Leone"></option>
      <option data-value="10157" value="Singapore"></option>
      <option data-value="10158" value="Slovakia"></option>
      <option data-value="10159" value="Slovenia"></option>
      <option data-value="10160" value="Solomon Islands"></option>
      <option data-value="10161" value="Somalia"></option>
      <option data-value="10162" value="South Africa"></option>
      <option data-value="10163" value="Spain"></option>
      <option data-value="10164" value="Sri Lanka"></option>
      <option data-value="10165" value="Sudan"></option>
      <option data-value="10166" value="Suriname"></option>
      <option data-value="10167" value="Swaziland"></option>
      <option data-value="10168" value="Sweden"></option>
      <option data-value="10169" value="Switzerland"></option>
      <option data-value="10170" value="Syria"></option>
      <option data-value="10171" value="Tajikistan"></option>
      <option data-value="10172" value="Tanzania"></option>
      <option data-value="10173" value="Thailand"></option>
      <option data-value="10174" value="Timor-Leste (East Timor)"></option>
      <option data-value="10175" value="Togo"></option>
      <option data-value="10176" value="Tonga"></option>
      <option data-value="10177" value="Trinidad and Tobago"></option>
      <option data-value="10178" value="Tunisia"></option>
      <option data-value="10179" value="Turkey"></option>
      <option data-value="10180" value="Turkmenistan"></option>
      <option data-value="10181" value="Tuvalu"></option>
      <option data-value="10182" value="Uganda"></option>
      <option data-value="10183" value="Ukraine"></option>
      <option data-value="10184" value="United Arab Emirates"></option>
      <option data-value="10185" value="United Kingdom"></option>
      <option data-value="10186" value="United States"></option>
      <option data-value="10187" value="Uruguay"></option>
      <option data-value="10188" value="Uzbekistan"></option>
      <option data-value="10189" value="Vanuatu"></option>
      <option data-value="10190" value="Vatican City"></option>
      <option data-value="10191" value="Venezuela"></option>
      <option data-value="10192" value="Vietnam"></option>
      <option data-value="10193" value="Yemen"></option>
      <option data-value="10194" value="Zambia"></option>
      <option data-value="10195" value="Zimbabwe"></option>
      <option data-value="10196" value="Abkhazia"></option>
      <option data-value="10197" value="China, Republic of (Taiwan)"></option>
      <option data-value="10198" value="Nagorno-Karabakh"></option>
      <option data-value="10199" value="Northern Cyprus"></option>
      <option data-value="10200" value="Pridnestrovie (Transnistria)"></option>
      <option data-value="10201" value="Somaliland"></option>
      <option data-value="10202" value="South Ossetia"></option>
      <option data-value="10203" value="Ashmore and Cartier Islands"></option>
      <option data-value="10204" value="Christmas Island"></option>
      <option data-value="10205" value="Cocos (Keeling) Islands"></option>
      <option data-value="10206" value="Coral Sea Islands"></option>
      <option data-value="10207" value="Heard Island and McDonald Islands"></option>
      <option data-value="10208" value="Norfolk Island"></option>
      <option data-value="10209" value="New Caledonia"></option>
      <option data-value="10210" value="French Polynesia"></option>
      <option data-value="10211" value="Mayotte"></option>
      <option data-value="10212" value="Saint Barthelemy"></option>
      <option data-value="10213" value="Saint Martin"></option>
      <option data-value="10214" value="Saint Pierre and Miquelon"></option>
      <option data-value="10215" value="Wallis and Futuna"></option>
      <option data-value="10216" value="French Southern and Antarctic Lands"></option>
      <option data-value="10217" value="Clipperton Island"></option>
</datalist>
