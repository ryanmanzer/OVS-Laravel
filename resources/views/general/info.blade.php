<div class="panel panel-info">
    <div class="panel panel-heading">{{ $org->name}} Contact Info</div>
    <div class="panel panel-body">
        <h4>Point of Contact</h4>
        <ul>
            <li class="no-style">Contact Name: {{$org->contact_name}}</li>
            <li class="no-style">Contact Phone: {{$org->contact_phone}}</li>
            <li class="no-style">Contact Email: {{$org->contact_email}}</li>
        </ul>

    <p></p>
    <a url="/{{$type}}/{{$org->id}}/edit" tag="#contact_info" class="getAJAX">Update Info</a>
    </div>
</div>
<p></p>
<div class="panel panel-success" style="display:none;" id="mysource_details">

</div>
