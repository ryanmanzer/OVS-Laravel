<div id="orient-div" class="modal fade modal-wide" role="dialog">
    <div class="modal-dialog">
        <!-- building modal content -->
        <div class="modal-content">
            <!-- <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button> -->
                <h4 class="modal-title">OVS System Orientation</h4>
            </div>
            <div class="modal-body">
              <!-- NOTE: the idea here is to build a carousel that will step users through a series of images with captions.  The images will be screenshots of a user navigating the OVS while the captions provide guidance on how to accomplish the tasks being performed. -->
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                    {{foreach ($variable as $key => $value) {
                      # code...
                    }}}
                  </ol>
                </div>
            </div>
        </div>
    </div>
</div>
