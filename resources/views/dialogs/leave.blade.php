<div id="leaveForm" class="modal fade" role="dialog">
  <div class="modal-dialog">

      <!-- building modal content -->
      <div class="modal-content">
          <div class="modal-header">
              <button type="button" class="close" data-dismiss="modal"><icon class="fa fa-times-circle fa-lg"></icon></button>
              <h4 class="modal-title">Are You Sure?</h4>
          </div>
          <div class="modal-body">
             <h3>Are you sure you want to lave this page?  Any changes you have made will not be saved.</h3>
             <button class="btn btn-info btn-lg" id="leaveYes" data-dismiss="modal">Yes</button>
             <button class="btn btn-secondary btn-lg" id="delNo" data-dismiss="modal">No</button>
          </div>
      </div>
  </div>
</div>
