@extends('layouts.app')
@section('content')
  <div class="container-fluid body">
    <div class="row">
      <div class="col-sm-4" id="myoutput">

      </div>
      <div class="col-sm-8" id="myform">

      </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script>
  $(init);
  function init()
  {
    loadComponent("/test","#myform");
  }
  $("form").submit(function(e){
    e.preventDefault();
    var formData = $("#testToast").serialize();
    //console.log(formData);
    $.ajax({
      type: "POST",
      url: '/test',
      data: formData,
    }).done(function(data){
      $("#myoutput").html(data);
    })
  });
  </script>
@endsection
