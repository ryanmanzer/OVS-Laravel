@extends('layouts.app')

@section('content')
<div class="container-fluid body">
  <div class="row">
    <div class="col-sm-4">
      <form id="test-post">
        {{ csrf_field() }}
        <<div class='form-group'>
          <label for='thing1'></label>
          <input type="text" class='form-control' name="" id="thing1" value="" />
        </div>
        <div class='form-group'>
          <label for='thing2'></label>
          <input type="text" class='form-control' name="" id="thing2" value="" />
        </div>
        <button type="button" id="mysubmit"
        class="btn btn-default btn-sm postAJAX">Submit</button>
      </form>
    </div>
    <div class="col-sm-8" id="output">

    </div>
  </div>
</div>
@endsection
@section('scripts')
  $("#mysubmit").click(TestPost)
  function testPost ()
  {
    $.ajax({
      type:"POST",
      data: $("test-post").serialize(),
      url: "/test-post",
      success: function(data)
      {
        $("#output").html(data);
      }
    })
  }
@endsection
