
  <div class="container-fluid body">
    <div class= "page-header">
      <h3>Test Form with AJAX - Using POST</h3>
    </div>
    <form id="testToast" class="form">
      {{csrf_field() }}
        <div class="form-group">
          <label for="name">Name: </label>
          <input type="text" class="form-control" name="name" placeholder="Enter Name" />
        </div>
        <div class="form-group">
          <label for="email">Email: </label>
          <input type="email" class="form-control" name="email" placeholder="Enter Email" />
        </div>
        <div class="form-group">
          <label for="toast">Favorite Toast: </label>
          <input type="text" class="form-control" name="toast" placeholder="Enter Favorite Toast" />
        </div>
        <hr>
        <div class="form-group submit-button">
            <button id="submit" type="submit" class="btn btn-primary">Update Info</button>
        </div>
        <div class="form-group cancel-button">
            <button class="btn btn-default">Cancel</button>
        </div>
    </form>
    <div id="ajaxResponse">

    </div>
  </div>
