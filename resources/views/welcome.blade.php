@extends('layouts.app')

@section('content')
<div class="container-fluid body">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading"><h4>Welcome to the FishWise Onlinve Vendor Survey</h4></div>
                <div class="panel-body">
                    <p>This application is designed to speed up the reporting process for
                    seafood vendors and generally make everyone's work in this effort
                    more easy to complete.</p>
                    @if(!Auth::check())
                        <p>Access to the application requires an
                        authorized user account.  If you have an account for this system you
                        can log in <a href="/login">here</a> or by clicking the Login link in the
                        upper right of the screen.</p>
                    @else
                        <p>For access to the application plese click <a href="/home">here</a>
                           or click the home link in the upper left section of the navigation
                           bar.</p>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
