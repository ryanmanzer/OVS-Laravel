@extends('layouts.app')
@section('scripts')
<script>
    $(init);
    function init()
    {
        loadComponent('/ngo/{{$ngo->id}}/info',"#contact_info");
        loadComponent('/ngo/retailers','#retailers');
        loadComponent('/ngo/vendors','#vendors');
        loadComponent('/ngo/surveys','#surveys');
        /*
        $("body").on('click','.loadProducts', function(){
          var survey_id=this.id;
          //console.log('Survey id passed to loadSurveyProducts: ' + survey_id);
          var myurl="/survey/" + survey_id + "/products";
          loadComponent(myurl,"#surveys");
        });
        */
        $("body").on("click",'.getAJAX',function(e)
        {
          e.preventDefault();
          var myurl=$(this).attr("url");
          var tag=$(this).attr("tag");
          loadComponent(myurl,tag);
        });
    }


</script>
@stop
@section('content')
<div class="container-fluid body">
    <div class="page-header">
        <h3>{{ $ngo->name }} Home Page</h3>
        <p></p>
    </div>
    <div class="row">
        <div class="col-sm-3" id="contact_info">

        </div>
        <div class="col-sm-9" id="general_data">
            @include('messages.flash')
            <ul class="nav nav-tabs">
                <li ><a data-toggle="tab" href="#retailers">Retailers</a></li>
                <li><a data-toggle="tab" href="#vendors">Vendors</a></li>
                <li><a data-toggle="tab" href="#surveys">Surveys</a></li>
            </ul>
            <div class="tab-content">
                <div id="retailers" class="tab-pane fade in active ">

                </div>
                <div id="vendors" class="tab-pane fade ">

                </div>
                <div id="surveys" class="tab-pane fade ">

                </div>
            </div>
        </div>
    </div>
</div>
@stop
