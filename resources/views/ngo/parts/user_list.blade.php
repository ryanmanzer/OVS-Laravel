<!--- resources/views/ngo/parts/user_list.blade.php -->
@if(isset($users) and count($users)>0)
<div class="page-header">
  <h3>List of Users</h3>
</div>
<table class="table table-hover">
  <thead>
    <th>
      User Name
    </th>
    <th>
      User Email
    </th>
    <th>
      User Organization
    </th>
    <th>
      User Is Admin
    </th>
    <th>
      User Record Updated At
    </th>
    <th>
      Password Reset Link Sent
    </th>
    <th colspan="2">
    </th>
  </thead>
  <tbody>
    @foreach($users as $myuser)
    <tr>
        <td>
          {{$myuser->name}}
        </td>
        <td>
          {{$myuser->email}}
        </td>
        <td>
          {{$myuser->org->name or ''}}
        </td>
        <td>
          {{$myuser->admin or ''}}
        </td>
        <td>
          {{$myuser->updated_at or ''}}
        </td>
        <td>
          {{passwordReset($myuser)}}
        </td>
        <td colspan="2">
          <form class="form resetEmail" url="{{ url('/password/email') }}" tag="#users" data-url="admin/{{$admin->id}}/users">
            {{ csrf_field() }}
            <input style="display: none;" readonly="readonly" name="email" value="{{$myuser->email}}" />
            <button class="btn btn-sm btn-primary" name="button" type="submit"><i class="fa fa-btn fa-envelope" aria-hidden="true"></i> Send Password Email</button>
          </form>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
<script type="text/javascript">
  $("form.resetEmail").on('submit',sendResetEmail);
</script>
@endif
