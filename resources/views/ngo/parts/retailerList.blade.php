<!--
THIS FILE IS USED TO BUILD A TABLE OF ALL RETAILERS IN THE OVS SYSTEM
Given the number of times I am writing this same table code it will likely
be worth my time to invest a little more time and energy into writing a simple
buildTable() function that will write most of this for me.  But right now I know
what I need to do so I'm doing it.
-->

<p></p>

<button class="btn btn-sm btn-info" style="float:right;" data-toggle="modal" data-target="#newSource">Add New Retailer</button>
@if(@isset($retailers))
<table class="table table-striped">
    <thead>
        <tr>
            <th>Retailer Name</th>
            <th>Retailer Scale</th>
            <th>Contact Person</th>
            <th>Contact Email</th>
            <th># of Products</th>
            <th># of Surveys</th>
            <th>FW Comments</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($retailers as $retailer)
            <tr>
                <td>{{$retailer->name}}</td>
                <td>{{$retailer->Scale}}</td>
                <td>{{$retailer->contact_name}}</td>
                <td>{{$retailer->contact_email}}</td>
                <td><a url="retailer/{{$retailer->id}}/products"
                  tag= "#retailers" class="getAJAX">{{$retailer->products->count()}}</a></td>
                <td>{{$retailer->surveys->count()}}</td>
                <td>{{$retailer->fishwiseComments}}</td>
                <td>
                    <form style="display: inline;" action="/retailer/{{$retailer->id}}/edit" method="get">
                        <button class="btn btn=small btn-warning">Edit</button>
                    </form>
                </td>
                <td>
                    <button id="{{$retailer->id}}"  class="btn btn-small btn-danger delete"
                    data-toggle="modal" data-target="#delSource">Delete</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
@endif
