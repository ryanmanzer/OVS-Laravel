<!--
THIS FILE IS USED TO BUILD A TABLE OF ALL VENDORS IN THE OVS SYSTEM
It is part of the data presented to NGO staff at the NGO home page
-->
<p></p>

<button class="btn btn-sm btn-info" style="float:right;" data-toggle="modal" data-target="#newSource">Add New Vendor</button>
@if(@isset($vendors))

<table class="table table-striped">
    <thead>
        <tr>
            <th>Vendor Name</th>
            <th>
              # of Surveys
            </th>
            <th>Contact Person</th>
            <th>Contact Email</th>
            <th>FW Comments</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($vendors as $vendor)
            <tr>
                <td>{{$vendor->name}}</td>
                <td>
                  {{$vendor->surveys->count()}}
                </td>
                <td>{{$vendor->contact_name}}</td>
                <td>{{$vendor->contact_email}}</td>
                <td>{{$vendor->fishwiseComments}}</td>
                <td>
                    <button class="btn btn=small btn-warning getAJAX" tag="#vendors"
                    url="/vendor/{{$vendor->id}}/edit">Edit</button>
                </td>
                <td>
                    <button id="{{$vendor->id}}"  class="btn btn-small btn-danger delete"
                    data-toggle="modal" data-target="#delSource">Delete</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endif
