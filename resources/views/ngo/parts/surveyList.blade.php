<!--
THIS FILE IS USED TO BUILD A TABLE OF ALL SURVEYS IN THE OVS SYSTEM
It is part of the data presented to NGO staff at the NGO home page
-->
<p></p>

<form action="/survey/new" method="get">
    <button class="btn btn-sm btn-info myright" type="submit">Add New Survey</button>
</form>
@if(@isset($surveys))

<table class="table table-striped">
    <thead>
        <tr>
            <th>Retailer</th>
            <th>Vendor</th>
            <th>Date Issued</th>
            <th>Date Due</th>
            <th>
              Survey Status
            </th>
            <th># of Products</th>
            <th>Retailer Comments</th>
            <th>FishWise Comments</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($surveys as $survey)
          @if($survey->surveyStatus == "Submitted to Fishwise")
            <tr class="alert alert-success">

          @else
            <tr>

          @endif

                <td>{{$survey->retailer->name or 'Retailer'}}</td>
                <td>{{$survey->vendor->name or 'Vendor'}}</td>
                <td>{{$survey->dateIssued}}</td>
                <td>{{$survey->dateDue}}</td>
                <td>
                  {{$survey->surveyStatus}}
                </td>
                <td><a url='survey/{{$survey->id}}/products'
                  tag = "#surveys" class='getAJAX'> {{$survey->sp->count()}}</a></td>
                <td>{{$survey->retailerComments}}</td>
                <td>{{$survey->fishwiseComments}}</td>
                <td>
                    <form style="display: inline;" action="/survey/{{$survey->id}}/edit" method="get">
                        <button class="btn btn=small btn-warning">Edit</button>
                    </form>
                </td>
                <td>
                    <button id="{{$survey->id}}"  class="btn btn-small btn-danger delete"
                    data-toggle="modal" data-target="#delSource">Delete</button>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>

@endif
