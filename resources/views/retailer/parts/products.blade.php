<!--
resources/views/retailer/parts/products.blade.php
This file builds a table of products assocaited with a specific vendor
-->
@if(isset($products))

    <table class="table table-striped">
        <thead>
            <tr>
                <th>Product Code</th>
                <th>Product Description</th>
                <th>Seafood Category</th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr>
                    <td>{{$product->retailerProductCode}}</td>
                    <td>{{$product->retailerProductDescription}}</td>
                    <td>{{$product->seafoodCategory->category}}</td>
                    <td>
                    <!-- <form style="display: inline;" action="/product/{{$product->id}}/edit" method="get">
                        <button class="btn btn=small btn-warning">Edit</button> -->
                    </form>
                    </td>
                    <td>
                      <!--  <button id="{{$product->id}}"  class="btn btn-small btn-danger delete"
                        data-toggle="modal" data-target="#delSource">Delete</button> -->
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif
