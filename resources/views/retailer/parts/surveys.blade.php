<!-- /resources/views/retialer/parts/surveys.blade.php -->
@if(isset($surveys))
<table class="table table-striped">
  <thead>
    <th>Vendor</th>
    <th>Date Issued</th>
    <th>Date Due</th>
    <th>Survey Status</th>
    <th># of Products</th>
    <th>Retialer Comments</th>
    <th></th>
  </thead>
  <tbody>
    @foreach($surveys as $survey)
      <tr>
        <td>
          {{$survey->vendor->name}}
        </td>
        <td>
          {{$survey->dateIssued}}
        </td>
        <td>
          {{$survey->dateDue}}
        </td>
        <td>
          {{$survey->surveyStatus}}
        </td>
        <td>
          <a url="/survey/{{$survey->id}}/products" tag="#surveys"
            class="getAJAX" data-toggle="tooltip" title="Click to view survey products for this survey"
            >{{$survey->sp->count()}}</a>
        </td>
        <td>
          {{$survey->retailerComments}}
        </td>
        <td>
          <button class="btn btn-warning" type="button" name="button">Edit</button>
        </td>
      </tr>
    @endforeach
  </tbody>
</table>
@endif
<div class="modal">

</div>
<script>
  $(addTooltip);
</script>
