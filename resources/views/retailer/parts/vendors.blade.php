<!-- resources/views/retailer/parts/vendors.blade.php -->

<p></p>

@if(@isset($vendors))
<table class="table table-striped">
    <thead>
        <tr>
            <th>Vendor Name</th>
            <th>
              # of Surveys
            </th>
            <th>Contact Person</th>
            <th>Contact Email</th>
            <th></th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        @foreach($vendors as $vendor)
            <tr>
                <td>{{$vendor->name}}</td>
                <td>
                  <a url="/vendor/{{$vendor->id}}/surveys" tag="#vendors"
                    class="getAJAX" data-toggle="tooltip" title="Click to view vendor surveys for this vendor"
                    >{{$vendor->surveys->count()}}</a>
                </td>
                <td>{{$vendor->contact_name}}</td>
                <td>{{$vendor->contact_email}}</td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        @endforeach
    </tbody>
</table>
<script>
  $(addTooltip);
</script>
@endif
