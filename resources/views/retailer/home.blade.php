@extends('layouts.app')

@section('contents')
  <div class="container-fluid body">
      <div class="page-header">
          <h3>{{ $org->name }} Home Page</h3>
          <p></p>
      </div>
      <div class="row">
          <div class="col-sm-3" id="contact_info">

          </div>
          <div class="col-sm-9" id="general_data">
              @include('messages.flash')
              <ul class="nav nav-tabs">
                  <li class="active"><a data-toggle="tab" href="#retailers">Retailers</a></li>
                  <li><a data-toggle="tab" href="#vendors">Vendors</a></li>
                  <li><a data-toggle="tab" href="#surveys">Surveys</a></li>
              </ul>
              <div class="tab-content">
                  <div id="retailers" class="tab-pane fade in active table">

                  </div>
                  <div id="vendors" class="tab-pane fade table">

                  </div>
                  <div id="surveys" class="tab-pane fade table">

                  </div>
              </div>
          </div>
      </div>
  </div>

@endsection

@section('scripts')
  
@endsection
