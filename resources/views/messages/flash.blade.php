@if (session()->has('flash_message'))
    <div class="alert alert-{{session('flash_message_level')}}">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>{{session('flash_message')}}</strong>
    </div>
@endif