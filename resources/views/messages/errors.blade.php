@if (session()->has('error_message'))
    <div class="error_message alert alert-{{session('error_message_level')}}">
        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
        <strong>{{session('error_message')}}</strong>
    </div>
@endif
