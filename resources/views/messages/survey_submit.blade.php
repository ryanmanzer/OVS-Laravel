<!-- resources/views/messages/survey_submit.blade.Perhaps

THIS VIEW IS USED TO GENERATE A MESSAGE TO THE USER UPON SUBMITTING A SURVEY.  IT NOTIFIES THEM THAT THE SURVEY DATA IS NOW LOCKED AND IF THEY NEED TO MAKE ANY FURTHER EDITS THEY WILL HAVE TO CONTACT FISHWISE -->

<div class="alert alert-info">
    <h4>Survey Submitted</h4>
  <hr/>
  <p>
    This survey has been submitted and therefore can no longer be modified.  If you need to make some additional edits to this survey, please contact <a href="mailto: n.robinson@fishwise.org"><i class="fa fa-envelope" aria-hidden="true"></i> FishWise</a>.
  </p>
</div>
