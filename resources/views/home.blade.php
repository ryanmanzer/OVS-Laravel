@extends('layouts.app')
@section('scripts')
  <script>
  $(init);
  function init() {
    @foreach($loadComp as $tag=>$url)
      loadComponent("{{$url}}","#{{$tag}}");
    @endforeach
    assignGET();
    addTooltip();
    @if(isset($customInit))
      customInit($customInit);
    @endif

  }
  </script>
  
@endsection
@section('content')
<div class="container-fluid body">
    <div class="page-header">
        <h3>{{ $org->name }} Home Page</h3>
        <p></p>
    </div>
    <div class="row">
        <div class="col-sm-3" id="contact_info">

        </div>
        <div class="col-sm-9" id="general_data">
          @include('messages.flash')
          @include('errors.errlist')
            <ul class="nav nav-tabs">
          @foreach($tabs as $tag=>$title)
            @if(reset($tabs)==$title)
              <li class="active"><a data-toggle="tab" href="#{{$tag}}">{{$title}}</a></li>
            @else
              <li><a data-toggle="tab" href="#{{$tag}}">{{$title}}</a></li>
            @endif
          @endforeach
            </ul>
            <div class="tab-content">
              @foreach($tabs as $tag=>$title)
                @if(reset($tabs)==$title)
                  <div id="{{$tag}}" class="tab-pane fade in active table">
                @else
                  <div id="{{$tag}}" class="tab-pane fade in table">
                  @endif
                </div>
              @endforeach
          </div>
        </div>
      </div>
@if($user->user_type=="vendor")
  @include('/vendor/parts/modals')
@endif
@endsection
