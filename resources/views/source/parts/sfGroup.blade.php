
@if(isset($sp))
  <label for="_fish" >Seafood Common Name: </label><p class="source_err" id="seafood" style = "display: inline-block; margin-bottom: 5px"></p> 
  <select class="form-control seafoodSpecies" name="seafood" autocomplete="off" id="_fish">
  <option value selected="selected">--select Seafood Common Name--</option>
  @if(isset($sfs))
      @foreach($sfs as $sf)
        @if($sf->fk_seafoodCategory_id == $sp->product->fk_seafoodCategory_id)
            <option value="{{$sf->id}}" >{{$sf->seafoodSpecies}}</option>
        @endif
      @endforeach
      </select>
  @endif
@else
  
  <label for="_fish" >Seafood Common Name: </label><p class="source_err" id="seafood" style = "display: inline-block; margin-bottom: 5px"></p> 
      
    <select class="form-control " name="seafood" id = "_fish" autocomplete="off"
    value="">
    <option value selected="selected" >--no Seafood Category selected yet--</option>
    </select>
@endif