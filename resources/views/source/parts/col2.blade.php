<div class="form-group">
    <label for="wf">Wild or Farmed*: </label><p class="source_err" id="wf" style = "display: inline-block; margin-bottom: 5px"></p>

    @if(session()->get('view_only')==true)
      <p id="wf">
        {{$source->wild_farmed or '{no wild or farmed given}'}}
      </p>
    @else
      <select name="wf" id="wf" class="form-control WildFarmed">
      @if(is_null($source->wild_farmed)))
        <option value selected="selected">--select Wild or Farmed--</option>
        <option value="Wild">Wild</option>
        <option value="Farmed">Farmed</option>
      @elseif(is_null($source->wild_farmed))
          <option value>--select Wild or Farmed--</option>
          <option value="Wild">Wild</option>
          <option value="Farmed">Farmed</option>
      @else
        @if ($source->wild_farmed == 'Wild')
          <option value>--select Wild or Farmed--</option>
          <option value="Wild" selected>Wild</option>
          <option value="Farmed">Farmed</option>
        @else
          <option>--select Wild or Farmed--</option>
          <option value="Wild" >Wild</option>
          <option value="Farmed" selected>Farmed</option>
        @endif
      @endif
    </select>
    @endif
 </div>  
<div class="form-group">
    <label for="method_cat">Harvest Method Category*: </label><p class="source_err" id="method_cat" style = "display: inline-block; margin-bottom: 5px"></p>
    @if(session()->get('view_only')==true)
      <p id="method_cat">
        {{$source->harvestMethod->category->methodCategory or '{no method category given}'}}
      </p>
    @else
      <select class="form-control methodCategory" type="text" name="method_cat"
      autocomplete="off" id="methodCat">
      @if(is_null($source->wild_farmed))
        <option value selected="selected" disabled="disabled">--no Wild Farmed selected yet--</option>
      @else
        @if(is_null($source->methodCategory))
        <option value selected="selected">--select Method Category--</option>
        @else
        <option value>--select Method Category--</option>
        @endif
        @if(isset($gearCat))
          @foreach($gearCat as $cat)
            @if($cat->wild_farmed == $source->wild_farmed)
              @if($source->harvestMethod->category->id == $cat->id)
              <option selected="selected" value="{{$cat->id}}">
                {{$cat->methodCategory}}
              </option>
              @else
              <option value="{{$cat->id}}" >
                {{$cat->methodCategory}}
              </option>
              @endif
            @endif
          @endforeach
        @endif

      @endif
      </select>
    @endif
    
</div>
<div class="form-group">
    <label for="gear_type">Specific Method*: </label><p class="source_err" id="gear_type" style = "display: inline-block; margin-bottom: 5px"></p>
    @if(session()->get('view_only')==true)
      <p id="gear_type">
        {{$source->harvestMethod->harvestMethod or '{no specific method given}'}}
      </p>
    @else
      <select name="gear_type" id="gear_type" class=" form-control"
      autocomplete="off">
      @if(is_null($source->method_category_id))
        <option value selected="selected" disabled="disabled">--no Method Category selected yet--</option>
      @else
        @if(is_null($source->fk_harvestMethod_id))
          <option value selected="selected">--select Specific Method--</option>
        @else
          <option value>--select Specific Method--</option>
        @endif
        @foreach($hMeths as $meth)
          @if ($meth->fk_methodCategory_id == $source->method_category_id)
            @if ($source->fk_harvestMethod_id == $meth->id)
                <option selected="selected" value="{{$meth->id}}" >{{$meth->harvestMethod}}</option>
            @else
                <option value="{{$meth->id}}" >{{$meth->harvestMethod}}</option>
            @endif
          @endif
        @endforeach
      @endif
      </select>
    @endif
    
</div>
<div class="form-group">
    <label for="catch_country">Catch Country*: </label><p class="source_err" id="catch_country" style = "display: inline-block; margin-bottom: 5px"></p>
    @if(session()->get('view_only')==true)
      <p id="catch_country">
        {{$source->catchCountry->countryName or '{no Catch Country given'}}
      </p>
    @else
      <select class="selectpicker dropdown form-control" data-dropup-auto = false data-size= 10 name="catch_country" autocomplete="off" data-live-search="true">
        @if(is_null($source->catchCountry))
        <option selected="selected" value>--select Catch Country--</option>
        @else
        <option value>--select Catch Country--</option>
        @endif
          @if(isset($countries))
              @foreach($countries as $country)
                @if(!is_null($source->catchCountry))
                  @if ($source->catchCountry->id==$country->id)
                    <option selected="selected" value="{{$country->id or 0}}">{{$country->countryName or ''}}</option>
                  @else
                    <option value="{{$country->id}}">{{$country->countryName}}</option>
                  @endif
                @else
                  <option value="{{$country->id}}">{{$country->countryName}}</option>
                @endif
                @if ($country->sortOrder == 13)
                  <option disabled>──────────</option>
                @endif
              @endforeach
          @endif
      </select>
    @endif
    
</div>
<div class="form-group">
    <label for="catch_region">Catch Region*: </label><p class="source_err" id="harvestRegion" style = "display: inline-block; margin-bottom: 5px"></p>
    @if(session()->get('view_only')==true)
      @if(is_null($source->harvestRegion) or $source->harvestRegion=='')
      <p id="catch_region">{{'{no Catch Region given}'}}</p>
      @else
      <p id="catch_region">{{$source->harvestRegion}}</p>
      @endif
    @else
      <input type="text" class="form-control" name="harvestRegion" value="{{$source->harvestRegion or ''}}">
    @endif
    
</div>
<div class="form-group">
    <label for="cool">Country of Origin Labeling (COOL)*:</label><p class="source_err" id="cool" style = "display: inline-block; margin-bottom: 5px"></p>
    @if(session()->get('view_only')==true)
      <p id="cool">
        {{$source->COOL->countryName or '{no COOL given}'}}
      </p>
    @else
      <select class="selectpicker dropdown form-control" data-dropup-auto = false data-size= 10 name="cool" autocomplete="off" data-live-search="true">
        @if(is_null($source->COOL))
        <option value selected="selected">--select COOL Country--</option>
        @else
        <option>--select COOL Country--</option>
        @endif
        @if(isset($countries))
            @foreach($countries as $country)
              @if(!is_null($source->COOL))
                @if ($source->COOL->id==$country->id)
                  <option selected="selected" value="{{$country->id or ''}}">{{$country->countryName or ''}}</option>
                @else
                  <option value="{{$country->id}}">{{$country->countryName}}</option>
                @endif
              @else
                <option value="{{$country->id}}">{{$country->countryName}}</option>
              @endif
              @if ($country->sortOrder == 13)
                <option disabled>──────────</option>
              @endif
            @endforeach
        @endif
      </select>
    @endif
    
</div>
