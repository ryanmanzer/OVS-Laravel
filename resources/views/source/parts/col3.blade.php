<div class="form-group">
    <label for="FAO_MFA">FAO Major Fishing Area: </label>
    @if(session()->get('view_only')==true)
      <p id="FAO_MFA">
        @if(is_null($source->faoMajorFishingArea) or $source->faoMajorFishingArea == '')
          {no FAO Area given}
        @else
          {{$source->faoMajorFishingArea}}
        @endif
      </p>
    @else
      <input type="text" class="form-control" name="faoMajorFishingArea"
      value="{{$source->faoMajorFishingArea or ''}}">
    @endif
</div>
<div class="form-group">
    <label for="RFMO">RFMO/High Seas Name</label>
    @if(session()->get('view_only')==true)
      <p id="RFMO">
        @if(is_null($source->highSeasName) or $source->highSeasName == '')
         {no RFMO/High Seas given}
        @else
          {{$source->highSeasName}}
        @endif
      </p>
    @else
      <input type="text" class="form-control" name="highSeasName"
      value="{{$source->highSeasName or ''}}">
    @endif
</div>
<div class="form-group">
    <label for="vessel_flag">Flag of Vessel</label>
    @if(session()->get('view_only')==true)
      <p id="vessel_flag">
        {{$source->vesselFlag->countryName or '{no Flag of Vessel given}'}}
      </p>
    @else
      <select class="selectpicker dropdown form-control" data-dropup-auto = false data-size= 10 name="vessel_flag" size = false autocomplete="off" data-live-search="true">
        @if(is_null($source->vesselFlag))
        <option value selected="selected" >--select Vessel Flag Country--</option>
        @else
        <option value>--select Vessel Flag Country--</option>
        @endif
        @if(isset($countries))
            @foreach($countries as $country)
              @if(!is_null($source->vesselFlag))
                @if ($source->vesselFlag->id==$country->id)
                  <option selected="selected" value="{{$country->id or 0}}">{{$country->countryName or ''}}</option>
                @else
                  <option value="{{$country->id}}">{{$country->countryName}}</option>
                @endif
              @else
                <option value="{{$country->id}}">{{$country->countryName}}</option>
              @endif
              @if ($country->sortOrder == 13)
                <option disabled>──────────</option>
              @endif
            @endforeach
        @endif
      </select>
    @endif
</div>
<div class="form-group">
    <label for="cert_type">Supplier Certification Type</label>
    @if(session()->get('view_only')==true)
      <p id="cert_type">
        {{$source->certification->certType or '{no Certification Type given}'}}
      </p>
    @else
      <select class="form-control" name="certification_types_id" id="certType">
      @if(is_null($source->wild_farmed))
        <option selected="selected" disabled="disabled" value>--no Wild Farmed selected yet--</option>
      @else
        @if(is_null($source->certification))
          <option selected="selected" value>--select Certification Type--</option>
        @else
          <option>--select Certification Type--</option>
        @endif
        @if(isset($certTypes))
          @foreach($certTypes as $cert)
            @if($cert->wild_farmed == $source->wild_farmed)
              @if($source->certification_types_id==$cert->id)
                <option value="{{$cert->id}}" selected="selected">
                  {{$cert->certType}}
                </option>
              @else
                <option value="{{$cert->id}}">
                  {{$cert->certType}}
                </option>
              @endif
            @endif
          @endforeach
        @endif
      @endif
      </select>
    @endif
</div>
<div class="form-group">
    <label for="cert_num">Supplier Certification Number</label>
    @if(session()->get('view_only')==true)
      @if(is_null($source->certificationNumber) OR $source->certificationNumber=='')
      <p id="cert_num">{{'{no Certification Number given}'}}</p>
     @else
      <p id="cert_num">{{$source->certificationNumber}}</p>
     @endif
    @else
      <input type="text" class="form-control" name="certificationNumber"
      value="{{$source->certificationNumber or ''}}">
    @endif
</div>
<div class='form-group'>
  <label for='Comments'>Comments: </label>
  @if(session()->get('view_only')==true)
    <p id="Comments">
      @if(is_null($source->vendorComments) OR $source->vendorComments=='')
        {none given}
      @else
        {{$source->vendorComments}}
      @endif
    </p>
  @else
    <textarea name="vendorComments" class="form-control">{{$source->vendorComments or ''}}</textarea>
  @endif
</div>
