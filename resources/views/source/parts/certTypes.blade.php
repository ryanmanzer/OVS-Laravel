@if(isset($certs))
  <option selected="selected">--select Certification Type--</option>
  @foreach ($certs as $cert)
    <option value="{{$cert->id}}">
      {{$cert->certType}}
    </option>
  @endforeach
@else
  <option selected="selected" disabled="disabled">--no Wild Farmed selected yet--</option>
@endif
