    @if(isset($sp))
      <label for="seafood_cat">Seafood Category: </label>
      <input type="text" class="form-control" value="{{$sp->product->seafoodCategory->category}}" disabled="disabled">
      <select name="seafoodCategory" class="form-control seafoodCategory" id="seafood_cat"
      autocomplete="off" style="display: none;">
      <option  selected="selected" value="{{$sp->product->fk_seafoodCategory_id}}"></option>
      </select>
    @else
      <label for="seafood_cat">Seafood Category: </label><p class="source_err" id="seafoodCategory" style = "display: inline-block; margin-bottom: 5px"></p>
      <input type="text" id = "seafoodCatText" class="form-control" value="" disabled="disabled" style = "display:none">
      <select name="seafoodCategory" class="form-control" id="seafood_cat"
      autocomplete="off" value="">
      <option value selected="selected">--select Seafood Category--</option>
      @if(isset($sfCat))
        @foreach($sfCat as $cat)
          <option value="{{$cat->id}}" >{{$cat->category}}</option>
        @endforeach
      @else
        <option value="Seafood Categories not set"></option>
      @endif
   </select>
   @endif
