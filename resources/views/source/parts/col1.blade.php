
<div class="form-group">
    <label for="source_company">Supplier*:</label><p class="source_err" id="supplierCompany" style = "display: inline-block; margin-bottom: 5px"></p>
    @if(session()->get('view_only')==true)
    <p id="source_company">
      {{$source->supplierCompany or '{no Supplier given}'}}
    </p>
    @else
      <input type="text" class="form-control" id = "formStart" name="supplierCompany"
      value="{{$source->supplierCompany or ''}}">
      
    @endif
</div>
<div class="form-group">
    <label for="contact_name">Contact Name: </label>
    @if(session()->get('view_only')==true)
      @if(is_null($source->contactName) OR $source->contactName=='')
      <p id="contact_name">{{'{no Contact Name given}'}}</p>
      @else
      <p id="contact_name">{{$source->contactName}}</p>
      @endif 
    @else
    <input type="text" class="form-control" name="contactName"
    value="{{$source->contactName or ''}}">
    @endif
</div>
<div class="form-group">
    <label for="contact_email">Contact Email: </label>
    @if(session()->get('view_only')==true)
      @if(is_null($source->contactEmail) OR $source->contactEmail=='')
      <p id="contact_email">{{'{no Contact Email given}'}}</p>
      @else
      <p id="contact_name">{{$source->contactName}}</p>
      @endif 
    @else
      <input type="text" class="form-control" name="contactEmail"
      value="{{$source->contactEmail or ''}}">
    @endif
</div>
<div class="form-group">
    
    <label for="seafood_cat">Seafood Category: </label>
    @if(session()->get('view_only')==true)
      <p id="seafood_cat">
       {{$source->seafoodCategory->category or '{no Seafood Category given}'}}
      </p>
    @else
      <input type="text" class="form-control" value="{{$source->seafoodCategory->category}}" disabled="disabled">
      <select name="seafoodCategory" class="form-control seafoodCategory" id="seafood_cat" autocomplete="off" style="display: none;">
      <option  selected="selected" value="{{$source->fk_seafoodCategory_id}}"></option>
      </select>
    @endif
</div>
{{--   <!--  <select name="seafoodCategory" class="form-control seafoodCategory" id="seafood_cat"
      autocomplete="off">
      @if(is_null($source->seafoodCategory))
        <option value selected="selected">--select Seafood Category--</option>
      @else
        <option value>--select Seafood Category--</option>
      @endif
      @if(isset($sfCat))
        @foreach($sfCat as $cat)
          @if($cat->id == $source->fk_seafoodCategory_id)
            <option  selected="selected" value="{{$cat->id}}">{{$cat->category}}</option>
          @else
            <option value="{{$cat->id}}">{{$cat->category}}</option>
          @endif
        @endforeach  
      @endif
      </select>
      <p class="source_err" id="seafoodCategory"></p>
-->    
--}}
<div class="form-group">
    <label for="_fish" >Seafood Common Name*: </label><p class="source_err" id="seafood" style = "display: inline-block; margin-bottom: 5px"></p> 
    @if(session()->get('view_only')==true)
      <p id="fish">
        {{$source->seafoodSpecies->seafoodSpecies or '{no Seafood Common Name given}'}}
      </p>
    @else
      <select class="selectpicker form-control seafoodSpecies" name="seafood" autocomplete="off" id="_fish">
      @if(is_null($source->seafoodCategory))
        <option value selected="selected" disabled="disabled">--no Seafood Category selected yet--</option>
      @else
        @if(is_null($source->seafoodSpecies))
          <option value selected="selected">--select Seafood Common Name--</option>
        @else
          <option value>--select Seafood Common Name--</option>  
        @endif
        @if(isset($sfs))
          @foreach($sfs as $sf)
            @if($sf->fk_seafoodCategory_id == $source->fk_seafoodCategory_id)
              @if ($source->fk_seafoodSpecies_id == $sf->id)
                <option selected="selected" value="{{$sf->id}}" >
                {{$sf->seafoodSpecies}}
                </option>
              @else
                <option value="{{$sf->id}}" >
                {{$sf->seafoodSpecies}}
                </option>
              @endif
            @endif
          @endforeach
        @endif
      @endif
    </select>
    @endif
     
</div>
<div class="form-group">
    <label for="sci_nam">Seafood Latin Name*:</label><p class="source_err" id="scientificName" style = "display: inline-block; margin-bottom: 5px"></p>
    @if(session()->get('view_only')==true)
      @if(is_null($source->scientificName) OR $source->scientificName=='')
        <p id="sci_nam">{{'{no Latin Name given}'}}</p>
      @else
        <p id="sci_nam">{{$source->scientificName}}</p>
      @endif
    @else
      <input type="text" class="form-control" name="scientificName" value="{{$source->scientificName or ''}}">
    @endif
    
</div>
<p>* = Required field</p>