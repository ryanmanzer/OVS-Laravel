@extends('layouts.app')
@section('scripts')
<script>
    $(function() {
    //First we load the complete lists and assign the correct selected values based on the foreign keys in our source model
        getWF();
        setSelect('#methodCat',{{$source->harvestMethod->fk_methodCategory_id}})
        getMeth();
        setSelect('#gear_type',{{$source->fk_harvestMethod_id}})
    //then we filter if users change anything
        $("#seafood_cat").change(getSF);
        $("#wf").change(getWF);
        $("#methodCat").change(getMeth);

    });
    function setSelect(tag,id)
    {
        selector= tag + " option[value='" + id + "']"
        em = $(selector);
        em.prop('selected',true);
    }
</script>
@stop
@section('content')
<div class="container-fluid body">
    <h2 class="page-header" style="text-align: center">Edit Source Info</h2>

    <form role="form" action="/source/{{$source->id}}" method="post">
        {{ method_field('PATCH') }}
        {{ csrf_field() }}
        <div class="row">
            <div class="col-sm-4">
                @include('source.parts.col1')
            </div>
            <div class="col-sm-4">
                @include('source.parts.col2')
            </div>
            <div class="col-sm-4">
                @include('source.parts.col3')
            </div>
        </div>
        <hr>
        <div class="form-group submit-button">
            <button type="submit" class="btn btn-primary">Update Info</button>
        </div>
    </form>
    <form action="/vendor" method="get">
        <div class="form-group cancel-button">
            <button class="btn btn-default">Cancel</button>
        </div>
    </form>
</div>
@stop
