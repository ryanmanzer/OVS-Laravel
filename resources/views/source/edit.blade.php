
<div class="container-fluid body" style = "background-color: transparent;">
    <div class="row" style = "padding-top: 10px; padding-bottom: 10px; border-bottom: 1px solid #e5e5e5">
      <div class="col-sm-11">
        @if(session()->get('view_only')==true)
          <h4>{{$source->seafoodCategory->category}} Source Information</h4>
        @else
          <h4>Edit {{$source->seafoodCategory->category}} Source Information</h4>
        @endif
      </div>
      <div class="col-sm-1">
        <div class="alert alert-default" style = "padding-top: 10px; padding-bottom: 10px;" >
          <a href="" tag="#sourcerow_{{$source->id}}" class="myright closeForm close"><i class="fa fa-times-circle fa-lg"></i></a>
        </div>
      </div>         
    </div>
  <!--
  <div class="row">
    <div class="col-sm-11">
      <h4></h4>
    </div>
    <div class="col-sm-1">
      <div class="alert alert-default">
        <a href="" tag="#sourcerow_{{$source->id}}" class="myright closeForm close"><i class="fa fa-times-circle fa-lg"></i></a>
      </div>
    </div>
  </div>
  -->
  <input type="hidden" id="source_id_number" value="{{$source->id}}">
  @if(session()->get('view_only')==true)
  <input type="hidden" id="source_expand_type" value="details">
  
  @else
  <input type="hidden" id="source_expand_type" value="edit">
  @endif

  
    <form role="form" url="/vendor/{{$vendor->id}}/sources/source_id/details" id="editSource" data-url="/source/{{$source->id}}/update" tag="#sources">

        {{ csrf_field() }}
        <div class="row" style = "padding-top: 10px; padding-bottom: 10px; border-bottom: 1px solid #e5e5e5">
            <div class="col-sm-4">
                @include('source.parts.col1')
            </div>
            <div class="col-sm-4">
                @include('source.parts.col2')
            </div>
            <div class="col-sm-4">
                @include('source.parts.col3')
            </div>
        </div>
        
        @if(session()->get('view_only')!=true)
        <div class="row" style = "padding-top: 10px; padding-left: 10px;">
    
          <div class="form-group submit-button">
            <button class="btn btn-primary updateSource" style="margin-right: 10px">Save Updates</button>
          </div>
        <!-- We don't need this with an inline form
        <div class="form-group cancel-button">
            <button tag="#sources" url="/vendor/{{$source->fk_vendor_id}}/sources"
              class="btn btn-default inlineAJAX">Cancel</button>
        </div>
      -->
          @endif
          <h4>
          @if(isset($sps))
            @if($sps->count() > 0)
              @if($sps->count() == 1)
                This source is currently used in 1 survey product: 
              @else
                This source is currently used in {{$sps->count()}} survey products:
              @endif
              @foreach($sps as $key=>$sp)
                @if($key > 0)
                  , 
                @endif
                  {{$sp->product->retailer->name}} {{$sp->product->retailerProductCode}} 
                @endforeach
            
              @else
                <p>Source is not currently applied to any survey products.</p>
              @endif
          @endif
        </h4>
      </div>

    </form>
        
</div>
<script>
  $("button.updateSource").off("click");
  $("button.updateSource").click(updateSource);
  $("a.closeForm").off("click");
  $("a.closeForm").click(closeInlineForm);   
  $("select#seafood_cat").off("change");
  $("select#seafood_cat").change(getSF);
  $("select#wf").off("change");
  $("select#wf").change(getWF);
  $("select#methodCat").off("change");
  $("select#methodCat").change(getMeth);
  selectPicker();
  function selectPicker()
  {
    $('.selectpicker').selectpicker({});
  }

</script>
