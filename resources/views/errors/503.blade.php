@extends('layouts.app')

@section('content')

<div class="container-fluid body">
  <div class="jumbotron">
    <div class="row">
      <div class="col-sm-8">
        <h1>Coming Soon - FishWise Online Vendor Survey</h1>
      </div>
      <div class="col-sm-4">
        <img src="css/images/logos/Fishwise_logo.jpg" class="img-rounded" width="368" height="124"
        style="" />
      </div>
    </div>


    <p>
      This site will soon be hosting the FishWise Online Vendor Survey.  At that
      time users will be able to log in interact with the system.  We anticipate
      this system will be available in October.
    </p>
  </div>
</div>
@endsection

<!--
<!DOCTYPE html>
<html>
    <head>
        <title>Be right back.</title>

        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                color: #B0BEC5;
                display: table;
                font-weight: 100;
                font-family: 'Lato', sans-serif;
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 72px;
                margin-bottom: 40px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Be right back.</div>
            </div>
        </div>
    </body>
</html>
-->
