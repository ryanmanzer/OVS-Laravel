@extends('layouts.app')

@section('content')
    <div class="container-fluid body">
        <div class="alert alert-warning">
                {{$message}}
        </div>
    </div>
@stop