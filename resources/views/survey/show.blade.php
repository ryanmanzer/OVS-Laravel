@extends('layout')


@section('content')
<div class="container-fluid body">
    <div class = "surveyDetails">
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Survey Status</th>
                <th>Survey Date</th>
                <th>Retailer</th>
                <th>Vendor</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>
                    {{$survey->survey_status}}
                </td>
                <td>           
                    {{$survey->survey_date}}
                </td>
                <td>
                    {{$survey->getRetailer->name}}
                </td>
                <td>
                    {{$survey->getVendor->vendor_name}}
                </td>
            </tr>
        </tbody>
    </table>
    
    <h3>Update Survey Status</h3>
    <form method="POST" action="/survey/{{$survey->id}}/status">
    <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="sel1">Set Survey Status</label>
            <select class="form-control" id="sel1" name="survey_status">
                <option>Unassigned</option>
                <option>Assigned to Vendor</option>
                <option>Ready for Fishwise Review</option>
                <option>Review In Progress</option>
                <option>Survey Complete</option>
            </select>
        </div>
        <div class="form-group">
            <button type="submit" class="btn btn-primary">Set Status</button>
        </div>
        
    </form>
    </div>
</div>
@stop