@extends('layouts.app')
@section('content')
<div class="container-fluid body">
    <div class="row">
        <div class="col-sm-10"
        <form action="/survey/make" action="post">
            {{ csrf_field() }}
            <div class="form group">
                <label for="surveyStatus">Survey Status: </label>
                <select class="form-control" id="surveyStatus" name="surveyStatus">
                    <option>Unassigned</option>
                    <option>Assigned to Vendor</option>
                    <option>Ready for Fishwise Review</option>
                    <option>Review In Progress</option>
                    <option>Survey Complete</option>
                </select>
            </div>
            <div class="form group">
                <label for="retailer">Retailer: </label>
                <select class="form-control" id="retailer" name="fk_retailer_id">
                    @foreach($retailers as $retailer)
                        <option value="{{$retailer->id}}">{{$retailer->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form group">
                <label for="retailer">Vendor: </label>
                <select class="form-control" id="retailer" name="fk_vendor_id">
                    @foreach($vendors as $vendor)
                        <option value="{{$vendor->id}}">{{$vendor->name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form group">
                <label for="fwComments">FishWise Comments: </label>
                <textarea id="fwComments" name="fishwiseComments"></textarea>
            </div>
            <hr>
            <div class="form-group submit-button">
                <button class="btn btn-info" type="submit">Update Info</button>
            </div>
        </form>
        <form action="/ngo" method="get">
            <div class="form-group cancel-button">
                <button class="btn btn-default">Cancel</button>
            </div>
        </form>
    </div>
</div>
@stop