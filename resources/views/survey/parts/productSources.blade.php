
@if(isset($sp))
  <!-- @include('messages.flash') -->
  <!-- @include('messages.errors') -->
 <!-- <div class="form1" style="background-color:#e6ffec;"> -->
  <div class="form1";">
    @include('survey.parts.product_sources') <!-- TABLE OF SOURCES -->
    
    @include('survey.parts.source_select') <!-- FORM FOR ASSIGNING SOURCES -->
  </div>
  <input type="hidden" id="sp_id_number" value="{{$sp->id}}">
             
<hr></hr>
  <div class="form2">
    @include('survey.parts.product_certs') <!-- TABLE OF CERTS -->
    @include('survey.parts.cert_select') <!-- FORM FOR ASSIGNING CERTS -->
  </div>
<script>
  selectPicker();
  function selectPicker()
  {
    $('.selectpicker').selectpicker({});
  }
</script>
@endif <!-- end of if(isset(sp)) conditional -->
