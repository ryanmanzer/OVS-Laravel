@if(isset($prod_certs))
  <div class="row">
    <div class="col-sm-10">
      <h4>Certifications assigned for <strong>{{$sp->product->retailerProductDescription or ''}}</strong></h4>
    </div>
  </div>
<table class="table table-striped table">
  <thead>
    <th>
      Certification Type
    </th>
    <th>
      Certification #
    </th>
    <th>
      Descpription
    </th>
    <th>

    </th>
  </thead>
  @if(count($prod_certs)>0)
    @foreach($prod_certs as $cert)
      <tr>
        <td>
        {{$cert->certification->certType->certType or 'No Certification Type'}}
        </td>
        <td>
          {{$cert->certification->certification_number or 'No Certification Number'}}
        </td>
        <td>
          {{$cert->certification->description or 'No Certification Description'}}
        </td>
        <td>
          <button class="btn btn-danger btn-sm delCert"
          @if($done)
            style="display: none"
            disabled="true"
          @endif
           data-url="spcert/{{$cert->id}}/rm" url="surveyProduct/{{$sp->id}}/assign" tag="#sources_{{$sp->id}}">Remove Certification</button>
        </td>
      </tr>
    @endforeach
  @else
    <tr><td colspan = "4">-- No certifications have been assigned to this product yet --</td></tr>  
  @endif

  </tbody>
</table>

@endif
