<!-- reources/views/survey/parts/source_select.blade.php

A FORM FOR ASSIGNING SOURCES TO A SPECIFIC SURVEY PRODUCT RECORD  - CREATES A SURVEY PRODUCT SOURCE RECORD -->
<div id="selectSource">
  @if(sourcePercent($sp->product) !== 1)
  @if(isset($sources))
  @if (count($sources)<1)
    <div class="form group">
            <label for="sourceSelect">
              Select {{$sp->product->seafoodCategory->category}} Seafood Source: (To add to this list click the button below)
            </label>
              
            <select id="sourceSelect_"{{$sp->id}}"" name="Source"
              @if($done)
                disabled="true"
              @endif
               val="{{$sp->id}}" class="selectpicker sourceSelect form-control">
                 <option selected="selected" disabled="disabled">--No {{$sp->product->seafoodCategory->category}} sources defined.  Click the button below or go to the Seafood Sources tab to add some--</option>
                  
            </select>
            
        </div>
 <button class="btn btn-sm btn-info assignSpModal"   id ="sp_{{$sp->id}}" style = "margin-top: 10px"><i class="fa fa-plus" aria-hidden="true" ></i>Add New {{$sp->product->seafoodCategory->category}} Source</button>
  @else
    
    <div class="alert alert-danger" style="display: none;" id="sourceWarn_{{$sp->id}}">
    </div>
    <form id="assignSourceForm_{{$sp->id}}" val="{{$sp->id}}"
      class="assignSources" method="GET" url="surveyProduct/{{$sp->id}}/assign">
      {{csrf_field() }}
        <div class="form group">
            <label for="sourceSelect">
              Select {{$sp->product->seafoodCategory->category}} Seafood Source: (To add to this list click the button below)
            </label>
            <select id="sourceSelect_"{{$sp->id}}"" name="Source" 
              @if($done)
                disabled="true"
              @endif
               val="{{$sp->id}}" class="selectpicker sourceSelect form-control">
                 <option selected="selected" disabled="disabled">{{'-- Existing '.$sp->product->seafoodCategory->category.' Sources -- ( Supplier:  Seafood - Harvest Method - Catch Country - Catch Region - COOL - Certification Type - Certification # )'}}</option>
                  <option disabled="disabled">──────────────────────────────────────────────────────────────────────────────────────────</option>
                  @foreach($sources as $source)
                    @if(!in_array($source->id,$sps_ids))
                        <option
                        class="sourceToolTip"
                        data-toggle="tooltip" value="{{$source->id}}">
                        <strong>{{$source->supplierCompany or 'Company Name'}} :  </strong>
                        {{$source->seafoodSpecies->seafoodSpecies or 'no species given'}} - {{$source->harvestMethod->harvestMethod or '{no method given}'}} -
                        {{$source->catchCountry->countryName or 'no catch country given'}} - {{$source->harvestRegion or 'no catch region given'}} - {{$source->COOL->countryName or 'no COOL given'}} - {{$source->certification->certType or '{no certification given}'}} - {{$source->certificationNumber or '{no cert # given}'}}  
                      </option>
                    @endif
                  @endforeach
            </select>
        </div>
        <p>
        </p>
        <div class='form-group'>
        <button type="submit"
          @if($done)
            disabled="true"
          @endif
          class="btn btn-sm btn-primary assignSource" style = "padding-left: 25px; padding-right: 25px; margin-right: 5px;">Assign Source</button>
              
              <label for='percentProduct'>% of Product From This Source: </label>
            <input type='number' class='percentProduct' name="Proportion_Of_Product"
            @if($done)
              disabled="true"
            @endif
             step='0.1' min='0' max= '{{100 * (1 - sourcePercent($sp->product))}}' 
 id="propProd_{{$sp->id}}"/>
             <input type="hidden" id = "remainingPercent" name="remainingPercent" value="{{100 * (1 - sourcePercent($sp->product))}}">
             <button class="btn btn-sm btn-primary 100" 
            @if($done)
              disabled="true"
            @endif
             val="propProd_{{$sp->id}}">
              <i class="fa fa-arrow-left" aria-hidden="true"></i> {{100 * (1 - sourcePercent($sp->product))}}%</button> 
                     
        </div>
        @foreach($sources as $source)
          <div class="panel panel-info" style="display:none" id="sourcedetails_{{$source->id}}">
            <div class="panel-heading">
              Source Details:
            </div>
            <div class="panel-body">
              {!!tooltipText($source)!!}
            </div>
          </div>
        @endforeach

    </form>
<button class="btn btn-sm btn-info assignSpModal"  id ="sp_{{$sp->id}}"><i class="fa fa-plus" aria-hidden="true"></i>Add New {{$sp->product->seafoodCategory->category}} Source</button>     

  @endif <!-- end of if(count($sources)) conditional -->
@endif<!-- end of if(isset($sources)) conditional -->
@endif
</div>
<script>
$("button.assignSpModal").off("click");
$("button.assignSpModal").click(newSourceSpModal);
  function newSourceSpModal()
  {
    newSourceHandler({{$sp->id}});
  }
</script>  