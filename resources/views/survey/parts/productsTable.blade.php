<!--
A simple table of products with some built in modifications for vendors to assing sources and such.
Eventually I will have to refactor these out since I am repeating WAY too much code!
-->
@if(isset($sp_list))
  <input type="hidden" id="survey_number" value="{{$survey->id}}">
  <div class="products_table_container">
    <div class="row">
      <div class="col-sm-12">
        @if($user->user_type != "vendor")
          <div class="header">
            <h3>Survey of Products for <strong>{{$survey->retailer->name}}</strong> by <strong>{{$survey->vendor->name}}</strong></h3>
          </div>
        @else
          <div class="header">
            <h3>Survey Products for {{$survey->retailer->name}}</h3>
          </div>
        @endif
  
      </div>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>{{$survey->retailer->name or '' }} Product Code</th>
                <th>Product Description</th>
                <th></th>
                <th>UPC Code</th>
                <th>Percent Assigned</th>
                <th>
                  @if(strcmp($user->user_type,'ngo')==0)
                    <button class="btn btn-sm btn-info getAJAX"
                    url="/ngo/surveys"
                    tag="#surveys"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</button>
                  @elseif(strcmp($user->user_type,'retailer')==0)
                    <button class="btn btn-sm btn-info getAJAX"
                    url="/retailer/{{$user->org_id}}/surveys"
                    tag="#surveys"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</button>
                  @else
                    <button class="btn btn-sm btn-info backToSurveys"
                    url="/vendor/{{$survey->vendor->id}}/surveys"
                    tag="#surveys"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back To Surveys</button>
                  @endif
                </th>
                <th>
                  <button class="btn btn-sm btn-info refreshSurvey"
                  url="/survey/{{$survey->id}}/products" tag="#surveys">
                    <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($sp_list as $sp)
              {!! catTable($sp->product,7) !!}
                <tr>
                    <td>{{$sp->product->retailerProductCode or '{no code supplied'}}</td>
                    <td>{{$sp->product->retailerProductDescription or '{no description supplied'}}</td>
                    <td></td>
                    <td>{{$sp->product->upcCode or '{none given}'}}</td>
                    <td><p id = "percent_{{$sp->id}}">{{sourcePercent($sp->product)*100}} %</p></td>
                    <td></td>
                    <td>
                      @if($user->user_type == "ngo")
                        <button class="btn btn-sm btn-warning expandSp"
                        url="/surveyProduct/{{$sp->id}}/view"
                        tag="#row_{{$sp->id}}">View Sources</button>
                      @elseif($user->user_type =="vendor")
                        <button class="btn btn-sm btn-warning expandSp"
                        url="/surveyProduct/{{$sp->id}}/assign"
                        tag="#row_{{$sp->id}}">Assign Sources/Certifications</button>
                      @else
                        <td></td>
                      @endif
                    </td>
                </tr>
                @if(isset($osp) and ($osp->id == $sp->id))
                <tr id="row_{{$sp->id}}" class="outputrow">
                @else
                <tr id="row_{{$sp->id}}" class="outputrow" style="display: none;">
                @endif
                  <td colspan="7" style="background-color:#f9f9f9;"">
                    <div id="sources_{{$sp->id}}" class="myoutput">
                    @if(isset($osp) and ($osp->id == $sp->id))
                      @include('survey.parts.productSources')
                    @endif
                    </div>
                  </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    {{$sp_list->links()}}
  </div>
@else
    <h1>Products variable not set</h1>
@endif
<script>
  $(init);
  function init()
  {
    assignFormClose('div.myoutput');
    inlineAJAX();
    b100();
    assignDelete();
    delYes('delSPS');
    formCheck();
    myPages('div.products_table_container','#surveys');
    sourceDetails('div.myoutput');
    assignCertForm("div.myoutput");
    rmCert("div.myoutput");
    saveRepack("div.myoutput");
    $('button.expandSp').off("click")
    $('button.expandSp').click(expandSp);
    $('button.refreshSurvey').off("click")
    $('button.refreshSurvey').click(refreshOpenSurvey);
    $('button.backToSurveys').off("click")
    $('button.backToSurveys').click(getAJAX);
  }

</script>