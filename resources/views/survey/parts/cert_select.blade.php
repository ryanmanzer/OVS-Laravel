<!-- resources/views/survey/parts/cert_select.blade.php

THIS FORM IS USED TO ASSIGN CERTIFICATIONS TO EACH SURVEY PRODUCT -->
@if(isset($vendor_certs))  
<form class="assignCert" url="surveyProduct/{{$sp->id}}/assignCert" data-url="surveyProduct/{{$sp->id}}/assign" data-value="{{$sp->id}}" method="get">
  {{ csrf_field() }}
  <div class='form-group'>
    <label for='Cert'>  Select Seafood Product Certification: (To add to this list or edit existing options, go to the Certifications tab)</label>
    <select id="Cert" class='selectpicker form-control' name="certification_id"
    @if($done)
      disabled="true"
    @endif
    >
    @if(count($vendor_certs)>0)
      <option selected = "selected" disabled = "disabled">{{'-- Certification Type - Certification # - Description --'}}</option>
      @foreach($vendor_certs as $cert)
        @if(!in_array($cert->id,$cert_ids))
          <option value="{{$cert->id}}" >
            {{$cert->certType->certType . " - " . $cert->certification_number . " - " . $cert->description}}
          </option>
        @endif
      @endforeach
    @else
      <option selected = "selected" disabled = "disabled">--No certifications defined. Go to Certifications tab to add some--</option>
    @endif  
    </select>
  </div>
  <div class="form-group">
    <button class="btn btn-sm btn-primary"
    @if($done)
      disabled="true"
    @endif
    type="submit" name="button"> Assign Certification</button>
  </div>
</form>
<button class="btn btn-sm btn-info assignSpCertModal"><i class="fa fa-plus" aria-hidden="true"></i>Add New Certification</button>
<form class="assignRepack" url="surveyProduct/{{$sp->id}}/assignRepack">
  {{ csrf_field()}}
  <div class='form-group'>
    <label for='repack'>Are you repacking this product?</label>
    @if($sp->repack)
      <div class="radio">
        <label><input type="radio" name="repack"
          @if($done)
            disabled="true"
          @endif
          checked="checked" value="1"> Yes</label>
      </div>
      <div class="radio">
        <label><input type="radio" name="repack"
          @if($done)
            disabled="true"
          @endif
          value="0"> No</label>
      </div>
    @else
      <div class="radio">
        <label><input type="radio" name="repack" value="1"
          @if($done)
            disabled="true"
          @endif
          data-value="{{$sp->id}}"> Yes</label>
      </div>
      <div class="radio">
        <label><input type="radio" name="repack"
          @if($done)
            disabled="true"
          @endif
          value="0" checked="checked" data-value="{{$sp->id}}"> No</label>
      </div>
    @endif
  </div>
</form>

@endif
<script>
$("button.assignSpCertModal").off("click");
$("button.assignSpCertModal").click(newCertModal);
</script>
