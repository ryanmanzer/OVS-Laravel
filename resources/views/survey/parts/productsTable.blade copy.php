<!--
A simple table of products with some built in modifications for vendors to assing sources and such.

Eventually I will have to refactor these out since I am repeating WAY too much code!
-->

@if(isset($sp))
  <div class="products_table_container">
    <div class="row">
      <div class="col-sm-6">
        @if($user->user_type != "vendor")
          <div class="page-header">
            <h3>Survey of Products for <strong>{{$survey->retailer->name}}</strong> by <strong>{{$survey->vendor->name}}</strong></h3>
          </div>
        @else
          <div class="page-header">
            <h3>Survey Products for {{$survey->retailer->name}}</h3>
          </div>
        @endif
      </div>
      <div class="col-sm-6">
        @if($done)
          @include('messages.survey_submit')
        @endif
      </div>
    </div>
    <table class="table table-hover">
        <thead>
            <tr>
                <th>{{$survey->retailer->name or '' }} Product Code</th>
                <th>Product Description</th>
                <th></th>
                <th>UPC Code</th>
                <th>Percent Assigned</th>
                <th>
                  @if(strcmp($user->user_type,'ngo')==0)
                    <button class="btn btn-sm btn-info getAJAX"
                    url="/ngo/surveys"
                    tag="#surveys"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</button>
                  @elseif(strcmp($user->user_type,'retailer')==0)
                    <button class="btn btn-sm btn-info getAJAX"
                    url="/retailer/{{$user->org_id}}/surveys"
                    tag="#surveys"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back</button>
                  @else
                    <button class="btn btn-sm btn-info getAJAX"
                    url="/vendor/{{$survey->vendor->id}}/surveys"
                    tag="#surveys"><i class="fa fa-arrow-left" aria-hidden="true"></i>Back To Surveys</button>
                  @endif
                </th>
                <th>
                  <button class="btn btn-sm btn-info getAJAX"
                  url="/survey/{{$survey->id}}/products" tag="#surveys">
                    <i class="fa fa-refresh" aria-hidden="true"></i> Refresh</button>
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach($sp as $mysp)
              {!! catTable($mysp->product,7) !!}
                <tr>
                    <td>{{$mysp->product->retailerProductCode or '{no code supplied'}}</td>
                    <td>{{$mysp->product->retailerProductDescription or '{no description supplied'}}</td>
                    <td></td>
                    <td>{{$mysp->product->upcCode or '{no UPC supplied}'}}</td>
                    <td><p id = "percent_{{$mysp->id}}">{{sourcePercent($mysp->product)*100}} %</p></td>
                    <td></td>
                    <td>
                      @if($user->user_type == "ngo")
                        <button class="btn btn-sm btn-warning inlineAJAX"
                        url="/surveyProduct/{{$mysp->id}}/view"
                        tag="#row_{{$mysp->id}}">View Sources</button>
                      @elseif($user->user_type =="vendor")
                        <button class="btn btn-sm btn-warning inlineAJAX"
                        url="/surveyProduct/{{$mysp->id}}/assign"
                        tag="#row_{{$mysp->id}}">Assign Sources/Certifications</button>
                      @else
                        <td></td>
                      @endif
                    </td>
                </tr>
                <tr id="row_{{$mysp->id}}" class="outputrow" style="display: none;">
                  <td colspan="7">
                    <div id="sources_{{$mysp->id}}" class="myoutput" style="background-color:#ddd;></div>
                  </td>
                </tr>
            @endforeach
        </tbody>

    </table>
    {{$sp->links()}}
  </div>
@else
    <h1>Products variable not set</h1>
@endif
<script>
  $(init);
  function init()
  {
    assignFormClose('div.myoutput');
    inlineAJAX();
    b100();
    assignDelete();
    delYes('delSPS');
    formCheck();
    myPages('div.products_table_container','#surveys');
    $("button.assignModal").click(newSourceModal);
    sourceDetails('div.myoutput');
    assignCertForm("div.myoutput");
    rmCert("div.myoutput");
    saveRepack("div.myoutput");
  }
  function newSourceModal(e)
  {
    e.preventDefault();
    var tag=$(this).attr('tag');
    var url=$(this).attr('url');
    newSourceHandler(tag,url);
  }
</script>
