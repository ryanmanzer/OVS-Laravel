<!-- resources/views/survey/parts/product_sources.blade.php

THIS IS SIMPLY THE TABLE TO VIEW THE ASSIGNED SOURCES FOR EACH PRODUCT -->
<div id="sourceTable_{{$sp->id}}" class="sourceTables">
  <div class="row">
    <div class="col-sm-10">
      <h4>Sources assigned for <strong>{{$sp->product->retailerProductDescription or ''}}</strong></h4>
    </div>
    <div class="col-sm-2">
      <div class="alert alert-default">
        <a href="" tag="#row_{{$sp->id}}" class="myright closeForm close"><icon class="fa fa-times-circle fa-lg"></icon></a>
      </div>
    </div>
  </div>

    <!--
    <form  method="get">
        @if(strcmp($user->user_type,'ngo')==0)
          <button url="/survey/{{$sp->survey->id}}/products" class="btn btn-info myright getAJAX"
            tag="#surveys">Back</button>
        @else
          <button url="/survey/{{$sp->survey->id}}/products" class="btn btn-info myright getAJAX"
            tag="#surveys">Back</button>
        @endif
    </form> -->

    <table class="table table-striped table">
        <thead>
            <tr>
                <th>Supplier</th>
                <th>Seafood</th>
                <th>Harvest Method</th>
                <th>Catch Country</th>
                <th>Catch Region</th>
                <th>COOL</th>
                <th>Certification Type</th>
                <th>Certification #</th>
                
                <th>Percent of Product</th>
                <th>

                </th>
            </tr>
        </thead>
        <tbody>
    @if($sps->count() > 0)
  
          @foreach($sps as $source)
              <tr>
                  <td>{{$source->source->supplierCompany or ''}}</td>
                  <td>{{$source->source->seafoodSpecies->seafoodSpecies or ''}}</td>
                  <td>{{$source->source->harvestMethod->harvestMethod or ''}}</td>
                  <td>{{$source->source->catchCountry->countryName or ''}}</td>
                  <td>{{$source->source->harvestRegion or ''}}</td>
                  <td>{{$source->source->COOL->countryName or ''}}</td>
                  <td>{{$source->source->certification->certType or ''}}</td>
                  <td>{{$source->source->certificationNumber or ''}}</td>
                  <td>{{$source->proportionOfProduct*100}} %</td>
                  <td>
                    @if(strcmp($user->user_type,'ngo')==0)
                      <!-- NGO staff do not have this capacity in the Web Application -->
                    @else
                      <button id="{{$source->id}}" url="/surveyProduct/{{$sp->id}}/assign"
                        tag="#sources_{{$sp->id}}" class='btn btn-danger delSource'
                        @if($done)
                          style="display: none;"
                          disabled="true"
                        @endif
                        data-toggle="modal" data-target="#delSource"><i class="fa fa-btn fa-trash" aria-hidden="true"></i> Remove Source</button>
                    @endif
                  </td>
              </tr>
          @endforeach
    @else
       <tr><td colspan = 10>-- No {{$sp->product->seafoodCategory->category}} sources have been assigned to this product yet --</td></tr> 
    @endif
        </tbody>
    </table>
    
