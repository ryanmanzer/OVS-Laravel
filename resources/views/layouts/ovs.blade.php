<!DOCTYPE html>
<html style = "height: 100%">
    <head>
            <meta charset='UTF-8' name="viewport" content="width=device-width, initial-scale=1"/>
        <title>@yield('title','Fishwise Online Vendor Survey')</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
        
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        
        <!-- Latest compiled JavaScript -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/start/jquery-ui.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
         </script>
        <link rel="stylesheet" href="/css/ovs.css">
    </head>
    <body>
        <div class="container">
            <div class="page-header">
                <img  height=120 width=180 id="ngo_logo" src="/css/images/Fishwise_logo.jpg" style="float: right; height: 50%;">
                <h1 style="height: 60px;">FishWise Online Vendor Survey Website</h1>
            </div>

            
                <nav class="navbar navbar-inverse">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target='#myNavBar'>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="/">Alliance OVS</a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavBar">
                        <ul class="nav navbar-nav">
                            <li><a href="/">Home</a></li>
                            <li><a  class="mynav" name="/OVS/PHP/Pages/staff.php">Staff</a></li>
                            <li><a class="mynav" name="/OVS/PHP/Pages/retailer.php">Retailers</a></li>
                            <li><a class="mynav" href="/vendor/1">Vendors</a></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right">
@yield('user','<li id="register"><a><span class="glyphicon glyphicon-user"></span> Register</a></li><li id="login"><a><span class="glyphicon glyphicon-log-in"></span> Log IN</a></li>')
                        </ul>
                    </div>
                </nav>
            
            @yield('content')
            
                <div class="footer">
                    <p>If you are having difficulty viewing this website or have any questions about it don't hesitate to send us an
                    <a href="mailto:r.manzer@fishwise.org">email  <span class="glyphicon glyphicon-envelope"></span></a></p>
                </div>
                <p></p>
         </div>
    </body>
</html>