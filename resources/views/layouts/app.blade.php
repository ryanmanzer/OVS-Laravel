<!DOCTYPE html>
<html lang="en" style = "height: 100%">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}" />

    <title>FishWise Online Vendor Survey</title>
    <link rel="apple-touch-icon" sizes="180x180" href="/favicons/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicons/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicons/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/favicons/manifest.json">
    <link rel="mask-icon" href="/favicons/safari-pinned-tab.svg" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">
    <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">
    
    
    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="/css/app.css">
    <script src='/js/all.js'></script>

</head>
<body id="app-layout" style = "height: 98%">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container" style = "width:95%">
            <div class="navbar-header">

                <!-- Branding Image -->
                <img src="/css/images/logos/FW_long.png" class="img-rounded navbar-brand" width="auto" height="26"/>
                <a class="navbar-brand" >Online Vendor Surveys</a>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
                <!-- Left Side Of Navbar -->
                <!--<ul class="nav navbar-nav">
                    <li><a href="{{ url('/') }}">Home</a></li>
                    @yield('navoptions')
                </ul> -->
                <ul class="nav navbar-nav navbar-middle"><li><a id = "userNameNavBar" style="font-size: 18px"></a></li><li></li></ul>
                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <!--<li><a href="{{ url('/login') }}">Login</a></li> -->
                        <!-- <li><a href="{{ url('/register') }}">Register</a></li> -->
                    @else
                        <li><a> {{ Auth::user()->name }} </a></li>
                        <li style="padding-left: 20px;">
                           <a href="{{ url('/logout') }}" style="padding: 0px;"><button class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-log-out" aria-hidden="true"></span> Logout</button>
                           </a>
                        </li>
                        
                        <li style = "padding-left: 20px;">
                           <a href="mailto:surveys@fishwise.org"  target="_blank" style="padding: 0px;"><button class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-envelope" aria-hidden="true"></span> Email Us</button>
                           </a>
                        </li>
                        <li style="padding-left: 20px;">
                           <a href="/tutorial_pdfs/ovs_training_2017_01_09.pdf"  target="_blank" style="padding: 0px;"><button class="btn btn-default navbar-btn"><span class="glyphicon glyphicon-save-file" aria-hidden="true"></span> Tutorial PDF</button>
                           </a>
                        </li>
                        <li style="padding-left: 20px;">
                            <button class="btn btn-default navbar-btn btn-tutorial" style = "">?</button>
                        </li>
                        
                    @endif
                </ul>
            </div>
        </div>
    </nav>
<div class = "row">
  <div class = "col-sm-2"></div>
  <div class="alert alert-warning fade in alert-dismissable opening-msg col-sm-8" style = "display:none">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Warning!</strong> This application does not support your browser's "Back" and "Forward" buttons.  Use the navigation options provided within the screen.
  </div>
   
</div>
<div class = "row">
  <div class = "col-sm-2"></div>
  <div class="alert alert-warning fade in alert-dismissable opening-msg col-sm-8"  style = "display:none">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Warning!</strong> This application will timeout after 15 minutes of inactivity and send you back to the login page.
  </div>
</div>
    @yield('content')


    <div id="leaveForm" class="modal fade" role="dialog">
      <div class="modal-dialog">

          <!-- building modal content -->
          <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal"><icon class="fa fa-times-circle fa-lg"></icon></button>
                  <h4 class="modal-title">Are You Sure?</h4>
              </div>
              <div class="modal-body">
                 <h3>Are you sure you want to leave this page?  Any changes you have made will not be saved.</h3>
                 <button class="btn btn-info btn-lg" id="leaveYes" data-dismiss="modal">Yes</button>
                 <button class="btn btn-secondary btn-lg" id="delNo" data-dismiss="modal">No</button>
              </div>
          </div>
      </div>
    </div>
    <!-- JavaScripts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    <script   src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"   integrity="sha256-VazP97ZCwtekAsvgPBSUwPFKdrwD3unUfSGVYrahUqU="   crossorigin="anonymous"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>

 <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/js/bootstrap-select.js"></script>
<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.11.2/css/bootstrap-select.css" rel="stylesheet">
    <script>
      $.ajaxSetup({ headers:
        {'csrftoken': '{{ csrf_token() }}',
         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')}
      });
    </script>
    @yield('scripts')
</body>
</html>
