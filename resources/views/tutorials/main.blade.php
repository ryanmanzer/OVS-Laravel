<style>
.carousel-indicators {
  bottom: -40px;
}
.carousel-indicators li {
  background-color: #999;
  background-color: rgba(70,70,70,.25);
  z-index: 10002;
}

.carousel-indicators .active {
  background-color: #444;
}
</style>
<div class="tutorial-carousel-wrapper{{ $hidden ? ' hidden' : ''}}" style = "z-index: 1001">
    <!--
    <h2>
        Tutorial title text
        <small>You need to complete the tutorial before start using this site</small>
    </h2>
    -->
    <div id="tutorial-carousel-{{ $tutorial }}" data-name="{{ $tutorial }}" class="carousel slide tutorial-carousel" data-ride="carousel" data-interval="false" data-wrap="false">
        <!-- Indicators -->
        
        <ol class="carousel-indicators">
            <li class="active"></li>
            <li data-slide-to="1"></li>
            <li data-slide-to="2"></li>
            <li data-slide-to="3"></li>
            
        </ol>
        
        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox" style = "width: 60%; margin: 0 auto">
            <div class="item active">
                <img class="center-block" src="/tutorial_slides/main/main_slide_1.png">
                <!--
                <div class="carousel-caption">
                    <h3>Slide text if needed</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex facere, inventore repudiandae rerum sequi tempore veniam? Consequatur corporis doloremque itaque maxime nobis placeat quod. Asperiores consequuntur deserunt dolores neque quam!</p>
                </div>
                -->
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/main/main_slide_2.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/main/main_slide_3.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/main/main_slide_4.png">
            </div>
        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control hidden" href="#tutorial-carousel-{{ $tutorial }}" role="button" data-slide="prev">
            <span>Previous slide</span>
        </a>
        <a class="right carousel-control" href="#tutorial-carousel-{{ $tutorial }}" role="button" data-slide="next">
            <span aria-hidden="true">Next slide</span>
        </a>
        <button class="btn-info btn-lg tutorial_close"  style = "float:right; display: none">
            <span aria-hidden="true">Close</span>
        </button>
  
    </div>
</div>
<script>
@if( Auth::user()->hasCompletedTutorial('main') )
  $('.tutorial_close').show();
@endif  
function closeTutorial(){
    $('.tutorial-carousel-wrapper').addClass('hidden');
}
$('.tutorial_close').click (closeTutorial);

</script>