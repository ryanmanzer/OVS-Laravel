<style>
.carousel-indicators {
  bottom: -40px;
}
.carousel-indicators li {
  background-color: #999;
  background-color: rgba(70,70,70,.25);
  z-index: 10002;
}

.carousel-indicators .active {
  background-color: #444;
}
</style>

<div class="tutorial-carousel-wrapper{{ $hidden ? ' hidden' : ''}}"  style = "z-index: 1001">
    <!--
    <h2>
        Survey tutorial title text
        <small>You need to complete the tutorial before start using this site</small>
    </h2>
    -->
    <div id="tutorial-carousel-{{ $tutorial }}" data-name="{{ $tutorial }}" class="carousel slide tutorial-carousel" data-ride="carousel" data-interval="false" data-wrap="false">
    
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li class="active"></li>
            <li data-slide-to="1"></li>
            <li data-slide-to="2"></li>
            <li data-slide-to="3"></li>
            <li data-slide-to="4"></li>
            <li data-slide-to="5"></li>
            <li data-slide-to="6"></li>
            <li data-slide-to="7"></li>
            <li data-slide-to="8"></li>
            <li data-slide-to="9"></li>
            <li data-slide-to="1"></li>
            <li data-slide-to="11"></li>
            
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox" style = "width: 60%; margin: 0 auto">
            <div class="item active">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_01.png">
                <!--
                <div class="carousel-caption">
                    <h3>Slide text if needed</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ex facere, inventore repudiandae rerum sequi tempore veniam? Consequatur corporis doloremque itaque maxime nobis placeat quod. Asperiores consequuntur deserunt dolores neque quam!</p>
                </div>
                -->            
            </div>
            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_02.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_03.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_04.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_05.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_06.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_07.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_08.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_09.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_10.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_11.png">
            </div>

            <div class="item">
                <img class="center-block" src="/tutorial_slides/survey/survey_slide_12.png">
            </div>

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control hidden" href="#tutorial-carousel-{{ $tutorial }}" role="button" data-slide="prev">
            <span>Previous slide</span>
        </a>
        <a class="right carousel-control" href="#tutorial-carousel-{{ $tutorial }}" role="button" data-slide="next">
            <span aria-hidden="true">Next slide</span>
        </a>
        <button class="btn-info btn-lg tutorial_close"  style = "float:right; display: none">
            <span aria-hidden="true">Close</span>
        </button>
         
    </div>
</div>
<script>

@if( Auth::user()->hasCompletedTutorial('survey') )
  $('.tutorial_close').show();
@endif    

function closeTutorial(){
    $('.tutorial-carousel-wrapper').addClass('hidden');
}
$('.tutorial_close').click (closeTutorial);

</script>