<div class = "page-header" style = "margin:0 auto">
<div class = "row">
	<div class= "col-sm-3"></div>
	<div class= "col-sm-6"><h1>Which retailer's surveys would you like to work on today?</h1></div>
</div>
<div class = "row">
	<div class= "col-sm-3"></div>
	<div class= "col-sm-6">	
 @if(isset($retailers))
 	<table class = "table survey-list"><tbody>
  	@if(count($retailers) > 0)
		<thead><th class = "col-sm-4" style = "font-size: 18px">Retailers</th><th class = "col-sm-2"></th></thead>
		 @foreach($retailers as $retailer)
			 <tr>
       			<td style= "font-size: 18px" >{{$retailer->name}}</td>
       			<td ><button class="btn btn-success btn-lg btn-block viewSurveys" url = "/vendor/{{$vendor->id}}/retailer/{{$retailer->id}}/surveysHome" tag = "#general_data" style = "vertical-align: middle; margin-left: auto; margin-right: auto;">View Surveys</button></td>
      		 </tr>
  			@endforeach 
  	@else
      <tr><td colspan = 5>There are currently no retailers for {{$vendor->name}}</td></tr>
  	@endif
  	</tbody></table>
@else
	<h3>Currently we have no surveys in our system for you.  If this is in error please contact FishWise or your contact at the retailer who is requesting this survey of you.</h3> 
@endif
</div>
	
	<script>

	$('button.viewSurveys').off("click");
	$('button.viewSurveys').click(getAJAX);
	$('a#userNameNavBar').html('{{$vendor->name}}');

</script>
@if(Auth::user()->login_count < 5)
<script>
	if($('.opening-msg').length > 0){
		$('.opening-msg').show()
	}
</script>	
@endif