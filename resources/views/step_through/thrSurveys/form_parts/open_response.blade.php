<div class="form-group">
  <input type="text" name="additional_info" class="form-control"
  @if($survey->complete)
    disabled="true"
    @if(!is_null($sq->answer))
      @foreach($sq->answer as $ans)
        @if(!is_null($ans->additional_info))
          value="{{$ans->additional_info}}"
        @endif
      @endforeach
    @endif
  @else
    @if(isset($survclear))
      placeholder="Please Describe..."
    @elseif(!is_null($sq->answer))
      @foreach($sq->answer as $ans)
        @if(!is_null($ans->additional_info))
          value="{{$ans->additional_info}}"
        @endif
      @endforeach
    @else
      placeholder="Please Describe..."
    @endif
    
  @endif
  id="{{$sq->id}}">
</div>
