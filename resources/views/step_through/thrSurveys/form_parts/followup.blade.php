
<div class="row myFollowUps" 
@if(isset($survclear))
    style="display:none;"
@elseif(has_answer($sq->id,$key))
    style="display:block;"
@else
    style="display:none;"
@endif
@if($survey->complete)
   disabled="true" 
@endif    
 
  id="follow_up_{{$sq->question->id}}">
    <div class="col-sm-10 col-sm-offset-1 well">
      @foreach($fus as $fuQ)
        <label for='{{$sq->id}}'>{{$fuQ->question_text}} </label>
          @if($fuQ->single_answer)
            @include('step_through.thrSurveys.form_parts.yesno', ['sq' => get_survey_question($fuQ->trace_hr_question_id,$survey->id),'survey'=>$survey])
          @elseif ($fuQ->is_fill_in)
            @include('step_through.thrSurveys.form_parts.open_response',['sq' => get_survey_question($fuQ->trace_hr_question_id,$survey->id),'survey'=>$survey])
          @else
            @include('step_through.thrSurveys.form_parts.checkbox',['sq'=>get_survey_question($fuQ->trace_hr_question_id,$survey->id),'survey'=>$survey])
          @endif
      @endforeach
    </div>
</div>
