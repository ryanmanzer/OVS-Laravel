<!-- resources/vendor/parts/trace_hr/surveyForm.blade.php -->
<!-- BUILDING HEADER AND SUBMISSION MESSAGE SECTION -->
<div class = "row" id = "survey_summary_data">
@include('step_through.thrSurveys.surveySummary')
</div>
<hr style = "margin-top: 0px;">
<div class = "row" id = "survey_alert">
</div>

<div class = "row">
<div class = "col-sm-1"></div>
<div class = "col-sm-10">
<form class="trace_hr_survey" id = "trace_hr" url="/thrSurvey/{{$survey->id}}/save" data-url="/vendor/{{$survey->fk_vendor_id}}/thr" tag="#survey_summary_data" style="
    padding-left: 5px;">
  {{ csrf_field() }}
  @include('step_through.thrSurveys.surveyFormOnly')
</form>
</div>
</div>


<script>

$("button.backToSurveys").off("click");
$("button.backToSurveys").click(getAJAX);

</script>
</div>
