  @foreach($survey->surveyquestions as $sq)
    @if(!$sq->question->is_followUp)
        <label for='{{$sq->id}}'>{{$sq->question->question_text}} </label>
        @if($sq->question->single_answer)
          @include('step_through.thrSurveys.form_parts.yesno', ['sq' => $sq,'survey'=>$survey])
        @elseif($sq->question->is_fill_in)
          @include('step_through.thrSurveys.form_parts.open_response',['sq' => $sq,'survey'=>$survey])
        @else
          @include('step_through.thrSurveys.form_parts.checkbox', ['sq' => $sq,'survey'=>$survey])
        @endif
    @endif
    <br>
  @endforeach
    <div class="row" style="padding-bottom: 20px;">
    <div class = "col-sm-2">
    <button class="btn btn-primary submit-button" style = "width:90%"
      @if($survey->complete == 1)
        disabled="true"
      @endif
      type="submit">Save Answers</button>
    </div>
    <div class = "col-sm-2">
    <button class="btn btn-default cancel-button clearThrSurvey" style = "width:90%"
        @if($survey->complete == 1)
          disabled="true"
        @endif
        type="reset" name="myreset" class="formReset">Clear Form</button>
        </div>
    <div class = "col-sm-6"></div>
    <div class = "col-sm-2">
      <button class="btn btn-success cancel-button submitThrSurvey" style = "width:90%"
        @if($survey->complete == 1)
          disabled="true"
        @endif
        type="reset" name="myreset" class="formReset">Submit Survey</button>

    </div>
    <script>
      // These are found in traceHr.js
      assignTraceHrSubmit("form.trace_hr_survey");
      assignFollowUp("input.hasFollowUp");
      hideFollowUp("input.noFollowUp");
      $('button.clearThrSurvey').off("click");
      $('button.clearThrSurvey').click(function(){
        loadComponent('/thrSurvey/{{$survey->id}}/thrClear','#trace_hr');
      });
      $('button.submitThrSurvey').off("click");
      $('button.submitThrSurvey').click(function(){
        $("form#trace_hr").submit();
        loadComponent('/thrSurvey/{{$survey->id}}/thrSubmit','#general_data');
      });
    </script>