<div class = "col-sm-2"></div>
<div class="alert alert-info fade in alert-dismissable col-sm-8" style="margin-top:18px;">
    <a href="#" class="close" data-dismiss="alert" aria-label="close" title="close">×</a>
    <strong>Success!</strong> Questionnaire results saved.  To submit the survey, finish answering and click the submit button at the bottom of this survey.
</div>
<div class = "col-sm-2"></div>