	
	<div class = "col-sm-1"><p style = "font-size: 20px">Survey Info</p></div>
	<div class = "col-sm-10">
	<table class = "table survey-list">
	<thead><th class = "col-sm-4">Survey Title</th><th class = "col-sm-2">Due Date</th><th class = "col-sm-2">Status</th><th class = "col-sm-2">Last Saved</th><th class = "col-sm-2"></th></thead>
  <tr><td>{{$survey->title}}</td><td>{{$survey->survey_date}}</td>
       @if($survey->complete == 1)
       <td>Complete!</td>
       <td>{{$survey->last_saved}}</td>
       @else
       <td>Incomplete</td>
       <td>{{$survey->last_saved}}</td>
       @endif
  </tr>
	</tbody>
	</table>
    </div>
    <div class = "col-sm-1" style = "display: table-cell">
    	<button class="btn btn-md btn-info backToSurveyList" url="/vendor/{{$survey->fk_vendor_id}}/surveysHome" tag="#general_data" data-tutorial="main" style = "vertical-align: middle;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Survey<br>List</button>
    </div>



<script>
$('button.backToSurveyList').off("click")
$('button.backToSurveyList').click(getAJAX)
</script>
@if(isset($success))
  <script>
    loadComponent('/thrSurvey/thrSurveySubmittedAlert', "#survey_alert");
    window.scrollTo(0, 0);
  </script> 
@endif
@if($survey->complete == 1)
  <script>
    loadComponent('/thrSurvey/thrSurveyCompletedAlert', "#survey_alert");
    window.scrollTo(0, 0);
  </script> 
@endif