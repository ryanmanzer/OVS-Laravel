@if($sp->submitted == 1)
<div class = "row">
	<div class = "col-sm-12">
		<label style = "font-size: 18px">Product complete and submitted!</label> <a href="#"><span class="glyphicon glyphicon-pencil editSubmit" url = "/surveyProduct/{{$sp->id}}/reOpenSubmit" tag = "#product_data_entry"  style = "margin-left: 10px"></span></a>
	</div>
</div>
<script>
	$('span.editSubmit').off("click");
	$('span.editSubmit').click(getAJAX);
</script>
@endif

@if($sp->survey->complete == 1)
<script>
    $('span.editSubmit').hide();
    </script>
@endif