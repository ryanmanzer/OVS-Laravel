<table class = "table">
<tbody>
<thead>
<tr  style = "height: 48px">
	<th class = "col-sm-2">{{$sp->survey->retailer->name}} Product Code</th>
	<th class = "col-sm-5">Product Description</th>
	<th class = "col-sm-2">UPC Code</th>
	<th class = "col-sm-3"></th>
</tr>
</thead>
<tr style = "height: 48px; background-color: #FFFFE0; font-weight: bold">
<td>{{$sp->product->retailerProductCode}}</td>
<td>{{$sp->product->retailerProductDescription}}</td>
<td>{{$sp->product->upcCode}}</td>
<td></td>
</tr>


</tbody>
</table>
<script>
$('button.backToCatList').off("click")
$('button.backToCatList').click(getAJAX)
</script>