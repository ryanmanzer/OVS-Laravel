<!--<div class="col-sm-12" style = "display: table-cell; text-align: center;"><label>Provide us with information on the sources of the seafood used in this product</label></div>-->

@if(sourcePercent($sp->product) == 1)
  <script>
    loadComponent('/surveyProduct/{{$sp->id}}/onToCertifications', '#product_data_entry');
  </script>
@elseif(count($sp->sps) == 0)
  <script>
    loadComponent('/surveyProduct/{{$sp->id}}/newSourceEntryForm', '#product_data_entry');
  </script>
@else
  <?php $openSPS = Null; ?>
  @foreach($sps as $spSource)
    @if($openSPS == Null)
      @if(is_null($spSource->proportionOfProduct))
        '<?php $openSPS = '.{{$spSource->id}}.'; ?>''
        <script>
          loadComponent('/sp/{{$sp->id}}/sps/{{$spSource->id}}/addSourceAmount', '#product_data_entry');
        </script>
      @endif
    @endif
  @endforeach
  @if(is_null($openSPS))
    <script>
    loadComponent('/surveyProduct/{{$sp->id}}/newSourceEntryForm', '#product_data_entry');
    </script>
  @else

  @endif
@endif
<script>
loadComponent('/sp/{{$sp->id}}/sourcesList', '#sourcesList');
</script>
<script>
loadComponent("surveyProduct/{{$sp->id}}/updateProgressBar", "#scriptHandler");
</script>