@if(isset($nextSp))

<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">
			<div class = "col-sm-12">
				<label style = "margin-bottom: 24px; font-size: 18px">What would you like to do next?</label>
			</div>
			<div class = "col-sm-3"></div>
			<div class = "col-sm-3">
				<button class = "button btn-primary btn-lg answerCompleted" url="/surveyProduct/{{$nextSp->id}}/productEntry" tag="#product_entry">Next Product: #{{$nextSp->product->retailerProductCode}}</button>
			</div>
			<div class = "col-sm-3">
				<button class = "button btn-primary btn-lg answerCompleted" url="/survey/{{$sp->survey->id}}/surveyCatList" tag="#product_survey_data">Back To Category List</button>
			</div>		
		</div>
		<div class = "col-sm-3"></div>
	</div>
	


@elseif(isset($nextCat))

<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">
			<div class = "col-sm-12">
				<label style = "margin-bottom: 24px; font-size: 18px">You have completed every product in this category.  What would you like to do next?</label>
			</div>
			<div class = "col-sm-3"></div>
			<div class = "col-sm-3">
				<button class = "button btn-primary btn-lg answerCompleted" url="survey/{{$sp->survey->id}}/sfCat/{{$nextCat->id}}" tag="#product_survey_data">Next Category: {{$nextCat->category}}</button>
			</div>
			<div class = "col-sm-3">
				<button class = "button btn-primary btn-lg answerCompleted" url="/survey/{{$sp->survey->id}}/surveyCatList" tag="#product_survey_data">Back To Category List</button>
			</div>		
		</div>
		<div class = "col-sm-3"></div>
</div>
	
	<script>
	  	$('button.answerCompleted').off("click");
		$('button.answerCompleted').click(getAJAX);
	</script>

@else

<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">
			<div class = "col-sm-12">
				<label style = "margin-bottom: 24px; font-size: 18px">All products on the survey are done!  Go back to the category list to submit this product survey as completed.</label>
			</div>
			<div class = "col-sm-4"></div>
			<div class = "col-sm-4">
				<button class = "button btn-primary btn-lg answerCompleted" url="/survey/{{$sp->survey->id}}/surveyCatList" tag="#product_survey_data">Back to Category List</button>
			</div>
		</div>
		<div class = "col-sm-3"></div>
</div>

@endif

<script>
  	$('button.answerCompleted').off("click");
	$('button.answerCompleted').click(getAJAX);
</script>