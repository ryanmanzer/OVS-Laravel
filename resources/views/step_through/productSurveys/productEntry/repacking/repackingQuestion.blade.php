@if(is_null($sp->repack))

	<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">
			<div class = "col-sm-12">
				<label style = "margin-bottom: 24px; font-size: 18px">Are you a certified repacker of this product?</label>
			</div>
			<div class = "col-sm-2">
				<button class = "button btn-primary btn-lg answerRepacking" url = "/surveyProduct/{{$sp->id}}/repacking/1" tag = "#product_data_entry">Yes</button>
			</div>
			<div class = "col-sm-2">
				<button class = "button btn-primary btn-lg answerRepacking" url = "/surveyProduct/{{$sp->id}}/repacking/0" tag = "#product_data_entry">No</button>
			</div>
			<div class = "col-sm-4">
				<button class = "button btn-primary btn-lg answerRepacking" url = "/surveyProduct/{{$sp->id}}/repacking/2" tag = "#product_data_entry">Do not repack</button>
			</div>
		</div>
		<div class = "col-sm-3"></div>
	</div>
	
	<script>
	  	$('button.answerRepacking').off("click");
		$('button.answerRepacking').click(getAJAX);
	</script>

@else

	<div class = "row" style = "margin-top: 20px">
		<div class = "col-sm-4"></div>
		<!--
		<div class = "col-sm-2" style = "display: table-cell; text-align: center">
			<button class = "button btn-primary btn-lg OnToActive" url = "/surveyProduct/{{$sp->id}}/reOpenRepacking" tag = "#product_data_entry">Change Repacking<i class="fa fa-arrow-edit" aria-hidden="true" style = "margin-left: 8px"></i></button>
		</div>
		-->
		<div class = "col-sm-2">		
			<button class = "button btn-primary btn-lg OnToActive" url = "/surveyProduct/{{$sp->id}}/onToActive" tag = "#product_data_entry">To Active Status<i class="fa fa-arrow-right" aria-hidden="true" style = "margin-left: 8px"></i></button>
		</div>
		<div class = "col-sm-4"></div>
	</div>
	<script>
		loadComponent("surveyProduct/{{$sp->id}}/repackingStatus", "#repackingStatus");
		$('span.editActive').show();
		$('button.OnToActive').off("click");
		$('button.OnToActive').click(getAJAX);
	</script>


@endif