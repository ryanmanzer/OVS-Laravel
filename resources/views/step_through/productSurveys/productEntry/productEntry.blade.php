<div class = "header">
{{--  Summary info on the product, id, name, UPC  --}}
@include('step_through.productSurveys.productEntry.summary')
</div>
<div class = "col-sm-1"></div>
<div id = "progress_bar" class = "col-sm-10">
	@include('step_through.productSurveys.productEntry.progressBar')
</div>
<div class = "col-sm-1"></div>
<div id = "sourcesList" style = "display:none"></div>
<div id = "certsList" style = "display:none"></div>
<div id = "repackingStatus" style = "display:none"></div>
<div id = "activeStatus" style = "display:none"></div>
<div id = "submitStatus" style = "display:none"></div>
<div id = "product_data_entry"></div>
<div id = "scriptHandler"></div>

<script>
	$('button.openSurveyProduct').show();
	$("button#expand_sp_{{$sp->id}}").hide();
	$('tr.catSideBar').css('background-color', '#eeeeee');
	$('tr.catSideBar').css('font-weight', 'normal');
	$("tr#catSideBar_{{$sp->id}}").css('background-color', '#FFFFE0');
	$("tr#catSideBar_{{$sp->id}}").css('font-weight', 'bold');

</script>
<script>
	loadComponent('/sp/{{$sp->id}}/sourcesList', '#sourcesList');
	loadComponent('/surveyProduct/{{$sp->id}}/certsList', '#certsList');
	loadComponent("/surveyProduct/{{$sp->id}}/repackingStatus", "#repackingStatus");
	loadComponent("/surveyProduct/{{$sp->id}}/activeStatus", "#activeStatus");
	loadComponent("/surveyProduct/{{$sp->id}}/submitStatus", "#submitStatus");
	loadComponent("/surveyProduct/{{$sp->id}}/productDataEntry", "#product_data_entry");
</script>
