@if(is_null($sp->active))

	<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">
			<div class = "col-sm-12">
				<label style = "margin-bottom: 24px; font-size: 18px">Has this product been discontinued (no longer sold to  {{$sp->survey->retailer->name}})?</label>
			</div>
			<div class = "col-sm-3"></div>
			<div class = "col-sm-2">
				<button class = "button btn-primary btn-lg answerActive" url = "/surveyProduct/{{$sp->id}}/active/1" tag = "#product_data_entry">Yes</button>
			</div>
			<div class = "col-sm-2">
				<button class = "button btn-primary btn-lg answerActive" url = "/surveyProduct/{{$sp->id}}/active/0" tag = "#product_data_entry">No</button>
			</div>		
		</div>
		<div class = "col-sm-3"></div>
	</div>
	
	<script>
	  	$('button.answerActive').off("click");
		$('button.answerActive').click(getAJAX);
	</script>

@else

	<div class = "row" style = "margin-top: 20px">
		<div class = "col-sm-4"></div>

		<div class = "col-sm-2">		
			<button class = "button btn-primary btn-lg OnToSubmit" url = "/surveyProduct/{{$sp->id}}/onToSubmit" tag = "#product_data_entry">To Product Submit<i class="fa fa-arrow-right" aria-hidden="true" style = "margin-left: 8px"></i></button>
		</div>
		<div class = "col-sm-4"></div>
	</div>
	<script>
		$('span.editActive').show();
		$('button.OnToSubmit').off("click");
		$('button.OnToSubmit').click(getAJAX);
	</script>


@endif





<script>
	loadComponent("/surveyProduct/{{$sp->id}}/repackingStatus", "#repackingStatus");
	loadComponent("/surveyProduct/{{$sp->id}}/activeStatus", "#activeStatus");
	loadComponent("surveyProduct/{{$sp->id}}/updateProgressBar/active", "#scriptHandler");
	
</script>