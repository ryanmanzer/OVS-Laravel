@if(count($sps)> 0)
<div class = "row">
  <div class = "col-sm-12"><label style = "font-size: 18px">Sources for this product:</label><a href = "#"><span class="glyphicon glyphicon-pencil editSources editSection" url = "/surveyProduct/{{$sp->id}}/reOpenSources" tag = "#product_data_entry" style = "display:none; margin-left: 10px"></span></a>
</div>
<table class="table table-striped table">
        <thead>
            <tr>
                <th>
                <th>Supplier</th>
                <th>Seafood</th>
                <th>Harvest Method</th>
                <th>Catch Country</th>
                <th>Catch Region</th>
                <th>COOL</th>
                <th>Certification Type</th>
                <th>Certification #</th>               
                <th>Percent of Product</th>
                <th></th><th></th>
            </tr>
      </thead>
      <tbody>
    
  
      @foreach($sps as $key=>$spSource)
              <tr id = "sps_{{$spSource->id}}" style = "height: 54px">
                  <td>Source #{{$key + 1}}</td>
                  <td>{{$spSource->source->supplierCompany or ''}}</td>
                  <td>{{$spSource->source->seafoodSpecies->seafoodSpecies or ''}}</td>
                  <td>{{$spSource->source->harvestMethod->harvestMethod or ''}}</td>
                  <td>{{$spSource->source->catchCountry->countryName or ''}}</td>
                  <td>{{$spSource->source->harvestRegion or ''}}</td>
                  <td>{{$spSource->source->COOL->countryName or ''}}</td>
                  <td>{{$spSource->source->certification->certType or '{none given}'}}</td>
                  @if($spSource->source->certificationNumber == '')
                    <td>{none given}</td>
                  @else
                    <td>{{$spSource->source->certificationNumber or '{none given}'}}</td>
                  @endif
                    <td id ="{{$spSource->id}}_percent">{{$spSource->proportionOfProduct*100}} %
                    <a href = "#"><span class="glyphicon glyphicon-pencil editPercent" url = "/sp/{{$sp->id}}/sps/{{$spSource->id}}/addSourceAmount" tag = "#product_data_entry" style = "display:none"></span></a>
                    </td>
                  <td>
                  <button class ="btn btn-danger btn-sm removeSps" post-url = "/sps/{{$spSource->id}}/removeSps" url = "/sps/{{$spSource->id}}/removeSps" tag = "#product_data_entry" style = "">Remove
                  <i class="fa fa-trash" aria-hidden="true" style = "margin-left: 4px"></i></button>                        
                  </td>
              </tr>
              @if(is_null($spSource->proportionOfProduct))                  
                <script>
                $('span.editPercent').hide();
                $('button.removeSps').hide();
                $("td#{{$spSource->id}}_percent").html('');
                $("tr#sps_{{$spSource->id}}").css('background', '#FFFFE0');
                </script>
              @endif
      @endforeach
    </tbody>
</table>
<script>
function removeSps(e){
  e.preventDefault();
  var post_url=$(this).attr("post-url");
  var get_tag=$(this).attr("tag");
  var get_url=$(this).attr("url");
  postAJAXmanual(post_url, get_url, get_tag)
}
$('button.removeSps').off("click");
$('button.removeSps').click(getAJAX);
$('span.editSources').off("click");
$('span.editSources').click(getAJAX);
$('span.editPercent').off("click");
$('span.editPercent').click(getAJAX);
@if($sp->sourceProgress == 2)
   $('span.editPercent').show();
@endif
</script>
@endif
