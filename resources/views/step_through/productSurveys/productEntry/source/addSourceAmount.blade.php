<div class = "row">
<div class = "col-sm-3"></div>
<div class = "col-sm-6" style = "display: table-cell; text-align: center">
<label  style = "margin-bottom: 24px; font-size: 18px">What portion of the raw seafood product is comprised of the highlighted source?</label>
</div>
</div>
<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-2">
			<button class = "button btn-info btn-lg assignRemaining" style = "height: 48px; width: 100%; padding-left: 0px; padding-right: 0px;">{{100 - (100*sourcePercent($sp))}}%<i class="fa fa-arrow-right" aria-hidden="true" style = "margin-left: 4px"></i></button>
		</div>
		<div class = "col-sm-2">
			<input type="text" class="form-control" id="sourcePercentage" autocomplete="off" style = "height:48px; text-align: center; font-size: 18px">
		</div>
		<div class = "col-sm-2">
			<button class = "button btn-primary btn-lg percentAssign" style = "height: 48px; width: 100%">Assign</button>		
		</div>
		<div class = "col-sm-3"></div>
		
</div>






<script>
$('button.assignRemaining').off("click");
$('button.assignRemaining').click(assignRemaining);
$('button.percentAssign').off("click");
$('button.percentAssign').click(assignPercent);
$('span.editPercent').hide();
$('button.removeSps').hide();
$("td#{{$sps->id}}_percent").html('');
$("tr#sps_{{$sps->id}}").css('background', '#FFFFE0');
//$("td#{{$sps->id}}_percent").css('background', '#dff0d8');
//$("td#{{$sps->id}}_percent").css('border','2px solid black');
//loadComponent('/sp/{{$sp->id}}/sourcesList', '#sourcesList');
function assignPercent(){
	var percent = $("#sourcePercentage").val();
	if(percent < 1 || percent > {{100 - (100*sourcePercent($sp))}} || (percent % 1) > 0 ) {
		$("#sourcePercentage").css('background', '#f2dede');
	} else {

		var posturl = "/sps/{{$sps->id}}/assignPercent/" + percent;
		var geturl = "/surveyProduct/{{$sp->id}}/productDataEntry/section/sources";
		var gettag = "#product_data_entry";

		postAJAXmanual(posturl,geturl, gettag);
	}
}

function assignRemaining(){
	var percent = {{100 - (100*sourcePercent($sp))}};
		$("input#sourcePercentage").val(percent);
}


</script>
