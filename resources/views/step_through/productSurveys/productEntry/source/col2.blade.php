<div class="form-group">
    <label for="wf">Wild or Farmed* </label><p class="source_err" id="wild_farmed" style = "display: inline-block; margin-bottom: 5px"></p>
    <select name="wild_farmed" id="wf" autocomplete="off" class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10 >
    {{-- class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10> --}}
      <option value>--select Wild or Farmed--</option>
      <option data-divider="true"></option>    
        @if(isset($mySource))
           @if($mySource->wild_farmed == "Wild")
            <option selected = "selected" value="Wild">Wild</option>
            <option value="Farmed">Farmed</option>
          @elseif($mySource->wild_farmed == "Farmed")
            <option value="Wild">Wild</option>
            <option selected = "selected" value="Farmed">Farmed</option>
          @endif
        @else
           <option value="Wild">Wild</option>
          <option value="Farmed">Farmed</option>          
        @endif
    </select>
</div>


<div class="form-group" id = "method_category_group">
{{--    
<!--
<label for="method_cat">Harvest Method Category*: </label><p class="source_err" id="method_category_id" style = "display: inline-block; margin-bottom: 5px"></p>
    <select class="form-control dropdown selectpicker" type="text" name="method_category_id" id="methodCat" autocomplete="off" data-dropup-auto = false data-size= 10>
    @if(isset($mySource))
      <option value>--select Method Category--</option>
      @foreach($methCats as $methCat)
        @if($mySource->method_category_id == $methCat->id)
          <option selected="selected" value="{{$methCat->id}}">{{$methCat->methodCategory}}</option>
        @else
          <option value="{{$methCat->id}}" >{{$methCat->methodCategory}}</option>
        @endif
      @endforeach    
    @else
      <option value selected="selected" disabled="disabled">--no Wild Farmed selected yet--</option>
    @endif
    
    </select>
-->
--}}
<label for="method_cat">Harvest Method Category* </label>
@if(isset($mySource))
<input type="text" class="form-control" value="{{$mySource->methodCategory->methodCategory}}" id = "method_category_text" disabled="disabled">
    <input type="text" class="form-control" id = "method_category_id" name="method_category_id" value = "{{$mySource->method_category_id}}" style = "display:none">
@else
<input type="text" class="form-control" value="--auto-fills up from Specific Method--"  id = "method_category_text" disabled="disabled">
    <input type="text" class="form-control" id = "method_category_id" name="method_category_id" value style= "display:none">

@endif
</div>

<div class="form-group">
    <label for="gear_type">Specific Method* </label><p class="source_err" id="fk_harvestMethod_id" style = "display: inline-block; margin-bottom: 5px"></p>
    <select name="fk_harvestMethod_id" id="gear_type" class="form-control dropdown selectpicker" data-live-search="true"  data-dropup-auto = false data-size= 10 disabled >
    {{-- class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10 > --}}
    @if(isset($mySource))
      <option value>--Select Specific Method--</option>
      @foreach($hMeths as $hMeth)
        @if($hMeth->order == 1)
          <optgroup label="{{$hMeth->category->methodCategory}}">
        @endif      
          <option value="{{$hMeth->id}}">{{$hMeth->harvestMethod}}</option>
        
      @endforeach
      <script>
        
        $("select#gear_type").removeAttr('disabled');
        $("select#gear_type").selectpicker({});
        $("select#gear_type").val('{{$mySource->fk_harvestMethod_id}}');
        $("select#gear_type").selectpicker('refresh');
        
      </script>
    @else  
      <option value>--no Wild or Farmed selected yet--</option>
    @endif
  </select>
</div>
<div class="form-group">
    <label for="catch_country">Catch Country* </label>
    <p class="source_err" id="fk_harvestCountry_id" style = "display: inline-block; margin-bottom: 5px"></p>
    <select name="fk_harvestCountry_id" id="catch_country"  autocomplete="off" class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10 data-live-search="true">
    <option value >--select Catch Country--</option>
      @foreach($countries as $country)
              @if ($country->sortOrder == 1)
                <optgroup label = "{{$country->type}}">
              @endif
              <option value="{{$country->id}}" >{{$country->countryName}}</option>
      @endforeach
  </select>
  @if(isset($mySource))
    <script>
        $("select#catch_country").val('{{$mySource->fk_harvestCountry_id}}');
        $("select#catch_country").selectpicker('refresh');  
    </script>
  @endif

</div>
<div class="form-group">
    <label for="catch_region">Catch Region*</label><p class="source_err" id="harvestRegion" style = "display: inline-block; margin-bottom: 5px"></p>
    <input type="text" class="form-control" id = "harvestRegion" name="harvestRegion" 
    @if(isset($mySource))
    value = "{{$mySource->harvestRegion or ''}}" 
    @endif
     autocomplete="off">
    
</div>
 
<div class="form-group">
    <label for="cool">Country of Origin Labeling (COOL)* </label><p class="source_err" id="fk_coolCountry_id" style = "display: inline-block; margin-bottom: 5px"></p>
    <select class="form-control dropdown selectpicker" name="fk_coolCountry_id" id="fk_coolCountry_id" data-dropup-auto = false data-size= 10 data-live-search="true" autocomplete="off">
    <option value>--select COOL Country--</option>
      @foreach($countries as $country)
        @if ($country->sortOrder == 1)
          <optgroup label = "{{$country->type}}">
        @endif
        <option value="{{$country->id}}" >{{$country->countryName}}</option>
      @endforeach
    @if(isset($mySource))
    <script>
        $("select#fk_coolCountry_id").val('{{$mySource->fk_coolCountry_id}}');
        $("select#fk_coolCountry_id").selectpicker('refresh');  
    </script>
    @endif
     
  </select> 
</div>
