<div class = "row">
<div class = "col-sm-3"></div>
<div class = "col-sm-6" style = "display: table-cell; text-align: center; font-size:18px">You have completed assigning sources for this product.  Move on to make certification claims or make changes to the information above.</div>
<div class = "col-sm-3"></div>
</div>
<div class = "row" style = "margin-top: 20px">
<div class = "col-sm-3"></div>
<div class = "col-sm-6" style = "display: table-cell; text-align: center">
		<button class = "button btn-primary btn-lg OnToCerts"  url = "/surveyProduct/{{$sp->id}}/completeSources" tag = "#product_data_entry">To Certifications<i class="fa fa-arrow-right" aria-hidden="true" style = "margin-left: 8px"></i></button>
</div>

<div class = "col-sm-3"></div>
</div>
<script>
$('button.OnToCerts').off("click");
$('button.OnToCerts').click(getAJAX);
</script>