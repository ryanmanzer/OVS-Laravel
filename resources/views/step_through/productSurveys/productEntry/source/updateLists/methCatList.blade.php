<script>
    $("#gear_type").selectpicker('refresh');
</script>

<label for="method_cat">Harvest Method Category* </label>
@if(isset($specMeth))
	<input type="text" class="form-control" value="{{$specMeth->category->methodCategory}}" id = "method_category_text" disabled="disabled">
 	<input type="text" class="form-control" id = "method_category_id" name="method_category_id" value = "{{$specMeth->fk_methodCategory_id}}" style = "display:none">
 @else
	<input type="text" class="form-control" value="--auto-fills up from Specific Method--" id = "method_category_text" disabled="disabled">
 	<input type="text" class="form-control" id = "method_category_id" name="method_category_id" value style = "display:none">
@endif
</script>



{{-- 

@if(isset($methCats))
        <option value selected="selected">--select Method Category--</option>
    @foreach($methCats as $cat)
        <option value="{{$cat->id}}" >
          {{$cat->methodCategory}}
        </option>
    @endforeach
@else
	<option selected="selected" disabled="disabled">--no Wild Farmed selected yet--</option>
@endif

<script>
@if(isset($specMeth))
	$("select#methodCat").val('{{$specMeth->fk_methodCategory_id}}');
@endif
	$("select#methodCat").selectpicker('refresh');
</script>

--}}