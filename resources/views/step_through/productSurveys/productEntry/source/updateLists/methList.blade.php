@if(isset($specMeths))
        <option value selected="selected">--select Specific Method--</option>
    @foreach($specMeths as $meth)
    	@if($meth->order == 1)
    		<optgroup label="{{$meth->category->methodCategory}}">
    	@endif
        <option value="{{$meth->id}}" >{{$meth->harvestMethod}}</option>
    @endforeach
<script>
	$("select#gear_type").removeAttr('disabled');
	$("select#gear_type").selectpicker('refresh');
</script>
@else
<option value selected="selected" disabled="disabled">--no Wild or Farmed selected yet--</option>
<script>
   $("select#gear_type").attr('disabled', 'true');
   $("select#gear_type").selectpicker('refresh');
</script>
@endif
