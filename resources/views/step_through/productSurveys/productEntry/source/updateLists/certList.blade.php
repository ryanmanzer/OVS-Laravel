@if(isset($certTypes))
  <option selected="selected" value>--select {{$certTypes[0]->wild_farmed}} Certification--</option>
  <optgroup label = "{{$certTypes[0]->wild_farmed}} Certifications"</optgroup>
  @foreach ($certTypes as $cert)
    <option value="{{$cert->id}}">
      {{$cert->certType}}
    </option>
  @endforeach
<script>
	$("select#certType").removeAttr('disabled');
	$("select#certType").selectpicker('refresh');
</script>
@else
  <option selected="selected" disabled="disabled" value>--no Wild Farmed selected yet--</option>
<script>
	$("select#certType").attr('disabled',true);
	$("select#certType").selectpicker('refresh');
</script>
@endif
