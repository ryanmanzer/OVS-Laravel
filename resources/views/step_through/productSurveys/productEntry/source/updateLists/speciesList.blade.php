
@if(count($seafood) > 0)
    <option selected="selected" disabled="disabled">--select Seafood Common Name--</option>
    <optgroup label = "{{$sfCat->category}} Species"</optgroup>
    @foreach($seafood as $sf)
        <option value="{{$sf->id}}" >
          {{$sf->seafoodSpecies}}
        </option>
    @endforeach
<script>
	$("select#_fish").removeAttr('disabled');
	$("select#_fish").val('');
	$("select#_fish").selectpicker('refresh');
	$("select#seafood_cat").selectpicker('refresh');
	
</script>
@else
	<option selected="selected" disabled="disabled">--no Wild Farmed selected yet--</option>
<script>
	$("select#_fish").attr('disabled', 'true');
	$("select#_fish").selectpicker('refresh');
	$("select#seafood_cat").selectpicker('refresh');

</script>
@endif
