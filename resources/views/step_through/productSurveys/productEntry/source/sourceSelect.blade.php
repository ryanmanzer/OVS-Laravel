  <div class = "row">
    <div class = "col-sm-12">
    <label for="sourceSelect">
            Enter {{$sp->product->seafoodCategory->category}} source information below or choose from previously entered {{$sp->product->seafoodCategory->category}} sources
    </label>
           
                              
    </div> 
  </div>
    <div class = "row">
      <div class = "col-sm-10">   
        <select id="sourceSelect" name="Source" class= "sourceSelect form-control selectpicker" autocomplete="off" data-dropup-auto = false data-size= 10 data-live-search="true" style = "margin-bottom: 8px">
                 <option selected="selected" value> {{'-- Existing '.$sp->product->seafoodCategory->category.' Sources -- ( Supplier:  Seafood - Harvest Method - Catch Country - Catch Region - COOL - Certification Type - Certification # )'}}</option>
                  @foreach($sources as $source)
                      @if(in_array($source->id, $firstSourceList))
                        <optgroup label="{{$source->seafoodSpecies->seafoodSpecies}}">
                      @endif
                      <option
                        class="sourceToolTip"
                        data-toggle="tooltip" value="{{$source->id}}">
                        <strong>{{$source->supplierCompany or 'Company Name'}} :  </strong>
                        {{$source->seafoodSpecies->seafoodSpecies or 'no species given'}} - {{$source->harvestMethod->harvestMethod or '{no method given}'}} -
                        {{$source->catchCountry->countryName or 'no catch country given'}} - {{$source->harvestRegion or 'no catch region given'}} - {{$source->COOL->countryName or 'no COOL given'}} - {{$source->certification->certType or '{no certification given}'}} - 
                        @if($source->certificationNumber == '')
                          {no cert# given}
                        @else
                          {{$source->certificationNumber or '{no cert# given}'}}
                        @endif
                      </option>
                    
                  @endforeach
        </select>
        </div>
      <div class = "col-sm-1"><button class="btn btn-default btn-md clearSource">Clear Form</button></div>
    </div>
<script>
$("select#sourceSelect").off("change");
$("select#sourceSelect").change(fillSource);
$("select#sourceSelect").selectpicker({});
function fillSource(){
  var source_id = $("select#sourceSelect").val();
  if(source_id > 0){
  var myurl = "sp/{{$sp->id}}/source/" + source_id + "/fillSource";
  var mytag = "#sourceForm";
  loadComponent(myurl, mytag);
  } else {
    clearSource();
  }
}
function clearSource(){
  var myurl = "/source/{{$sp->id}}/clearSourceForm";
  var mytag = "#sourceForm";
  loadComponent(myurl, mytag);
  $("select#sourceSelect").val('');
}
//$("button.fillSource").off("click");
//$("button.fillSource").click(fillSource);
$("button.clearSource").off("click");
$("button.clearSource").click(clearSource);
</script>
