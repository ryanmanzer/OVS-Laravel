<div class = "row col-sm-12"><label style = "font-size: 18px"><u>Source #{{count($sp->sps) + 1}}:</u></label></div>
@if(count($sources)>0)
   @include('step_through.productSurveys.productEntry.source.sourceSelect')
@else
  <div class = "row">
    <div class = "col-sm-12">
    	<label for="sourceSelect">Enter {{$sp->product->seafoodCategory->category}} source information below</label>
    </div>
   </div>
@endif 

<div id = "sourceForm" style = "margin-top: 15px">
  @include('step_through.productSurveys.productEntry.source.sourceFormOnly')
</div>
{{-- <div style = "height: 0px"></div> --}}
