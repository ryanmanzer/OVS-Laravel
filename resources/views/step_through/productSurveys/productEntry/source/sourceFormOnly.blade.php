
 
<form id="addsource" role="form" method="GET" post-url = "/surveyProduct/{{$sp->id}}/addsource" tag = "#product_data_entry" url = "/surveyProduct/{{$sp->id}}/productDataEntry" 
@if(isset($mySource))
 data-id = "{{$mySource->id}}" 
@endif
 >
    {{ csrf_field() }}        
        <div class="row">
            <div class="col-sm-4 ">    
                @include('step_through.productSurveys.productEntry.source.col1')
            </div>
            <div class="col-sm-4 ">
                @include('step_through.productSurveys.productEntry.source.col2')
            </div>
            <div class="col-sm-4 ">
                @include('step_through.productSurveys.productEntry.source.col3')
            </div>
        </div>
        @include('step_through.productSurveys.productEntry.source.bottomSection')        
</form>
<script>
$("select#seafood_cat").off("change");
$("select#seafood_cat").change(getSF);
$("select#wf").off("change");
$("select#wf").change(getWF);
$("select#methodCat").off("change");
$("select#methodCat").change(getMethCat);
$("select#gear_type").off("change");
$("select#gear_type").change(getGearType);
$('button.enterSource').off("click");
$('button.enterSource').click(enterSource);
$('.selectpicker').selectpicker({ 
    });
</script>
