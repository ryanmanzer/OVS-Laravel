<div class="form-group">
    <label for="FAO_MFA">FAO Major Fishing Area </label>
    {{--
    <!--
    <input type="text" class="form-control" name="faoMajorFishingArea" id="faoMajorFishingArea"
    @if(isset($mySource))
        value = "{{$mySource->faoMajorFishingArea or ''}}" 
    @endif
     autocomplete="off">
     -->
     --}}
     <select class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10 name="faoMajorFishingArea" id="faoMajorFishingArea"  data-live-search="true" autocomplete="off" style = "display:none">
       <option value>--select FAO Major Fishing Area--</option>
    @foreach($faoAreas as $faoArea)
      @if($faoArea->order == 1)
        <optgroup label="{{$faoArea->type}}">
      @endif
      <option value="{{$faoArea->faoNumber}}">{{$faoArea->faoNumber.' - '.$faoArea->faoAreaName}}</option>
    @endforeach
    </select>
    <input type="text" class="form-control hideWild" value = "-- only for wild-caught --" disabled>
</div>
<div class="form-group">
    <label for="RFMO">RFMO/High Seas Name</label>
    <input type="text" class="form-control" name="highSeasName" id="highSeasName" value autocomplete="off" style = "display:none">
    <input type="text" class="form-control hideWild" value = "-- only for wild-caught --" disabled>
</div>
<div class="form-group">
    <label for="vessel_flag">Flag of Vessel</label>
    <select class="form-control dropdown selectpicker showWild" data-dropup-auto = false data-size= 10 name="fk_vesselFlag_id" id="fk_vesselFlag_id"  data-live-search="true" autocomplete="off" style = "display:none">
    <option value>--select Vessel Flag Country--</option>
      @foreach($countries as $country)
          @if ($country->sortOrder == 1)
                <optgroup label = "{{$country->type}}">
          @endif
          <option value="{{$country->id}}" >{{$country->countryName}}</option>          
      @endforeach
    </select>
    <input type="text" class="form-control hideWild" value = "-- only for wild-caught --" disabled>
</div>
<div class="form-group">
    <label for="cert_type">Supplier Certification Name</label>
    <select class="form-control dropdown selectpicker" data-dropup-auto = false data-size= 10  name="certification_types_id" id="certType" data-live-search="true" 
    @if(isset($mySource))
        >
        <option value>--select {{$mySource->wild_farmed}} Certification--</option>
        <optgroup label = "{{$mySource->wild_farmed}} Certifications"</optgroup>
        @foreach ($certTypes as $certType)
            @if($mySource->certification_types_id == $certType->id)
                <option value="{{$certType->id}}" selected="selected">{{$certType->certType}}</option>
            @else
                <option value="{{$certType->id}}">{{$certType->certType}}</option>
            @endif
        @endforeach
    @else
      disabled >
      <option selected="selected" disabled="disabled" value>--no Wild Farmed selected yet--</option>
    @endif
      
    </select>
</div>
<div class="form-group">
    <label for="cert_num">Supplier Certification Number</label>
    <input type="text" class="form-control" id="cert_num" name="certificationNumber"
    @if(isset($mySource))
        value = "{{$mySource->certificationNumber or ''}}" 
    @endif
     autocomplete="off">
</div>
<div class="form-group">
    <label for="cert_num">FIP Name</label>
    <input type="text" class="form-control" id="fip_name" name="fip_name"
    @if(isset($mySource))
        value = "{{$mySource->fip_name or ''}}" 
    @endif
     autocomplete="off">
</div>
<script>
    $("select#faoMajorFishingArea").selectpicker('hide');
    $("select#fk_vesselFlag_id").selectpicker('hide');
</script>
@if(isset($mySource))
  <script>
    $("select#faoMajorFishingArea").val('{{$mySource->faoMajorFishingArea}}');
    $("select#fk_vesselFlag_id").val('{{$mySource->fk_vesselFlag_id}}');
    $("input#highSeasName").val('{{$mySource->highSeasName}}');
    $("textarea#vendorComments").val('{{trim($mySource->vendorComments)}}');
    $("select#faoMajorFishingArea").selectpicker('refresh');
    $("select#fk_vesselFlag_id").selectpicker('refresh');
    @if($mySource->wild_farmed == "Wild")
        {{-- If source is wild then enable the three wild-only fields  --}}
        $('.hideWild').hide();
        $("select#faoMajorFishingArea").selectpicker('show');
        $("select#fk_vesselFlag_id").selectpicker('show');
        $("input#highSeasName").show();        
    @endif
  </script>
@endif