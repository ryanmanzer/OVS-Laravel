<div class="form-group">
    <label for="supplierCompany">Supplier* </label><p class="source_err" id="supplierCompany" style = "display: inline-block; margin-bottom: 5px"></p>
    <input type="text" class="form-control" id = "supplierCompany" name="supplierCompany"
    @if(isset($mySource))
    value = "{{$mySource->supplierCompany or ''}}" 
    @endif
    autocomplete="off">
    
</div>

<div class="form-group">
    <label for="contact_name">Supplier Contact Name </label>
    <input type="text" class="form-control" id="contactName" name="contactName" autocomplete="off"
    @if(isset($mySource))
    value = "{{$mySource->contactName or ''}}"
    @endif
>
</div>

<div class="form-group">
    <label for="contact_email">Supplier Contact Email </label>
    <input type="text" class="form-control" id = "contactEmail" name="contactEmail" autocomplete="off"
    @if(isset($mySource))
    value = "{{$mySource->contactEmail or ''}}"
    @endif
     >
</div>

@if($sp->product->fk_seafoodCategory_id != 9999)
  <div class="form-group">
      <label for="seafood_cat">Seafood Category </label>
      <input type="text" class="form-control" value="{{$sp->product->seafoodCategory->category}}" disabled="disabled">
      <select name="fk_seafoodCategory_id" class="form-control seafoodCategory" id="seafood_cat" autocomplete="off" style="display: none;">
      <option  selected="selected" value="{{$sp->product->fk_seafoodCategory_id}}"></option>
      </select>
  </div>
@else
  <div class="form-group">
      <label for="seafood_cat">Seafood Category </label>
      <select name="fk_seafoodCategory_id" class="form-control seafoodCategory selectpicker" id="seafood_cat" autocomplete="off" data-dropup-auto = false data-size= 10 data-live-search="true">
        <option value>--select Seafood Category--</option>
        <optgroup label = "Seafood Categories"</optgroup>
        
        @foreach($sfCats as $sfCat)
          <option value="{{$sfCat->id}}">{{$sfCat->category}}</option>
        @endforeach
        </select>
  </div>
    <script>
    $("#seafood_cat").selectpicker({});
    @if(isset($mySource))
      $("#seafood_cat").val('{{$mySource->fk_seafoodCategory_id}}');
      $("#seafood_cat").selectpicker('refresh');
    @endif
      
    </script>
@endif

  <div class="form-group" id = "sfGroup">
    <label for="_fish" >Seafood Common Name* </label><p class="source_err" id="fk_seafoodSpecies_id" style = "display: inline-block; margin-bottom: 5px"></p>  
      <select class="form-control dropdown selectpicker" autocomplete="off" name="fk_seafoodSpecies_id" data-dropup-auto = false data-size= 10 data-live-search="true" id="_fish">   
      @if(isset($mySource))
        <option value>--select Seafood Common Name--</option>
        <optgroup label = "{{$mySource->seafoodCategory->category}} Species"</optgroup>
        
        @foreach($sfs as $sf)
          @if($mySource->fk_seafoodSpecies_id == $sf->id)
            <option selected = "selected" value="{{$sf->id}}">{{$sf->seafoodSpecies}}</option>
          @else
            <option value="{{$sf->id}}">{{$sf->seafoodSpecies}}</option>
          @endif
        @endforeach
      @else
        @if($sp->product->seafoodCategory->category == "Mixed")
          
          <option value selected="selected" disabled="disabled">--no Seafood Category selected yet--</option>
        @else
          <option value>--select Seafood Common Name--</option>
          <optgroup label = "{{$sp->product->seafoodCategory->category}} Species"</optgroup>
         
          @if(isset($sfs))
            @foreach($sfs as $sf)
              <option value="{{$sf->id}}">{{$sf->seafoodSpecies}}</option>
            @endforeach
          @endif
        @endif
      @endif
    </select>     
  </div>






  <div class="form-group">
    <label for="sci_nam">Seafood Latin Name* </label><p class="source_err" id="scientificName" style = "display: inline-block; margin-bottom: 5px"></p>
    <input type="text" class="form-control" id="scientificName" name="scientificName" autocomplete="off" 
    @if(isset($mySource))
      value = "{{$mySource->scientificName or ''}}"
    @endif
    autocomplete="off">
  </div>
