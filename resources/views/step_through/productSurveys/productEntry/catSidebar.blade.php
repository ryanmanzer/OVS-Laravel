{{-- $sfCat and $openSp and $surveyProducts  --}}
<table class = "table">
<tbody>
	<thead>
		<tr style = "height: 48px">
		<th colspan="6">{{$sfCat->category}} Products</th>
		<th></th>
		<th colspan = "2"></th>
	</tr>
	</thead>
@foreach($sps as $surveyProduct)
  	<tr class = "catSideBar" id = "catSideBar_{{$surveyProduct->id}}" style = "background-color: #eeeeee; height: 48px; vertical-align: middle;">
		<td>{{$surveyProduct->product->retailerProductCode}}</td>
		<td colspan = 4 style="padding-top: 0px;    padding-bottom: 0px; vertical-align: middle;">{{$surveyProduct->product->retailerProductDescription}}</td>
		<td colspan = 2><span class="glyphicon glyphicon-ok" id = "submitted_{{$surveyProduct->id}}" 
			@if($surveyProduct->submitted != 1)
				style = "display:none"
			@endif
			></span>
		</td>
		<td>	
			<button class="btn btn-sm btn-info openSurveyProduct" url="/surveyProduct/{{$surveyProduct->id}}/productEntry" tag="#product_entry" id = "expand_sp_{{$surveyProduct->id}}" data-id="{{$surveyProduct->id}}" ><i class="fa fa-arrow-right" aria-hidden="true"></i></button>
		</td>
  	</tr>
@endforeach
</tbody>
</table>
<span class="glyphicon glyphicon-ok" style = "margin-right: 5px"></span> = Product Complete and Submitted<br>
<br>
<button class="btn btn-sm btn-info" disabled style = "opacity: 1" ><i class="fa fa-arrow-right" aria-hidden="true"></i></button> To open a specific product<br>
<script>
$('button.openSurveyProduct').off("click");
$('button.openSurveyProduct').click(getAJAX);
$('button#backButton').html("<i class=\"fa fa-arrow-left\" aria-hidden=\"true\" style = \"margin-right: 10px\"></i>Category <br> List</button>");
$('button#backButton').attr('url', '/survey/{{$survey->id}}/surveyCatList');
$('button#backButton').attr('tag', '#product_survey_data');


</script>