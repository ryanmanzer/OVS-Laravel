@if($sp->submitted != 1)

	<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">
			<div class = "col-sm-12">
				<label style = "margin-bottom: 24px; font-size: 18px">Submit the product information above as completed?</label>
			</div>
			<div class = "col-sm-3"></div>
			<div class = "col-sm-4">
				<button class = "button btn-primary btn-lg submitProduct" url = "/surveyProduct/{{$sp->id}}/submit" tag = "#product_data_entry">Yes!</button>
			</div>
		</div>
		<div class = "col-sm-3"></div>
	</div>
	
	<script>

	  	$('button.submitProduct').off("click");
		$('button.submitProduct').click(getAJAX);
	</script>
@endif

<script>
	loadComponent("/surveyProduct/{{$sp->id}}/activeStatus", "#activeStatus");
	loadComponent("surveyProduct/{{$sp->id}}/submitStatus", "#submitStatus");
	loadComponent("surveyProduct/{{$sp->id}}/updateProgressBar/submit", "#scriptHandler");
	$("td#survey_progress").html('{{100 * $sp->survey->progress}}% Complete' );
</script>