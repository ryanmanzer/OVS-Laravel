<style>
/*.sources, .certs, .repacking, .active, .submit{*/
.progressDot{
  position:absolute;
  margin-top:-5px;
  z-index:0;
  height:20px;
  width:20px;
  border-radius:25px;
  background-color:inherit;
}
.sources{
  left:0%;
}
.certs{
  left:24%;
}
.repacking{
  left:49%;
}
/*.active{
  left:74%;
}*/
.submit{
  left:98%;
}
/*.primary-color{
  background-color:#4989bd;
}
.success-color{
  background-color:#5cb85c;
}
.danger-color{
  background-color:#d9534f;
}
.warning-color{
  background-color:#f0ad4e;
}
.info-color{
  background-color:#5bc0de;
}
.no-color{
  background-color:inherit;
}
*/
.progressDot{
  background-color:inherit;  
}
.progress-label{
  position:relative; 
  left: -90px; 
  bottom: 20px; 
  width: 200px; ; 
  text-align: center;
}
</style>

<div class="row"><br/>
  <div class="col-md-12">
      <div class="progress" style = "height: 10px">
          <div class="progressDot sources no-color" id = "sourcesDot">  
             <p class = "progress-label" id = "sources_label">Sources</p>
          </div>
          <div class="progressDot certs no-color" id = "certsDot">  
            <p class = "progress-label" id = "certs_label">Certifications</p>
          </div>
          <div class="progressDot repacking no-color" id = "repackingDot">  
            <p class = "progress-label" id = "repacking_label">Repacking</p>
          </div>
          <div class="progressDot active no-color" id = "activeDot" style = "left: 74%">  
            <p class = "progress-label" id = "active_label">Active</p>
          </div>
          <div class="progressDot submit no-color" id = "submitDot">  
            <p class = "progress-label" id = "submit_label">Submit</p>
          </div>
        <div class="progress-bar progress-bar-success" id = "progressBar"></div>
      </div>
  </div>
</div>

