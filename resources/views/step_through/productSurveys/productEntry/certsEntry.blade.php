<div class = "row">
	<div id = "certs_data_entry">
  		@include('step_through.productSurveys.productEntry.certs.addCertsQuestion')
	</div>
</div>
<script>
	loadComponent('/surveyProduct/{{$sp->id}}/certsList', '#certsList');
	$("div#repackingStatus").hide();
	$("div#activeStatus").hide();
	$("div#submitStatus").hide();
	loadComponent("surveyProduct/{{$sp->id}}/updateProgressBar/certs", "#scriptHandler");
	//loadComponent('/surveyProduct/{{$sp->id}}/certsList', '#certs_data_entry');
</script>

