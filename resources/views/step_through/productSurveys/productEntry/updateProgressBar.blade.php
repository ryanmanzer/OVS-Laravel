<script>

  $('.progressDot').css('border','none');
  $('p.progress-label').css('font-weight','normal');
  $('p.progress-label').css('font-size','14px');
  @if(isset($section))
    $("#{{$section}}Dot").css('border','2px solid black');
    $("#{{$section}}_label").css('font-weight','bold');
    $("#{{$section}}_label").css('font-size','16px');
  @endif

  @foreach($progressDots as $key => $value)
    @if($value == 0)
      $("#{{$key}}Dot").css('background-color','inherit');
    @elseif($value == 1)
      $("#{{$key}}Dot").css('background-color','#5cb85c');
    @elseif($value == 2)
      $("#{{$key}}Dot").css('background-color','#f0ad4e');
    @endif
  @endforeach

  $("#progressBar").css('width', '{{$sp->surveyProgress}}%')

@if($sp->sourceProgress == 1)
  $("div#sourcesList").show();
  $('span.editSources').show();
  $('button.removeSps').hide();
  $('span.editPercent').hide();
@elseif($sp->sourceProgress == 2)
  $("div#sourcesList").show();
  $('span.editSources').hide();
  $('button.removeSps').show();
  $('span.editPercent').show();
@endif

@if($sp->certProgress == 0)
  $("div#certsList").hide();
  $('span.editCerts').hide();
@elseif($sp->certProgress == 1)
  $("div#certsList").show();
  $('span.editCerts').show();
  $('button.removeCert').hide();  
@elseif($sp->certProgress == 2)
  $("div#certsList").show();
  $('span.editCerts').hide();
  $('button.removeCert').show();
@endif

@if($sp->repackingProgress == 0)
  $("div#repackingStatus").hide();
@else
  $("div#repackingStatus").show();
@endif

@if($sp->activeProgress == 0)
  $("div#activeStatus").hide();
@else
  $("div#activeStatus").show();
@endif

  @if($sp->submitted == 1)
    $('span.editSection').hide();
    $("div#submitStatus").show();
    $('span#submitted_{{$sp->id}}').show();
  @elseif($sp->submitted == 2)
    $('span.editSection').show();
    $("div#submitStatus").hide();
    $('span#submitted_{{$sp->id}}').hide();
  @endif
@if($sp->survey->complete == 1)
    $('span.editSubmit').hide();
@endif
</script>



