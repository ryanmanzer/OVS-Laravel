@if($sp->certProgress != 1)

	<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">

			@if(count($sp->certification) == 0)
				<div class = "col-sm-12"><label style = "margin-bottom: 24px; font-size: 18px">Do you have any vendor certification claims that apply to this product (e.g. MSC Chain of Custody, BAP Repacker)?</label></div>
			@else
				<div class = "col-sm-12"><label style = "margin-bottom: 24px; font-size: 18px">Do you have any additional vendor certification claims that apply to this product (e.g. MSC Chain of Custody, BAP Repacker)?</label></div>
			@endif
			<div class = "col-sm-3"></div>

			<div class = "col-sm-2">
				<button class = "button btn-primary btn-lg onToRepacking" url = "/surveyProduct/{{$sp->id}}/newCertEntry" tag = "#product_data_entry">Yes</button>
			</div>			
			<div class = "col-sm-2">
				<button class = "button btn-primary btn-lg onToRepacking" url = "/surveyProduct/{{$sp->id}}/doneWithCerts" tag = "#product_data_entry">No</button>
			</div>

		</div>
		<div class = "col-sm-3"></div>
	</div>
	
	<script>
	  	$('button.enterCerts').off("click");
		$('button.enterCerts').click(getAJAX);
	  	$('button.onToRepacking').off("click");
		$('button.onToRepacking').click(getAJAX);
	</script>

@else

	
	<div class = "row" style = "margin-top: 20px">
		<div class = "col-sm-4"></div>
		<div class = "col-sm-4">		
			<button class = "button btn-primary btn-lg OnToRepacking" url = "/surveyProduct/{{$sp->id}}/productEntry/section/repacking" tag = "#product_entry">To Repacking<i class="fa fa-arrow-right" aria-hidden="true" style = "margin-left: 8px"></i></button>
		</div>
		<div class = "col-sm-4"></div>
	</div>
	<script>
		$('button.OnToRepacking').off("click");
		$('button.OnToRepacking').click(getAJAX);
	</script>

@endif

