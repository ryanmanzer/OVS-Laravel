  <div class="row" style="margin-top: 15px">
    <div class="col-sm-2"></div>
    <div class="col-sm-6">
        <form class="form form-default addCert" post-url= "/surveyProduct/{{$sp->id}}/AddNewCert" url = "/surveyProduct/{{$sp->id}}/certsEntry" tag = "#product_data_entry" id = "enterCert">
       
        {{csrf_field()}}
        <div class='form-group'>
          <label for='#cert_type'>Vendor Certification Name </label><p class="cert_err" id="certification_type_id" style = "display: inline-block; margin-bottom: 5px; color: #a94442"></p>
          <select class = "form-control selectpicker" name="certification_type_id" id="certification_type_id" data-live-search="true"  data-dropup-auto = false data-size= 10 >
           <option value>--Select Certification Name--</option>         
              @foreach ($cert_types as $cert_type)
                @if($cert_type->order == 1)
                <optgroup label = "{{$cert_type->wild_farmed}}">
                @endif
              <option value="{{$cert_type->id}}">{{$cert_type->certType}}</option>
              @endforeach
          </select>
          @if(isset($myCert))
          <script>
            $("select#certification_type_id").val('{{$myCert->certification_type_id}}');
            $("select#certification_type_id").selectpicker('refresh');
          </script>
          @endif
        </div>
        
        <div class='form-group'>
          <label for='#cert_num'>Vendor Certification Number </label><p class="cert_err" id="certification_number" style = "display: inline-block; margin-bottom: 5px; color: #a94442"></p>
          <input type="text" class='form-control' name="certification_number" id="certification_number"
          @if(isset($myCert))
           value="{{$myCert->certification_number}}"
          @else
           value=""
          @endif
           />
        </div>
        <div class='form-group'>
          <label for='desc'>Vendor Certification Description (e.g. species, scope, etc) </label><p class="cert_err" id="description" style = "display: inline-block; margin-bottom: 5px; color: #a94442"></p>
          <input type="text" class='form-control' name="description" id="description" 
          @if(isset($myCert))
           value="{{$myCert->description}}"
          @else
           value=""
          @endif />
        </div>
      </form>
      <div class="row">
          <div class = "col-sm-3">  <button class="btn btn-primary enterCert" type = "submit"><i class="fa fa-plus-square" aria-hidden="true"></i> Add Cert</button></div>
          <div class = "col-sm-3"><button class="btn btn-info btn-md cancelCert">Cancel</button></div>
      </div>
    </div>
  </div>
<script>
function enterCertFormDataInPage(){
  $('p.cert_err').html('');
  var errorCheck = 0;
  if($("select#certification_type_id").val().length == 0){
    $("p#certification_type_id").text(' *Required Field*');
    
  }
  $("input#certification_number").val($("input#certification_number").val().trim());
  if($("input#certification_number").val().length == 0){
    $("p#certification_number").text('*Required Field*');
    var errorCheck = 1;
  }
  $("input#description").val($("input#description").val().trim());
  if($("input#description").val().length == 0){
    $("p#description").text(' *Required Field*');
    var errorCheck = 1;
  }
  if (errorCheck == 1){
    //alert("passing data " + post_data );
    return;
  }
  var post_url = $("form#enterCert").attr('post-url');
  var post_data = JSON.stringify($("form#enterCert").serializeArray());
  var post_data = $("form#enterCert").serialize();
  var get_url = $("form#enterCert").attr('url');
  var get_tag = $("form#enterCert").attr('tag');
  $.ajax({
    type: "POST",
    url: post_url,
    data: post_data,
    dataType: 'json',
    success: function(){
        loadComponent(get_url, get_tag);      
      }
    });
  }

  $('.selectpicker').selectpicker({});   
  $('button.enterCert').off("click");
  $('button.enterCert').click(enterCertFormDataInPage);
$("button.cancelCert").off("click");
$("button.cancelCert").click(cancelCert);
</script>
