@if(count($certs) > 0)

	<div class = "row">
  <div class = "col-sm-12"><label style = "font-size: 18px">Vendor certification claims for this product:</label><a href="#"><span class="glyphicon glyphicon-pencil editCerts editSection" url = "/surveyProduct/{{$sp->id}}/reOpenCerts" tag = "#product_data_entry" style = "margin-left: 10px"></span></a>
</div>
<table class="table table-striped table">
        <thead>
            <tr>
                <th class = "col-sm-2"></th>
                <th class = "col-sm-3">Vendor Certification Type</th>
                <th class = "col-sm-3">Vendor Certification Number</th>
                <th class = "col-sm-3">Description</th>
                <th class = "col-sm-2"></th>
            </tr>
      </thead>
      <tbody>
    
  
      @foreach($certs as $key=>$cert)
              <tr>
                  <td>Cert #{{$key + 1}}</td>
                  <td>{{$cert->certification->certType->certType or ''}}</td>
                  <td>{{$cert->certification->certification_number or ''}}</td>
                  <td>{{$cert->certification->description or ''}}</td>
                  <td>
                  <button class ="btn btn-danger btn-sm removeCert" {{-- post-url = "/spCert/{{$cert->id}}/removeCert" --}} url = "/spCert/{{$cert->id}}/removeCert" tag = "#product_data_entry" style = "display:none">Remove
                  <i class="fa fa-trash" aria-hidden="true" style = "margin-left: 4px"></i></button>            
                  </td>
              </tr>
      @endforeach
    </tbody>
</table>
<script>
$('button.removeCert').off("click");
$('button.removeCert').click(getAJAX);
</script>
@else
	@if($sp->certProgress == 1)
		
    <div class = "row">
  			<div class = "col-sm-12"><label style = "font-size: 18px">Vendor certification claims for this product: <b>None</b></label><a href="#"><span class="glyphicon glyphicon-pencil editCerts editSection" url = "/surveyProduct/{{$sp->id}}/reOpenCerts" tag = "#product_data_entry" style = "margin-left: 10px"></span></a>
		</div>
	@endif
@endif
<script>
$('span.editCerts').off("click");
$('span.editCerts').click(getAJAX);
</script>
@if($sp->certProgress == 2)
<script>
  $('span.editCerts').hide();
  $('button.removeCert').show();
</script>
@else
<script>
  $('span.editCerts').show();
  $('button.removeCert').hide();
</script>
@endif

