@if(count($certs)>0)
 	<div class = "row">
    	
      <div class = "col-sm-2"></div>
    	<div class = "col-sm-9">
      	<label for="sourceSelect">Enter vendor certification information below or auto-fill from previously entered certifications</label>                              
    	</div> 
  	</div>
    <div class = "row">
      	<div class = "col-sm-2"></div>
        <div class = "col-sm-5">   
       		<select id="certSelect" name="Cert" class= "certSelect form-control selectpicker" style = "margin-bottom: 8px">
                 <option selected="selected"  value>--Existing Certifications (Certification Type - Certification # - Description - )--</option>
                 <option disabled="disabled">─────────────────────────────────────</option>
                  @foreach($certs as $cert)
                      <option value="{{$cert->id}}">{{$cert->certType->certType}} - {{$cert->certification_number}} - {{$cert->description}}</option>  
                  @endforeach
        	</select>
        </div>
      	
      	<div class = "col-sm-1"><button class="btn btn-default btn-md clearCert">Clear Form</button></div>
        
    </div>

<script>
  function fillCert(){
  var cert_id = $("select#certSelect").val();
  if(cert_id > 0){
  var myurl = "surveyProduct/{{$sp->id}}/oldCert/" + cert_id + "/fillForm";
  var mytag = "#certForm";
  loadComponent(myurl, mytag);
  } else {
    clearCert();
  }
}
function clearCert(){
  loadComponent('/surveyProduct/{{$sp->id}}/newCertForm', "#certForm");
}
function cancelCert(){
  loadComponent("/surveyProduct/{{$sp->id}}/addCertsQuestion", "#product_data_entry");
}

$("select#certSelect").off("change");
$("select#certSelect").change(fillCert);
$("select#certSelect").selectpicker({});


$("button.clearCert").off("click");
$("button.clearCert").click(clearCert);
$("button.cancelCert").off("click");
$("button.cancelCert").click(cancelCert);
</script>

@else
	<div class = "row">
    	<div class = "col-sm-2"></div>
      <div class = "col-sm-4">
    		<label for="sourceSelect">Enter vendor certification information below</label>
      </div>
      <div class = "col-sm-1"><button class="btn btn-default btn-md clearCert">Clear Form</button></div>
      
    	 
  </div>
@endif
<script>
  function fillCert(){
  var cert_id = $("select#certSelect").val();
  if(cert_id > 0){
  var myurl = "surveyProduct/{{$sp->id}}/oldCert/" + cert_id + "/fillForm";
  var mytag = "#certForm";
  loadComponent(myurl, mytag);
  }
}
function clearCert(){
  loadComponent('/surveyProduct/{{$sp->id}}/newCertForm', "#certForm");
}
function cancelCert(){
  loadComponent("/surveyProduct/{{$sp->id}}/addCertsQuestion", "#product_data_entry");
}

$("button.fillCert").off("click");
$("button.fillCert").click(fillCert);
$("button.clearCert").off("click");
$("button.clearCert").click(clearCert);

</script>
<div id = "certForm">
@include('step_through.productSurveys.productEntry.certs.certEntryForm')
</div>

