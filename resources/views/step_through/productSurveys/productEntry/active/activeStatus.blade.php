	<div class = "row">
		<div class = "col-sm-12">
	@if($sp->active == 1)
		<label style = "font-size: 18px">Product has been discontinued (no longer sold to {{$sp->survey->retailer->name}}): <b>Yes</b></label>
	@elseif($sp->active == 0)
		<label style = "font-size: 18px">Product has been discontinued (no longer sold to {{$sp->survey->retailer->name}}): <b>No</b></label>
	@endif	
		<a href="#"><span class="glyphicon glyphicon-pencil editActive editSection" url = "/surveyProduct/{{$sp->id}}/reOpenActive" tag = "#product_data_entry" style = "margin-left: 10px"></span></a>
		</div>
	</div>

	<script>
		$('span.editActive').off("click");
		$('span.editActive').click(getAJAX);
	</script>
	@if(!is_null($sp->active))
		<script>
			$('span.editActive').show();
		</script>
	@endif