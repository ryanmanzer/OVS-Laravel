	
	<!--
	<div class = "row" id = "survey_summary">
	
	<div class = "col-sm-2"></div>
	<div class = "col-sm-8" style = "text-align:center"><p style = "font-size: 30px;">Product Survey Information</p>
	
	</div>
	<div class = "col-sm-2"></div>
	</div>
	-->
    
	<div class = "row" id = "survey_summary_data">
	<div class = "col-sm-1"><p style = "font-size: 20px">Survey Info</p></div>
	<div class = "col-sm-10">
	<table class = "table survey-list">
	<tbody>
	<thead><th class = "col-sm-5">Survey Title</th><th class = "col-sm-2">Due Date</th><th class = "col-sm-2">Progress</th><th class = "col-sm-3">Products On Survey</th></thead>
	<tr>
       <td >{{$survey->title}}</td>
       <td >{{$survey->dateDue}}</td>
       <td id = "survey_progress">{{100 * $survey->progress}}% Complete</td>
       <td >{{count($survey->sp)}}</td>
	</tr>
	</tbody>
	</table>
    </div>
    <div class = "col-sm-1">
    	<button class="btn btn-md btn-info backOneStep" data-tutorial = "main" url="" id = "backButton" tag="" style = "display: inline-block; float:right"></button>
    </div>
    </div>
<script>
$('button#backButton').off("click");
$('button#backButton').click(getAJAX);
$('button#backButton').html("<i class=\"fa fa-arrow-left\" aria-hidden=\"true\" style = \"margin-right: 10px\"></i>Survey<br> List</button>");
$('button#backButton').attr('url', '/vendor/{{$survey->fk_vendor_id}}/surveysHome');
$('button#backButton').attr('tag', '#general_data');
</script>