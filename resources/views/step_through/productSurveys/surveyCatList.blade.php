@if($survey->complete==1)

<div class="row">
<div class = "col-sm-3"></div>
<div class = "col-sm-6">
	<h3>This survey has been completed and submitted.  To view the information you've submitted click on a category below.</h3>  
</div>
<div class = "col-sm-3"></div>	
</div>
<div class="row">


@elseif($survey->progress == 1)

<div class="row">
<div class = "col-sm-3"></div>
<div class = "col-sm-6">
	<h3>All products have been completed.  Would you like to submit this information to FishWise as completed?</h3>  
</div>
<div class = "col-sm-3"></div>	
</div>
<div class="row">
<div class = "col-sm-5"></div>
			<div class = "col-sm-2">
				<button class = "button btn-primary btn-lg submitSurvey" url="/survey/{{$survey->id}}/complete" tag="#product_survey_data" style = "width: 100%">Submit Survey!</button>
			</div>
<div class = "col-sm-5"></div>
</div>
@endif

<div class = "col-sm-3"></div>
<div class = "col-sm-6">
	<h2>Please select a seafood category to begin your survey entry.</h2>  
	<table class = "table  cat-list col-sm-6 col-offset-sm-3"><tbody>
	<thead>
	<th class = "col-sm-6">Seafood Category</th><th class = "col-sm-2"># of Products</th><th class = "col-sm-2">Progress</th><th class = "col-sm-2"></th>
	</thead>
	@foreach($surveyCats as $surveyCat)
				@if($survey->complete==1)
				<tr style = "background-color: #d9edf7;"><td>{{$surveyCat[1]}}</td><td>{{$surveyCat[2]}}</td><td>{{$surveyCat[3]}} %</td><td><button class="btn btn-success btn-lg btn-block openSurveyCat" url = "survey/{{$survey->id}}/sfCat/{{$surveyCat[0]}}" tag = "#product_survey_data">View</button></td></tr>
				@else
				<tr style = "background-color: #d9edf7;"><td>{{$surveyCat[1]}}</td><td>{{$surveyCat[2]}}</td><td>{{$surveyCat[3]}} %</td><td><button class="btn btn-success btn-lg btn-block openSurveyCat" url = "survey/{{$survey->id}}/sfCat/{{$surveyCat[0]}}" tag = "#product_survey_data">Go!</button></td></tr>
				@endif
	@endforeach
	</tbody></table>
</div>
<div class = "col-sm-3"></div>
<script>
$('button.openSurveyCat').off("click")
$('button.openSurveyCat').click(getAJAX)
$('button.submitSurvey').off("click")
$('button.submitSurvey').click(getAJAX)
$('button#backButton').html("<i class=\"fa fa-arrow-left\" data-tutorial = \"main\" aria-hidden=\"true\" style = \"margin-right: 10px\"></i>Survey<br>List</button>");
$('button#backButton').attr('url', '/vendor/{{$survey->fk_vendor_id}}/surveysHome');
$('button#backButton').attr('tag', '#general_data');
</script>