{{-- will need an opensp var --}}
<div class = "col-sm-3" id = "cat_product_list">
	@include('step_through.productSurveys.productEntry.catSidebar')
</div>
<div class = "col-sm-9" id = "product_entry">

    </div>
</div>
@if(isset($openSp))
<script>
	loadComponent( "/surveyProduct/{{$openSp->id}}/productEntry" ,"#product_entry");
</script>
@else
<script>
	loadComponent( "/survey/{{$survey->id}}/sfCat/{{$sfCat->id}}/completed" ,"#product_entry");
</script>
@endif