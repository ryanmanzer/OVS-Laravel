<div class = "row">
		<div class = "col-sm-3"></div>
		<div class = "col-sm-6">
			<div class = "col-sm-12">
				<label style = "margin-bottom: 24px; font-size: 18px">You have completed reporting all {{$sfCat->category}} products.  What would you like to do next?</label>
			</div>
			<div class = "col-sm-3"></div>
			@if(isset($nextCat))

			<div class = "col-sm-3">
				<button class = "button btn-primary btn-lg answerCompleted" url="survey/{{$survey->id}}/sfCat/{{$nextCat->id}}" tag="#product_survey_data">Next Category: {{$nextCat->category}}</button>
			</div>
			@endif
			<div class = "col-sm-3">
				<button class = "button btn-primary btn-lg answerCompleted" url="/survey/{{$survey->id}}/surveyCatList" tag="#product_survey_data">Back To Category List</button>
			</div>		
		</div>
		<div class = "col-sm-3"></div>
</div>
	
	<script>
	  	$('button.answerCompleted').off("click");
		$('button.answerCompleted').click(getAJAX);
	</script>
