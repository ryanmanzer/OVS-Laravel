<style type="text/css">
  table.survey-list td { 
     font-size: 18px;
     vertical-align: middle !important;
  }
</style>
<div class = "page-header" style = "margin:0 auto">
  <div class="row">
    <div class = "col-sm-11">      <h1>Which survey would you like to complete today? </h1> </div>
    <div class = "col-sm-1">   <button class="btn btn-md btn-info backToSplash" url="/{{$vendor->id}}/splash" tag="#general_data" data-tutorial="main" style = "vertical-align: middle;"><i class="fa fa-arrow-left" aria-hidden="true"></i>&nbsp;&nbsp;Retailer<br>List</button>     </div>
  </div>
</div>
{{-- List the Product Surveys First --}}
<div class = "header col-sm-10 col-offset-sm-1"><h4>Product Surveys</h4></div>
@if(isset($surveys))
  	{{-- Check for surveys, if found make a list, if not, then just text that states there are no surveys --}}
    <table class = "table survey-list"><tbody>
  	@if(count($surveys) > 0)
		  <thead><th class = "col-sm-4">Survey Title</th><th class = "col-sm-2">Due Date</th><th class = "col-sm-2">Progress</th><th class = "col-sm-2">Last Opened</th><th class = "col-sm-2"></th></thead>
		  @foreach($surveys as $survey)
			 <tr>
      @if($survey->complete == 1)
       <td><span class="glyphicon glyphicon-ok-sign" style="color:green; font-size: 125%; margin-right: 10px"></span>{{$survey->title}}</td>
      @else
       <td>{{$survey->title}}</td>
      @endif
       <td>{{$survey->dateDue}}</td>
       @if($survey->complete == 1)
       <td >Submitted</td>
       <td>{{$survey->lastOpened}}</td>
       <td ><button class="btn btn-success btn-lg btn-block openSurvey" url = "/survey/{{$survey->id}}/productSurveyHome" tag = "#general_data" data-tutorial="survey">View Survey!</button></td>
       @else
       <td >{{100 * $survey->progress}}% Complete</td>
       <td>{{$survey->lastOpened}}</td>
       <td ><button class="btn btn-success btn-lg btn-block openSurvey" url = "/survey/{{$survey->id}}/productSurveyHome" tag = "#general_data" data-tutorial="survey">Start Survey!</button></td>
       @endif
       </tr>
  		@endforeach 
  	@else
      <tr><td colspan = 5>There are currently no pending product surveys for {{$vendor->name}}</td></tr>
  	@endif
  	</tbody></table>
@else
	SURVEYS VARIABLE NOT SET
@endif
<hr>
<div class = "header col-sm-10 col-offset-sm-1"><h4>Traceability / Human Rights Questionnaires</h4></div>
@if(isset($thr_surveys))
    {{-- Check for surveys, if found make a list, if not, then just text that states there are no surveys --}}
    <table class = "table survey-list""><tbody>
    @if(count($thr_surveys) > 0)
      <thead><th class = "col-sm-4">Survey Title</th><th class = "col-sm-2">Due Date</th><th class = "col-sm-2">Status</th><th class = "col-sm-2">Last Saved</th><th class = "col-sm-2"></th></thead>
      @foreach($thr_surveys as $thr_survey)
       <tr>
       @if($thr_survey->complete == 1)
       <td><span class="glyphicon glyphicon-ok-sign" style="color:green; font-size: 125%; margin-right: 10px"></span>{{$thr_survey->title}}</td>
      @else
       <td>{{$thr_survey->title}}</td>
      @endif
       
       <td>{{$thr_survey->survey_date}}</td>
       @if($thr_survey->complete == 1)
       <td>Complete!</td>
       <td>{{$thr_survey->last_saved}}</td>
       <td><button class="btn btn-success btn-lg btn-block openSurvey" url = '/thrSurvey/{{$thr_survey->id}}/thrSurveyHome' tag = "#general_data" data-tutorial="questionnaire">View My Answers</button></td>
       @else
       <td>Incomplete</td>
       <td>{{$thr_survey->last_saved}}</td>
       <td><button class="btn btn-success btn-lg btn-block openSurvey" url = '/thrSurvey/{{$thr_survey->id}}/thrSurveyHome' tag = "#general_data" data-tutorial="questionnaire">Start Questionnaire!</button></td>
      @endif 
       </tr>
      @endforeach 
    @else
      <tr><td colspan = 5>There are currently no pending Traceabilty or Human Rights questionnaires for {{$vendor->name}}</td></tr>
    @endif
    </tbody></table>
@else
  TRACE HR SURVEYS VARIABLE NOT SET
@endif


<script>
$('button.openSurvey').off("click")
$('button.openSurvey').click(getAJAX)
$('button.backToSplash').off("click")
$('button.backToSplash').click(getAJAX)
</script>