// resources/assets/js/vendor/vendor.js

function vendorInit()
{
  $("select#seafood_cat").change(getSF);
  $("select#wf").change(getWF);
  $("select#methodCat").change(getMeth);
  $("#delYes").click(deleteSource);
  $("body").on("click",".delete",function()
  {
      var source_id = this.id;
      $("#delYes").val(source_id);
  });
  assignGET();
}
