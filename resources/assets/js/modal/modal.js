/**
 * THIS SCRIPT HOUSES FUNCTIONS THAT ASSIST IN THE DYNAMIC BUILDING
 * AND MANIPULATION OF MODAL DIALOG WINDOWS
 */

// Add basic modal dialog blade to <div id="modal"> element on each page
function getModal()
{
    $.get('/modal/get', function(data)
          {
            $(".modal").html(data);
          });
}
// Populate the basic modal dialog blade with
function loadModal(url,id)
{
    $.get(url, function(data)
        {
            $(id).html(data);
            $(id).modal("toggle");
        });
}
// At preset (8/23/2016) the above functions require too much extra effort and I
// need to solve problems now.  Therefore we will do a little bit of code requested
//Below funciton loads the Delete? Yes/No modal.
function loadDelModal()
{
    $.get('/modal/delete', function(data){
      $("#modal").html(data);
    });
}
