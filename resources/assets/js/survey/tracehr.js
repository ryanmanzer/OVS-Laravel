// Javascript functions to help handle Traceabiliyt/Human Rights surveys

function assignFollowUp(tag)
{
  $(tag).change(showFollowUp);
}
function showFollowUp(e)
{
  var tag= $(this).attr('tag');
  if($(this).is(':checked'))
  {
    $(tag).show();
  }
  else {
    $(tag).find(':input').each(function(){$(this).prop("checked",false)});
    $(tag).hide();
  }
}
function assignTraceHrSubmit(tag)
{
  $(tag).on('submit',TraceHrForm);
}
function TraceHrForm(e)
{
  e.preventDefault();
  var post_url=$(this).attr('url');
  var get_url=$(this).data('url');
  var tag=$(this).attr('tag');
  var checked = $("input[name*='sq']:checked");
  var addinfo=$("input[name='additional_info']");
  var post_data='{"survey_response": [';
  checked.each(function(){
    post_data += '{"sq_id":"' + this.id + '", "response": "' + this.value + '"},';
  });
  addinfo.each(function(){
    if(this.value.length>0)
    {
      post_data += '{"sq_id":"' + this.id + '", "additional_info": "' + this.value + '"},';
    }
  });
  var ind = post_data.length -1;
  post_data = post_data.substring(0,ind);
  post_data += ']}';
  $.ajax({
    type: "post",
    url: post_url,
    data: post_data
  }).done(function(response){
    if(response=="1")
    {
      loadComponent(post_url,"#survey_summary_data");
      
    }
    else {
      alert('Trace Survey Did not save');
    }
  });
}
function hideFollowUp(sel)
{
  $(sel).click(function(){
    var tag=$(this).attr('tag')
    if($(this).is(':checked'))
    {
      $(tag).find(':input').each(function(){$(this).prop("checked",false)});
      $(tag).hide();
    }
  })

}
