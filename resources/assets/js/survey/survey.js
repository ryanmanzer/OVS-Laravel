

/**
* FUNCTION FOR REVEALING AND LOADING OR HIDING THE FORM FOR ASSIGNING PREDEFINED
* SOURCES TO SURVEY PRODUCTS.
*/

//ASSIGNING THE ACTION THAT WILL RE-ASSIGN THE FUNCTION OF THE DELETE MODAL WINDOW
function assignDelete()
{
  $("div.myoutput").off("click",".delSource",delSourceAction);
  $("div.myoutput").on("click",".delSource",delSourceAction);
}
function inlineAJAX ()
{
  $("body").off('click','.inlineAJAX',expandForm);
  $("body").on('click','.inlineAJAX',expandForm);
}
function delYes(functionName)
{
  switch (functionName) {
    case 'delSPS':
      $('#delSource #delTitle').html('Remove Source');
      $('#delSource #delMessage').html('Are you sure you want to remove this source from this product?')
      $("div.modal-body").off('click','#delYes',deleteSource);
      $("div.modal-body").off('click','#delYes',delSPS);
      $("div.modal-body").on("click","#delYes",delSPS);
      break;
    case 'delSource':
      $('#delSource #delTitle').html('Remove Source');
      //Grab the appropriate message if the source is linked to survey products
      var url = 'source/' + id + '/sourceDeleteConfirm';
      loadComponent(url, '#delSource #delMessage');
      $("div.modal-body").off('click','#delYes',delSPS);
      $("div.modal-body").off('click','#delYes',deleteSource);
      $("div.modal-body").on('click','#delYes',deleteSource);
      break;
    default:

  }
}
function b100()
{
  $("div.myoutput").on('click',".100",function(e){
    e.preventDefault();
    var mytag="#"+$(this).attr('val');
    var remPerc = $("#remainingPercent").val();
    $(mytag).val(remPerc);
  });
}
// FUNCTION FOR DELETING AN SPS RECORD AND THEREBY REMOVING A SOURCE FROM A
// SURVEYED PRODUCT
function delSPS()
{
  //console.log('delYes action triggered');
  checkServer();
  var sps_id=$(this).val();
  deleteSPS(sps_id)
  
}
/**
* ASSIGNING THE FUNCTION THAT WILL BE CALLED WHEN THE YES BUTTON IS CLICKED ON
* THE DELETE MODAL WINDOW FOR THE SURVEY PRODUCTS TABLE
*/
function delSourceAction()
{
  var source_id = this.id;
  //console.log('ID passed to DelYes button: ' + source_id);
  $("#delYes").val(source_id);
  $("#delYes").attr('url',$(this).attr('url'));
  $("#delYes").attr('tag',$(this).attr('tag'));
  delYes('delSPS',source_id);
}

//FUNCTION FOR EXPANDING HIDDEN TABLE ROWS TO EXPAND FORMS INLINE
// NO LONGER USED.  Replaced by Functions that are called specifically for each button for source or product expand and then point to expandFormRow(), whcih only handles the actual loadcomponent step. 
function expandForm()
{
  //console.log('Exapnd form fired');
  var tag=$(this).attr('tag');
  var url=$(this).attr('url');
  //If its a surveyproduct-expand, then close the existing survey products
  /*if (url.includes("surveyProduct")) {
    var open_sp_id = $('#sp_id_number').val();  
    var sp_id = tag.replace("#row_","");
    //if i'm attempting to expand the same survey product thats already open do nothing
    if (open_sp_id != sp_id) {
      closeOpenSps();
    } else {
      return;  
    }
  
  }*/
  // if its a source expand then close existing sources
  if (url.includes("source"))
    {
      var open_source_id = $('#source_id_number').val();  
      var source_id = tag.replace("#sourcerow_","");
      var expand_type = $("#source_expand_type").val();
    // if you are trying to edit and already open edit source then do nothing so that you dont erase changes
      if (expand_type == "edit" && open_source_id == source_id) {
          return;
      } 
      closeOpenSources();
    }

  var em = $(tag).find('div.myoutput');
  var emTag= '#' + em.attr('id');
  $.ajax({
    type: "GET",
    url: url,
    success: function(response)
      {
        if(response!="0")
        {
          $("emTag").html(response);
          $('.selectpicker').selectpicker({});
          $(tag).show();
        }
    }
    });
}
  //NEED TO CHEC ON THIS - NOT GETTING TO ALL THE RIGHT ELEMENTS
  /*$('tr.outputrow').not(tag).each(function(){
    $(this).hide();
    var em = $(this).find('div.myoutput');
    em.html('');
  });
  */

function expandSp()
{
  var open_sp_id = $('#sp_id_number').val();  
  var get_url = $(this).attr('url');
  var get_tag = $(this).attr('tag');
  
    var sp_id = get_tag.replace("#row_","");
    //if i'm attempting to expand the same survey product thats already open do nothing
    if (open_sp_id != sp_id) {
      closeOpenSps();
    } else {
      return;  
    }
  expandFormRow(get_tag, get_url);

}


//Function used to submit product Source form
function formCheck(form_tag,load_url,load_tag)
{
  checkServer();
  if(typeof form_tag == 'undefined')
  {
    var f_tag='form.assignSources';
  }
  else {
    var f_tag=form_tag;
  }
  $('div.myoutput').on('submit',f_tag,function(e){
    e.preventDefault();
    var tag="#sourceWarn_" + $(this).attr('val');
    var selector = "select." + $(this).attr('val');

    if($(selector).val()=="0" && $(this).attr('class')=='assignSources')
    {
      var myWarn="Please select a valid source for assignment";
      $(tag).html(myWarn);
      $(tag).show();
    }
    else if ($('#propProd_' + $(this).attr('val'))=='')
    {
      var myWarn="Please select a percent of the total product provided by this source";
      $(tag).html(myWarn);
      $(tag).show();
    }
    else
    {
      $(tag).html('');
      $(tag).hide();
      var post_data=$(this).serialize();
      var post_url=$(this).attr('url');
      var tag="#sources_" + $(this).attr('val');
      var sp_id = $(this).attr('val');
      //console.log("data: " + post_data + "\n url: " + post_url );

      $.ajax({
        type: 'POST',
        url: post_url,
        data: post_data,
      }).done(function(response){
        if(response)
        {
          if(response!="0")
          {
            if(typeof load_url === 'undefined')
            {
              loadComponent(post_url,tag);
              //code to update percent
              var tag2 = "#percent_" + sp_id ;
              var myurl2 = "/surveyProduct/" + sp_id + "/percent";
              loadComponent(myurl2,tag2);
              
            }
            else
            {
              loadComponent(load_url,load_tag);
            }

          }
        }
      })
    }
  });
}
function sourceDetails(tag)
{
 // $(tag).off('change',"select.sourceSelect", showSourceDetails);
 // $(tag).on('change',"select.sourceSelect", showSourceDetails);
}
function showSourceDetails()
{
  if($(this).val()=="0")
  {
    $("#mysource_details").html('');
    $("#mysource_details").hide();
  }
  else {
    var sel="div#sourcedetails_" + $(this).val();
    var myhtml=$(sel).html();
    $("#mysource_details").html(myhtml)
    $("#mysource_details").show();
  }
}
function assignCertForm(tag)
{ //I'm using this approach because it allows me to easier stagger the .on() and .off() settings to avoid multiple assignments. Seriously a pain in the behind so if any functions start freaking out, try turning them off BEFORE turning them on....weird I know.
  $(tag).on('submit','form.assignCert',myAssignCert);
}
function myAssignCert(event)
{
  checkServer();
  event.preventDefault();
  var post_url=$(this).attr('url');
  var post_data=$(this).serialize();
  var get_url=$(this).data('url');
  var tag="#sources_" + $(this).data('value');
  $.ajax({
    type: "post",
    url: post_url,
    data: post_data
  }).done(function(response){
    if(response=="1")
    {
      loadComponent(get_url,tag);
    }
    else{

    }
  })
}
function saveRepack(tag)
{
  $(tag).on('click','input[type="radio"]',function(){
    var myform = $(this).closest('form');
    myform.submit();
  });
  $(tag).on('submit','form.assignRepack',function(e){
    e.preventDefault();
    var post_url=$(this).attr('url');
    var post_data=$(this).serialize();
    $.ajax({
      type: "post",
      url: post_url,
      data: post_data
    }).done(function(response){
    });
  });
}
function rmCert(tag)
{
  $(tag).on('click','button.delCert',function(){
    var post_url = $(this).data('url');
    var get_url=$(this).attr('url');
    var tag = $(this).attr('tag');
    $.ajax({
      type: "post",
      url: post_url
    }).done(function(response){
      if(response=="1")
      {
        loadComponent(get_url,tag);
      }
    })
  });
}
//AM TRYING TO WRITE SCRIPTS TO MAKE A NEW SOURCE MODAL FOR WITHIN THE SP WINDOW
 /*function newSpSource()
 {
   var spid = $('#sp_id_number').val();
   var sfCat = $('#sp_seafoodCat').val();

 }*/

//closes all open sps windows
function closeOpenSps()
{

  var sp_id = $("#sp_id_number").val();
  if (sp_id) {
    var tag = "tr#row_" + sp_id;
    var em = $(tag).find('div.myoutput');
    $(tag).hide();
    em.html('');
  }
  var sp_id = $("#sp_id_number").val();
    if (sp_id) {
      closeOpenSps();
    }
  
}

function expandFormRow(tag, url)
{
  var em = $(tag).find('div.myoutput');
  var emTag= '#' + em.attr('id');
  
  loadComponent(url,emTag);
  $(tag).show();


}


function refreshOpenSurvey()
{
  var survey_id = $("#survey_number").val();
  //if a survey is open then lets refresh it
  if (survey_id > 0) 
  {
    var sp_id = $("#sp_id_number").val();
    //if a surveyproduct is open then lets open that as well

    if(sp_id > 0)
    {
      var myurl = "/survey/" + survey_id + "/products/" + sp_id + "/assign";
      var mytag = "#surveys";
      loadComponent (myurl, mytag);
    } else {
      var myurl = "/survey/" + survey_id + "/products";
      var mytag = "#surveys";
      loadComponent (myurl, mytag);
    } 
  }
}

function refreshOpenSp()
{
    var sp_id = $("#sp_id_number").val();
    var mytag = "#percent_" + sp_id ;
    var myurl = "/surveyProduct/" + sp_id + "/percent";
    loadComponent(myurl,mytag);
    var myurl = "/surveyProduct/" + sp_id + "/assign";
    var mytag = "#row_" + sp_id;
    expandFormRow (mytag, myurl);
}


