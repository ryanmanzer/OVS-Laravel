/**
 * BASIC JAVASCRIPT/JQUERY FILE
 * ----------------------------------------
 *  This script stores the most general and multipurpose of the functions
 *  we need for this application.  More specific functions will be found in
 *  specific directories.
 */

//BASIC AJAX LOADING FUNCTION
 function loadComponent(url,tag)
 {
    checkServer();
    $.get(url,function(data)
    {
        //if(! data){
          //console.log("loadComponent fail; url: " +  url + ", tag: " +  tag + ", data: " + data);
          //window.location='/login';
        //}
         //console.log(data);
         $(tag).empty();
         $(tag).html(data); //writing the HTML code to the element identified by the tag
    });
 }
//THIS IS MY FIRST STAB AT USING AJAX METHODS FOR POSTING DATA TO THE DB
//THE DATA RETURNED IS A RELOAD OF THE SAME VIEW THAT SHOULD UPDATE THE DATA
//PRESENTED TO THE USER. <- THIS IS STILL PROVING DIFFICULT

// check for i.e. or safari
  function checkBrowser(){
    //check for Safari
        var isOpera = (!!window.opr && !!opr.addons) || !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    // Firefox 1.0+
        var isFirefox = typeof InstallTrigger !== 'undefined';
    // Safari <= 9 "[object HTMLElementConstructor]" 
        var isSafari = Object.prototype.toString.call(window.HTMLElement).indexOf('Constructor') > 0;
    // Internet Explorer 6-11
        var isIE = /*@cc_on!@*/false || !!document.documentMode;
    // Edge 20+
        var isEdge = !isIE && !!window.StyleMedia;
    // Chrome 1+
        var isChrome = !!window.chrome && !!window.chrome.webstore;
    // Blink engine detection
        var isBlink = (isChrome || isOpera) && !!window.CSS;
        if (isSafari) {
           $.ajax({ 
                type :'GET', 
                url: '/unsupported_browser/Safari', 
                success: function(response){
                    $("#main_body").html(response);
                }
            });
        } else if (isIE) {
            $.ajax({ 
                type :'GET', 
                url: '/unsupported_browser/IE', 
                success: function(response){
                    $("#main_body").html(response); 
                }
            });
        } else if (isChrome) {
            $.ajax({ 
                type :'GET', 
                url: '/unsupported_browser/Chrome', 
                success: function(response){
                    $("#main_body").html(response); 
                }
            });
        }

    }

function postComponent(url,tag, formData)
{
  checkServer();
  console.log(formData);
  $.ajax({
    type: "POST",
    url: 'test/form',
    data: formData
  }).done(function(data){
    if(! data){
      //window.location="/login";
    }
    console.log(data);
    $(tag).html(data);
  });
}

//THIS IS MY ATTEMPT TO BIND VARIOUS ELEMENTS WITH GET AJAX CALLS USING MY
//CURRENT METHODOLOGY (URL/TAG ATTRIBUTES OF ELEMENT DETERMINE WHAT GETS LOADED
//WHERE). THIS WILL NEED TO BE INCLUDED IN EACH PAGE'S INIT FUNCTION.
function assignGET()
{
  $("body").on("click",'.getAJAX', getAJAX);
}
function getAJAX(e)
{
  e.preventDefault();
  var myurl=$(this).attr("url");
  var tag=$(this).attr("tag");
  loadComponent(myurl,tag);

  if ($(this).data('tutorial')) {
    loadTutorial($(this).data('tutorial'));
  }
}

function postAJAX(e)
{
  e.preventDefault();
  var posturl=$(this).attr("post-url");
  var tag=$(this).attr("tag");
  var myurl=$(this).attr("url");
  $.ajax({
    type: 'POST',
    url: posturl,
    success: function(){
      loadComponent(myurl,tag);
    }
  })
  
}

function postAJAXmanual(post_url, get_url, get_tag)
{
  $.ajax({
    type: 'POST',
    url: post_url,
    success: function(response){
      loadComponent(get_url,get_tag);
    }
  })
  
}

function addTooltip()
{
  $('[data-toggle="tooltip"]').tooltip();
}

function customInit(script_name)
{
  switch (script_name)
  {
    case 'vendorInit':
        vendorInit();
      break;
    case 'staffInit':
      staffInit();
      break;


  }
}
//FUNCTION TO PING SERVER AND DETERMINE IF USER IS STILL LOGGED IN
function checkServer()
{
  $.ajax({
    type: "GET",
    url: '/is_logged_in',
    success: function(response)
    {
      if(response=="true")
      {
        //we're good here
      }
      else {
        window.location='/login';
      }
    },
    error: function(response)
    {
      window.location='/login';
    }
  });
}
//FUNCTION TO CHECK IF A FORM IS PARTIALLY FILLED OUT AND PROMPT USER WHETHER THEY WANT TO SAVE OR NOT
function areYouSure(button,form,modal)
{
  $('#leaveForm').modal({show:false});
  $(button).off('click');
  $(button).on('click',function(e){
    if($(form).find('input:checked').length >0)
    {
      $("#leaveYes").attr('url')=$(button).attr('url');
      $("#leaveYes").attr('tag')=$(button).attr('url');
      $("#leaveYes").click(getAJAX);
    }
    else {
      getAJAX();
    }
  });
}


// TUTORIALS
function loadTutorial(tutorial) {
    $.ajax({
        type: 'GET',
        url: 'api/tutorial/' + tutorial,
        success: function(data){
            $('.tutorial-carousel-wrapper').remove();
            $('body').append(data);

            $('.tutorial-carousel').on('slid.bs.carousel', function (data) {
                var items = $(this).find('.item');
                var itemActive = $(this).find('.item.active');
                var controlLeft = $(this).find('.carousel-control.left');
                var controlRight = $(this).find('.carousel-control.right');

                if (itemActive.index() == 0) {
                    controlLeft.addClass('hidden');
                } else {
                    controlLeft.removeClass('hidden');
                }

                if (itemActive.index() == items.length - 1) {
                    controlRight.find('span').html('Complete!');
                } else {
                    controlRight.find('span').html('Next slide');
                }
            })

            $('.tutorial-carousel .carousel-control.right span').click(function () {
                var items = $('.tutorial-carousel').find('.item');
                var itemActive = $('.tutorial-carousel').find('.item.active');

                // ignore all slides except last
                if (itemActive.index() != items.length - 1) {
                    return;
                }

                // mark completed and hide
                $.post('/api/tutorial/' + $('.tutorial-carousel').data('name'), function () {
                    $('.tutorial-carousel-wrapper').addClass('hidden');
                    $('.tutorial-carousel-wrapper .carousel').carousel(0);
                });
            });
        }
    })
}


