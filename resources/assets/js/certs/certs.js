function addCerts(tag)
{
  $(tag).off('submit', assignCert);
  $(tag).on('submit', assignCert);
}
function rmCerts(tag,get_url)
{
  data={gurl:get_url};
  $(tag).on('click',data,removeCert);
}

function assignCert(event)
{
  event.preventDefault();
  var url = $(this).attr('url');
  var post_data=$(this).serialize();
  $.ajax({
    type: "post",
    data: post_data,
    url: url,
    success: function(response)
    {
      $("#addCertModal").modal('hide');
      vendor_id = $("#vendor_id").val();
      myurl = "/vendor/" + vendor_id + "/certs"
      loadComponent(myurl,"#vendor_certs");
      refreshOpenSurvey();
    }
    ,
    error: function(data)
      {
        var errors=data.responseJSON;
        $.each(errors,function(key,val){
          var sel="p#"+key;
          $(sel).html("--Required field missing--");
          //$(sel).toggle();
        })
      }
  });
}

function removeCert(event)
{
  event.preventDefault();
  var check_url = $(this).attr('check-url');
  var post_url=$(this).attr('data-url');
  var get_url = $(this).attr('url');
  var get_tag = $(this).attr('tag');
  $.ajax({
     type: 'GET',
     url: check_url,
     success: function(response){
      $('#flexOk').hide();
      $('#flexYes').show();
      $('#flexNo').show();
      $("#flexYes").attr('url',get_url);
      $("#flexYes").attr('tag',get_tag);
      $("#flexYes").attr('data-url',post_url);
      $('#flexModal #flexTitle').html('Remove Certification');
      $('#flexModal #flexMessage').html(response);
      $("div.modal-body").off('click','#flexYes');
      $("div.modal-body").on("click","#flexYes",deleteCert);
      $("#flexModal").modal('show');
    }
  })
}

function deleteCert()
{
  var post_url = $(this).attr('data-url');
  var get_url = $(this).attr('url');
  var get_tag = $(this).attr('tag');
  $.ajax({
     type: 'POST',
     url: post_url,
     success: function(){
      loadComponent(get_url, get_tag)
      refreshOpenSurvey();
     }

    })




/*  $.ajax({
    type:"post",
    url: post_url
  }).done(function(response){
    if(response=="1")
    {
      vendor_id = $("#vendor_id").val();
      myurl = "/vendor/" + vendor_id + "/certs"
      loadComponent(myurl,"#vendor_certs");
      refreshOpenSp();
    }
    else {
      window.location('/');
    }
  }) */
}



function  newCertModal(get_url)
{
  vendor_id = $("#vendor_id").val();
  $.ajax({
    type: 'GET',
    url: "/vendor/" + vendor_id + "/newCertModal",
    success: function(response)
      {
        {
          $("#addCertModal").html(response);
          addCertListeners();
          $("#addCertModal").modal('show');
        }
      }
    
  })
}

function  addCertListeners()
{
  $("#addCertModal").off('submit','form',assignCert);
  $("#addCertModal").on('submit','form', assignCert); 


}







