function myPages(origin,div_tag) //origin is an identifier of where the pagination links live, div_tag indicates where the resultant page will be output to.
{
  mydata={tag: div_tag};
  $(origin).off('click','.pagination a',mydata, directPages);
  $(origin).on('click','.pagination a',mydata, directPages);
}
//event handles are passed the event always, any additional data included is passed
// as a property (i.e. event.data).
function directPages(e)
{
  e.preventDefault();
  var url=$(this).attr('href');
  var tag = e.data.tag;
  loadComponent(url,tag);
}
