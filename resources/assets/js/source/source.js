/**
 *  JAVASCRIPT FUNCTIONS SPECIFIC TO SEAFOOD SOURCE GENERATION/VIEWS
 */

// Assigning the custom form submit action
 function formCheck()
 {
   $('div.myoutput').off('submit','form.assignSources', assignThisSource);
   $('div.myoutput').on('submit','form.assignSources', assignThisSource);
 }

 function assignThisSource(e){
     e.preventDefault();
     var post_data=$(this).serialize();
     var post_url=$(this).attr('url');
     var tag="#sources_" + $(this).attr('val');
     //console.log("data: " + post_data + "\n url: " + post_url );

     $.ajax({
       type: 'POST',
       url: post_url,
       data: post_data,
     }).done(function(response){
       if(response)
       {
         if(response=="1")
         {
           loadComponent(post_url,tag);
         }
       }
     })
   }

function newSourceHandler(sp_id)
{
    closeOpenSources();
    vendor_id = $("#vendor_id").val();
    geturl = "/vendor/" + vendor_id + "/newSource";
    gettag = "#newSource";
    if (sp_id > 0) {
      geturl = geturl + "/" + sp_id + "/sp";
    }
    $.ajax({
    type: "GET",
    url: geturl,
    success: function(response)
      {
        if(response!="0")
        {
          $("#newSource").html(response);
          newSourceListeners();
          $("#newSource").modal('show');
        }
    }
    });



/*
  //NAR version of close open source boxe
    pass_data={
    tag: get_tag,
    url: get_url,
  };
  closeOpenSources();

  var sp_id = $("this").context.activeElement.id;
  sp_id = sp_id.replace("sp_", "");
  //only when triggered fom a product will it have this data
  if (sp_id > 0) {
    loadComponent("/surveyProduct/" + sp_id + "/newModalTitle", "#newSource #newSourceTitle");
    loadComponent("/surveyProduct/" + sp_id + "/sfCatGroup", "#seafoodCatGroup");
    loadComponent("/surveyProduct/" + sp_id + "/sfGroup", "#sfGroup");
    $("#newSource_sp_id").val(sp_id);

  }
 //Binding form submission to the function genNewSource
    vendor_id = $("#vendor_id").val();
    geturl = "vendor/" & vendor_id & "/newSource";
    gettag = "#newSource";
    $.ajax({
    type: "GET",
    url: geturl,
    success: function(response)
      {
        if(response!="0")
        {
          $("#newSource").html(response);
          newSourceListeners();
          $("#newSource").modal("show");
        }
    }
    });
*/
}

function newSourceListeners()
{

  $("select#seafood_cat").off("change");
  $("select#seafood_cat").change(getSF);
  $("select#wf").off("change");
  $("select#wf").change(getWF);
  $("select#methodCat").off("change");
  $("select#methodCat").change(getMeth);
  /*
  $("div#newSource button.close").off("click");
  $("div#newSource button.close").click(formReset);
  */
  $("#newSource").off('submit','form',genNewSource);
  $("#newSource").on('submit','form',genNewSource); 
}

function editSourceHandler(selector, get_tag,get_url)
{
  pass_data={
    tag: get_tag,
    url: get_url,
  };
  $("p.source_err").html('');
  $(selector).on('change','select')
  $(selector).off('submit','form.editSource',editSource);
  $(selector).on('submit','form.editSource',editSource);
}
function genNewSource(e)
{
  $("p.source_err").html('');
  e.preventDefault();
  var post_data=$(this).serialize();
  var post_url=$(this).attr('url');
  $.ajax({
    type: "POST",
    url: post_url,
    data: post_data,
    dataType: 'json',
    success: function(response)
    {
      /*if(response=="1")
      {
        loadSources();
      }
      /*$("#newSource").toggle();
      $('.modal-backdrop').toggle();
      $("body").removeClass("modal-open");
      //formReset();*/
      $("#newSource").modal('hide');
      loadSources();
      refreshOpenSp();

    },
    error: function(data)
    {
        var errors=data.responseJSON;
        $.each(errors,function(key,val){
          var sel="p#"+key;
          $(sel).html("--Required--");
          //$(sel).toggle();
        })
    }
  });
}
function updateSource(e)
{
  //alert('You are trying to edit this source!');
  $("p.source_err").html('');
  e.preventDefault();
  var post_data=$("form#editSource").serialize();
  var post_url=$("form#editSource").attr('data-url');

  // var old_id = post_url.replace("/source/","").replace("/edit","");
  // var spsCount = $("#" + old_id + "_sps_count").val();
  // var source_tag='div#' + $(this).data('tag');
  // var trid=$(this).data('row');
  $.ajax({
    type: "POST",
    url: post_url,
    data: post_data,
    dataType: 'json',
    success: function(response)
    {
      if(response!="0")
      {
        var new_id = response;
        var url = $("form#editSource").attr('url');
        var url = url.replace("source_id", new_id);
        var tag = $("form#editSource").attr('tag');
        // add the id of the new source and edit to the url to 
//        updateAllSPS(old_id, new_id);
//        completeSourceUpdate (new_id, old_id)
        loadComponent(url,tag);
        listUpdatedProducts(new_id); 
  /*      if(spsCount > 0) 
        { 
          listUpdatedProducts(new_id);
        }*/
        refreshOpenSp();  
        // $.when(loadComponent(e.data.url,e.data.tag)).then(completeSourceUpdate (new_id, old_id) );
        
      }
    },
    error: function(data)
    {
        var errors=data.responseJSON;
        $.each(errors,function(key,val){
          var sel="p#"+key;
          $(sel).html("--Required--");
          //$(sel).toggle();
        })
    }
  });
}
/**
* ADDING THE DELETE MODAL WINDOW ASSIGNMENT FUNCTION TO THE DELETE BUTTONS ON
* THE SEAFOOD SORUCES LIST.  THIS WILL ALLOW THEM TO REASSIGN THE ACTION OF THE
* YES BUTTON ON THE DELETE MODAL WINDOW EACH TIME THEY ARE CLICKED.
*/
function sourceDelete()
{

  var source_id=this.id;
  $("#delYes").attr('value',source_id);
  delYes('delSource', source_id);

}
function getSF() //updating Seafoord Species based on Seafood Category
{
    //Since this is a data list, not a select menu, we have to be a little
    //more sneaky to get the id value we want.
    var id = $("select#seafood_cat").val();
    
    var url= '/source/' + id + "/sf_list";
    loadComponent(url,"#_fish"); 
}

// AJAX METHOD TO SET HARVEST METHOD CATEGORIES BASED ON WILD/FARMED SELECTION
function getWF()
{
    var wf = $("select#wf").val();
    var url1 = "/source//setMethCat";

//    var url1 = "/source/" + wf + "/setMethCat";
    if(wf.length > 0)
    {
      var url2 = "/source/" + wf + "/wfSpecMeth";
      //If wild then enable the wild only fields and hide the placeholder inputs

    } else {
      var url2 = "/source/clearSpecMeth";
    }
    if(wf == "Wild"){
      $('.hideWild').hide();
      $("select#faoMajorFishingArea").selectpicker('show');
      $("select#fk_vesselFlag_id").selectpicker('show');
      $("input#highSeasName").show();
    } else {
      $('.hideWild').show();
      $("select#faoMajorFishingArea").selectpicker('hide');
      $("select#fk_vesselFlag_id").selectpicker('hide');
      $("input#highSeasName").hide();
    }
    var url3 = "/source/" + wf + "/setCertList";
    var tag1 = "#method_category_group";
    var tag2 = "select#gear_type";
    var tag3 = "select#certType";
    loadComponent(url1,tag1);
    loadComponent(url2,tag2);
    loadComponent(url3,tag3);
    
//    if(wf.length > 0)
 //   {
    //var myUrl='/source/' + wf + "/setMethCat";
   /*   loadComponent(myUrl,tag);
      $('select#gear_type').html('<option selected=\"selected\" disabled=\"disabled\" value>--no Method Category selected yet--</option>');
   var myUrl='/source/' + wf + "/cert_list";
      loadComponent(myUrl,tag2)
    }
    else {
     $('select#gear_type').html('<option selected=\"selected\" disabled=\"disabled\" value>--no Method Category selected yet--</option>');
     $('select#methodCat').html('<option selected=\"selected\" disabled=\"disabled\" value>--no Wild Farmed selected yet--</option>');
     $('select#certType').html('<option selected=\"selected\" disabled=\"disabled\" value>--no Wild Farmed selected yet--</option>');
    }
    $('.selectpicker').selectpicker('refresh');
*/
}

// AJAX METHOD TO SET SPECIFIC METHOD OPTIONS BASED ON METHOD CATEGORY SELECTED
function getMethCat()
{
    var methCat=$("select#methodCat").val();
    if(methCat.length > 0){
      var myurl = "/source/" + methCat + "/setSpecMeth";
    } else {
      var wf = $("select#wf").val();
      var myurl = "/source/" + wf + "/wfSpecMeth";
    }
    var mytag = "select#gear_type";
    loadComponent(myurl,mytag);
    /*if(parseInt(id)>0)
    {
      url='/vendor/' + id + '/meth_list';
      tag='select#gear_type';
      //console.log(url);
       loadComponent(url,tag);

    }
    else {
      $('select#gear_type').html('<option selected=\"selected\" disabled=\"disabled\" value>--no Method Category selected yet--</option>');
    }*/
}

function getGearType(){
  var gear_type = $("select#gear_type").val();
  if(gear_type.length > 0){
    loadComponent("/source/" + gear_type + "/setMethCatByGear",  "#method_category_group");
  } else {
    loadComponent("/source//setMethCat",  "#method_category_group");
  }
}

// AJAX METHOD TO SET SPECIFIC METHOD OPTIONS BASED ON METHOD CATEGORY SELECTED
function getMeth()
{
    var id=$("select#methodCat").val();
    if(parseInt(id)>0)
    {
      url='/vendor/' + id + '/meth_list';
      tag='select#gear_type';
      //console.log(url);
       loadComponent(url,tag);

    }
    else {
      $('select#gear_type').html('<option selected=\"selected\" disabled=\"disabled\" value>--no Method Category selected yet--</option>');
    }
}


// AJAX FUNCTION TO DELETE SOURCE (SET SHOWSOURCE=0)
function deleteSource()
{

    $("#delSource").modal("hide");
    var source_id = $(this).val();
    var spsCount = $("#" + source_id + "_sps_count").val();
    if(spsCount > 0 ) 
    {
    listDeletedProducts(source_id);
    }
    deleteAllSPS(source_id);
    var myurl='/source/' + source_id + '/delete';
    $.ajax({
      type: 'POST',
      url: myurl,
      data: "",
    }).done(function(response){
      if(response=="1")
      {
        loadSources();
        refreshOpenSurvey();  
      }
      else {
        location.reload();
      }
    });
}

//AJAX FUNCTION TO DELETE SPS RECORD - ACTUALLY DELETES THE RECORD THIS TIME
function deleteSPS(myid)
{
  if(typeof myid === 'undefined'){myid = this.value;}
  var posturl='/sps/' + myid;
  var myurl=$(this).attr('url');
  var tag=$(this).attr('tag');
        
  $.ajax({
    type: "POST",
    url: posturl,
    data: "",
    success: function(response){
      if(response=="1")
      {
        refreshOpenSp();
      } 
      //alert("Success!  Did the whole page or just the div reload?");
    }
  })
}

//AJAX FUNCTION TO DUPLICATE A SOURCE RECORD AND REFRESH SOURCE TABLE
function duplicateSource(get_url,id)
{
  var source_id = $(this).attr('sourceid');
  var post_url = '/source/' + source_id + "/duplicate";
  var tag = $(this).attr('tag');
  var get_url=$(this).attr('url');
  $.post(post_url,function(data){
    if(data=="1")
    {
      loadComponent(get_url,tag);
      refreshOpenSp();
    }
  });
}
function assignEditFunctions(selector)
{
  $(selector).off('change','select.seafoodCategory').on('change','select.seafoodCategory',getSF);
  $(selector).off('change','select.WildFarmed').on('change','select.WildFarmed',getWF);
  $(selector).off('change','select.methodCategory').on('change','select.methodCategory',getMeth);
}

function assignFormClose(selector)
{
  $(selector).off('click','a.closeForm').on('click','a.closeForm',closeInlineForm);
}
function closeInlineForm(e)
{
  e.preventDefault();
  $("#mysource_details").hide();
  $("#mysource_details").html('');
  var tag = $(this).attr('tag');
  $(tag).hide();
  var em = $(tag).find('div.myoutput');
  em.html("");
}


//Opening the details of a source after the edit button is pressed
//This is now obsolete since there is a url to call where you can include new source id to appear as edit
/*
function completeSourceUpdate(new_id, old_id){
  
  updateAllSPS(old_id, new_id);
  var tag = "#sourcerow_" + new_id;
  var url = "source/" + new_id + "/details";
  expandFormRow(tag, url);
  window.location = "#sourceAnchor";
}
*/


//RESETTING THE VALUES IN THE NEW SOURCE FORM UPON SUBMISSION OR CLOSE
function formReset()
{

    vendor_id = $("#vendor_id").val();
    geturl = "vendor/" & vendor_id & "/newSource";
    gettag = "#newSource";
    $.ajax({
    type: "GET",
    url: geturl,
    success: function(response)
      {
        if(response!="0")
        {
          $("#newSource").html(response);
          newSourceListeners();
          
        }
    }
    });

/*
  $("form#addsource").each(function()
  {
    
    $("p.source_err").html('');
    loadComponent("vendor/refreshSfCat", "#seafoodCatGroup");
    loadComponent("vendor/refreshSf", "#sfGroup");
    $("#newSource_sp_id").val('');
    $("#newSource #newSourceTitle").html("Enter New Source Information");
    this.reset();
    getWF();
    getMeth();

  });
*/
}

function deleteAllSPS(myid)
{
  var myurl='/source/' + myid + '/deleteAllSps' ;
  $.post(myurl);
}

function updateAllSPS(oldid, newid)
{
  var myurl = '/source/' + oldid  + '/' + newid + '/updateSps';
  $.post(myurl);
}

function closeOpenSources ()
{

  var source_id = $("#source_id_number").val();
  if (source_id) {
    var tag = "tr#sourcerow_" + source_id;
    var em = $(tag).find('div.myoutput');
    $(tag).hide();
    em.html('');
    closeOpenSources();
  }

}

function sourceDetailsButton ()
{
  var tag=$(this).attr('tag');
  var url=$(this).attr('url');
  //If this same source is already expanded, either as edit or details then do nothing
    var open_source_id = $('#source_id_number').val();  
    var source_id = tag.replace("#sourcerow_","");
    // if you are trying to edit and already open edit source then do nothing so that you dont erase changes
    if (open_source_id == source_id) {
        return;
    } 
  closeOpenSources();
  expandFormRow(tag, url);

}

function sourceEditButton ()
{
  var tag=$(this).attr('tag');
  var url=$(this).attr('url');
  var source_id = tag.replace("#sourcerow_","");
    
  //If this same source is already expanded as edit then do nothing
  var open_source_id = $('#source_id_number').val();  
  var expand_type = $("#source_expand_type").val();
  if (expand_type == "edit" && open_source_id == source_id) {
    return;
  } 
  //Otherwise check to see if this source has any sps
  // this doesnt seem to work as a live test - going to try AJAX
  myurl = "/source/" + source_id + "/countSps";
  $.ajax({
    type: "GET",
    url: myurl,
    success: function(response)
      {
        if(response > 0)
        {
          confirmEditModal(source_id);          
        } else {
          closeOpenSources();
          get_url = "/source/" + source_id + "/edit";
          get_tag = "#sourcerow_" + source_id
          expandFormRow(get_tag, get_url);
        }
    }
    });

 // mytag = "#" + source_id +  "_sps_count_div";
 // myurl = "/source/" + source_id + "/updateCountSps";
  //otherwise
  //GOing POSTAL on this one i.e. making it a post


 // $.when(loadComponent(myurl, mytag)).done(confirmEditModal(source_id) );
  /*confirmEditModal(source_id, tag, url);*/
  

}

function confirmEditModal(id)
{
  get_url = "/source/" + id + "/edit";
  get_tag = "#sourcerow_" + id

  $('#flexOk').hide();
  $('#flexYes').show();
  $('#flexNo').show();
  $("#flexYes").attr('url',get_url);
  $("#flexYes").attr('tag',get_tag);
  $('#flexModal #flexTitle').html('Edit Source');
  $("div.modal-body").off('click','#flexYes',openEditWithSps);
  $("div.modal-body").on("click","#flexYes",openEditWithSps);
  var myurl = 'source/' + id + '/sourceEditConfirm';
  $.ajax({
    type: "GET",
    url: myurl,
    success: function(response)
      {
        $("#flexModal #flexMessage").html(response);
        $('#flexModal').modal("show");         
      }
    }) 
}

function openEditWithSps()
{
  var tag=$(this).attr('tag');
  var url=$(this).attr('url');
  closeOpenSources();
  expandFormRow(tag, url);
}


function listUpdatedProducts(id)
{
  //If product has >0 sp assigned then flash this message
    
 myurl = "/source/" + id + "/countSps";
  $.ajax({
    type: "GET",
    url: myurl,
    success: function(response)
      {
        if(response > 0)
        {
          
          $('#flexOk').show();
          $('#flexYes').hide();
          $('#flexNo').hide();
          $('#flexModal #flexTitle').html('Source Update Saved');
          var myurl = '/source/' + id + '/sourceEditsListed';
          $.ajax({
              url: myurl,
              success: function(response)
                {
                  $("#flexModal #flexMessage").html(response);
                  $('#flexModal').modal("show");         
                }            
             });
        } 
    }
  });    
             
}

function listDeletedProducts(id)
{
  
    var url = '/source/' + id + '/sourceDeletesListed';
    loadComponent(url, '#flexModal #flexMessage');
    $('#flexOk').show();
    $('#flexYes').hide();
    $('#flexNo').hide();
    $('#flexModal #flexTitle').html('Source Delete');
    $('#flexModal').modal("show");         
}

function updateSpsCount(id)
{
  var myurl = "/source/" + id + "/updateCountSps"
  var mytag = id + "_sps_count_div";
  loadComponent (myurl, mytag)
}

function updateContactInfo()
{
  var post_data=$("form#contactInfo").serialize();
  var posturl = url=$("form#contactInfo").attr('data-url');
  $.ajax({
    type: "POST",
    url: posturl,
    data: post_data,
    success: function(response){
      if(response=="1")
      {
        var tag=$("form#contactInfo").attr('tag');
        var url=$("form#contactInfo").attr('url');
        loadComponent(url, tag);
      } 
      //alert("Success!  Did the whole page or just the div reload?");
    }
  })
}

function selectPicker()
{
  $('.selectpicker').selectpicker({});
}

function enterSource(e)
{
  $("p.source_err").html('');
  //trim all the open response fields
  $("input#supplierCompany").val($("input#supplierCompany").val().trim());
  $("input#contactEmail").val($("input#contactEmail").val().trim());
  $("input#scientificName").val($("input#scientificName").val().trim());
  $("input#harvestRegion").val($("input#harvestRegion").val().trim());
  $("input#highSeasName").val($("input#highSeasName").val().trim());
  $("input#cert_num").val($("input#cert_num").val().trim());
  $("textarea#vendorComments").val($("textarea#vendorComments").val().trim());


  e.preventDefault();
  var post_data=$("form#addsource").serialize();
  var post_url=$("form#addsource").attr('post-url');
  var source_id = $("form#addsource").attr('data-id');
  if (source_id > 0){
  var post_url= post_url + "/" + source_id + "/oldSource";  
  }
  var get_url=$("form#addsource").attr('url');
  var get_tag=$("form#addsource").attr('tag');
  
  $.ajax({
    type: "POST",
    url: post_url,
    data: post_data,
    dataType: 'json',
    success: function(response)
    {
      
        loadComponent(get_url, get_tag);

    },
    error: function(data)
    {
        var errors=data.responseJSON;
        $.each(errors,function(key,val){
          var sel="p#"+key;
          $(sel).html("--Required--");
          //$(sel).toggle();
        })
    }
  });
}

    



function enterCertData(){
    var post_url = $("form#enterCert").attr("post-url");
    var post_data = $("form#enterCert").serialize();
    var get_url=$("form#enterCert").attr('url');
    var get_tag=$("form#enterCert").attr('tag');
    $.ajax({
      type: 'POST',
      url: post_url,
      data: post_data,
      dataType: 'json',
      success: function(response){
          loadComponent(get_url, get_tag);
      },
    error: function(data)
      {
        var errors=data.responseJSON;
        $.each(errors,function(key,val){
          var sel="p#"+key;
          $(sel).html("--Required--");
          //$(sel).toggle();
        })
      }
    });
  }    

  function enterCert()
{
  $("p.cert_err").html('');
  e.preventDefault();
  var post_data=$("form#enterCert").serialize();
  var post_url=$("form#enterCert").attr('post-url');
  var get_url=$("form#enterCert").attr('url');
  var get_tag=$("form#enterCert").attr('tag');
  
  $.ajax({
    type: "POST",
    url: post_url,
    data: post_data,
    dataType: 'json',
    success: function(response)
    {
      
        loadComponent(get_url, get_tag);

    },
    error: function(data)
    {
        var errors=data.responseJSON;
        $.each(errors,function(key,val){
          var sel="p#"+key;
          $(sel).html("--Required--");
          //$(sel).toggle();
        })
    }
  });
}